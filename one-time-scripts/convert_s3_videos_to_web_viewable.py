import os
import subprocess
import multiprocessing
from shutil import rmtree

import boto3
import tqdm

BUCKET_NAME = 'dev.mturk'
DATASET_NAME = 'Moments_in_Time_256x256_30fps'
N_PROC = 7


def handbrake_it(fn):
    parent_dirs = os.path.split(fn)[0]
    if not os.path.exists(parent_dirs):
        os.makedirs(parent_dirs)

    s3_client = boto.client('s3')
    s3_client.download_file(fn)

    _ = subprocess.Popen(
        ['HandBrakeCLI', '-i', fn, '-o', fn, '-r', '30'],
        stderr=subprocess.PIPE
    ).communicate()

    s3_client.upload_file(fn, BUCKET_NAME, fn)
    os.remove(fn)
    return fn
    

s3 = boto3.resource('s3')
files_of_interest = map(lambda x: x.key, s3.Bucket(BUCKET_NAME).objects.all())

# Do for all validation and training data
with multiprocessing.Pool(N_PROC) as pool:
    for partition in ['validation', 'training']:
        foi = filter(
            lambda x: x.startswith(os.path.join(dataset_name,
                                                partition)),
            files_of_interest
        )

        pool.map(handbrake_it, foi)

        # cleanup
        rmtree(DATASET_NAME)
