srun \
    --ntasks=32 \
    --partition=dcs \
    --mail-type=ALL \
    --mail-user=DLANbrdj \
    --time=240 \
    --gres=gpu:1 \
    python testbench.py \
        --learnable_features \
        --lambda_ling=0.1,0.1,0.1,0.1 \
        --lambda_vis=0.1,0.1,0.1 \
        --lr=1e-4 \
        --randomize_lr
