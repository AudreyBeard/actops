CODE_DIR=$HOME/code
DATA_DIR=$HOME/data
COCO_ACTIONS_DIR=$DATA_DIR/coco-actions
HERE=$PWD

cd actops/external/bhtsne
g++ sptree.cpp tsne.cpp tsne_main.cpp -o bh_tsne -O2

# Initialize COCO-Actions data directory and put COCO-Attributes in it
mkdir -p $COCO_ACTIONS_DIR
ln -s $HERE/actops/external/cocottributes/data/cocottributes_eccv_version.pkl $COCO_ACTIONS_DIR/coco-attributes.pkl


# Get COCO
cd $COCO_ACTIONS_DIR
mkdir coco
cd coco

## Annotations
wget http://images.cocodataset.org/annotations/annotations_trainval2014.zip
unzip annotations_trainval2014.zip
rm annotations_trainval2014.zip

## Train
wget http://images.cocodataset.org/zips/train2014.zip
unzip train2014.zip
rm train2014.zip

## Val
wget http://images.cocodataset.org/zips/val2014.zip
unzip val2014.zip
rm val2014.zip

## Test
wget http://images.cocodataset.org/zips/test2014.zip
unzip test2014.zip
rm test2014.zip
wget http://images.cocodataset.org/annotations/image_info_test2014.zip
unzip image_info_test2014.zip
rm image_info_test2014.zip


# Get COCO-Stuff
cd $COCO_ACTIONS_DIR
mkdir coco-stuff
cd coco-stuff
wget http://calvin.inf.ed.ac.uk/wp-content/uploads/data/cocostuffdataset/stuff_trainval2017.zip
unzip stuff_trainval2017.zip
rm stuff_trainval2017.zip


# Get GloVe embeddings
mkdir 
cd $COCO_ACTIONS_DIR
mkdir glove
cd glove
wget --show-progress https://www.cs.utexas.edu/~tushar/attribute-ops/attr-ops-data.tar.gz
tar -zxvf attr-ops-data.tar.gz --strip 1
mv data/glove/glove* .

## Toss the stuff I don't need
rm attr-ops-data.tar.gz
rm -r cv
rm -r data
rm -r tensor-completion


# Requirements
sudo apt-get install ffmpeg
pip install -r requirements.txt

# Non-Python Requirements
# CUDA 10.0
# libcudart
# CUDA Drivers 418
