from setuptools import setup
from setuptools import find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='actops',
      version='0.0.0',
      description='Actions as Operators: language-inspired image and video action modeling',
      long_description=long_description,
      author='Audrey Beard',
      author_email='audrey.s.beard@gmail.com',
      packages=find_packages(),
      install_requires=['numpy',
                        'torch',
                        'tqdm',
                        'netdev',
                        'pillow',
                        'tensorboard_logger',
                        'torchvision>=0.3.0'],
      url='https://github.com/AudreyBeard/actops',
      changelog={'0.0.0': 'SVO action modeling with triplet and auxiliary loss',
                 }
      )
