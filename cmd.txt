python main.py --device=2 --epochs=200 --lr=1e-4 --randomize_lr --nice_name=actops --lambda_aux=1 --sample_only_valid_triplets --n_candidates=31 --batch_size=256
