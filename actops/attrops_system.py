import os

import torch
from torch.nn import functional as F
import netdev as nd
import ipdb  # NOQA

import utils


class AttropsEmbeddingPredictor(object):
    def __init__(self, dset, pairs=None, model=None):
        self.embeddings = None
        self.model = model
        self.pairs = dset.pairs if pairs is None else pairs
        self.pairs_indices = torch.LongTensor([
            (
                dset.attr2idx[v],
                dset.obj2idx[s],
            )
            for v, s in self.pairs
        ]).cuda().permute(1, 0).contiguous()

        if self.model is not None:
            self.generate_embeddings(model)

    def generate_embeddings(self, model=None):
        """ Generate embeddings for pairs by passing them through the current model
        """
        if model is not None:
            self.model = model

        with torch.no_grad():
            self.embeddings = self.model.compose(
                self.pairs_indices[0],
                self.pairs_indices[1]
            )

    def __call__(self, point, k=1, model=None):
        dist = F.pairwise_distance(
            self.embeddings,
            point.unsqueeze(0).repeat(self.embeddings.shape[0], 1)
        )

        if k > 1:
            pairs = [self.pairs[i.item()] for i in dist.argsort()[:k]]
        else:
            pairs = self.pairs[dist.argmin().item()]

        return pairs


class AttropsNamespace(object):
    """ Quick class for emulating args namespace
    """
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class AttropsSVTripletEmbeddingSystem(nd.NetworkSystem):
    """ System for learning an embedding of videos and SVO identities using the triplet loss
    """
    constraints = {
        'miner': None,
        'lambda_aux': (float, int),
        'lambda_comm': (float, int),
        'out_dim': int,
        'margin': (float, int),
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': int,
    }
    defaults = {
        'miner': None,
        'lambda_aux': 1.0,
        'lambda_comm': 1.0,
        'out_dim': 300,
        'margin': 0.5,
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': 10,
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self._v > 0:
            print("Initialized {}".format(self.__class__.__name__))

    def forward(self, data):
        # Grab image data, subjects, and verbs (ignore object)
        anchors = data[0].to(self.device)
        subjects, verbs = [
            torch.LongTensor(t).to(self.device)
            for t in data[1:3]
        ]

        embeds_anchor = self.model.image_embedder(anchors)
        embeds_pos = self.model.compose(verbs, subjects)

        pairs = torch.tensor([
            self.model.dset.pair2idx[(
                self.model.dset.idx2verb[i_v.item()],
                self.model.dset.idx2subject[i_s.item()]
            )]
            for i_v, i_s in zip(verbs, subjects)
        ])

        if self.training:
            # Grab candidates and push to device (ignore object)
            subjects_neg, verbs_neg = [
                torch.stack(t).to(self.device)
                for t in data[4:6]
            ]

            # Mine hardest triplets in batch
            embeds_hard, indices_hard = self.miner(
                embeds_anchor,
                embeds_pos,
                subjects_neg,
                verbs_neg,
            )

            # Compute loss with hardest triplets
            # model input: img, attrs, objs, pairs neg_attrs, neg_objs, inv_attrs, comm_attrs
            verbs_hard = verbs_neg.gather(0, indices_hard.unsqueeze(0).repeat(verbs_neg.shape[0], 1))[0]
            subjects_hard = subjects_neg.gather(0, indices_hard.unsqueeze(0).repeat(subjects_neg.shape[0], 1))[0]
            loss, _ = self.model.train_forward([
                anchors,
                verbs, subjects, pairs,
                verbs_hard, subjects_hard,
                None, None
            ])

        else:
            # Just convert to Long and push to device
            subjects_neg, verbs_neg = [
                torch.LongTensor(t).to(self.device)
                for t in data[4:6]
            ]

            # AttributeOperator doesn't return loss when model.training == False
            self.model.training = True

            # Compute loss
            loss, predictions = self.model.forward([anchors, verbs, subjects, pairs, verbs_neg, subjects_neg, None, None])

        pred_top_1 = [self.predictor_open(anc) for anc in embeds_anchor]
        pred_closed_1 = [self.predictor_closed(anc) for anc in embeds_anchor]

        actual = [
            (
                self.loaders['train'].dataset.idx2verb[v.item()],
                self.loaders['train'].dataset.idx2subject[s.item()],
            )
            for s, v in zip(
                subjects,
                verbs,
            )
        ]

        acc_open = sum([
            pred == act
            for pred, act in zip(
                pred_top_1,
                actual
            )
        ]) / len(pred_top_1)

        acc_closed = sum([
            pred == act
            for pred, act in zip(
                pred_closed_1,
                actual
            )
        ]) / len(pred_closed_1)

        # Include at least these
        retval = {
            'loss': loss,
            #'loss_triplet': loss.pop(),
            'acc_open': acc_open,
            'acc_closed': acc_closed,
        }

        if False:
            # Unnecessary because loss is just a scalar, not a list (computed in model)
            # Add regularization terms
            if self.model.args.lambda_aux > 0:
                retval.update({'loss_aux': loss.pop()})
            if self.model.args.lambda_inv > 0:
                retval.update({'loss_inv': loss.pop()})
            if self.model.args.lambda_comm > 0:
                retval.update({'loss_comm': loss.pop()})
            if self.model.args.lambda_ant > 0:
                retval.update({'loss_ant': loss.pop()})

        return retval

    def on_epoch(self):
        super().on_epoch()
        self.predictor_open.model = self.model
        self.predictor_closed.model = self.model
        self.miner.model = self.model

    @staticmethod
    def create_nice_name(args):
        """ Creates a nice name from the arguments (NameSpace) given
        """
        nice_name = args.nice_name
        nice_name += "_{}".format("coco-actions-svo")
        nice_name += "_subset={}".format(0.9)
        nice_name += "_n-candidates={}".format(args.n_candidates)
        nice_name += "_embed-dim={}".format(args.embed_dim)
        nice_name += "_triplet-margin={}".format(args.triplet_margin)
        nice_name += "_train-ratio={}".format(args.train_ratio)
        nice_name += "_lr={:0.2e}".format(args.lr)
        nice_name += "_lambda-aux={}".format(args.lambda_aux)
        nice_name += "_lambda-comm={}".format(args.lambda_comm)
        nice_name += "_sample-all-triplets" if args.sample_all_triplet_combos else ''
        return nice_name

    @staticmethod
    def parse_nice_name(fpath):
        items = fpath.split(os.path.sep)
        nice_name = items[-2]

        nn_split = nice_name.split('_')

        parse_dict = {
            'dataset': nn_split.pop(1),
            'user_defined_nice_name': nn_split.pop(0),
            'epoch': int(os.path.splitext(items[-1])[0].split('=')[1])
        }
        parse_dict.update({
            hyper[0]: True
            if len(hyper) == 1
            else float(hyper[1])
            for hyper in map(lambda x: x.split('='), nn_split)
        })
        return parse_dict

    @staticmethod
    def load_cache(save_name):
        return torch.load(utils.path(save_name))


class AttropsSVNegativeMiner(object):
    """ Mines data for the hardest pairs in a subset of data
        To be called in training loop.
    """
    def __init__(self, model=None, device=None, objective=None, n_candidates=1,
                 positive_threshold=True):
        """
            Parameters:
                model: any callable object that produces something to put into
                    the objective function
                device (str): torch device to put data onto ['cuda' | 'cpu']
                objective: any callable object that produces output for each
                    sample
        """
        assert model is not None
        assert objective is not None
        if device is None:
            device = 'cpu'
        self._dev = device
        self.model = model
        self.objective = objective
        self.n_candidates = n_candidates
        self.positive_threshold = positive_threshold
        return

    def __repr__(self):
        s = self.__class__.__name__
        s += " for {} candidates on device {}".format(self.n_candidates, self._dev)
        s += "\n  model: {}".format(self.model)
        s += "\n  objective: {}".format(self.objective)
        return s

    def mine(self, anchors, pos, subjects_neg, verbs_neg):
        #with torch.no_grad():
        if True:
            # Evaluate loss for each anchor-candidate pair - don't need gradient
            outs_candidates = self.model.compose(
                verbs_neg.view(-1),
                subjects_neg.view(-1),
            )

            # Mold anchor images into same shape, duplicating to compare loss
            loss_candidates = self.objective(
                anchors.repeat(self.n_candidates, 1),
                pos.repeat(self.n_candidates, 1),
                outs_candidates)

            # Find hardest candidate
            loss, i_hard = loss_candidates.view(-1, self.n_candidates).max(dim=1)

            # Shape: (batch_size x n_candidates x n_out_dim)
            outs_candidates = outs_candidates.view(anchors.shape[0], self.n_candidates, -1)

            # Reshape for gather step
            i_hard = i_hard.view(-1, 1, 1)
            repeats = [int(a / b) for a, b in zip(outs_candidates.shape, i_hard.shape)]
            i_hard = i_hard.repeat(*repeats)

            # Use indices of hardest samples to collect them
            outs_hard = torch.gather(outs_candidates, 1, i_hard)[:, 0, :]

            outputs = outs_hard, i_hard[:, 0, 0]

        return outputs

    def __call__(self, anchors, pos, subjects_neg, verbs_neg):
        return self.mine(anchors, pos, subjects_neg, verbs_neg)
