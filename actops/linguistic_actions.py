import torch
from torch import nn
import torch.nn.functional as F
import numpy as np
import netdev as nd
import ipdb
import os

from external.attributes_as_operators.models.models import load_word_embeddings
from dataproc.data_cocoactions import COCOActionsSVODatasetActivationsTriplet
import utils


class LinearLayers(nn.Module):
    """ Simple ANN with He-Kaiming initialization
    """
    def __init__(self, in_dim, out_dim, n_layers=1, use_bias=True, use_relu=True):
        super().__init__()
        layers = []
        for l in range(n_layers - 1):
            linear = utils.set_he_normal(nn.Linear(
                in_dim,
                in_dim,
                bias=use_bias), in_dim)
            layers.append(linear)
            layers.append(nn.ReLU(True))

        layers.append(utils.set_he_normal(nn.Linear(
            in_dim,
            out_dim,
            bias=use_bias), in_dim)
        )
        if use_relu:
            layers.append(nn.ReLU(True))

        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)


class EmbeddingPredictor(object):
    def __init__(self, dset, svo=None, model=None):
        self.embeddings = None
        self.model = model
        self.svo = dset.unique_svo if svo is None else svo
        self.svo_indices = torch.LongTensor([
            (
                dset.subject2idx[s],
                dset.verb2idx[v],
                dset.object2idx[o],
            )
            for s, v, o in self.svo
        ]).cuda().permute(1, 0).contiguous()

        if self.model is not None:
            self.generate_embeddings(model)

    def generate_embeddings(self, model=None):
        if model is not None:
            self.model = model

        with torch.no_grad():
            self.embeddings = self.model.compose(
                self.svo_indices[0],
                self.svo_indices[1],
                self.svo_indices[2],
            )

    def __call__(self, point, k=1, model=None):
        dist = F.pairwise_distance(
            self.embeddings,
            point.unsqueeze(0).repeat(self.embeddings.shape[0], 1)
        )

        if k > 1:
            svo = [self.svo[i.item()] for i in dist.argsort()[:k]]
        else:
            svo = self.svo[dist.argmin().item()]

        return svo


class SVONegativeMiner(object):
    """ Mines data for the hardest pairs in a subset of data
        To be called in training loop.
    """
    def __init__(self, model=None, device=None, objective=None, n_candidates=1,
                 positive_threshold=True):
        """
            Parameters:
                model: any callable object that produces something to put into
                    the objective function
                device (str): torch device to put data onto ['cuda' | 'cpu']
                objective: any callable object that produces output for each
                    sample
        """
        assert model is not None
        assert objective is not None
        if device is None:
            device = 'cpu'
        self._dev = device
        self.model = model
        self.objective = objective
        self.n_candidates = n_candidates
        self.positive_threshold = positive_threshold
        return

    def __repr__(self):
        s = self.__class__.__name__
        s += " for {} candidates on device {}".format(self.n_candidates, self._dev)
        s += "\n  model: {}".format(self.model)
        s += "\n  objective: {}".format(self.objective)
        return s

    def mine(self, anchors, pos, subjects_neg, verbs_neg, objects_neg):
        #with torch.no_grad():
        if True:
            # Evaluate loss for each anchor-candidate pair - don't need gradient
            outs_candidates = self.model.compose(
                subjects_neg.view(-1),
                verbs_neg.view(-1),
                objects_neg.view(-1)
            )

            # Mold anchor images into same shape, duplicating to compare loss
            loss_candidates = self.objective(
                anchors.repeat(self.n_candidates, 1),
                pos.repeat(self.n_candidates, 1),
                outs_candidates)

            # Find hardest candidate
            loss, i_hard = loss_candidates.view(-1, self.n_candidates).max(dim=1)

            # Shape: (batch_size x n_candidates x n_out_dim)
            outs_candidates = outs_candidates.view(anchors.shape[0], self.n_candidates, -1)

            # Reshape for gather step
            i_hard = i_hard.view(*([-1] + [1 for _ in outs_candidates.shape][1:]))
            repeats = [int(a / b) for a, b in zip(outs_candidates.shape, i_hard.shape)]
            i_hard = i_hard.repeat(*repeats)

            # Use indices of hardest samples to collect them
            outs_hard = torch.gather(outs_candidates, 1, i_hard)[:, 0, :]

            outputs = outs_hard, i_hard[:, 0, 0]

        return outputs

    def __call__(self, anchors, pos, subjects_neg, verbs_neg, objects_neg):
        return self.mine(anchors, pos, subjects_neg, verbs_neg, objects_neg)


class LinguisticActionEmbeddingModel(nn.Module):
    """ Model for learning an embedding for linguistic actions project
    """
    def __init__(self, dset, vis_in_dim=512, out_dim=300,
                 lambda_ling=1.0, lambda_vis=1.0, n_subjects=None, n_verbs=None,
                 n_objects=None, **kwargs):
        super().__init__()
        self.dset = dset
        self.out_dim = out_dim

        subjects, verbs, objects = (self.dset.subjects, self.dset.verbs, self.dset.objects)

        self.visual_embedder = LinearLayers(
            vis_in_dim,
            out_dim,
            kwargs.get('visual_embedder_depth', 3)
        )

        # He-Kaiming initialization
        self.visual_embedder.layers[0] = utils.set_he_normal(
            self.visual_embedder.layers[0],
            vis_in_dim
        )

        self.subjective_verb_ops = nn.ParameterList([
            nn.Parameter(torch.eye(out_dim))
            for _ in range(len(verbs))
        ])
        self.objective_verb_ops = nn.ParameterList([
            nn.Parameter(torch.eye(out_dim))
            for _ in range(len(verbs))
        ])
        self.subject_embedder = nn.Embedding(len(subjects), out_dim)
        self.object_embedder = nn.Embedding(len(objects), out_dim)

        self.init_regularizer_vis(lambda_vis, n_subjects, n_verbs, n_objects)
        self.init_regularizer_ling(lambda_ling, n_subjects, n_verbs, n_objects)

        if kwargs.get('glove_init', True):
            word_embedding_file = kwargs.get(
                'word_embedding_file',
                utils.path('~/data/coco-actions/glove/glove.6B.300d.txt')
            )
            pretrained_weight_sub = load_word_embeddings(word_embedding_file, self.dset.subjects)
            pretrained_weight_obj = load_word_embeddings(word_embedding_file, self.dset.objects)
            self.subject_embedder.weight.data.copy_(pretrained_weight_sub)
            self.object_embedder.weight.data.copy_(pretrained_weight_obj)

        self.training = False

    def init_regularizer_vis(self, lambda_vis, n_subjects, n_verbs, n_objects):
        self.lambda_vis = lambda_vis
        if self.lambda_vis > 0:
            self.subject_clf_visual = utils.set_he_normal(
                nn.Linear(self.out_dim, n_subjects),
                self.out_dim
            )

            self.verb_clf_visual = utils.set_he_normal(
                nn.Linear(self.out_dim, n_verbs),
                self.out_dim
            )

            self.object_clf_visual = utils.set_he_normal(
                nn.Linear(self.out_dim, n_objects),
                self.out_dim
            )
        else:
            self.subject_clf_visual = self.verb_clf_visual = self.object_clf_visual = lambda x: x

    def init_regularizer_ling(self, lambda_ling, n_subjects, n_verbs, n_objects):
        self.lambda_ling = lambda_ling
        if self.lambda_ling > 0:
            self.subject_clf_linguistic = utils.set_he_normal(
                nn.Linear(self.out_dim, n_subjects),
                self.out_dim
            )

            self.verb_clf_linguistic = utils.set_he_normal(
                nn.Linear(self.out_dim, n_verbs),
                self.out_dim
            )

            self.object_clf_linguistic = utils.set_he_normal(
                nn.Linear(self.out_dim, n_objects),
                self.out_dim
            )
        else:
            self.subject_clf_linguistic = self.verb_clf_linguistic = self.object_clf_linguistic = lambda x: x

    def eval_loss_vis(self, vis_embed, subjects, verbs, objects):
        """ Evaluates the visual loss

        """
        if self.lambda_vis > 0:

            # Which data points have objects?
            valid_object_indices = objects >= 0
            objects_rectified = torch.where(
                objects < 0,
                torch.zeros_like(objects),
                objects
            )

            # How well does the simple classifier predict subjects from the visual embedding?
            subject_loss = F.cross_entropy(self.subject_clf_visual(vis_embed), subjects)
            verb_loss = F.cross_entropy(self.verb_clf_visual(vis_embed), verbs)

            # Only use valid objects here, to not throw errors. Scale this loss
            # to keep it in the same order of magnitude as the rest
            if True:
                object_loss = F.cross_entropy(
                    self.object_clf_visual(vis_embed),
                    objects_rectified
                )
                loss_vis = self.lambda_vis * (subject_loss + verb_loss + object_loss)
            elif valid_object_indices.sum() > 0:
                object_loss = F.cross_entropy(
                    self.object_clf_visual(vis_embed[valid_object_indices]),
                    objects[valid_object_indices]
                ) * (len(valid_object_indices) / valid_object_indices.sum())
                loss_vis = self.lambda_vis * (subject_loss + verb_loss + object_loss)

            # If no objects were present, ignore them
            else:
                loss_vis = self.lambda_vis * (subject_loss + verb_loss)

        else:
            loss_vis = 0
        return loss_vis

    def eval_loss_ling(self, sv_embeds, ov_embeds, subjects, verbs, objects):
        """ Evaluates the component loss

        """
        if self.lambda_ling > 0:

            # Which data points have objects?
            valid_object_indices = objects >= 0

            # How well does the simple classifier predict subjects and verbs
            # from the SV embeddings?
            svs_loss = F.cross_entropy(self.subject_clf_linguistic(sv_embeds), subjects)
            svv_loss = F.cross_entropy(self.verb_clf_linguistic(sv_embeds), verbs)

            # Only use valid objects here, to not throw errors. Scale this loss
            # to keep it in the same order of magnitude as the rest
            if valid_object_indices.sum() > 0:

                ovv_loss = F.cross_entropy(
                    self.verb_clf_linguistic(ov_embeds[valid_object_indices]),
                    verbs[valid_object_indices]
                ) * (len(valid_object_indices) / valid_object_indices.sum())

                ovo_loss = F.cross_entropy(
                    self.object_clf_linguistic(ov_embeds[valid_object_indices]),
                    objects[valid_object_indices]
                ) * (len(valid_object_indices) / valid_object_indices.sum())

                loss_ling = self.lambda_ling * (svs_loss + svv_loss + ovv_loss + ovo_loss)

            # If no objects were present, ignore them
            else:
                loss_ling = self.lambda_ling * (svs_loss + svv_loss)

        else:
            loss_ling = 0
        return loss_ling

    def dist(self, img_feats, id_embedding):
        return F.pairwise_distance(img_feats, id_embedding)

    def _apply_verb_ops(self, ops, vecs):
        """ Applies a tensor of verb operations to a tensor of entity
            embeddings
            Parameters:
                ops (B x 300 x 300 tensor): operators
                vecs (B x 1 x 300 tensor): operators
        """
        out = torch.bmm(ops, vecs.unsqueeze(2)).squeeze(2)
        out = F.relu(out)
        return out

    def compose_sv(self, subjects_idx, verbs_idx):
        subject_embeddings = self.subject_embedder(subjects_idx)
        subjective_verb_ops = torch.stack([
            self.subjective_verb_ops[v_i.item()]
            for v_i in verbs_idx
        ])
        subjective_embeddings = self._apply_verb_ops(subjective_verb_ops,
                                                     subject_embeddings)
        return subjective_embeddings

    def compose_ov(self, objects_idx, verbs_idx):
        """ Compose objects and verbs. If any objects are -1 (Null), set their
            embeddings to the zero-vector
        """
        valid_objects = objects_idx >= 0

        objective_embeddings = torch.zeros(
            objects_idx.shape[0],
            self.out_dim
        ).cuda()

        if valid_objects.sum() > 0:
            object_embeddings = self.object_embedder(torch.masked_select(
                objects_idx,
                valid_objects
            ))
            objective_verb_ops = torch.stack([
                self.objective_verb_ops[v_i.item()]
                for v_i in torch.masked_select(
                    verbs_idx,
                    valid_objects
                )
            ])

            applied = self._apply_verb_ops(
                objective_verb_ops,
                object_embeddings
            )

            # Mask on whether objects actually exist
            objective_embeddings[valid_objects, :] = applied
        return objective_embeddings

    def compose(self, subjects_idx, verbs_idx, objects_idx):
        """ Compose SVOs into actions
        """

        subjective_embeddings = self.compose_sv(subjects_idx, verbs_idx)
        objective_embeddings = self.compose_ov(objects_idx, verbs_idx)
        action_embeddings = subjective_embeddings + objective_embeddings
        return action_embeddings

    def forward(self, vis, sub, verb, obj):
        vis_embeds = self.visual_embedder(vis) if vis is not None else None

        svo_embeds = self.compose(sub, verb, obj) if sub is not None and verb is not None else None
        return vis_embeds, svo_embeds


class LinguisticActionTripletEmbeddingSystem(nd.NetworkSystem):
    """ System for learning an embedding of videos and SVO identities using the triplet loss
    """
    constraints = {
        'miner': None,
        'lambda_vis': (float, int),
        'lambda_ling': (float, int),
        'out_dim': int,
        'margin': (float, int),
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': int,
    }
    defaults = {
        'miner': None,
        'lambda_vis': 1.0,
        'lambda_ling': 1.0,
        'out_dim': 300,
        'margin': 0.5,
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': 10,
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self._v > 0:
            print("Initialized {}".format(self.__class__.__name__))

    def forward(self, data):
        anchors = data[0].to(self.device)
        subjects, verbs, objects = [
            torch.LongTensor(t).to(self.device)
            for t in data[1:4]
        ]

        embeds_anchor, embeds_pos = self.model(
            anchors,
            subjects,
            verbs,
            objects
        )

        if self.training:
            # Grab candidates and push to device
            subjects_neg, verbs_neg, objects_neg = [
                torch.stack(t).to(self.device)
                for t in data[4:7]
            ]

            # Mine hardest triplets in batch
            embeds_hard, indices_hard = self.miner(
                embeds_anchor,
                embeds_pos,
                subjects_neg,
                verbs_neg,
                objects_neg
            )

            # Compute loss with hardest triplets
            loss_triplet = self.objective(
                embeds_anchor,
                embeds_pos,
                embeds_hard,
            )
        else:
            # Just convert to Long and push to device
            sub_neg, verb_neg, obj_neg = [
                torch.LongTensor(t).to(self.device)
                for t in data[4:7]
            ]

            # Compute loss
            embeds_neg = self.model.compose(sub_neg, verb_neg, obj_neg)
            loss_triplet = self.objective(
                embeds_anchor,
                embeds_pos,
                embeds_neg,
            )

        pred_top_1 = [self.predictor_open(anc) for anc in embeds_anchor]
        pred_closed_1 = [self.predictor_closed(anc) for anc in embeds_anchor]

        actual = [
            (
                self.loaders['train'].dataset.idx2subject[s.item()],
                self.loaders['train'].dataset.idx2verb[v.item()],
                self.loaders['train'].dataset.idx2object[o.item()] if o.item() > 0 else "None"
            )
            for s, v, o in zip(
                subjects,
                verbs,
                objects
            )
        ]

        acc_open = sum([
            pred == act
            for pred, act in zip(
                pred_top_1,
                actual
            )
        ]) / len(pred_top_1)

        acc_closed = sum([
            pred == act
            for pred, act in zip(
                pred_closed_1,
                actual
            )
        ]) / len(pred_closed_1)

        loss_vis = self.model.eval_loss_vis(
            embeds_anchor,
            subjects, verbs, objects)
        loss_ling = self.model.eval_loss_ling(
            self.model.compose_sv(subjects, verbs),
            self.model.compose_ov(objects, verbs),
            subjects, verbs, objects)
        loss_total = loss_triplet + loss_vis + loss_ling

        retval = {
            'loss': loss_total,
            'loss_triplet': loss_triplet,
            'loss_vis': loss_vis,
            'loss_ling': loss_ling,
            #'error_top_1': error_1,
            #'error_top_5': error_5,
            'acc_open': acc_open,
            'acc_closed': acc_closed,
        }
        if self._v > 2:
            print("[system] done with forward")
        return retval

    def on_epoch(self):
        super().on_epoch()
        self.predictor_open.model.generate_embeddings(self.model)
        self.predictor_closed.model.generate_embeddings(self.model)
        if self.miner is not None:
            self.miner.model = self.model

    @staticmethod
    def create_nice_name(args):
        """ Creates a nice name from the arguments (NameSpace) given
        """
        nice_name = args.nice_name
        nice_name += "_{}".format("coco-actions-svo")
        nice_name += "_subset={}".format(0.9)
        nice_name += "_n-candidates={}".format(args.n_candidates)
        nice_name += "_embed-dim={}".format(args.embed_dim)
        nice_name += "_triplet-margin={}".format(args.triplet_margin)
        nice_name += "_train-ratio={}".format(args.train_ratio)
        nice_name += "_lr={:0.2e}".format(args.lr)
        nice_name += "_lambda-vis={}".format(args.lambda_vis)
        nice_name += "_lambda-ling={}".format(args.lambda_ling)
        nice_name += "_sample-all-triplets" if args.sample_all_triplet_combos else ''
        return nice_name

    @staticmethod
    def parse_nice_name(fpath):
        items = fpath.split(os.path.sep)
        nice_name = items[-2]

        nn_split = nice_name.split('_')

        parse_dict = {
            'dataset': nn_split.pop(1),
            'user_defined_nice_name': nn_split.pop(0),
            'epoch': int(os.path.splitext(items[-1])[0].split('=')[1])
        }
        parse_dict.update({
            hyper[0]: True
            if len(hyper) == 1
            else float(hyper[1])
            for hyper in map(lambda x: x.split('='), nn_split)
        })
        return parse_dict

    @staticmethod
    def load_cache(save_name):
        return torch.load(utils.path(save_name))


def mine_dset_item(dset, model, miner, index):
    """ Quickly test mining
    """
    with ipdb.launch_ipdb_on_exception():
        item = dset[index]
        vis = item[0].unsqueeze(0)
        sub = torch.LongTensor([item[1]])
        verb = torch.LongTensor([item[2]])
        obj = torch.LongTensor([item[3]])
        subs_neg = torch.LongTensor([item[4]])
        verbs_neg = torch.LongTensor([item[5]])
        objs_neg = torch.LongTensor([item[6]])

        emb_anc, emb_pos = model(
            vis,
            sub,
            verb,
            obj
        )
        mined = miner(
            emb_anc,
            emb_pos,
            subs_neg,
            verbs_neg,
            objs_neg
        )
    return mined


def mine_batch(dset, model, miner, index, batch_size=16, shuffle=False,
               num_workers=1, device=0):
    """ Quickly test mining
    """
    loader = torch.utils.data.DataLoader(
        dset,
        batch_size=batch_size,
        shuffle=shuffle,
        num_workers=num_workers
    )
    with ipdb.launch_ipdb_on_exception():
        for item in loader:

            vis = item[0].to(device)
            sub = torch.LongTensor(item[1]).to(device)
            verb = torch.LongTensor(item[2]).to(device)
            obj = torch.LongTensor(item[3]).to(device)

            # Shape: (candidates x batch_size)
            subs_neg = torch.stack(item[4]).to(device)
            verbs_neg = torch.stack(item[5]).to(device)
            objs_neg = torch.stack(item[6]).to(device)

            with ipdb.launch_ipdb_on_exception():
                emb_anc, emb_pos = model(
                    vis,
                    sub,
                    verb,
                    obj
                )
                mined = miner(
                    emb_anc,
                    emb_pos,
                    subs_neg,
                    verbs_neg,
                    objs_neg
                )
                break
    return mined


def compute_mAP(loader, model, device, n_thresh=30):
    # TODO I can't implement this on a per-batch basis - it must be over the whole dataset
    dist = F.pairwise_distance
    with torch.no_grad():
        min_anc_pos_dist = np.inf
        max_anc_pos_dist = None
        for i, data in enumerate(loader):
            anchors = data[0].to(device)
            subjects, verbs, objects = [
                torch.LongTensor(t).to(device)
                for t in data[1:4]
            ]

            embeds_anchor, embeds_pos = model(
                anchors,
                subjects,
                verbs,
                objects
            )

            actual = [  # NOQA
                (
                    loader.dataset.idx2subject[s.item()],
                    loader.dataset.idx2verb[v.item()],
                    loader.dataset.idx2object[o.item()] if o.item() > 0 else "None"
                )
                for s, v, o in zip(
                    subjects,
                    verbs,
                    objects
                )
            ]

            anc_pos_dist = dist(embeds_anchor, embeds_pos)
            min_anc_pos_dist = min(min_anc_pos_dist, anc_pos_dist.min().item())
            max_anc_pos_dist = max(max_anc_pos_dist, anc_pos_dist.max().item())

        for thresh in torch.linspace(min_anc_pos_dist, max_anc_pos_dist, n_thresh + 1)[1:]:
            candidates_retrieval = torch.stack([  # NOQA
                dist(embed, embeds_anchor) < thresh
                for embed in embeds_pos
            ])
            candidates_recog = torch.stack([  # NOQA
                dist(embed, embeds_pos) < thresh
                for embed in embeds_anchor
            ])
        return


def test_mine_batch():
    device = 0
    dset = COCOActionsSVODatasetActivationsTriplet()
    model = LinguisticActionEmbeddingModel(dset=dset, vis_in_dim=512, out_dim=300).to(device)
    mining_objective = nn.TripletMarginLoss(margin=0.5, reduction='none')
    miner = SVONegativeMiner(model=model, device=device, n_candidates=3, objective=mining_objective)
    mined = mine_batch(dset, model, miner, 0, batch_size=16, shuffle=False, num_workers=1)
    return mined


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        default='~/data',
        help='location of data'
    )
    parser.add_argument(
        '--df_fname',
        default='coco-actions-df_predicted_single-estimated_attrops-split_svo-split.pkl',
        help='name of pickled DataFrame file containing annotations'
    )
    parser.add_argument(
        '--csv_fname',
        default=None,
        help="name of csv of dataframe. If not given, it's inferred from df_fname"
    )
    parser.add_argument(
        '--n_candidates',
        default=3,
        type=int,
        help='Positive and negative samples to use when mining hard positives and negatives'
    )
    parser.add_argument(
        '--n_workers',
        default=8,
        type=int,
        help='Number of threads'
    )
    parser.add_argument(
        '--embed_dim',
        default=300,
        type=int,
        help='dimensionality of embedding space'
    )
    parser.add_argument(
        '--batch_size',
        default=512,
        type=int,
        help='loader batch size'
    )
    parser.add_argument(
        '--test',
        action='store_true',
        help='run tests instead of train'
    )
    parser.add_argument(
        '--prep',
        action='store_true',
        help='prepare for training'
    )
    parser.add_argument(
        '--reset',
        action='store_true',
        help='Reset model, ignoring caches'
    )
    parser.add_argument(
        '--randomize_lr',
        action='store_true',
        help='randomize LR in same order of magnitude'
    )
    parser.add_argument(
        '--nice_name',
        default='triplet',
        help='Human-readable name for model saves'
    )
    parser.add_argument(
        '--cache_dir',
        default='~/.cache/cocoactions',
        help='location for all cached data'
    )
    parser.add_argument(
        '--verbosity',
        default=1,
    )
    parser.add_argument(
        '--triplet_margin',
        default=0.5,
        type=float,
        help='margin for use in triplet loss'
    )
    parser.add_argument(
        '--lambda_vis',
        default=1.0,
        type=float,
        help='weight for visual loss'
    )
    parser.add_argument(
        '--lambda_ling',
        default=1.0,
        type=float,
        help='weight for lingustic loss'
    )
    parser.add_argument(
        '--epochs',
        default=1000,
        type=int,
        help='epochs to train'
    )
    parser.add_argument(
        '--device',
        default=0,
        type=int,
        help='CUDA device to use'
    )
    parser.add_argument(
        '--lr',
        default=1e-8,
        type=float,
        help='Learning rate'
    )
    parser.add_argument(
        '--momentum',
        default=0.9,
        type=float,
        help='SGD momentum'
    )
    parser.add_argument(
        '--train_ratio',
        default=0.7,
        type=float,
        help='ratio of train images to total'
    )
    parser.add_argument(
        '--sample_only_valid_triplets',
        action='store_true',
        help='Do we only work in a space of valid triplets or possible ones?'
    )
    return parser.parse_args()


def convert_df_to_csv(df_fpath, csv_fpath=None):
    import pandas as pd
    if csv_fpath is None:
        csv_fpath = utils.new_ext(df_fpath, 'csv')

    print("Converting DataFrame at\n  {}\nto csv at\n  {}".format(df_fpath, csv_fpath))
    print("reading df now...")
    df = pd.read_pickle(df_fpath)
    import ipdb
    ipdb.set_trace()
    relevant_df = df[['split', 'subject', 'single_action', 'single_object',
                      'svo_split', 'crop_x1', 'crop_y1', 'crop_x2', 'crop_y2']]
    relevant_df.loc[:, 'fname'] = list(map(lambda x: x['file_name'], df['image']))
    print("writing csv now...")
    relevant_df.to_csv(csv_fpath)


def train(args):
    # If specced, generate random LR in approximately same order of magnitude
    if args.randomize_lr:
        args.lr = args.lr * (int(np.random.rand() * 1000) / 100)

    nice_name = args.nice_name
    nice_name += "_{}".format("coco-actions-svo")
    nice_name += "_subset={}".format(0.9)
    nice_name += "_n-candidates={}".format(args.n_candidates)
    nice_name += "_embed-dim={}".format(args.embed_dim)
    nice_name += "_triplet-margin={}".format(args.triplet_margin)
    nice_name += "_train-ratio={}".format(args.train_ratio)
    nice_name += "_lr={:0.2e}".format(args.lr)
    nice_name += "_lambda-vis={}".format(args.lambda_vis)
    nice_name += "_lambda-ling={}".format(args.lambda_ling)
    nice_name += "_sample-only-valid-triplets" if args.sample_only_valid_triplets else ''

    with torch.cuda.device(args.device):
        trackable_metrics = [
            'loss_train',
            'loss_triplet_train',
            'loss_vis_train',
            'loss_ling_train',
            'loss_val',
            #'error_top_1_train',
            #'error_top_5_train',
            #'error_top_1_val',
            #'error_top_5_val',
            'acc_open_val',
            'acc_closed_val',
            #'pos_loss_train',
            #'neg_loss_train',
        ]
        dsets = {
            split: COCOActionsSVODatasetActivationsTriplet(
                dpath=args.data_dir,
                split=split,
                verbosity=int(args.verbosity),
                train_ratio=args.train_ratio,
                sample_only_valid_triplets=args.sample_only_valid_triplets,
                n_candidates=args.n_candidates,
            )
            for split in ('train', 'test')
        }
        # To appeas the network system
        dsets['val'] = dsets['test']
        loaders = {
            split: torch.utils.data.DataLoader(
                dsets[split],
                batch_size=args.batch_size,
                shuffle=split == 'train',
                num_workers=args.n_workers
            )
            for split in dsets.keys()
        }

        # TODO
        model = LinguisticActionEmbeddingModel(
            dset=dsets['train'],
            vis_in_dim=dsets['train'].features.shape[1],
            word_in_dim=args.embed_dim,
            out_dim=args.embed_dim,
            lambda_vis=args.lambda_vis,
            lambda_ling=args.lambda_ling,
            n_subjects=len(loaders['train'].dataset.subjects),
            n_verbs=len(loaders['train'].dataset.verbs),
            n_objects=len(loaders['train'].dataset.objects),
            visual_embedder_depth=3,
        ).cuda()

        objective = nn.TripletMarginLoss(margin=args.triplet_margin)

        optimizer = torch.optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)

        # In order to mine, reduction must be 'none'
        mining_objective = nn.TripletMarginLoss(
            margin=args.triplet_margin,
            reduction='none'
        )

        with ipdb.launch_ipdb_on_exception():
            predictor_open = EmbeddingPredictor(dset=dsets['train'], model=model)
            predictor_closed = EmbeddingPredictor(dset=dsets['train'], svo=dsets['train'].svo_test, model=model)

        miner = SVONegativeMiner(
            model=model,
            objective=mining_objective,
            device=torch.cuda.current_device(),
            n_candidates=args.n_candidates
        )

        hash_on = {
            'model': model,
            'objective': objective,
            'optimizer': optimizer,
            'mining_objective': mining_objective,
        }

        system = LinguisticActionTripletEmbeddingSystem(
            device=torch.cuda.current_device(),
            verbosity=args.verbosity,
            hash_on=hash_on,
            nice_name=nice_name,
            metrics=trackable_metrics,
            model=model,
            objective=objective,
            miner=miner,
            loaders=loaders,
            optimizer=optimizer,
            margin=args.triplet_margin,
            lambda_vis=args.lambda_vis,
            lambda_ling=args.lambda_ling,
            predictor_open=predictor_open,
            predictor_closed=predictor_closed,
            selection_metric='acc_open_val',
            selection_metric_goal='min',
        )

        with ipdb.launch_ipdb_on_exception():
            system.train(args.epochs)


if __name__ == "__main__":
    args = parse_args()
    if False:
        data_dir_cache = utils.Cache(args.data_dir)
        working_cache = utils.Cache(args.cache_dir)

        # If we haven't put the dataframe into the data_dir_cache, go ahead and do that
        if not data_dir_cache.exists(args.df_fname):
            data_dir_cache.cp('./' + args.df_fname)

        # If csv file name is given, grab it. OTW, infer it from the DF file name
        csv_fname = args.csv_fname if args.csv_fname is not None else utils.new_ext(args.df_fname, 'csv')

        # If we haven't already converted to a csv, do so now
        if not data_dir_cache.exists(csv_fname):
            convert_df_to_csv(
                data_dir_cache.fpath(args.df_fname),
                data_dir_cache.fpath(csv_fname)
            )
    train(args)
