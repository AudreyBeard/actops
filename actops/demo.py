import argparse
from math import pi
import os

from PIL import Image
import torch
from torch import random as R
from matplotlib import pyplot as plt
import ipdb  # NOQA

from actops import utils
from actops.dataproc.data_cocoactions import (
    #COCOActionsCompositionDatasetActivationsEmbeddings,
    COCOActionsSVODatasetActivationsEmbeddings
)

WALK_METHOD_OPTIONS = ['random',
                       'straight',
                       'clustered',
                       ]


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile',
                        help='input file (assumes tsne)'
                        )
    parser.add_argument('--indices',
                        type=str,
                        default=None,
                        help='dataset indices [optional]'
                        )
    parser.add_argument('--n_samples',
                        type=int,
                        default=20,
                        help='Number of samples, for when indices are not provided'
                        )
    parser.add_argument('--seed',
                        type=int,
                        default=None,
                        help='RNG seed used for generating walk indices, for when indices are not provided'
                        )
    parser.add_argument('--walk_method',
                        type=str,
                        default='random',
                        choices=WALK_METHOD_OPTIONS,
                        help='How will we walk the data?'
                        )
    parser.add_argument('--startxy',
                        type=str,
                        default=None,
                        help='Starting (x, y) location, in format x,y')
    return parser.parse_args()


def get_next_point(curr_index, walk_dir, data, closest=True):
    """ Computes the next embedding index based on current index and walk
        direction
    """
    def exclude_index(tensor, index):
        return torch.cat((tensor[:index], tensor[index + 1:]))

    curr_index = int(curr_index)

    # Compute distance from walked-to location to all embeddings
    d1 = torch.norm(data - (data[curr_index] + walk_dir), dim=1)

    # If we want to get the point that is closest to walk location by Euclidean
    # distance
    if closest:
        dist = exclude_index(d1, curr_index)
        metric, next_index = dist.min(0)

    # If we want to get the point that is most closely aligned with walk
    # direction (in general, not the nearest by Euclidean distance)
    else:
        d0 = torch.norm(data - data[curr_index], dim=1)
        dist_diff = exclude_index(d1 - d0, curr_index)
        metric, next_index = dist_diff.max(0)

    # Account for the fact that we excluded the current index
    next_index += 1 if next_index >= curr_index else 0

    return next_index, metric


def random_direction_2(low=0, high=(2 * pi)):
    """ Generate random direction in range [low, high) radians, returned as
        (dx, dy) tuple
    """
    # If a range is specified:
    dir_rad = torch.rand(1) * (high - low) + low
    dxy = torch.cat((torch.tensor([1.]), torch.tan(dir_rad)))
    dxy = dxy / torch.norm(dxy)
    if dir_rad > pi / 2 and dir_rad < 3 / 2 * pi:
        dxy *= -1
    return dxy


def get_distance_to_edge(embeddings, index, direction):
    distance = (embeddings - embeddings[index]).mm(direction.view(-1, 1)).max()
    return distance


def generate_dset_indices(dset, method, n_samples, seed=None, starting_point=None):
    if method == 'random':
        # Seed the RNG
        generator = R.manual_seed(seed) if seed else R.seed()

        # Generate random numbers and look up their dataset indices
        indices = torch.LongTensor([
            dset.idx_lookup[i]
            for i in torch.randint(len(dset), (n_samples, ), generator=generator)
        ])
    elif method == 'straight':

        # Get embeddings and walk direction
        embeddings = dset.data['embeddings'][torch.LongTensor(dset.idx_lookup)]
        radius = (embeddings.max(0)[0].min() - embeddings.min(0)[0].max()) / 2
        walk_direction = random_direction_2() * (radius / n_samples)

        # Get point closest to center of mass
        indices = [-1]
        _, indices[0] = torch.norm(embeddings - embeddings.mean(dim=0), dim=1).min(0)

        # Compute indices on a straight walk
        for i in range(1, n_samples):
            indices.append(get_next_point(indices[i - 1],
                                          walk_direction,
                                          embeddings)[0])
        indices = torch.stack(indices)
    elif method == 'clustered':
        indices = [-1]
        embeddings = dset.data['embeddings'][torch.LongTensor(dset.idx_lookup)]

        # Get point closest to center of mass
        if starting_point is not None:
            starting_point = torch.tensor(starting_point)
            _, indices[0] = torch.norm(embeddings - starting_point, dim=1).min(0)
        else:
            _, indices[0] = torch.norm(embeddings - embeddings.mean(dim=0), dim=1).min(0)

        for i in range(1, n_samples):
            walk_direction = random_direction_2()
            indices.append(get_next_point(indices[i - 1],
                                          walk_direction,
                                          embeddings)[0])

    else:
        raise NotImplementedError("Walk type {} not implemented (yet)".format(method))
    return indices


def go_for_walk(dset, indices):
    walk_embed = []
    walk_fpaths = []
    walk_svo = []

    for i in indices:
        dset_i = dset.idx_lookup[i]
        walk_embed.append(dset.data['embeddings'][dset_i])
        walk_fpaths.append(os.path.join(
            dset.dpath,
            'coco-actions',
            'coco',
            dset.data['coco_split'][dset_i],
            dset.data['fname'][dset_i]
        ))
        walk_svo.append((
            dset.data['subject'][dset_i],
            dset.data['verb'][dset_i],
            dset.data['object'][dset_i],
        ))

    walk_embed = torch.stack(walk_embed)
    return walk_embed, walk_fpaths, walk_svo


def create_demo_video(walk_fpaths, walk_svo, walk_method, save_name=None, dset_indices=None,
                      video_dims=(256, 256), n_frames_per=10, starting_point=None):
    cache = utils.Cache('./demos')
    video = []
    if dset_indices is None:
        dset_indices = [None for _ in walk_fpaths]
    for i, (fpath, svo, dset_idx) in enumerate(zip(walk_fpaths, walk_svo, dset_indices)):

        # Load up this image and generate text to show
        img = Image.open(fpath).resize(video_dims)
        if dset_idx is None:
            text = "{0}: {1[0]} {1[1]} {1[2]}".format(i, svo)
        else:
            text = "{0}: {2}\n{1[0]} {1[1]} {1[2]}".format(i, svo, dset_idx)

        # Extend the demo video and draw text on the appropriate frames
        video.extend([utils.AnnotatedImage(pil_image=img) for i in range(n_frames_per)])
        for j in range(n_frames_per):
            video[i * n_frames_per + j].draw_texts([(0, 0)], [text])

    save_name = save_name + '_demo-video' if save_name else 'demo_video'

    # Add some metadata to file name, including index
    save_name += '_n-samples={}'.format(len(walk_fpaths))
    save_name += '_{}-walk'.format(walk_method)
    save_name += '_starting-point=({0[0]},{0[1]})'.format(starting_point) if starting_point else ""
    save_name += '_0.mp4'

    # Increment index as needed to ensure no collisions
    while cache.exists(save_name):
        last_token = (save_name.split('.mp4')[0]).split('_')[-1]
        save_name = '_'.join((save_name.split('.mp4')[0]).split('_')[:-1]) + '_' + str(int(last_token) + 1) + '.mp4'

    save_name = cache.fpath(save_name)
    utils.write_mp4(video, save_name, n_frames_per // 3, dims=video_dims)
    return save_name


def create_demo_plot(dset, walk_embeds, walk_svo, walk_method, title=None,
                     reduction=1, save_name=None):
    cache = utils.Cache('./demos')
    if title is None:
        title = "{} Walk Through t-SNE Embedding Space".format(walk_method.title())

    walk_points = walk_embeds.numpy()
    all_points = dset.data['embeddings'][torch.LongTensor(dset.idx_lookup)].numpy()

    fig, ax = plt.subplots()

    ax.scatter(
        all_points[::reduction, 0],
        all_points[::reduction, 1],
        marker='.'
    )
    ax.plot(walk_points[:, 0], walk_points[:, 1], '-ok')
    ax.set_title(title)

    save_name = '_'.join(title.split()) if save_name is None else save_name

    fig.set_size_inches(w=16, h=16)
    fig.savefig(cache.fpath(save_name))
    plt.close('all')

    return save_name


if __name__ == "__main__":
    args = parse_args()
    demo_cache = utils.Cache('~/.cache/tsne_demos')

    # Grab data
    dset = COCOActionsSVODatasetActivationsEmbeddings(
        embeddings_fname=args.infile,
        split='test'
    )
    data = dset.data['embeddings'][
        torch.LongTensor(dset.idx_lookup)
    ]

    # If indices are specified, get them
    if args.indices:
        indices = (
            int(s)
            for s in args.indices.split(',')
            if s.isnumeric()
        )
        starting_point = None
    else:
        if args.startxy is not None:
            starting_point = [float(s) for s in args.startxy.split(',')]
        else:
            starting_point = None

        indices = generate_dset_indices(
            dset,
            args.walk_method,
            args.n_samples,
            starting_point=starting_point
        )

    # Go through the samples
    walk_embeds, walk_fpaths, walk_svo = go_for_walk(dset, indices)

    # Create video
    video_save_name = create_demo_video(walk_fpaths,
                                        walk_svo,
                                        args.walk_method,
                                        starting_point=starting_point,
                                        dset_indices=indices)
    # Create plot
    plot_save_name = os.path.splitext(video_save_name)[0] + '.png'
    plot_save_name = 'plot'.join(plot_save_name.split('video'))
    create_demo_plot(dset,
                     walk_embeds,
                     walk_svo,
                     args.walk_method,
                     save_name=plot_save_name)

    print("Saved video at {}".format(video_save_name))
    print("Saved plot at {}".format(plot_save_name))
