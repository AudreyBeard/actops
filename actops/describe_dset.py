from argparse import ArgumentParser
import ipdb

from actops import utils

# Import datasets
from actops.dataproc.data_cocoactions import (
    COCOActionsSVODatasetActivationsTriplet,
    COCOActionsSVODatasetActivationsEmbeddings,
)


def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        '--data_dir',
        default='~/data/coco-actions',
        help='location of data'
    )
    parser.add_argument(
        '--train_ratio',
        default=0.7,
        type=float,
        help='ratio of train images to total'
    )
    parser.add_argument(
        '--sample_all_triplet_combos',
        action='store_true',
        help='Do we work in a space of all possible triplet combinations? Less bias, harder to learn.'
    )
    parser.add_argument(
        '--cache_dir',
        default='~/.cache.bak/cocoactions',
        help='location for all cached data'
    )

    return parser.parse_args()


def split_index(dset, i, key):
    return dset.data[key][dset.idx_lookup[i]]


def is_match(dset, i, svo, match_list):
    rc = all([
        dset.data[key][i] == value
        for key, value in zip(match_list, svo)
    ])
    return rc


def plot_svo_histograms(dsets):
    for split, dset in dsets.items():
        svo_counts = {k: 0 for k in dset.unique_svo}
        sv_counts = {k[:2]: None for k in dset.unique_svo}
        sv_counts = {k: 0 for k in set(sv_counts.keys())}
        v_counts = {k[1]: None for k in dset.unique_svo}
        v_counts = {k: 0 for k in set(v_counts.keys())}

        for svo in dset.unique_svo:
            svo_counts[svo] += len([
                None
                for i in range(len(dset))
                if is_match(dset, dset.idx_lookup[i], svo, ('subject', 'verb', 'object'))
            ])

            sv_counts[svo[:2]] += len([
                None
                for i in range(len(dset))
                if is_match(dset, dset.idx_lookup[i], svo[:2], ('subject', 'verb'))
            ])

            v_counts[svo[1]] += len([
                None
                for i in range(len(dset))
                if is_match(dset, dset.idx_lookup[i], [svo[1]], ['verb'])
            ])

        with ipdb.launch_ipdb_on_exception():
            import plottingtools.plot as pt
            labels, y = [x for x in zip(*v_counts.items())]
            title = "Histogram of Verbs\n({})".format(split)
            bars = pt.Bars(
                y=y,
                labels=labels,
                multicolor=False,
                #sort_by='descending',
                title=title
            )
            bars._fig.tight_layout(rect=(0, 0.1, 1, 1))
            bars.save()
        with ipdb.launch_ipdb_on_exception():
            import plottingtools.plot as pt
            labels, y = [x for x in zip(*sv_counts.items())]
            labels = ['_'.join(l) for l in labels]
            title = "Histogram of SV\n({})".format(split)
            bars = pt.Bars(
                y=y,
                labels=labels,
                multicolor=False,
                #sort_by='descending',
                title=title
            )
            bars._fig.tight_layout(rect=(0, 0.1, 1, 1))
            bars.save()
        with ipdb.launch_ipdb_on_exception():
            import plottingtools.plot as pt
            labels, y = [x for x in zip(*svo_counts.items())]
            labels = ['_'.join(l) for l in labels]
            title = "Histogram of SVO\n({})".format(split)
            bars = pt.Bars(
                y=y,
                labels=labels,
                multicolor=False,
                #sort_by='descending',
                title=title
            )
            bars._fig.tight_layout(rect=(0, 0.1, 1, 1))
            bars.save()

        pt.plt.close('all')
        #ipdb.set_trace()
        print()


if __name__ == "__main__":
    args = parse_args()
    dsets = {
        split: COCOActionsSVODatasetActivationsTriplet(
            dpath=args.data_dir,
            split=split,
            verbosity=1,
            train_ratio=args.train_ratio,
            cache=utils.Cache(args.cache_dir),
            sample_only_valid_triplets=not args.sample_all_triplet_combos,
            n_candidates=0,
        )
        for split in ('train', 'test')
    }

    plot_svo_histograms(dsets)
