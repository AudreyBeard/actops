""" actops_svo.py
    2019-11-02
    This is a re-write of linguistic_actions.py to make it simpler and support
    a more fine-grained view of the various loss components
"""

import os

import torch
from torch import nn
from torch.nn import functional as F
from netdev import NetworkSystem
#from memory_profiler import profile

from actops import utils
from actops.external.attributes_as_operators.models.models import load_word_embeddings


class LinearLayers(nn.Module):
    """ Simple ANN with He-Kaiming initialization
    """
    def __init__(self, in_dim, out_dim, n_layers=1, use_bias=True, use_relu=True, reduction=1):
        super().__init__()
        layers = []
        prev_width = in_dim
        for l in range(n_layers - 1):
            linear = nn.Linear(
                prev_width,
                prev_width // reduction,
                bias=use_bias
            )
            #nn.init.kaiming_uniform_(linear.bias, nonlinearity='relu')
            nn.init.kaiming_normal_(linear.weight, nonlinearity='relu')

            layers.append(linear)
            layers.append(nn.ReLU(True))
            prev_width = prev_width // reduction

        linear = nn.Linear(
            prev_width,
            out_dim,
            bias=use_bias
        )
        #nn.init.kaiming_uniform_(linear.bias, nonlinearity='relu')
        nn.init.kaiming_normal_(linear.weight, nonlinearity='relu')
        layers.append(linear)
        if use_relu:
            layers.append(nn.ReLU(True))

        self.layers = nn.Sequential(*layers)

    def forward(self, x):
        return self.layers(x)


class ActopsSVOTripletEmbeddingModel(nn.Module):
    def __init__(self, visual_input_dim=512, out_dim=300,
                 lambda_ling=[1, 1, 1, 1], lambda_vis=[1, 1, 1],
                 subjects=None, verbs=None, objects=None,
                 visual_embedder_depth=3, glove_init=True,
                 glove_fpath='~/data/coco-actions/glove/glove.6B.300d.txt',
                 null_object_index=0, use_precomputed_features=True,
                 backbone=None,
                 **kwargs):

        super().__init__()
        # TODO does the dset need to be a part of the model?
        #self.dset = dset
        self.out_dim = out_dim
        self.lambda_vis = lambda_vis
        self.lambda_ling = lambda_ling
        self.null_object_index = null_object_index

        # Initialize visual embedder
        if use_precomputed_features:
            self.visual_embedder = LinearLayers(
                visual_input_dim,
                out_dim,
                visual_embedder_depth
            )
        else:
            fc = LinearLayers(
                visual_input_dim * visual_input_dim * 16,
                out_dim,
                4,
                reduction=2,
            )
            # If we want to backprop to feature extractor, make it
            if backbone == 'resnet':
                from torchvision.models import resnet50
                backbone_net = resnet50(pretrained=True)

                self.visual_embedder = nn.Sequential(
                    backbone_net.conv1,
                    backbone_net.bn1,
                    backbone_net.relu,
                    backbone_net.maxpool,
                    backbone_net.layer1,
                    backbone_net.layer2,
                    backbone_net.layer3,
                    backbone_net.layer4,
                    backbone_net.avgpool,
                    fc.layers
                )
            elif backbone == 'squeezenet':
                from torchvision.models import squeezenet
                backbone_net = squeezenet.squeezenet1_0(pretrained=True)
                self.visual_embedder = nn.Sequential(
                    backbone_net.features,
                    fc.layers,
                )

        # Subject and object embedders
        self.subject_embedder = nn.Embedding(len(subjects), out_dim)
        self.object_embedder = nn.Embedding(len(objects), out_dim)

        # Subjective and objective verb operators
        self.subjective_verb_ops = nn.ParameterList([
            nn.Parameter(torch.eye(out_dim))
            for _ in range(len(verbs))
        ])
        self.objective_verb_ops = nn.ParameterList([
            nn.Parameter(torch.eye(out_dim))
            for _ in range(len(verbs))
        ])

        # Initialize regularizers
        self._init_regularizer_ling(len(subjects), len(verbs), len(objects))
        self._init_regularizer_vis(len(subjects), len(verbs), len(objects))

        if glove_init:
            glove_fpath = utils.path(glove_fpath)
            pretrained_weight_subject = load_word_embeddings(glove_fpath,
                                                             subjects)
            pretrained_weight_object = load_word_embeddings(glove_fpath,
                                                            objects)

            self.subject_embedder.weight.data.copy_(pretrained_weight_subject)
            self.object_embedder.weight.data.copy_(pretrained_weight_object)
            self.object_embedder.weight[self.null_object_index] = 0

            # Don't update the word embeddings if they're already trained
            # Commented out because torch doesn't like using requites_grad_ on
            # the 'computed' variable of 0
            #self.subject_embedder.weight.requires_grad_(False)
            #self.object_embedder.weight.requires_grad_(False)
            self.subject_embedder.weight.detach_()
            self.object_embedder.weight.detach_()

        self.training = False

    def _init_regularizer_vis(self, n_subjects, n_verbs, n_objects):
        """ Initialize the visual regularizer, which ensures visual embeddings
            can be used to classify subjects and objects
        """
        if self.lambda_vis is not None:
            if not isinstance(self.lambda_vis, list):
                self.lambda_vis = [self.lambda_vis for _ in ('s', 'v', 'o')]

            self.clf_visual_subject = nn.Linear(self.out_dim, n_subjects, bias=True)
            self.clf_visual_verb = nn.Linear(self.out_dim, n_verbs, bias=True)
            self.clf_visual_object = nn.Linear(self.out_dim, n_objects, bias=True)

            nn.init.kaiming_normal_(self.clf_visual_subject.weight, nonlinearity='relu')
            nn.init.kaiming_normal_(self.clf_visual_verb.weight, nonlinearity='relu')
            nn.init.kaiming_normal_(self.clf_visual_object.weight, nonlinearity='relu')

            #nn.init.kaiming_uniform_(self.clf_visual_subject.bias, nonlinearity='relu')
            #nn.init.kaiming_uniform_(self.clf_visual_verb.bias, nonlinearity='relu')
            #nn.init.kaiming_uniform_(self.clf_visual_object.bias, nonlinearity='relu')

        else:
            self.clf_visual_subject = None
            self.clf_visual_verb = None
            self.clf_visual_object = None

    def _init_regularizer_ling(self, n_subjects, n_verbs, n_objects):
        """ Initialize the linguistic regularizer, which ensures linguistic
            embeddings can be used to classify subjects and objects
        """
        if self.lambda_ling is not None:
            if not isinstance(self.lambda_ling, list):
                self.lambda_ling = [self.lambda_ling for _ in ('sv_s', 'sv_v', 'ov_o', 'ov_v')]

            self.clf_linguistic_subject = nn.Linear(self.out_dim, n_subjects, bias=True)
            self.clf_linguistic_verb = nn.Linear(self.out_dim, n_verbs, bias=True)
            self.clf_linguistic_object = nn.Linear(self.out_dim, n_objects, bias=True)

            nn.init.kaiming_normal_(self.clf_linguistic_subject.weight, nonlinearity='relu')
            nn.init.kaiming_normal_(self.clf_linguistic_verb.weight, nonlinearity='relu')
            nn.init.kaiming_normal_(self.clf_linguistic_object.weight, nonlinearity='relu')

            #nn.init.kaiming_uniform_(self.clf_linguistic_subject.bias, nonlinearity='relu')
            #nn.init.kaiming_uniform_(self.clf_linguistic_verb.bias, nonlinearity='relu')
            #nn.init.kaiming_uniform_(self.clf_linguistic_object.bias, nonlinearity='relu')

        else:
            self.clf_linguistic_subject = None
            self.clf_linguistic_verb = None
            self.clf_linguistic_object = None

    def eval_loss_vis(self, embed_vis, target_subject, target_verb, target_object):
        """ Evaluates the visual subject and verb loss (how well does the
            visual embedding predict subject and verb?)
        """
        if self.lambda_vis is not None:
            loss_subject = F.cross_entropy(self.clf_visual_subject(embed_vis), target_subject)
            loss_verb = F.cross_entropy(self.clf_visual_verb(embed_vis), target_verb)
            loss_object = F.cross_entropy(self.clf_visual_object(embed_vis), target_object)
        else:
            loss_subject = loss_verb = loss_object = 0

        losses = [
            lambda_ * loss
            for lambda_, loss in zip(
                self.lambda_vis,
                (
                    loss_subject,
                    loss_verb,
                    loss_object
                )
            )
        ]
        return losses

    def eval_loss_ling(self, embed_sv, embed_ov, target_subject, target_verb, target_object):
        """ Evaluates the linguistic subject, verb, and object loss (how well
            does the SV composition embedding predict subject and verb, and the
            same for the OV?)
        """
        if self.lambda_ling is not None:
            loss_sv_subject = F.cross_entropy(
                self.clf_linguistic_subject(embed_sv),
                target_subject
            )
            loss_sv_verb = F.cross_entropy(
                self.clf_linguistic_verb(embed_sv),
                target_verb
            )

            loss_ov_object = F.cross_entropy(
                self.clf_linguistic_object(embed_ov),
                target_object,
            )

            # Don't use null-objects in predicting verb, since it's impossible
            valid_object_indices = target_object != self.null_object_index
            if valid_object_indices.sum() > 0:
                loss_ov_verb = F.cross_entropy(
                    self.clf_linguistic_verb(embed_ov[valid_object_indices]),
                    target_verb[valid_object_indices],
                )
            else:
                loss_ov_verb = 0
        else:
            loss_sv_subject = loss_sv_verb = loss_ov_object = loss_ov_verb = 0

        losses = [
            lambda_ * loss
            for lambda_, loss in zip(
                self.lambda_ling,
                (
                    loss_sv_subject,
                    loss_sv_verb,
                    loss_ov_object,
                    loss_ov_verb
                )
            )
        ]
        return losses

    def _apply_verb_ops(self, ops, vecs):
        """ Applies a tensor of verb operations to a tensor of entity
            embeddings
            Parameters:
                ops (B x 300 x 300 tensor): operators
                vecs (B x 1 x 300 tensor): operators
        """
        out = torch.bmm(ops, vecs.unsqueeze(2)).squeeze(2)
        out = F.relu(out)
        return out

    def compose_sv(self, idx_subjects, idx_verbs):
        """ Composes a subject and verb tensors
        """
        embed_subjects = self.subject_embedder(idx_subjects)
        subjective_verb_ops = torch.stack([
            self.subjective_verb_ops[v_i.item()]
            for v_i in idx_verbs
        ])
        subjective_embeddings = self._apply_verb_ops(
            subjective_verb_ops,
            embed_subjects
        )
        return subjective_embeddings

    def compose_ov(self, idx_objects, idx_verbs):
        """ Composes an object and verb tensors
        """
        # Any NULL objects are 0-vector
        embed_objects = torch.zeros(
            idx_objects.shape[0],
            self.out_dim
        ).cuda()

        # Only set objects that are valid
        valid_object_indices = idx_objects >= 0
        embed_objects[valid_object_indices] = self.object_embedder(
            idx_objects[valid_object_indices]
        )

        objective_verb_ops = torch.stack([
            self.objective_verb_ops[v_i.item()]
            for v_i in idx_verbs
        ])
        objective_embeddings = self._apply_verb_ops(
            objective_verb_ops,
            embed_objects
        )
        return objective_embeddings

    def compose(self, subjects_idx, verbs_idx, objects_idx):
        """ Compose SVOs into actions
        """
        subjective_embeddings = self.compose_sv(subjects_idx, verbs_idx)
        objective_embeddings = self.compose_ov(objects_idx, verbs_idx)
        action_embeddings = subjective_embeddings + objective_embeddings
        return action_embeddings

    #@profile
    def forward(self, visual=None, idx_subjects=None, idx_verbs=None, idx_objects=None):
        """ Feed forward
        """
        if visual is not None:
            embed_vis = self.visual_embedder(visual)
        else:
            embed_vis = None

        if idx_subjects is not None and idx_verbs is not None and idx_objects is not None:
            embed_svo = self.compose(idx_subjects, idx_verbs, idx_objects)
        else:
            embed_svo = None

        return embed_vis, embed_svo


class ActopsSVOTripletEmbeddingSystem(NetworkSystem):
    """ System for learning an embedding of videos and SVO identities using the triplet loss
    """
    constraints = {
        'miner': None,
        'lambda_vis': list,
        'lambda_ling': list,
        #'lambda_vis': (float, int),
        #'lambda_ling': (float, int),
        'out_dim': int,
        'margin': (float, int),
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': int,
        'monitor_weights': bool,
    }
    defaults = {
        'miner': None,
        'lambda_vis': 1.0,
        'lambda_ling': 1.0,
        'out_dim': 300,
        'margin': 0.5,
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': 10,
        'monitor_weights': False,
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        print("Initialized {}".format(self.__class__.__name__))

    #@profile
    def forward(self, data):
        """ Feed forward
        """
        # Grab anchors and positive/negative subjects, verbs, objects
        anchors = data[0].to(self.device)
        idx_pos_subjects, idx_pos_verbs, idx_pos_objects = [
            torch.LongTensor(t).to(self.device)
            for t in data[1:4]
        ]
        if self.miner is not None and self.loaders['train'].dataset.n_candidates > 1:
            idx_neg_subjects, idx_neg_verbs, idx_neg_objects = [
                torch.stack(t).to(self.device)
                for t in data[4:7]
            ]
        else:
            idx_neg_subjects, idx_neg_verbs, idx_neg_objects = [
                t.to(self.device)
                for t in data[4:7]
            ]

        # We're not concerned about invalid objects - the model handles that
        embeds_anc, embeds_pos = self.model(
            anchors,
            idx_pos_subjects,
            idx_pos_verbs,
            idx_pos_objects
        )
        if self.training and self.miner is not None:
            embeds_neg, indices_hard = self.miner.mine(
                embeds_anc,
                embeds_pos,
                idx_neg_subjects,
                idx_neg_verbs,
                idx_neg_objects
            )
        else:
            embeds_neg = self.model.compose(idx_neg_subjects, idx_neg_verbs, idx_neg_objects)

        # Open and closed predictions
        svo_pred_top_1_open = torch.stack([
            self.predictor_open(anc)
            for anc in embeds_anc
        ])
        svo_pred_top_1_closed = torch.stack([
            self.predictor_closed(anc)
            for anc in embeds_anc
        ])

        # Actual
        svo_actual = torch.cat((
            idx_pos_subjects.reshape(-1, 1),
            idx_pos_verbs.reshape(-1, 1),
            idx_pos_objects.reshape(-1, 1),
        ), 1)

        # SVO accuracy
        svo_acc_open = ((svo_pred_top_1_open == svo_actual).sum(1) == 3).float().mean()
        svo_acc_closed = ((svo_pred_top_1_closed == svo_actual).sum(1) == 3).float().mean()

        # SV accuracy TODO this needs to be revisited
        sv_acc_open = ((svo_pred_top_1_open[:, :2] == svo_actual[:, :2]).sum(1) == 2).float().mean()
        sv_acc_closed = ((svo_pred_top_1_closed[:, :2] == svo_actual[:, :2]).sum(1) == 2).float().mean()

        # Subject, verb, object accuracy TODO all of these need to be revisited
        s_acc_open = (svo_pred_top_1_open[:, 0] == svo_actual[:, 0]).float().mean()
        v_acc_open = (svo_pred_top_1_open[:, 1] == svo_actual[:, 1]).float().mean()
        o_acc_open = (svo_pred_top_1_open[:, 2] == svo_actual[:, 2]).float().mean()
        s_acc_closed = (svo_pred_top_1_closed[:, 0] == svo_actual[:, 0]).float().mean()
        v_acc_closed = (svo_pred_top_1_closed[:, 1] == svo_actual[:, 1]).float().mean()
        o_acc_closed = (svo_pred_top_1_closed[:, 2] == svo_actual[:, 2]).float().mean()
        loss_triplet = self.objective(
            embeds_anc,
            embeds_pos,
            embeds_neg
        )
        loss_vis_sub, loss_vis_verb, loss_vis_obj = self.model.eval_loss_vis(
            embeds_anc,
            idx_pos_subjects,
            idx_pos_verbs,
            idx_pos_objects
        )

        loss_ling = self.model.eval_loss_ling(
            self.model.compose_sv(idx_pos_subjects, idx_pos_verbs),
            self.model.compose_ov(idx_pos_objects, idx_pos_verbs),
            idx_pos_subjects,
            idx_pos_verbs,
            idx_pos_objects
        )
        loss_ling_sv_sub, loss_ling_sv_verb, loss_ling_ov_obj, loss_ling_ov_verb = loss_ling

        loss_total = \
            loss_triplet \
            + loss_vis_sub \
            + loss_vis_verb \
            + loss_vis_obj \
            + loss_ling_sv_sub \
            + loss_ling_sv_verb \
            + loss_ling_ov_obj \
            + loss_ling_ov_verb

        retval = {
            'loss': loss_total,
            'loss_triplet': loss_triplet,
            'loss_vis_sub': loss_vis_sub,
            'loss_vis_verb': loss_vis_verb,
            'loss_ling_sv_sub': loss_ling_sv_sub,
            'loss_ling_sv_verb': loss_ling_sv_verb,
            'loss_ling_ov_obj': loss_ling_ov_obj,
            'loss_ling_ov_verb': loss_ling_ov_verb,
            'svo_acc_open': svo_acc_open,
            'svo_acc_closed': svo_acc_closed,
            'sv_acc_open': sv_acc_open,
            'sv_acc_closed': sv_acc_closed,
            's_acc_open': s_acc_open,
            's_acc_closed': s_acc_closed,
            'v_acc_open': v_acc_open,
            'v_acc_closed': v_acc_closed,
            'o_acc_open': o_acc_open,
            'o_acc_closed': o_acc_closed,
        }

        if self.monitor_weights:
            # TODO pick up here!
            for i in (0, 2, 4):
                for j in range(0, min(self.model.visual_embedder.layers[i].weight.shape), 50):
                    retval['visual_embedder[{0}][{1}, {1}]'.format(i, j)] = \
                        self.model.visual_embedder.layers[i].weight[j, j].item()
            for i in range(len(self.model.subjective_verb_ops)):
                for j in range(0, min(self.model.subjective_verb_ops[i].shape), 100):
                    retval['subjective_verb[{0}][{1}, {1}]'.format(i, j)] = \
                        self.model.subjective_verb_ops[i][j, j].item()
                    retval['objective_verb[{0}][{1}, {1}]'.format(i, j)] = \
                        self.model.objective_verb_ops[i][j, j].item()

        return retval

    def on_epoch(self):
        super().on_epoch()
        self.predictor_open.generate_embeddings(self.model)
        self.predictor_closed.generate_embeddings(self.model)
        if self.miner is not None:
            self.miner.model = self.model

    @staticmethod
    def create_nice_name(args):
        """ Creates a nice name from the arguments (NameSpace) given
        """
        nice_name = args.nice_name
        nice_name += "_{}".format("coco-actions-svo")
        nice_name += "_subset={}".format(0.9)
        nice_name += "_n-candidates={}".format(args.n_candidates)
        nice_name += "_embed-dim={}".format(args.embed_dim)
        nice_name += "_triplet-margin={}".format(args.triplet_margin)
        nice_name += "_train-ratio={}".format(args.train_ratio)
        nice_name += "_lr={:0.2e}".format(args.lr)
        nice_name += "_lambda-vis={}".format("-".join(["{:0.2e}".format(lam) for lam in args.lambda_vis]))
        nice_name += "_lambda-ling={}".format("-".join(["{:0.2e}".format(lam) for lam in args.lambda_ling]))
        nice_name += "_sample-all-triplets" if args.sample_all_triplet_combos else ''
        nice_name += "_learnable_features" if args.learnable_features else ''
        nice_name += "_backbone={}".format(args.backbone) if args.learnable_features else ''
        return nice_name

    @staticmethod
    def parse_nice_name(fpath):
        items = fpath.split(os.path.sep)
        nice_name = items[-2]

        nn_split = nice_name.split('_')

        parse_dict = {
            'dataset': nn_split.pop(1),
            'user_defined_nice_name': nn_split.pop(0),
            'epoch': int(os.path.splitext(items[-1])[0].split('=')[1])
        }
        parse_dict.update({
            hyper[0]: True
            if len(hyper) == 1
            else float(hyper[1])
            for hyper in map(lambda x: x.split('='), nn_split)
        })
        return parse_dict

    def load_state_dict(self, state_dict):
        """ Load state dict, especially useful for initialization seeds
        """
        if state_dict.get('lambda_ling'):
            self.model.lambda_ling = state_dict.pop('labmda_ling')
        if state_dict.get('lambda_vis'):
            self.model.lambda_vis = state_dict.pop('labmda_vis')

        self.model.load_state_dict(state_dict)


class ActopsSVOPredictor(object):
    """ Predicts the unique SVO index, as given by the indices of dset.unique_svo
        Can provide string predictions as well
    """
    def __init__(self, dset, svo=None, model=None):
        self.embeddings = None
        self.model = model
        self.svo = dset.unique_svo if svo is None else svo
        self.svo_indices = torch.LongTensor([
            (
                dset.subject2idx[s],
                dset.verb2idx[v],
                dset.object2idx[o],
            )
            for s, v, o in self.svo
        ]).cuda().permute(1, 0).contiguous()
        #]).cuda().contiguous()

        if self.model is not None:
            self.generate_embeddings(model)

    def generate_embeddings(self, model=None):
        if model is not None:
            self.model = model

        with torch.no_grad():
            self.embeddings = self.model.compose(
                self.svo_indices[0],
                self.svo_indices[1],
                self.svo_indices[2],
            )

    def __call__(self, point, k=1, model=None, as_str=False):
        """ Computes the nearest point(s) and returns as indices
        """
        dist = F.pairwise_distance(
            self.embeddings,
            point.unsqueeze(0).repeat(self.embeddings.shape[0], 1)
        )

        if k > 1:
            idx = dist.argsort()[:k]
        else:
            idx = dist.argmin().item()

        if as_str:
            if k > 1:
                svo = [self.svo[svo_i.item()] for svo_i in idx]
            else:
                svo = self.svo[svo.item()]

        else:
            if k > 1:
                svo = torch.stack([self.svo_indices[:, svo_i] for svo_i in idx])
            else:
                svo = self.svo_indices[:, idx]

        return svo


class SVONegativeMiner(object):
    """ Mines data for the hardest pairs in a subset of data
        To be called in training loop.
    """
    def __init__(self, model, objective, device=None, n_candidates=1):
        """
            Parameters:
                model (object): any callable object that produces
                    something to put into the objective function, such as
                    torch.nn.Module
                device (torch.cuda.device, int, str, None): torch device to put
                    data onto. If None, uses current device
                objective (object): any callable object that produces a loss
                    value for each sample individually. in PyTorch parlance,
                    this translates to reduction='none'
        """
        self.model = model
        self.objective = objective
        self.n_candidates = n_candidates
        if device is None:
            self.device = device = torch.cuda.current_device()

    def __repr__(self):
        s = self.__class__.__name__
        s += " for {} candidates on device {}".format(self.n_candidates, self.device)
        s += "\n  model: {}".format(self.model.__class__.__name__)
        s += "\n  objective: {}".format(self.objective.__class__.__name__)
        return s

    def mine(self, embeds_anc, embeds_svo_pos,
             idx_neg_sub, idx_neg_verb, idx_neg_obj):

        embeds_svo_candidates = self.model.compose(
            idx_neg_sub.view(-1),
            idx_neg_verb.view(-1),
            idx_neg_obj.view(-1)
        )

        # Mold anchor images into same shape, duplicating to compare loss
        loss_candidates = self.objective(
            embeds_anc.repeat(self.n_candidtes, 1),
            embeds_svo_pos.repeat(self.n_candidates, 1),
            embeds_svo_candidates
        )

        # Find hardest candidate
        loss, idx_hard = loss_candidates.view(-1, self.n_candidates).max(dim=1)

        # Shape: (batch_size x n_candidates x n_out_dim)
        embeds_svo_candidates = embeds_svo_candidates.view(
            embeds_anc.shape[0],
            self.n_candidates,
            -1
        )

        # Reshape for gather step
        idx_hard = idx_hard.view(*([-1] + [1 for _ in embeds_svo_candidates.shape[1:]]))
        repeats = [int(a / b) for a, b in zip(embeds_svo_candidates.shape, idx_hard.shape)]
        idx_hard = idx_hard.repeat(*repeats)

        # Use indices of hardest samples to collect them
        embeds_hard = torch.gather(embeds_svo_candidates, 1, idx_hard)[:, 0, :]

        return embeds_hard, idx_hard[:, 0, 0]

    def __call__(self, embeds_anc, embeds_svo_pos, idx_neg_sub, idx_neg_verb, idx_neg_obj):
        return self.mine(embeds_anc, embeds_svo_pos, idx_neg_sub, idx_neg_verb, idx_neg_obj)


def generate_kaiming_presets(dsets, args, n_presets):
    cache = utils.Cache('~/.cache/kaiming_presets')
    if args.debug:
        import ipdb
        ipdb.set_trace()

    for i in range(n_presets):
        torch.random.seed()

        model = ActopsSVOTripletEmbeddingModel(
            visual_input_dim=dsets['train'].features.shape[1],
            out_dim=args.embed_dim,
            lambda_ling=args.lambda_ling,
            lambda_vis=args.lambda_vis,
            subjects=dsets['train'].subjects,
            verbs=dsets['train'].verbs,
            objects=dsets['train'].objects,
            visual_embedder_depth=args.vis_embedder_depth,
            glove_init=True,
            null_object_index=dsets['train'].object2idx['None'],
        )

        fname = "{}_kaiming-init_{}.t7".format(model.__class__.__name__, i)

        cache.save(model.state_dict(), fname)
