""" svo_classification_system.py
Defines the functionality of a simple SVO classification system
"""
import os

import torch
from torch import nn
import netdev as nd
import ipdb  # NOQA

from linguistic_actions import LinearLayers


class SVOClassificationPredictor(object):
    def __init__(self, dset, svo=None):
        self.svo = dset.unique_svo if svo is None else svo
        self.svo_indices = torch.LongTensor([
            (
                dset.subject2idx[s],
                dset.verb2idx[v],
                dset.object2idx[o],
            )
            for s, v, o in self.svo
        ]).cuda().permute(1, 0).contiguous()
        self.idx2subject = dset.idx2subject
        self.idx2verb = dset.idx2verb
        self.idx2object = dset.idx2object
        return

    def __call__(self, outs):
        # Since model is a classifier, the predicted classes are just the argmax
        pred_s, pred_v, pred_o = [out.transpose(1, 0).argmax(dim=0) for out in outs]

        # Convert indices to tuples of strings
        #pred_str_tup = [
        #    (
        #        self.idx2subject[s.item()],
        #        self.idx2verb[v.item()],
        #        self.idx2object[o.item()]
        #    )
        #    for s, v, o in zip(pred_s, pred_v, pred_o)
        #]
        return torch.stack((pred_s, pred_v, pred_o), dim=1)


class SVOClassificationModel(nn.Module):
    """ Model for learning SVO classes
    """
    def __init__(self, dset, vis_in_dim=512, clf_depth=5):
        super().__init__()

        # Subject, Verb, Object classifiers
        self.clf_s = LinearLayers(
            in_dim=vis_in_dim,
            out_dim=len(dset.subjects),
            n_layers=clf_depth)
        self.clf_v = LinearLayers(
            in_dim=vis_in_dim,
            out_dim=len(dset.verbs),
            n_layers=clf_depth)
        self.clf_o = LinearLayers(
            in_dim=vis_in_dim,
            out_dim=len(dset.objects),
            n_layers=clf_depth)

    def forward(self, x):
        pred_s = self.clf_s(x)
        pred_v = self.clf_v(x)
        pred_o = self.clf_o(x)
        return pred_s, pred_v, pred_o


class SVOClassificationSystem(nd.NetworkSystem):
    constraints = {
        'out_dim': int,
        'margin': (float, int),
        'eval_frequency': int,
    }
    defaults = {
        'out_dim': 300,
        'margin': 0.5,
        'eval_frequency': 10,
        'selection_metric': 'acc_open_svo_val',
        'selection_metric_goal': 'max',
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        if self._v > 0:
            print("Initialized {}".format(self.__class__.__name__))

    def forward(self, data):
        """ Forward-propagation behavior
        """
        # We only care about visual features and actual labels
        vis, act_s, act_v, act_o = [data[i].to(self.device) for i in range(4)]

        # Rectify for Null-object case
        act_o[act_o < 0] = 0
        outs = self.model(vis)

        # Find predicted classes and see which are correct
        pred_s, pred_v, pred_o = [out.argmax(dim=1) for out in outs]
        correct_svo = (((pred_s == act_s) + (pred_v == act_v) + (pred_o == act_o)) == 3).float()
        correct_sv = (((pred_s == act_s) + (pred_v == act_v)) == 2).float()
        correct_v = (pred_v == act_v).float()

        #ipdb.set_trace()
        #DEBUG=True
        #if DEBUG:
        #    # The verb function classifier output is wrong shape - only 5
        #    for k, pred, act in zip(['subject', 'verb', 'object'], outs, [act_s, act_v, act_o]):
        #        loss = self.objective(pred, act)

        # Initialize the batch stats dict with various losses
        retval = {
            'loss_' + k: self.objective(pred, act)
            for k, pred, act in zip(
                ['subject', 'verb', 'object'],
                outs, [act_s, act_v, act_o]
            )
        }
        retval.update({'loss': sum([v for v in retval.values()])})

        # Add accuracies
        retval.update({
            'acc_open_svo': correct_svo.sum() / len(correct_svo),
            'acc_open_sv': correct_sv.sum() / len(correct_sv),
            'acc_open_v': correct_v.sum() / len(correct_v),
        })
        return retval

    @staticmethod
    def create_nice_name(args):
        """ Creates a nice name from the arguments (NameSpace) given
        """
        nice_name = args.nice_name
        nice_name += "_{}".format("coco-actions-svo")
        nice_name += "_subset={}".format(0.9)
        nice_name += "_n-candidates={}".format(args.n_candidates)
        nice_name += "_embed-dim={}".format(args.embed_dim)
        nice_name += "_triplet-margin={}".format(args.triplet_margin)
        nice_name += "_train-ratio={}".format(args.train_ratio)
        nice_name += "_lr={:0.2e}".format(args.lr)
        return nice_name

    @staticmethod
    def parse_nice_name(fpath):
        items = fpath.split(os.path.sep)
        nice_name = items[-2]

        nn_split = nice_name.split('_')

        parse_dict = {
            'dataset': nn_split.pop(1),
            'user_defined_nice_name': nn_split.pop(0),
            'epoch': int(os.path.splitext(items[-1])[0].split('=')[1])
        }
        parse_dict.update({
            hyper[0]: True
            if len(hyper) == 1
            else float(hyper[1])
            for hyper in map(lambda x: x.split('='), nn_split)
        })
        return parse_dict
