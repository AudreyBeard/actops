""" actops_sv.py
    pared-down version of actops_svo.py to test without objects
"""
import os

import torch
from torch import nn
from torch.nn import functional as F
from netdev import NetworkSystem

from actops import utils
from actops.actops_svo import LinearLayers
from actops.external.attributes_as_operators.models.models import load_word_embeddings


class SVActopsModel(nn.Module):
    """ Actions as Operators model for embedding visual data and subject-verb
        compositions
    """
    def __init__(self, dset, visual_input_dim=512, out_dim=300,
                 lambda_ling=1.0, lambda_vis=1.0, subjects=None, verbs=None,
                 visual_embedder_depth=3, glove_init=True,
                 glove_fpath='~/data/coco-actions/glove/glove.6B.300d.txt',
                 **kwargs):
        super().__init__()
        self.out_dim = out_dim
        self.lambda_vis = lambda_vis
        self.lambda_ling = lambda_ling

        # Initialize visual embedder
        self.visual_embedder = LinearLayers(
            visual_input_dim,
            out_dim,
            visual_embedder_depth
        )

        # Subject embedder and subjective verb operators
        self.subject_embedder = nn.Embedding(len(subjects), out_dim)
        self.subjective_verb_ops = nn.ParameterList([
            nn.Parameter(torch.eye(out_dim))
            for _ in range(len(verbs))
        ])

        # Initialize regularizers
        self._init_regularizer_ling(len(subjects), len(verbs))
        self._init_regularizer_vis(len(subjects), len(verbs))

        if glove_init:
            glove_fpath = utils.path(glove_fpath)
            pretrained_weight_subject = load_word_embeddings(glove_fpath,
                                                             subjects)
            self.subject_embedder.weight.data.copy_(pretrained_weight_subject)

            # Don't update the word embeddings if they're already trained
            self.subject_embedder.weight.requires_grad_(False)

        self.training = False

    def _init_regularizer_vis(self, n_subjects, n_verbs):
        """ Initialize the visual regularizer, which ensures visual embeddings
            can be used to classify subjects and objects
        """
        if self.lambda_vis > 0:
            self.clf_visual_subject = utils.set_he_normal(
                nn.Linear(self.out_dim, n_subjects),
                self.out_dim
            )
            self.clf_visual_verb = utils.set_he_normal(
                nn.Linear(self.out_dim, n_verbs),
                self.out_dim
            )
        else:
            self.clf_visual_subject = self.clf_visual_verb = lambda x: x

    def _init_regularizer_ling(self, n_subjects, n_verbs):
        """ Initialize the linguistic regularizer, which ensures linguistic
            embeddings can be used to classify subjects and objects
        """
        if self.lambda_ling > 0:
            self.clf_linguistic_subject = utils.set_he_normal(
                nn.Linear(self.out_dim, n_subjects),
                self.out_dim
            )

            self.clf_linguistic_verb = utils.set_he_normal(
                nn.Linear(self.out_dim, n_verbs),
                self.out_dim
            )

        else:
            self.subject_clf_linguistic = self.verb_clf_linguistic = lambda x: x

    def eval_loss_vis(self, embed_vis, target_subject, target_verb):
        """ Evaluates the visual subject and verb loss (how well does the
            visual embedding predict subject and verb?)
        """
        if self.lambda_vis > 0:
            loss_subject = F.cross_entropy(self.clf_visual_subject(embed_vis), target_subject)
            loss_verb = F.cross_entropy(self.clf_visual_verb(embed_vis), target_verb)
        else:
            loss_subject = loss_verb = 0
        return loss_subject, loss_verb

    def eval_loss_ling(self, embed_sv, target_subject, target_verb):
        """ Evaluates the linguistic subject and verb loss (how well does the
            SV composition embedding predict subject and verb?)
        """
        if self.lambda_vis > 0:
            loss_subject = F.cross_entropy(self.clf_linguistic_subject(embed_sv), target_subject)
            loss_verb = F.cross_entropy(self.clf_linguistic_verb(embed_sv), target_verb)
        else:
            loss_subject = loss_verb = 0
        return loss_subject, loss_verb

    def _apply_verb_ops(self, ops, vecs):
        """ Applies a tensor of verb operations to a tensor of entity
            embeddings
            Parameters:
                ops (B x 300 x 300 tensor): operators
                vecs (B x 1 x 300 tensor): operators
        """
        out = torch.bmm(ops, vecs.unsqueeze(2)).squeeze(2)
        out = F.relu(out)
        return out

    def compose(self, idx_subjects, idx_verbs):
        """ Composes a subject and verb tensors
        """
        embed_subjects = self.subject_embedder(idx_subjects)
        subjective_verb_ops = torch.stack([
            self.subjective_verb_ops[v_i.item()]
            for v_i in idx_verbs
        ])

        # Apply the verb operations to the subject vectors
        subjective_embeddings = self._apply_verb_ops(
            subjective_verb_ops,
            embed_subjects
        )
        return subjective_embeddings

    def forward(self, input_vis=None, idx_subjects=None, idx_verbs=None):
        """ Passing data through the model. May specify visual or linguistic or both.
        """
        embeds_vis = embeds_ling = None

        if input_vis is not None:
            embeds_vis = self.visual_embedder(input_vis)
        if idx_subjects is not None and idx_verbs is not None:
            embeds_ling = self.compose(idx_subjects, idx_verbs)

        return embeds_vis, embeds_ling


class SVNegativeMiner(object):
    """ Mines data for the hardest pairs in a subset of data
        To be called in training loop.
    """
    def __init__(self, model, objective, device=None, n_candidates=1):
        """
            Parameters:
                model (object): any callable object that produces
                    something to put into the objective function, such as
                    torch.nn.Module
                device (torch.cuda.device, int, str, None): torch device to put
                    data onto. If None, uses current device
                objective (object): any callable object that produces a loss
                    value for each sample individually. in PyTorch parlance,
                    this translates to reduction='none'
        """
        self.model = model
        self.objective = objective
        self.n_candidates = n_candidates
        if device is None:
            self.device = device = torch.cuda.current_device()

    def __repr__(self):
        s = self.__class__.__name__
        s += " for {} candidates on device {}".format(self.n_candidates, self.device)
        s += "\n  model: {}".format(self.model.__class__.__name__)
        s += "\n  objective: {}".format(self.objective.__class__.__name__)
        return s

    def mine(self, embeds_anc, embeds_sv_pos, idx_neg_sub, idx_neg_verb):
        embeds_sv_candidates = self.model.compose(
            idx_neg_sub.view(-1),
            idx_neg_verb.view(-1)
        )

        # Mold anchor images into same shape, duplicating to compare loss
        loss_candidates = self.objective(
            embeds_anc.repeat(self.n_candidtes, 1),
            embeds_sv_pos.repeat(self.n_candidates, 1),
            embeds_sv_candidates
        )

        # Find hardest candidate
        loss, idx_hard = loss_candidates.view(-1, self.n_candidates).max(dim=1)

        # Shape: (batch_size x n_candidates x n_out_dim)
        embeds_sv_candidates = embeds_sv_candidates.view(
            embeds_anc.shape[0],
            self.n_candidates,
            -1
        )

        # Reshape for gather step
        idx_hard = idx_hard.view(*([-1] + [1 for _ in embeds_sv_candidates.shape[1:]]))
        repeats = [int(a / b) for a, b in zip(embeds_sv_candidates.shape, idx_hard.shape)]
        idx_hard = idx_hard.repeat(*repeats)

        # Use indices of hardest samples to collect them
        embeds_hard = torch.gather(embeds_sv_candidates, 1, idx_hard)[:, 0, :]

        return embeds_hard, idx_hard[:, 0, 0]

    def __call__(self, embeds_anc, embeds_sv_pos, idx_neg_sub, idx_neg_verb):
        return self.mine(embeds_anc, embeds_sv_pos, idx_neg_sub, idx_neg_verb)


class SVActopsSystem(NetworkSystem):
    constraints = {
        'miner': None,
        'lambda_vis': (float, int),
        'lambda_ling': (float, int),
        'out_dim': int,
        'margin': (float, int),
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': int,
    }
    defaults = {
        'miner': None,
        'lambda_vis': 1.0,
        'lambda_ling': 1.0,
        'out_dim': 300,
        'margin': 0.5,
        'predictor_open': None,
        'predictor_closed': None,
        'eval_frequency': 10,
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        print("Initialized {}".format(self.__class__.__name__))

    def forward(self, data):
        """ Feed forward
        """
        # Extract anchors, positives, and negatives
        anchors = data[0].to(self.device)
        idx_subjects, idx_verbs = [
            torch.LongTensor(t).to(self.device)
            for t in data[1:3]
        ]
        idx_neg_subjects, idx_neg_verbs = [
            torch.stack(t).to(self.device)
            for t in data[4:6]
        ]

        embeds_anchor, embeds_sv_pos = self.model(anchors, idx_subjects, idx_verbs)

        # Do mining only if we're training and miner is specified. Otherwise,
        # just compute the SV embeddings normally
        if self.training and self.miner is not None:
            embeds_sv_neg, indices_hard = self.miner.mine(
                embeds_anchor,
                embeds_sv_pos,
                idx_neg_subjects,
                idx_neg_verbs
            )
        else:
            embeds_sv_neg = self.model.compose(idx_neg_subjects, idx_neg_verbs)

        sv_pred_top_1_open = torch.stack([
            self.predictor_open(anc)
            for anc in embeds_anchor
        ])
        sv_pred_top_1_closed = torch.stack([
            self.predictor_closed(anc)
            for anc in embeds_anchor
        ])

        act_sv = torch.cat((idx_subjects.view(-1, 1), idx_verbs.view(-1, 1)))

        sv_acc_open = ((sv_pred_top_1_open == act_sv).sum(1) == 2).mean()
        sv_acc_closed = ((sv_pred_top_1_closed == act_sv).sum(1) == 2).mean()

        s_acc_open = (sv_pred_top_1_open[:, 0] == act_sv[:, 0]).mean()
        s_acc_closed = (sv_pred_top_1_closed[:, 0] == act_sv[:, 0]).mean()

        v_acc_open = (sv_pred_top_1_open[:, 1] == act_sv[:, 1]).mean()
        v_acc_closed = (sv_pred_top_1_closed[:, 1] == act_sv[:, 1]).mean()

        loss_triplet = self.objective(
            embeds_anchor,
            embeds_sv_pos,
            embeds_sv_neg
        )
        loss_vis_sub, loss_vis_verb = self.model.eval_loss_vis(
            embeds_sv_pos,
            idx_subjects,
            idx_verbs
        )

        loss_ling_sub, loss_ling_verb = self.model.eval_loss_vis(
            embeds_anchor,
            idx_subjects,
            idx_verbs
        )
        loss_total = \
            loss_triplet \
            + loss_vis_sub \
            + loss_vis_verb \
            + loss_ling_sub \
            + loss_ling_verb
        retval = {
            'loss': loss_total,
            'loss_triplet': loss_triplet,
            'loss_vis_sub': loss_vis_sub,
            'loss_vis_verb': loss_vis_verb,
            'loss_ling_sub': loss_ling_sub,
            'loss_ling_verb': loss_ling_verb,
            'sv_acc_open': sv_acc_open,
            'sv_acc_closed': sv_acc_closed,
            's_acc_open': s_acc_open,
            's_acc_closed': s_acc_closed,
            'v_acc_open': v_acc_open,
            'v_acc_closed': v_acc_closed,
        }
        return retval

    def on_epoch(self):
        super().on_epoch()
        self.predictor_open.generate_embeddings(self.model)
        self.predictor_closed.generate_embeddings(self.model)
        if self.miner is not None:
            self.miner.model = self.model

    @staticmethod
    def create_nice_name(args):
        """ Creates a nice name from the arguments (NameSpace) given
        """
        nice_name = args.nice_name
        nice_name += "_{}".format("coco-actions-svo")
        nice_name += "_subset={}".format(0.9)
        nice_name += "_n-candidates={}".format(args.n_candidates)
        nice_name += "_embed-dim={}".format(args.embed_dim)
        nice_name += "_triplet-margin={}".format(args.triplet_margin)
        nice_name += "_train-ratio={}".format(args.train_ratio)
        nice_name += "_lr={:0.2e}".format(args.lr)
        nice_name += "_lambda-vis={}".format(args.lambda_vis)
        nice_name += "_lambda-ling={}".format(args.lambda_ling)
        nice_name += "_sample-all-triplets" if args.sample_all_triplet_combos else ''
        return nice_name

    @staticmethod
    def parse_nice_name(fpath):
        items = fpath.split(os.path.sep)
        nice_name = items[-2]

        nn_split = nice_name.split('_')

        parse_dict = {
            'dataset': nn_split.pop(1),
            'user_defined_nice_name': nn_split.pop(0),
            'epoch': int(os.path.splitext(items[-1])[0].split('=')[1])
        }
        parse_dict.update({
            hyper[0]: True
            if len(hyper) == 1
            else float(hyper[1])
            for hyper in map(lambda x: x.split('='), nn_split)
        })
        return parse_dict

    def load_state_dict(self, state_dict):
        """ Load state dict, especially useful for initialization seeds
        """
        if state_dict.get('lambda_ling'):
            self.model.lambda_ling = state_dict.pop('labmda_ling')
        if state_dict.get('lambda_vis'):
            self.model.lambda_vis = state_dict.pop('labmda_vis')

        self.model.load_state_dict(state_dict)


class SVIndexPredictor(object):
    def __init__(self, dset, sv=None, model=None):
        self.embeddings = None
        self.model = model
        self.sv = [sv_[:2] for sv_ in dset.unique_svo] if sv is None else sv
        self.sv_indices = torch.LongTensor([
            (
                dset.subject2idx[s],
                dset.verb2idx[v],
            )
            for s, v in self.sv
        ]).cuda().permute(1, 0).contiguous()

        if self.model is not None:
            self.generate_embeddings(model)

    def generate_embeddings(self, model=None):
        if model is not None:
            self.model = model

        with torch.no_grad():
            self.embeddings = self.model.compose(
                self.sv_indices[0],
                self.sv_indices[1],
            )

    def __call__(self, point, k=1, model=None, as_str=False):
        dist = F.pairwise_distance(
            self.embeddings,
            point.unsqueeze(0).repeat(self.embeddings.shape[0], 1)
        )

        if k > 1:
            sv = dist.argsort()[:k]
        else:
            sv = dist.argmin().item()

        if as_str:
            if k > 1:
                sv = [self.sv[sv_i.item()] for sv_i in sv]
            else:
                sv = self.sv[sv.item()]

        return sv
