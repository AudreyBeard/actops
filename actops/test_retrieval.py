from argparse import ArgumentParser
import os

import ipdb
import torch
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image

from actops import utils
from actops.dataproc.data_cocoactions import (
    COCOActionsSVODatasetActivationsEmbeddings,
)

from actops.linguistic_actions import (
    LinguisticActionTripletEmbeddingSystem,
    LinguisticActionEmbeddingModel,
    EmbeddingPredictor,
)

SAMPLE_LOAD = '~/models/LinguisicActionTripletEmbeddingSystem/datafix-lossfix_coco-actions-svo_subset=0.9_n-candidates=31_embed-dim=300_triplet-margin=0.5_train-ratio=0.7_lr=9.72e-06_lambda-vis=1.0_lambda-ling=1.0/E=199.t7'
VISUAL_EMBEDDER_DEPTH = 3


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('--model',
                        default='actops',
                        help='model code')
    parser.add_argument('--load',
                        default=SAMPLE_LOAD,
                        help='location of trained model')
    parser.add_argument('--dpath',
                        default='~/data/coco-actions',
                        help='parent directory containing data')
    parser.add_argument('--device',
                        type=int,
                        default=0,
                        help='CUDA device')
    parser.add_argument('--k',
                        type=int,
                        default=6,
                        help='Number of closest samples to consider')
    return parser.parse_args()


def show_closest(dset, closest_tensor):
    """ Given a dataset and the derived closest indices to the unique SVO
        embeddings, show them
    """
    inches_per_image = 4
    # TODO debug
    for i in range(closest_tensor.shape[0]):

        # Show no more than 3 columns
        n_closest = closest_tensor.shape[1]
        n_cols = 3
        n_rows = int(np.ceil(n_closest / n_cols))

        fig, axs = plt.subplots(
            n_rows, n_cols,
            figsize=(inches_per_image * n_cols, inches_per_image * n_rows),
            subplot_kw=dict(frame_on=False, xticks=[], yticks=[])
        )
        for j, split_index in enumerate(closest_tensor[i]):
            col = j % n_cols
            row = j // n_cols
            fpath = os.path.join(
                dset.dpath,
                'coco',
                dset.data['coco_split'][dset.idx_lookup[split_index]],
                dset.data['fname'][dset.idx_lookup[split_index]]
            )
            img = Image.open(fpath)
            axs[row, col].imshow(img)
            #fig.add_subplot(n_rows, n_cols, j + 1)
            #plt.imshow(img)

        title = "Closest {0} Images to\n'{1[0]} {1[1]} {1[2]}'".format(
            n_closest,
            dset.unique_svo[i]
        )
        fig.suptitle(title)
        save_name = "_".join(title.split()) + '.png'
        save_name = os.path.join('demos', save_name)
        #plt.savefig(save_name)
        fig.savefig(save_name)
        plt.close('all')


def load_or_generate_and_save_retrieval(load_fpath, dsets, k=6):
    cache = utils.Cache(
        os.path.split(load_fpath)[0],
    )
    retrieval_results_name = 'top-{}_SVO_retrieval_results.t7'.format(k)
    if cache.exists(retrieval_results_name):
        print("Found retrieval results, loading now:\n  {}".format(retrieval_results_name))
        retrieval_results = torch.load(cache.fpath(retrieval_results_name))
    else:
        print("Didn't find retrieval results, calculating now:\n  {}".format(retrieval_results_name))
        saved_info = torch.load(load_fpath, map_location=torch.device('cuda'))

        model = LinguisticActionEmbeddingModel(
            dset=dsets['train'],
            vis_in_dim=dsets['test'].features.shape[1],
            word_in_dim=int(parsed_args_dict['embed-dim']),
            out_dim=int(parsed_args_dict['embed-dim']),
            lambda_vis=parsed_args_dict['lambda-vis'],
            lambda_ling=parsed_args_dict['lambda-ling'],
            n_subjects=len(dsets['train'].subjects),
            n_verbs=len(dsets['train'].verbs),
            n_objects=len(dsets['train'].objects),
            visual_embedder_depth=VISUAL_EMBEDDER_DEPTH,
        )

        model.load_state_dict(saved_info['model'])
        model = model.cuda()

        dsets['test'].load_or_generate_and_save_embeddings(model)

        # Grab the indices of each as tensors for feeding into the model
        idx_sub = []
        idx_verb = []
        idx_obj = []
        for sub, verb, obj in dsets['test'].unique_svo:
            idx_sub.append(torch.LongTensor([dsets['test'].subject2idx[sub]]))
            idx_verb.append(torch.LongTensor([dsets['test'].verb2idx[verb]]))
            idx_obj.append(torch.LongTensor([dsets['test'].object2idx[obj]]))

        idx_sub = torch.stack(idx_sub).cuda().squeeze()
        idx_verb = torch.stack(idx_verb).cuda().squeeze()
        idx_obj = torch.stack(idx_obj).cuda().squeeze()

        # Compute embeddings for each SVO composition
        svo_embeddings = model.compose(idx_sub, idx_verb, idx_obj).cpu()

        data_indices = torch.LongTensor(dsets['test'].idx_lookup)
        retrieval_results = []
        for svo in svo_embeddings:
            closest = torch.norm(dsets['test'].embeddings[data_indices] - svo, dim=1).argsort(0)[:args.k]
            retrieval_results.append(closest)

        # Top closest samples to each SVO composition
        retrieval_results = torch.stack(retrieval_results)

        print("Saving retrieval results:\n  {}".format(cache.fpath(retrieval_results_name)))
        torch.save(retrieval_results, cache.fpath(retrieval_results_name))

    return retrieval_results


if __name__ == "__main__":
    args = parse_args()
    with torch.no_grad():
        with torch.cuda.device(args.device):
            # Expand common variables like $HOME and ~
            args.load = utils.path(args.load)
            if args.model == 'actops':
                parsed_args_dict = LinguisticActionTripletEmbeddingSystem.parse_nice_name(args.load)

            dsets = {
                split: COCOActionsSVODatasetActivationsEmbeddings(
                    dpath=args.dpath,
                    split=split,
                    verbosity=0,
                    cache=utils.Cache('~/.cache.bak/cocoactions'),
                    train_ratio=parsed_args_dict['train-ratio'],
                    sample_only_valid_triplets=not parsed_args_dict.get('sample-all-triplets'),
                    n_candidates=0,
                )
                for split in ['train', 'test']
            }

            predictor_open = EmbeddingPredictor(
                dset=dsets['test'],
            )
            #predictor_closed = EmbeddingPredictor(

            #    dset=dset,
            #    svo=dset.svo_test,
            #)

            ipdb.set_trace()
            closest_split_indices = load_or_generate_and_save_retrieval(
                args.load,
                dsets,
                k=args.k
            )

            show_closest(dsets['test'], closest_split_indices)

            # TODO:
            # [x] go through all svo
            # [x] find top 5 images
            # [ ] Save to pyplot
