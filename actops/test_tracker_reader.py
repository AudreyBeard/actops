import argparse
import numpy as np

DEFAULT_FPATH = './test_tracker.csv'


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'test_tracker_file',
        default='test_tracker.csv',
        help='where do we keep track of test performance metrics?'
    )
    return parser.parse_args()


def harmonic_mean(*args):
    return 1 / (sum([1 / a for a in args]) / len(args))


def rank_models(p_dict, *metrics):
    n_models = len(p_dict['save_name'])
    placing = np.array([np.zeros(n_models) for m in metrics])
    for m_i, m in enumerate(metrics):
        for rank, idx in enumerate(np.argsort(p_dict[m])[::-1], 1):
            placing[m_i, idx] = rank / n_models

    harmonic_ranking = np.array([harmonic_mean(*placing[:, i]) for i in range(n_models)])
    order = harmonic_ranking.argsort()
    return order, harmonic_ranking[order]


def load_performance_dict(fpath=DEFAULT_FPATH):
    with open(fpath, 'r') as fid:
        lines = fid.readlines()
    columns = zip(*[l.strip().split(',') for l in lines])
    performance_dict = {
        col[0]: col[1:]
        if col[0] == 'save_name' else np.array(list(map(float, col[1:])))
        for col in columns
    }
    return performance_dict


def compute_auxiliary_metrics(performance_dict):
    performance_dict['svo_accuracy_hmean'] = np.array([
        harmonic_mean(c, o) if c >= 0 and o >= 0 else max(c, o)
        for c, o in zip(
            performance_dict['svo_accuracy_closed'],
            performance_dict['svo_accuracy_open']
        )
    ])

    performance_dict['sv_accuracy_hmean'] = np.array([
        harmonic_mean(c, o) if c >= 0 and o >= 0 else max(c, o)
        for c, o in zip(
            performance_dict['sv_accuracy_closed'],
            performance_dict['sv_accuracy_open']
        )
    ])

    performance_dict['v_accuracy_hmean'] = np.array([
        harmonic_mean(c, o) if c >= 0 and o >= 0 else max(c, o)
        for c, o in zip(
            performance_dict['v_accuracy_closed'],
            performance_dict['v_accuracy_open']
        )
    ])

    return performance_dict


if __name__ == '__main__':
    args = parse_args()
    selection_metrics = ['svo_accuracy_hmean']
    print("Choosing best model based on:\n  {}".format("\n".join(selection_metrics)))
    p_dict = load_performance_dict(fpath=args.test_tracker_file)
    import ipdb
    with ipdb.launch_ipdb_on_exception():
        p_dict = compute_auxiliary_metrics(p_dict)
    ordering, rank_score = rank_models(p_dict, *selection_metrics)
    best_model_index = ordering[0]
    print("\n".join([
        "{}: {}".format(k, v[best_model_index])
        for k, v in p_dict.items()
    ]))
