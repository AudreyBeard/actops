import os
import shutil
import re
import functools
import subprocess
import json
import hmac  # NOQA
import hashlib
from warnings import warn

import torch
import torch.nn.functional as functional
from torchvision import transforms
from PIL import ImageDraw, Image
from numpy import uint8
import numpy as np
import pickle

__all__ = [
    'COCO_INSTANCE_CATEGORY_NAMES',
    'COCO_SUPER_CATEGORY_NAMES',
    'AnnotatedImage',
    'BBox',
    'ResizedLetterBox',
    'n_output_features_conv',
    'execute_vimtmp',
    'Cache',
    'path',
    'TODO',
]


DATA_DIR = "$HOME/data/Moments_in_Time_256x256_30fps"
MIT_DATA_DIR = "$HOME/data/Moments_in_Time_256x256_30fps"


COCO_INSTANCE_CATEGORY_NAMES = [
    '__background__', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
    'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'N/A',
    'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse',
    'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'N/A', 'backpack',
    'umbrella', 'N/A', 'N/A', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis',
    'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
    'skateboard', 'surfboard', 'tennis racket', 'bottle', 'N/A', 'wine glass',
    'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich',
    'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake',
    'chair', 'couch', 'potted plant', 'bed', 'N/A', 'dining table', 'N/A',
    'N/A', 'toilet', 'N/A', 'tv', 'laptop', 'mouse', 'remote', 'keyboard',
    'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator',
    'N/A', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
    'toothbrush'
]

COCO_SUPER_CATEGORY_NAMES = [
    '_', 'person_accessory', 'vehicle', 'vehicle', 'vehicle', 'vehicle',
    'vehicle', 'vehicle', 'vehicle', 'vehicle', 'outdoor_obj', 'outdoor_obj',
    '_', 'outdoor_obj', 'outdoor_obj', 'outdoor_obj', 'animal', 'animal',
    'animal', 'animal', 'animal', 'animal', 'animal', 'animal', 'animal',
    'animal', '_', 'person_accessory', 'person_accessory', '_', '_',
    'person_accessory', 'person_accessory', 'person_accessory', 'sports',
    'sports', 'sports', 'sports', 'sports', 'sports', 'sports', 'sports',
    'sports', 'sports', 'kitchenware', '_', 'kitchenware', 'kitchenware',
    'kitchenware', 'kitchenware', 'kitchenware', 'kitchenware', 'food', 'food',
    'food', 'food', 'food', 'food', 'food', 'food', 'food', 'food',
    'furniture', 'furniture', 'furniture', 'furniture', '_', 'furniture',
    '_', '_', 'furniture', '_', 'electronics', 'electronics',
    'electronics', 'electronics', 'electronics', 'electronics', 'appliance',
    'appliance', 'appliance', 'appliance', 'appliance', '_',
    'indoor_obj', 'indoor_obj', 'indoor_obj', 'indoor_obj',
    'indoor_obj', 'indoor_obj', 'indoor_obj'
]

MIT_SUPERCLASSES = {
    'balancing': ['balancing'],
    'fighting': ['attacking',
                 'boxing',
                 'punching',
                 'wrestling',
                 ],
    'chasing': ['chasing'],
    'biting': ['biting', 'chewing', 'eating'],
    'clawing': ['clawing', 'scratching'],
    'clinging': ['clinging'],
    'colliding': ['colliding', 'crashing'],
    'intimacy': ['cuddling',
                 'hugging',
                 'kissing',
                 'snuggling',
                 'tickling',
                 ],
    'digging': ['digging', 'burying', 'shoveling'],
    'dunking': ['dipping', 'dunking'],
    'wetting': ['baptizing',
                'diving',
                'drenching',
                'dripping',
                'bathing',
                'raining',
                'splashing',
                'submerging',
                'swimming',
                'wetting',
                ],
    'drinking': ['drinking'],
    'driving': ['driving', 'steering', 'swerving'],
    'feeding': ['feeding'],
    'filling': ['fillilng', 'fueling'],
    'grooming': ['combing', 'grooming', 'brushing', 'shaving'],
    'jumping': ['jumping', 'leaping'],
    'landing': ['landing'],
    'launching': ['launching'],
    'bending': ['bending', 'bowing', 'squatting'],
    'blocking': ['blocking', 'guarding'],
    'licking': ['licking'],
    'lifting': ['lifting', 'raising'],
    'loading': ['loading'],
    'flying': ['flying', 'piloting'],
    'floating': ['floating'],
    'planting': ['planting', 'sowing'],
    'playing': ['playing+fun', 'playing'],
    'pulling': ['pulling', 'towing'],
    'pushing': ['pushing'],
    'queuing': ['queuing'],
    'roaring': ['roaring', 'shouting'],
    'resting': ['resting', 'sleeping'],
    'rolling': ['rolling'],
    'hurrying': ['running', 'jogging', 'racing', 'sprinting'],
    #'racing': ['racing'],
    #'running': ['running', 'jogging'],
    'shaking': ['shaking'],
    'sitting': ['sitting'],
    'slipping': ['slipping', 'sliding'],
    'smelling': ['smelling', 'sniffing'],
    'snapping': ['snapping'],
    'sneezing': ['sneezing'],
    'sptting': ['spitting'],
    'spreading': ['spreading'],
    'squinting': ['squinting'],
    'standing': ['standing'],
    'stealing': ['stealing'],
    'stopping': ['stopping'],
    'stretching': ['stretching'],
    'tearing': ['tearing'],
    'falling': ['tripping', 'falling', 'dropping'],
    'unloading': ['unloading'],
    'waking': ['waking'],
    'walking': ['walking'],
    'watering': ['watering'],
    'waving': ['waving'],
    'yawning': ['yawning'],
}

high_variance_actions = {
    'fighting': ['attacking',
                 'fighting',
                 'boxing',
                 'punching',
                 'wrestling',
                 ],
    'waking_starting': ['waking', 'starting'],
    'feeding': ['biting', 'chewing', 'eating', 'feeding'],
    'hurrying': ['running', 'jogging', 'racing', 'sprinting'],
    'roaring': ['roaring', 'shouting'],
    'wetting': ['baptizing',
                'diving',
                'drenching',
                'dripping',
                'bathing',
                'raining',
                'splashing',
                'submerging',
                'swimming',
                'wetting',
                ],
    'resting': ['resting', 'sleeping'],
}


def bbox2lines(bbox_xyxy):
    """ Translates from object localization bounding box to PIL-compatible lines
        Parameters:
            bbox_xyxy (torch.Tensor): bbox in form (x0, y0, x1, y1)
        Returns:
            (list(list(tuples))): top, left, bottom, right lines defined as [(x0, y0), (x1, y1)]
    """
    bbox_arr = bbox_xyxy.cpu().detach().numpy()
    tlbr = [[(bbox_arr[0], bbox_arr[1]), (bbox_arr[2], bbox_arr[1])],
            [(bbox_arr[0], bbox_arr[1]), (bbox_arr[0], bbox_arr[3])],
            [(bbox_arr[0], bbox_arr[3]), (bbox_arr[2], bbox_arr[3])],
            [(bbox_arr[2], bbox_arr[1]), (bbox_arr[2], bbox_arr[3])]]
    return tlbr


class AnnotatedImage(object):
    """ Class defining an annotated PIL image
    """
    def __init__(self, img_tensor=None, fpath=None, line_fill='green', text_fill='green', pil_image=None):
        if img_tensor is not None:
            self.img = transforms.ToPILImage()(
                ((img_tensor - img_tensor.min()) / (img_tensor.max() - img_tensor.min())).cpu())
        elif fpath is not None:
            self.img = Image.open(fpath)
        elif pil_image is not None:
            self.img = pil_image
        else:
            self.img = None

        self._annots = None
        self.line_fill = line_fill
        self.text_fill = text_fill

    def _draw_bbox(self, bbox_xyxy=None, color=None):
        if bbox_xyxy is not None:
            fill = self.line_fill if color is None else color
            if self._annots is None:
                self._annots = ImageDraw.Draw(self.img)

            for line in self._bbox2lines(bbox_xyxy):
                self._annots.line(line, fill=fill)

    def draw_bboxes(self, bboxes_xyxy=None, color=None):
        """ Draws bounding boxes on an image
            Parameters:
                self (AnnotatedImage)
                bboxes_xyxy (torch.Tensor): shape (x, 4), where x = number of boxes to draw
        """
        for i in range(bboxes_xyxy.shape[0]):
            self._draw_bbox(bboxes_xyxy[i, ...], color)

    def save(self, name='annotated_img.png'):
        self.img.save(name)

    def _draw_text(self, xy, text, color=None):
        fill = self.text_fill if color is None else color
        self._annots.text(xy, text, fill=fill)

    def draw_texts(self, xys, texts, color=None):
        if self._annots is None:
            self._annots = ImageDraw.Draw(self.img)
        for xy, text in zip(xys, texts):
            self._draw_text(xy, text, color)

    def _bbox2lines(self, bbox_xyxy):
        """ Translates from object localization bounding box to PIL-compatible lines
            Parameters:
                bbox_xyxy (torch.Tensor): bbox in form (x0, y0, x1, y1)
            Returns:
                (list(list(tuples))): top, left, bottom, right lines defined as [(x0, y0), (x1, y1)]
        """
        bbox_arr = bbox_xyxy.cpu().detach().numpy()
        tlbr = [[(bbox_arr[0], bbox_arr[1]), (bbox_arr[2], bbox_arr[1])],
                [(bbox_arr[0], bbox_arr[1]), (bbox_arr[0], bbox_arr[3])],
                [(bbox_arr[0], bbox_arr[3]), (bbox_arr[2], bbox_arr[3])],
                [(bbox_arr[2], bbox_arr[1]), (bbox_arr[2], bbox_arr[3])]]
        return tlbr


class BBox(object):
    """ Class for keeping track of and doing simple operations on
        bounding boxes, regardless of their format
    """
    def __init__(self, bbox, fmt):
        if isinstance(bbox, list):
            bbox = torch.tensor(bbox)
        self.bbox = bbox.reshape(-1, 4)
        self.fmt = fmt

    def __repr__(self):
        s = self.__class__.__name__
        s += ' with corners:\n'
        s += '  TL: ({0[0]:.2f}, {0[1]:.2f})\n  BR: ({0[2]:.2f}, {0[3]:.2f})'.format(self.xyxy[0])
        return s

    def to_xyxy(self):
        self.bbox = self.xyxy
        self.fmt = 'xyxy'

    @property
    def xyxy(self):
        """ Clones self.bbox and converts if necessary
        """
        bbox = self.bbox.clone()
        if self.fmt == 'xywh':
            if len(bbox.shape) > 1:
                bbox[:, 2:] = bbox[:, :2] + bbox[:, 2:]
            else:
                bbox[2:] = bbox[:2] + bbox[2:]
        elif self.fmt == 'xyxy':
            pass
        return bbox

    @property
    def center(self):
        if self.fmt == 'xywh':
            center = self.bbox[:, :2] + (self.bbox[:, 2:] / 2)
        elif self.fmt == 'xyxy':
            center = self.bbox[:, :2] + ((self.bbox[:, 2:] - self.bbox[:, :2]) / 2)
        return center

    @property
    def area(self):
        if self.fmt == 'xywh':
            area = self.bbox[:, 2] * self.bbox[:, 3]
        elif self.fmt == 'xyxy':
            wh = self.bbox[:, 2:] - self.bbox[:, :2]
            area = wh[:, 0] * wh[:, 1]
        return area

    def iou(self, other):
        """ Compute iou with another (single) bounding box
        """
        def iou_single(box_a, box_b):
            # determine the (x, y)-coordinates of the intersection rectangle
            xA = max(box_a[0], box_b[0])
            yA = max(box_a[1], box_b[1])
            xB = min(box_a[2], box_b[2])
            yB = min(box_a[3], box_b[3])

            # compute the area of intersection rectangle
            interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

            # compute the area of both the prediction and ground-truth
            # rectangles
            boxAArea = (box_a[2] - box_a[0] + 1) * (box_a[3] - box_a[1] + 1)
            boxBArea = (box_b[2] - box_b[0] + 1) * (box_b[3] - box_b[1] + 1)

            # compute the intersection over union by taking the intersection
            # area and dividing it by the sum of prediction + ground-truth
            # areas - the interesection area
            iou = interArea / float(boxAArea + boxBArea - interArea)

            # return the intersection over union value
            return torch.tensor(iou)

        other_xyxy = other.xyxy
        ious = torch.cat([
            iou_single(s_xyxy, other_xyxy[0]).unsqueeze(0)
            for s_xyxy in self.xyxy
        ]) if self.bbox.shape[0] > 0 else torch.tensor([[]])
        return ious.clone().detach()

    @staticmethod
    def merge(bbox_a, bbox_b):
        """ Merges two bounding boxes into one (in general) larger
            bounding box
        """
        bbox_a = bbox_a if isinstance(bbox_a, torch.Tensor) else bbox_a.xyxy
        bbox_b = bbox_b if isinstance(bbox_b, torch.Tensor) else bbox_b.xyxy
        new_t = torch.stack((bbox_a, bbox_b))
        new_t[:, :2] = new_t.min(0).values[:2]
        new_t[:, 2:] = new_t.max(0).values[2:]
        return BBox(new_t[0:1, :], 'xyxy')


class ResizedLetterBox(object):
    """ Performs a resize on the image and a letterbox padding on an image,
        assuming the images are returned as CHW
    """
    def __init__(self, size):
        self.size = size

    def __call__(self, image):

        h, w = image.size

        min_side_len = min(h, w)
        max_side_len = max(h, w)
        new_min_side_len = int(np.ceil(min_side_len / max_side_len * self.size))
        resized_image = functional.resize(image, new_min_side_len)

        # Create a black canvas a paste resized image into it at center
        letterboxed = Image.new(mode=image.mode, size=(self.size, self.size))
        letterboxed.paste(resized_image,
                          box=[(self.size - dim) // 2 for dim in resized_image.size[::-1]])

        return letterboxed


def write_tensor_mp4(fchw, name='tensor_video.mp4', fps=2):
    import cv2

    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(name, fourcc, fps, (256, 256), True)

    for i in range(fchw.shape[0]):
        out.write((fchw[i, ...].permute(1, 2, 0).numpy() * 255).astype(uint8)[:, :, ::-1])

    # Release everything if job is finished
    out.release()


def write_mp4(annotated_imgs, name='annotated_video.mp4', fps=1, verbose=False, dims=256):
    import cv2

    to_tensor = transforms.ToTensor()

    # Define the codec and create VideoWriter object
    #fourcc = cv2.CV_FOURCC(*'MP4V')
    #fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')

    name = "-".join(name.split())
    out = cv2.VideoWriter(name, fourcc, fps, dims, True)

    for img in annotated_imgs:
        img = (to_tensor(img.img.resize(dims)).permute(1, 2, 0).numpy() * 255).astype(uint8)[:, :, ::-1]
        out.write(img)

    # Release everything if job is finished
    out.release()
    if verbose:
        print("wrote {}".format(name))


def object_heuristics(inputs, outputs,
                      multi_object_min_thresh=0.7,
                      max_bbox_area_ratio_thresh=0.56,
                      single_object_min_thresh=0.3):
    """ Applies a set of heuristics on a single input video
        Parameters:
            - inputs (torch.Tensor): inputs to model
            - outputs (list of dicts): predictions from Faster R-CNN model
            - multi_object_min_thresh (float): minimum acceptable score for
              filtering out noise when many objects are present
            - max_bbox_area_ratio_thresh (float): maximum ratio of bbox area to
              frame area, used for tossing bounding boxes that are too large -
              defaults to just below 9:16
            - single_object_minimum_thresh (float): minimum threshold for
              passing objects in case of no detections
    """
    def filter_outputs(outputs, indices):
        outputs = [
            {
                key: out[key][i]
                for key in out.keys()
            }
            for out, i in zip(outputs, indices)
        ]
        return outputs

    # First, filter out detections that are low confidence
    object_indices = [np.where(pred['scores'].cpu() > multi_object_min_thresh)
                      for pred in outputs]

    # If nothing passed confidence threshold, grab best candidate, provided
    # something was detected and its confidence is above some threshold
    object_indices = [
        (np.array([0]), )
        if any(outputs[j]['scores'] > single_object_min_thresh) and len(idx) == 0
        else idx
        for j, idx in enumerate(object_indices)
    ]
    outputs = filter_outputs(outputs, object_indices)

    # Ignore bounding boxes that are trivially large
    bbox_areas = [
        (o['boxes'][:, 2] - o['boxes'][:, 0]) * (o['boxes'][:, 3] - o['boxes'][:, 1])
        for o in outputs
    ]
    image_frame_size = np.prod(inputs.shape[-2:])
    object_indices = [
        np.where(area.detach().cpu().numpy() / image_frame_size < max_bbox_area_ratio_thresh)
        for area in bbox_areas
    ]

    outputs = filter_outputs(outputs, object_indices)

    return outputs


def valid_video(inputs, min_avg_pixel_diff=2, verbose=False):
    #normed_frames = (inputs - inputs.min()) / (inputs.max() - inputs.min())
    max_pixel_diff = (inputs[1:, ...] - inputs[:-1, ...]).abs().max()
    valid = max_pixel_diff > 2
    if verbose and not valid:
        print("max pixel difference between consecutive frames: {}".format(max_pixel_diff))

    return valid


def rectify_object_annotation(outputs, action, lambda_mean=1, lambda_freq=0.5):
    """ Determines which object should be used based on action class using some
        heuristics
    """
    # TODO heuristic for determining most likely subject
    # TODO heuristics for each action class
    #object_names = [[coco_superclass_names[i]
    #                 for i in indices]
    #                for indices in outputs['labels']]

    n_detections = sum([len(o['scores']) for o in outputs])
    unique_object_classes = torch.cat([
        o['labels']
        for o in outputs
    ]).unique().detach().cpu().numpy()

    # Indices of each distinct object-class instance
    indices = [[np.where(o['labels'].detach().cpu().numpy() == label) for o in outputs]
               for label in unique_object_classes]

    # Scores for each distinct object-class instance
    scores = [torch.cat([o['scores'][i] for o, i in zip(outputs, idx)])
              for idx in indices]

    # Frequency-weighted and mean scores
    freq_score = [s.sum() / n_detections for s in scores]
    mean_score = [s.mean() for s in scores]

    # Compute class weight
    class_weight = [lambda_mean * m + lambda_freq * f for m, f in zip(mean_score, freq_score)]

    best_labels = [unique_object_classes[idx] for idx in np.argsort(class_weight)[-1:-3:-1]]

    return best_labels


def n_output_features_conv(n_in, k, p, s):
    """ Computes number of output features for a convolutional filter given its
        input features, kernel size, padding, and stride
        Parameters:
            n_in (int): # input features
            k (int): convolutional kernel size (assumed square)
            p (int): image padding (assumed to be same on all sides)
            s (int): convolutional stride (assumed to be same in x- and y-directions)
        Returns:
            (int): # output features
    """
    return np.floor((n_in + 2 * p - k) / s) + 1


def execute_vimtmp(vimtmp, **kwargs):
    """ Executes code stored in vimtmp - get it with `!cat /tmp/vimtmp`
    """
    import ipdb
    ipdb.set_trace()
    context = "".join(["{}={};".format(key, value) for key, value in kwargs.items()])
    command = "".join([line.strip() for line in vimtmp])
    exec(context + command)


class Cache(object):
    """ Simple caching mechanism
    """
    def __init__(self, dpath='~/.cache/actions', verbosity=1):
        """
            Example:
                >>> self = Cache()
                >>> os.path.exists(self.dpath)
                True
        """
        self.verbosity = verbosity

        # Expand variables and store this path
        self.dpath = os.path.realpath(
            os.path.expanduser(
                os.path.expandvars(dpath)))
        if not os.path.exists(self.dpath):
            os.makedirs(self.dpath)
            if self.verbosity > 0:
                print("[cache] Init at {} - created".format(self.dpath))
        elif self.verbosity > 0:
            print("[cache] Init at {}".format(self.dpath))

    def exists(self, fname=''):
        """
            Example:
                >>> self = Cache()
                >>> self.exists()
                True
                >>> self.exists('TEST_FILE_THAT_SHOULD_NOT_EXIST')
                False
        """
        found = os.path.exists(self.fpath(fname))
        if self.verbosity > 0:
            if found:
                print("[cache] {} found".format(fname))
            else:
                print("[cache] {} NOT found".format(fname))
        return found

    def fpath(self, fname):
        """
            Example:
                >>> self = Cache()
                >>> dpath, fname = os.path.split(self.fpath('test'))
                >>> assert dpath == self.dpath
                >>> assert fname == 'test'
        """
        return os.path.join(self.dpath, fname)

    def pickle(self, item, fname):
        """
            Example:
                >>> self = Cache()
                >>> test_dict = {'a':-1, 'b':0, 'c':1}
                >>> self.pickle(test_dict, 'test_dict.pkl')
                >>> os.path.exists(self.fpath('test_dict.pkl'))
                True
        """
        # If cache path is given for some reason:
        if os.path.split(fname)[0] == self.dpath:
            fname = os.path.split(fname)[1]

        if self.verbosity > 0:
            print("[cache] pickling {} . . . ".format(fname), end='')

        with open(self.fpath(fname), 'wb') as fid:
            pickle.dump(item, fid)

        print("DONE")

    def save(self, item, fname):
        """ A somewhat more flexible saving function that support torch writing
            for faster tensor support
        """
        # If cache path is given for some reason:
        if os.path.split(fname)[0] == self.dpath:
            fname = os.path.split(fname)[1]

        if fname.endswith('.t7'):
            torch.save(item, self.fpath(fname))
            if self.verbosity > 0:
                print("DONE")

        elif fname.endswith('.pkl'):
            self.pickle(item, fname)

        elif fname.endswith('.txt'):
            self.write_str(item, fname)

        else:
            print("unrecognized filetype {}".format(os.path.splitext(fname)[-1]))

    def load(self, fname):
        # If cache path is given for some reason:
        if os.path.split(fname)[0] == self.dpath:
            fname = os.path.split(fname)[1]

        if not self.exists(fname):
            item = None
        else:
            if fname.endswith('.t7'):
                item = torch.load(self.fpath(fname))
                if self.verbosity > 0:
                    print("DONE")
            elif fname.endswith('.pkl'):
                item = self.unpickle(fname)
            else:
                print("unrecognized filetype {}".format(os.path.splitext(fname)[-1]))

        return item

    def unpickle(self, fname):
        """
            Example:
                >>> self = Cache()
                >>> test_dict = {'a':-1, 'b':0, 'c':1}
                >>> self.pickle(test_dict, 'test_dict.pkl')
                >>> test_dict_unpickled = self.unpickle('test_dict.pkl')
                >>> assert len(test_dict) == len(test_dict_unpickled)
                >>> assert all([v1 == v2
                ...            for v1, v2 in zip(
                ...                test_dict.values(),
                ...                test_dict_unpickled.values())])
        """
        # If cache path is given for some reason:
        if os.path.split(fname)[0] == self.dpath:
            fname = os.path.split(fname)[1]

        if not self.exists(fname):
            item = None
        else:
            if self.verbosity > 0:
                print("[cache] unpickling {} . . . ".format(fname), end='')

            with open(self.fpath(fname), 'rb') as fid:
                item = pickle.load(fid)

            if self.verbosity > 0:
                print("DONE")

        return item

    def write_str(self, data, fname):
        # If cache path is given for some reason:
        if os.path.split(fname)[0] == self.dpath:
            fname = os.path.split(fname)[1]
        if self.verbosity > 0:
            print("[cache] writing {}".format(fname))
        with open(self.fpath(fname), 'w') as fid:
            fid.write(str(data))

    def cp(self, fpath_src):
        """ Copies file to cache
        """
        if self.verbosity > 0:
            print("[cache] copying {} to {}".format(fpath_src, self.dpath))
        shutil.copy(fpath_src, self.dpath)

    def ls(self):
        """ List contents in cache directory
            Example:
                >>> self = Cache()
                >>> self.pickle({'a':1}, 'test_dict.pkl')
                >>> assert 'test_dict.pkl' in len(self.ls())
        """
        return os.listdir(self.dpath)


def TODO(msg="Implement this!"):
    raise NotImplementedError(msg)


def path(fpath):
    """ Shorthand for expanding common symbols in path names
        Example:
            >>> home = '$HOME/something/else'
            >>> tilde = '~/something/else'
            >>> assert path(tilde) == path(home)
    """
    return os.path.expanduser(os.path.expandvars(fpath))


def pathjoin(*args):
    """ shorthard for os.path.join(*args). Also fewer imports in other files
        Example:
            >>> import os
            >>> dirs = ['some', 'thing', 'I', 'made', 'up.txt']
            >>> assert pathjoin(*dirs) == os.path.join(*dirs)
    """
    return path(os.path.sep.join(args))


def new_ext(fname, new_ext):
    """ Replaces extension with new one
        Parameters:
            - fname (str): original filename or path
            - new_ext (str): new extension (with or without dot)
        Returns:
            - (str): fname, with extension replaced with new_ext
        Example:
            >>> fname = '~/data/something.boring/blah/whate.ver'
            >>> new_ext(fname, 'ven_are_you_doing')
            '~/data/something.boring/blah/whate.ven_are_you_doing'
            >>> new_ext(fname, '.ven_are_you_doing')
            '~/data/something.boring/blah/whate.ven_are_you_doing'
    """
    if new_ext.startswith('.'):
        new_ext = new_ext[1:]
    return os.path.splitext(fname)[0] + '.' + new_ext


def to_onehot(n, length):
    """ Quick scalar-to-onehot encoding
    """
    onehot = np.zeros(length)
    onehot[n] = 1
    return onehot


def extract_frames(video_file, num_frames, cache_dir=None):
    """Return a list of PIL image frames uniformly sampled from an mp4 video."""
    frames_temp_dir = pathjoin(os.getcwd(), 'frames') if cache_dir is None else pathjoin(cache_dir, 'frames')
    try:
        os.makedirs(frames_temp_dir)
    except OSError:
        pass
    output = subprocess.Popen(['ffmpeg', '-i', video_file],
                              stderr=subprocess.PIPE).communicate()
    # Search and parse 'Duration: 00:05:24.13,' from ffmpeg stderr.
    re_duration = re.compile(r'Duration: (.*?)\.')
    duration = re_duration.search(str(output[1])).groups()[0]

    seconds = functools.reduce(lambda x, y: x * 60 + y,
                               map(int, duration.split(':')))
    rate = num_frames / float(seconds)

    output = subprocess.Popen(['ffmpeg', '-i', video_file,
                               '-vf', 'fps={}'.format(rate),
                               '-vframes', str(num_frames),
                               '-loglevel', 'panic',
                               frames_temp_dir + '/%d.jpg']).communicate()
    frame_paths = sorted(
        [
            pathjoin(frames_temp_dir, frame)
            for frame in os.listdir(frames_temp_dir)
        ],
        key=lambda x: int(os.path.splitext(os.path.split(x)[-1])[0])
    )
    frames = load_frames(frame_paths, num_frames=min(num_frames, len(frame_paths)))
    subprocess.call(['rm', '-rf', frames_temp_dir])
    return frames


def load_frames(frame_paths, num_frames=8):
    """Load PIL images from a list of file paths."""
    frames = [Image.open(frame).convert('RGB') for frame in frame_paths]
    if len(frames) >= num_frames:
        return frames[::int(np.ceil(len(frames) / float(num_frames)))]
    else:
        raise ValueError('Video must have at least {} frames'.format(num_frames))


def crypthash(obj):
    hashed_string = hashlib.md5(json.dumps(obj).encode('utf-8')).hexdigest()
    return hashed_string


def set_he_normal(linear_layer, fan_in):
    torch.nn.init.uniform_(linear_layer.bias, 0, 0)
    torch.nn.init.normal_(linear_layer.weight, mean=0, std=np.sqrt(2 / fan_in))
    return linear_layer


def distinct_colors(n):
    colors = [
        (230, 25, 75),
        (60, 180, 75),
        (255, 225, 25),
        (0, 130, 200),
        (245, 130, 48),
        (145, 30, 180),
        (70, 240, 240),
        (240, 50, 230),
        (210, 245, 60),
        (250, 190, 190),
        (0, 128, 128),
        (230, 190, 255),
        (170, 110, 40),
        (255, 250, 200),
        (128, 0, 0),
        (170, 255, 195),
        (128, 128, 0),
        (255, 215, 180),
        (0, 0, 128),
        (85, 85, 85),
        (170, 170, 170),
        (255, 255, 255),
    ]
    return colors[:n]


def deprecated(f):
    """ Allows function decoration to raise a warning when called
        Raises a UserWarning instead of DeprecationWarning so that it's
        detectable in an IPython session.
    """
    def deprec_warn():
        deprecation_msg = '{} is deprecated - consider replacing it'.format(f.__name__)
        warn(deprecation_msg)
        f()
    return deprec_warn


# Taken from attributes_as_operators/data/dataset.py
def imagenet_transform(phase):
    mean, std = [0.485, 0.456, 0.406], [0.229, 0.224, 0.225]

    if phase == 'train':
        transform = transforms.Compose([
            transforms.RandomResizedCrop(224),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean, std)
        ])
    elif phase == 'test':
        transform = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize(mean, std)
        ])

    return transform


def ls_r(directory):
    """ Recursive list directory
    """
    all_files = []
    for root, _, fns in os.walk(directory):
        all_files.extend([os.path.join(root, fn) for fn in fns])
    return all_files


if __name__ == "__main__":
    import doctest
    doctest.testmod()
