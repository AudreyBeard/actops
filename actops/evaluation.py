# evaluation.py
# For all your evaluation needs!

import torch
import argparse
import numpy as np
from matplotlib import pyplot as plt

import utils


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--load',
                        default=None,
                        help='Model or directory containing models')
    parser.add_argument('--dataset',
                        default='coco-actions-svo',
                        choices=['coco-actions-svo', 'coco-actions-sv', 'moments-svo'],
                        help='name of dataset')

    return parser.parse_args()


def show_verb_embeddings(dset,
                         title='Validation Data in t-SNE Space\n(Colored by Actions)',
                         verbs=None,
                         color_indices=None):
    def plot_data_and_verb(fig, all_x, all_y, verb_x, verb_y, n_rows, n_columns,
                           index, title, label, verb_color,
                           data_color=np.array([[0.75, 0.75, 0.75, 0.75]]),
                           marker='.'):

        # Add subplot to figure
        ax = fig.add_subplot(
            n_rows,
            n_columns,
            index,
            title=title,
            frame_on=True,
            xticks=[],
            yticks=[]
        )

        # Add all data and specfic verbs
        ax.scatter(all_x, all_y, c=data_color, marker=marker, label=label)
        ax.scatter(verb_x, verb_y, c=verb_color, marker=marker, label=label)

        return ax

    # This makes it faster and eliminates errors when converting to numpy in mpl
    with torch.no_grad():

        # Sort verbs for consistent plotting and get indices for quick filtering later
        if verbs is None:
            verbs = sorted({svo[1] for svo in dset.svo_test})
        else:
            verbs = sorted(verbs)
        verb_svo_lookup = {k: [svo for svo in dset.svo_test if svo[1] == k] for k in verbs}

        # Grab some distinct colors
        if color_indices is None:
            colors = utils.distinct_colors(len(verbs))
        else:
            # If distinct color indices are specified, grab those
            colors = utils.distinct_colors(max(color_indices) + 1)
            colors = [colors[i] for i in color_indices]

        # Grab dataset indices of each verb
        verb_indices = {
            verb: torch.LongTensor([
                i
                for i in dset.idx_lookup
                if dset.data['verb'][i] == verb
            ])
            for verb in verbs
        }
        verb_indices = {k: v for k, v in verb_indices.items() if len(v) > 0}

        # Indices of validation data
        only_val = torch.LongTensor(dset.idx_lookup)

        # Arrange the subplots so there are no more than four columns
        n_verbs = len(verb_indices)
        n_rows = 1
        n_cols = int(np.ceil(n_verbs / n_rows))
        while n_cols > 4:
            n_rows += 1
            n_cols = int(np.ceil(n_verbs / n_rows))

        fig, axs = plt.subplots(n_rows, n_cols, subplot_kw=dict(frame_on=False, xticks=[], yticks=[]))
        fig.suptitle(title, fontsize=20)

        for verb_index, (verb, data_indices) in enumerate(verb_indices.items()):
            ax_row = verb_index // n_cols
            ax_col = verb_index % n_cols

            # Subplot title
            this_verb_svo = '\n'.join([' '.join(svo) for svo in verb_svo_lookup[verb]])
            this_title = '{}\n{}'.format(verb, this_verb_svo)

            # Plot the data on the figure
            ax = plot_data_and_verb(
                fig=fig,
                all_x=dset.embeddings[only_val, 0],
                all_y=dset.embeddings[only_val, 1],
                verb_x=dset.embeddings[data_indices, 0],
                verb_y=dset.embeddings[data_indices, 1],
                n_rows=n_rows, n_columns=n_cols, index=verb_index + 1,
                title=this_title, label=verb,
                verb_color=np.array([colors[verb_index]]) / 255
            )

            # Keep track of Axes object (idk if this is necessary)
            if isinstance(axs, np.ndarray):
                axs[ax_row, ax_col] = ax
            else:
                axs = ax

        fig.set_size_inches(w=12 * n_cols,
                            h=12 * n_rows)

        save_name = '_'.join(title.split()) + '.png'
        print('saving {}'.format(save_name))
        fig.savefig(save_name)
        plt.close('all')


if __name__ == "__main__":
    args = parse_args()
