import argparse
import re

import torch
import numpy as np
import ipdb  # NOQA
from PIL import Image
import matplotlib.pyplot as plt

from actops import utils
from actops.dataproc.data_moments import MomentsInTimeActOpsDatasetActivations
from actops.dataproc.data_cocoactions import (
    COCOActionsCompositionDatasetActivationsEmbeddings,
    COCOActionsSVODatasetActivationsEmbeddings
)

from actops.external.attributes_as_operators.models.models import AttributeOperator


cache = utils.Cache('~/data/Moments_in_Time_256x256_30fps')
demo_cache = utils.Cache('~/.cache/tsne_demos')
action_mapping = {
    v: k
    for k, action_list in utils.MIT_SUPERCLASSES.items()
    for v in action_list
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--in_file',
                        default='mit_actops_embeddings_tsne.t7',
                        help='input file, in .t7 format')
    # Demo parameters
    parser.add_argument('--n_walk_directions', type=int, default=1, help='Number of directions to walk, spaced evenly')

    parser.add_argument('--dataset', default='moments', help='moments')
    parser.add_argument('--data_dir', default='$HOME/data/Moments_in_Time_256x256_30fps', help='data root dir')
    parser.add_argument('--cv_dir', default='$HOME/.cache/mit_actops', help='dir to save checkpoints to')
    parser.add_argument('--load', default=None, help='path to checkpoint to load from')
    parser.add_argument('--height', default=256, help='video height')
    parser.add_argument('--width', default=256, help='video width')
    parser.add_argument('--start_verb', default=None, help='verb from which to start from')
    parser.add_argument('--start_subject', default=None, help='subject from which to start from')

# model parameters
    parser.add_argument('--model', default='attributeop', help='visprodNN|redwine|labelembed+|attributeop')
    parser.add_argument('--start_vid', default=None, help='video to start demo seeding')
    parser.add_argument('--output_name', default='demo_video.mp4', help='name of output video')
    parser.add_argument('--emb_dim', type=int, default=300, help='dimension of common embedding space')
    parser.add_argument('--n_videos', type=int, default=20, help='number of videos to use in demo')
    parser.add_argument('--n_iterations', type=int, default=5, help='Number of demos to make')
    parser.add_argument('--n_frames_per_video', type=int, default=30, help='number of frames to use per video in demo')
    parser.add_argument('--nlayers', type=int, default=2, help='number of layers for labelembed+')
    parser.add_argument('--glove_init', action='store_true', default=False, help='initialize inputs with word vectors')
    parser.add_argument('--clf_init', action='store_true', default=False, help='initialize inputs with SVM weights')
    parser.add_argument('--static_inp', action='store_true', default=False, help='do not optimize input representations')

    args = parser.parse_args()
    args.n_walk_directions = min(26, args.n_walk_directions)
    return args
    args = parser.parse_args()
    return args


def get_next_point(curr_index, walk_dir, data, closest=True):
    def exclude_index(tensor, index):
        return torch.cat((tensor[:index], tensor[index + 1:]))

    curr_index = int(curr_index)

    d1 = torch.norm(data - (data[curr_index] + walk_dir), dim=1)

    if closest:
        dist = exclude_index(d1, curr_index)
        metric, next_index = dist.min(0)
    else:
        d0 = torch.norm(data - data[curr_index], dim=1)
        dist_diff = exclude_index(d1 - d0, curr_index)
        metric, next_index = dist_diff.max(0)

    next_index += 1 if next_index >= curr_index else 0
    return next_index, metric


def get_next_low_high_range(walk_dir):
    margin = np.pi / 8
    if walk_dir[0, 0] == 0:
        walk_dir_rad = np.pi / 2 if walk_dir[0, 1] > 0 else 3 / 2 * np.pi
    else:
        walk_dir_rad = torch.atan(walk_dir[0, 1] / walk_dir[0, 0])

        # Correct to within (0, 2pi)
        if walk_dir[0, 0] < 0:
            walk_dir_rad += np.pi
        elif walk_dir[0, 1] < 0:
            walk_dir_rad += 2 * np.pi

        walk_dir_rad += (2 * np.pi) if walk_dir_rad < 0 else 0
    low_rad = walk_dir_rad + (np.pi / 2 - margin)
    high_rad = walk_dir_rad + (np.pi / 2 + margin)
    if low_rad < 0:
        low_rad += (2 * np.pi)
        high_rad += (2 * np.pi)
    elif high_rad >= 2 * np.pi:
        low_rad -= (2 * np.pi)
        high_rad -= (2 * np.pi)

    return low_rad, high_rad


def random_direction(dim=2, low=None, high=None):
    if low is not None and high is not None:
        x = None
        y = None
        if low >= 0 and high < np.pi:
            y = torch.rand(1)
        elif low >= np.pi:
            y = torch.rand(1) - 1
        elif low >= 0 and low < np.pi / 2 or low >= 3 / 2 * np.pi and low < 2 * np.pi:
            x = torch.rand(1)
        else:
            x = torch.rand(1) - 1

        if x is None:
            x = y / torch.tan(torch.rand(1) * (high - low) + low)
        else:
            y = x * torch.tan(torch.rand(1) * (high - low) + low)

        walk_dir = torch.cat((x, y))
    else:
        walk_dir = torch.rand(dim) * 2 - 1

    walk_dir /= torch.norm(walk_dir)
    return walk_dir


def walk_around(args, data=None):
    n_videos_per_walk = int(np.ceil(args.n_videos / args.n_walk_directions))

    if data is None:
        in_file = cache.fpath(args.in_file)
        data = torch.load(in_file)

    #data_radius = (data.max(0)[0] - data.min(0)[0]).mean() / 2
    #max_walk_distance = data_radius / args.n_walk_directions

    video_indices = torch.zeros(args.n_videos).long()
    video_metrics = torch.zeros(args.n_videos)
    walk_indices = torch.zeros(args.n_videos).long()

    # Start with point closest to the mean
    _, curr_idx = (torch.norm(data - data.mean(dim=0), dim=1)).min(0)
    video_indices[0] = curr_idx
    walk_dir = None

    for i in range(0, args.n_videos):
        if i % n_videos_per_walk == 0:
            if i > 0:
                low_rad, high_rad = get_next_low_high_range(walk_dir)
                walk_dir = random_direction(data.shape[1], low=low_rad, high=high_rad)

                distance_to_edge = (data - data[curr_idx]).mm(walk_dir.unsqueeze(1)).max()
                walk_dir = walk_dir.unsqueeze(0) * distance_to_edge / n_videos_per_walk * 0.8
                walk_indices[i] = walk_indices[i - 1] + 1
            else:
                walk_dir = random_direction(data.shape[1])
                distance_to_edge = (data - data[curr_idx]).mm(walk_dir.unsqueeze(1)).max()
                walk_dir = walk_dir.unsqueeze(0) * distance_to_edge / n_videos_per_walk * 0.8

        if i > 0:
            curr_idx, curr_metric = get_next_point(curr_idx, walk_dir, data)
            video_indices[i], video_metrics[i] = (curr_idx, curr_metric)

    return data, video_indices, video_metrics, walk_indices


def create_video(args, dset, video_indices, video_metrics, walk_indices, output_name=None):
    def get_fp(i):
        if args.dataset == 'moments':
            fp = utils.pathjoin(
                dset.dpath,
                'validation',
                dset.data['fname'][i]
            )
        elif args.dataset == 'cocoactions':
            fp = dset.data['fpath'][i]
        elif args.dataset == 'coco-actions-svo':
            fp = utils.pathjoin(
                dset.dpath,
                'coco_attr',
                dset.data['coco_split'][i],
                dset.data['fname'][i],
            )
        return fp

    demo_cache = utils.Cache('~/.cache/tsne_demos')
    video = []

    for i_vid, i_walk, metric in zip(video_indices, walk_indices, video_metrics):
        i_dset = dset.idx_lookup[i_vid]
        fpath = get_fp(i_dset)

        if args.dataset == 'moments':
            frames = utils.extract_frames(
                fpath,
                args.n_frames_per_video,
                demo_cache.dpath)
        else:
            frames = [
                Image.open(fpath).resize((args.height, args.width))
                for i in range(args.n_frames_per_video)
            ]

        for frame in frames:
            annot_frame = utils.AnnotatedImage(pil_image=frame)
            if args.dataset == 'coco-actions-svo':
                text = "{} {} {}\nDIR: {}\nDIST: {}".format(
                    dset.data['subject'][i_dset],
                    dset.data['verb'][i_dset],
                    dset.data['object'][i_dset],
                    i_walk,
                    metric
                )
            else:
                text = "{} {}\nDIR: {}\nDIST: {}".format(
                    dset.data['subject'][i_dset],
                    dset.data['verb'][i_dset],
                    i_walk,
                    metric
                )
            annot_frame.draw_texts([(0, 0)], [text])
            video.append(annot_frame)
    if output_name is None:
        output_name = args.output_name.strip('.mp4')
        output_name += '_t-SNE'
        output_name += '_n-videos={}'.format(args.n_videos)
        output_name += '_n-walks={}'.format(args.n_walk_directions)
        output_name += '_0'
        output_name += '.mp4'

        while cache.exists(output_name):
            last_token = (output_name.split['.mp4'][0]).split('_')[-1]
            output_name = '_'.join((output_name.split['.mp4'][0]).split('_')[:-1]) + '_' + str(int(last_token) + 1)
    print("[create_video] Writing {}".format(output_name))
    dims = (args.height, args.width)
    utils.write_mp4(video, output_name, args.n_frames_per_video // 3, dims=dims)
    return output_name


def create_nice_names(args, cache):
    output_name = args.output_name if args.output_name is not None else 'walk'
    output_name += '_t-SNE'
    output_name += '_n-videos={}'.format(args.n_videos)
    output_name += '_n-walks={}'.format(args.n_walk_directions)
    output_name += '_0'
    while cache.exists(output_name + '.mp4'):
        last_token = output_name.split('_')[-1]
        output_name = '_'.join(output_name.split('_')[:-1]) + '_' + str(int(last_token) + 1)
    video_name = cache.fpath(output_name + '.mp4')
    plot_name = cache.fpath(output_name + '.png')
    return video_name, plot_name


def show_walk(data, video_indices, walk_indices, title='Walk in t-SNE space'):
    reduction = 1

    walk_points = (data[video_indices]).numpy()
    data = data.numpy()

    fig, ax = plt.subplots()
    ax.scatter(data[::reduction, 0], data[::reduction, 1], marker='.')
    ax.plot(walk_points[:, 0], walk_points[:, 1], '-ok')
    ax.set_title(title)

    save_name = '_'.join(title.split())
    if not save_name.endswith('.png'):
        save_name += '.png'
    fig.set_size_inches(w=16, h=16)
    fig.savefig(save_name)
    plt.close('all')


def load_checkpoint(dset, args):
    def parse_lambda_value(checkpoint_path, lambda_name):
        regex = r'{}=[0-9]+\.[0-9]+'.format(lambda_name)
        re_search = re.compile(regex).search(checkpoint_path)
        value = float(re_search.group().split('=')[-1]) if re_search is not None else 0.0
        return value

    fpath = utils.path(args.load)
    args.lambda_aux = parse_lambda_value(fpath, 'aux')
    args.lambda_comm = parse_lambda_value(fpath, 'comm')
    args.lambda_inv = parse_lambda_value(fpath, 'inv')
    args.lambda_ant = 0.0

    model = AttributeOperator(dset, args)
    model.cuda()
    checkpoint = torch.load(fpath)
    model.load_state_dict(checkpoint['net'])
    return model


def embed_dset(dset, args):
    import os
    model = load_checkpoint(dset, args)
    embeddings_fname = 'embeddings_' + os.path.split(args.load)[-1]
    dset.embed(model, embeddings_fname=embeddings_fname)


def show_verb_embeddings(dset, args, title=None):
    #verbs = dset.attrs if 'SVO' not in dset.__class__.__name__ else dset.verbs
    verbs = sorted({svo[1] for svo in dset.svo_test})
    verb_svo_lookup = {k: [svo for svo in dset.svo_test if svo[1] == k] for k in verbs}
    colors = utils.distinct_colors(len(verbs))

    verb_indices = {
        verb: torch.LongTensor([
            i
            for i in dset.idx_lookup
            if dset.data['verb'][i] == verb
        ])
        for verb in verbs
    }
    verb_indices = {k: v for k, v in verb_indices.items() if len(v) > 0}
    n_verbs = len(verb_indices)
    n_rows = 1
    n_cols = int(np.ceil(n_verbs / n_rows))
    while n_cols > 4:
        n_rows += 1
        n_cols = int(np.ceil(n_verbs / n_rows))

    fig, axs = plt.subplots(n_rows, n_cols, subplot_kw=dict(frame_on=False, xticks=[], yticks=[]))
    only_val = torch.LongTensor(dset.idx_lookup)

    if title is None:
        title = 'Validation Data in t-SNE Space\n(Colored by Actions)'
    fig.suptitle(title, fontsize=20)
    #axes = [None for i in range(n_verbs)]
    ipdb.set_trace()
    for verb_index, (verb, data_indices) in enumerate(verb_indices.items()):
        ax_row = verb_index // n_cols
        ax_col = verb_index % n_cols
        this_verb_svo = '\n'.join([' '.join(svo) for svo in verb_svo_lookup[verb]])
        this_title = '{}\n{}'.format(verb, this_verb_svo)
        axs[ax_row][ax_col] = fig.add_subplot(n_rows, n_cols, verb_index + 1, title=this_title, frame_on=True, xticks=[], yticks=[])
        axs[ax_row][ax_col].scatter(
            dset.embeddings[only_val, 0],
            dset.embeddings[only_val, 1],
            c=np.array([[.75, .75, .75, .75]]),
            marker='.',
            label=verb,
        )
        axs[ax_row][ax_col].scatter(
            dset.embeddings[data_indices, 0],
            dset.embeddings[data_indices, 1],
            c=np.array([colors[verb_index]]) / 255,
            marker='.',
            label=verb,
        )
    save_name = '_'.join(title.split())
    if not save_name.endswith('.png'):
        save_name += '.png'
    fig.set_size_inches(w=6 * n_cols, h=6 * n_rows)
    print('saving {}'.format(save_name))
    fig.savefig(save_name)
    plt.close('all')


def show_embeddings(dset, title='Validation Data in t-SNE Space'):
    import plottingtools as pt
    plot = pt.Scatter(data, title=title)
    plot.save()
    pt.close_all()


def show_subject_embeddings_for_verb(dset, args, verb, title=None):
    idx_lookup_subset = [i for i in dset.idx_lookup if dset.data['verb'][i] == verb]
    subjects_for_verb = sorted({dset.data['subject'][i] for i in idx_lookup_subset})

    subject_indices = {
        subject: torch.LongTensor([
            i
            for i in idx_lookup_subset
            if dset.data['subject'][i] == subject
        ])
        for subject in subjects_for_verb
    }

    n_subjects = len(subjects_for_verb)
    colors = utils.distinct_colors(n_subjects)
    n_rows = 1
    n_cols = int(np.ceil(n_subjects / n_rows))
    while n_cols > 4:
        n_rows += 1

    fig, axs = plt.subplots(n_rows, n_cols)
    only_verb = torch.LongTensor(idx_lookup_subset)

    if title is None:
        title = "Subject Distribution for '{}' in t-SNE Space".format(verb)
    fig.suptitle(title, fontsize=20)
    axes = [None for i in range(n_subjects)]
    for subject_index, (subject, data_indices) in enumerate(subject_indices.items()):
        axes[subject_index] = fig.add_subplot(n_rows, n_cols, subject_index + 1, title=subject)
        axes[subject_index].scatter(
            dset.embeddings[only_verb, 0],
            dset.embeddings[only_verb, 1],
            c=np.array([[.75, .75, .75, .75]]),
            marker='.',
            label=subject,
        )
        axes[subject_index].scatter(
            dset.embeddings[data_indices, 0],
            dset.embeddings[data_indices, 1],
            c=np.array([colors[subject_index]]) / 255,
            marker='.',
            label=subject,
        )
    save_name = '_'.join(title.split())
    if not save_name.endswith('.png'):
        save_name += '.png'
    fig.set_size_inches(w=6 * n_cols, h=6 * n_rows)
    print('saving {}'.format(save_name))
    fig.savefig(save_name)
    plt.close('all')


if __name__ == "__main__":
    args = parse_args()
    if args.dataset == 'moments':
        dset = MomentsInTimeActOpsDatasetActivations(
            dpath=args.data_dir,
            split='validation',
            transforms=utils.imagenet_transform('test'),
            n_frames=None,
            action_mapping=action_mapping,
            keep_only_superclasses=True,
            glove_init=args.glove_init,
        )
        data = None
    elif args.dataset == 'cocoactions':
        dset = COCOActionsCompositionDatasetActivationsEmbeddings(
            embeddings_fname=args.in_file,
            split='test'
        )
        data = dset.data.embeddings[torch.LongTensor(dset.idx_lookup)]
    elif args.dataset == 'coco-actions-svo':
        dset = COCOActionsSVODatasetActivationsEmbeddings(
            embeddings_fname=args.in_file,
            split='test'
        )
        data = dset.data['embeddings'][torch.LongTensor(dset.idx_lookup)]

    for i in range(args.n_iterations):
        data, video_indices, video_metrics, walk_indices = walk_around(args, data=data)
        video_name, plot_name = create_nice_names(args, demo_cache)
        create_video(
            args,
            dset,
            video_indices,
            video_metrics,
            walk_indices,
            output_name=video_name
        )
        show_walk(data, video_indices, walk_indices, title=plot_name)
