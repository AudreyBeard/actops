import argparse
import os
import sys
import subprocess

import torch

import actops

# TODO necessary?
TNSE_SCRIPT_LOCATION = os.path.join(actops.__path__[0], "external", "bhtsne")
if not os.path.exists(TNSE_SCRIPT_LOCATION):
    print("First clone bhtsne from {} and follow the setup instructions".format(
        "git@github.com:lvdmaaten/bhtsne.git"
    ))
    sys.exit(1)
sys.path.append(TNSE_SCRIPT_LOCATION)


def run_bhtsne(in_file, outfile, in_dim=300, out_dim=2):
    print("Running t-SNE now. Come back later...")
    subprocess.Popen(
        [
            'python',
            os.path.join(TNSE_SCRIPT_LOCATION, 'bhtsne.py'),
            '-d', '2',
            '-n', '300',
            '-i', in_file,
            '-o', out_file
        ],
        stderr=subprocess.PIPE
    ).communicate()
    print("I'm back!")


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--in_file', default=None,
        help='input file path, either a .t7 (torch.Tensor encoding) or'
             ' a .tsv (tab-separated variable)'
    )
    parser.add_argument(
        '--out_file', default=None, help='output file path (optional)'
    )
    return parser.parse_args()


def tensor_to_tsv(in_file):
    data = torch.load(in_file)
    text = '\n'
    text += '\n'.join(['\t'.join(['{}'.format(val) for val in d]) for d in data])

    in_file = os.path.splitext(in_file)[0] + '.tsv'
    with open(in_file, 'w') as fid:
        fid.write(text)

    return in_file


def tsv_to_tensor(in_file):
    with open(in_file, 'r') as fid:
        lines = fid.readlines()

    data = map(lambda x: x.strip().split(), lines)
    data = list(map(lambda x: (float(x[0]), float(x[1])), data))
    data = torch.tensor(data)
    in_file = os.path.splitext(in_file)[0] + '.t7'
    torch.save(data, in_file)
    return in_file


if __name__ == "__main__":
    args = parse_args()
    assert args.in_file is not None

    out_file = args.out_file
    if out_file is None:
        out_file = '_tsne'.join(os.path.splitext(args.in_file))
        print("out_file not specified - saving to {}".format(out_file))

    in_file_split = os.path.splitext(args.in_file)

    import ipdb
    with ipdb.launch_ipdb_on_exception():
        # If it's a tensor, make an intermediate file first
        if in_file_split[-1] == '.t7':
            print("in_file is a saved tensor - converting to tsv for t-SNE embedding")
            in_file = tensor_to_tsv(args.in_file)
        else:
            in_file = args.in_file

        run_bhtsne(in_file, out_file)

        # Turn it into a tensor for later
        print("Converting t-SNE output to tensor now")
        out_tensor_file = tsv_to_tensor(out_file)
