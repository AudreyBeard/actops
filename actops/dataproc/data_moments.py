import re

import torch
from torch import nn
from torch.nn.functional import cosine_similarity
import torchvision as tv
import torchvision.models as tmodels
import tqdm
import ipdb
import numpy as np
from sklearn.decomposition import PCA

from actops import utils

DEFAULT_DPATH = utils.path("$HOME/data")

__all__ = [
    "MomentsInTimeDataset",
    "MomentsInTimeActOpsDataset",
    "MomentsInTimeActOpsDatasetActivations",
    "MomentsInTimeActOpsDatasetActivationsEmbeddings",
]


class MomentsInTimeDataset(torch.utils.data.Dataset):
    def __init__(self, dpath=None, split='train', transforms=None,
                 n_frames=12, max_returned=None, subset_on_superclasses=True):
        import os

        # Get the correct dpath and ground truth filename
        self.split = 'train' if split.lower().startswith('train') else 'val'
        partition = 'training' if split.lower().startswith('train') else 'validation'
        self.dpath = os.path.join(dpath, partition)
        gt_fn = os.path.join(dpath, '{}Set.csv'.format(partition))

        # Information contained in ground truth file, from README.txt
        keys = ['filename', 'label', 'n_agree', 'n_disagree']

        # Dictionary of lists is slightly bigger memory footprint in exchange
        # for clarity and speed on load time
        with open(gt_fn, 'r') as fid:
            lines = fid.readlines()
        self._gt = {k: v
                    for k, v in zip(
                        keys,
                        zip(*map(lambda x: x.split(','), lines))
                    )}
        self._gt['filename'] = [os.path.join(self.dpath, fn)
                                for fn in self._gt['filename']]

        # Number of annotators who agree and disagree with label
        self._gt['n_agree'] = np.array(self._gt['n_agree']).astype(int)
        self._gt['n_disagree'] = np.array(self._gt['n_disagree']).astype(int)

        # Lookup table for labels to one-hot vector
        with open(utils.pathjoin(dpath, 'moments_categories.txt'), 'r') as fid:
            lines = fid.readlines()

        all_labels = list(map(
            lambda x: x.split(',')[0],
            lines
        ))
        n_labels = len(all_labels)
        self.one_hot = {k: utils.to_onehot(i, length=n_labels) for i, k in enumerate(all_labels)}

        self.transforms = transforms if transforms else lambda x: x
        self.n_frames = n_frames
        self.max_returned = max_returned
        self._subset = None
        self._unique_actions = None

        if subset_on_superclasses:
            self._superclass_lookup = {
                v: k
                for k, action_list in utils.MIT_SUPERCLASSES.items()
                for v in action_list
            }
            self.subset({act for act in self._superclass_lookup.values()})
            with ipdb.launch_ipdb_on_exception():
                # replace actions with superclasses if they're there
                labels = [
                    self._superclass_lookup.get(action, action)
                    for action in self._gt['label']
                ]
                self._gt['label'] = labels

        return

    def __len__(self):
        length = len(self._gt['filename'])

        if self.max_returned:
            length = min(length, self.max_returned)

        if self._subset is not None:
            length = min(length, len(self._subset))

        return length

    def __getitem__(self, index):
        with ipdb.launch_ipdb_on_exception():
            item = {k: self.getindex(self._gt[k], index) for k in self._gt.keys()}
            item['target'] = self.one_hot[item['label']]

            # C x F x H x W
            frames = utils.extract_frames(item['filename'], num_frames=self.n_frames)
            item['data'] = torch.stack([self.transforms(f) for f in frames], 1)
        return item

    @property
    def actions(self):
        if self._unique_actions is None:
            self._unique_actions = {
                self.getindex(self._gt['label'], i)
                for i in range(len(self))
            }
        return self._unique_actions

    @actions.setter
    def actions(self, val):
        self._unique_actions = val

    def __repr__(self):
        return "{} with {} videos".format(self.__class__.__name__, len(self))

    def getindex(self, prop, index):
        if self._subset is None:
            return prop[index]
        else:
            return prop[self._subset[index]]

    def subset(self, actions):
        """ Creates internally-used subset of indices to use from dataset
            Parameters:
                - actions (str) or other (Iterable): action(s) to subset on
        """
        # Single action given
        if isinstance(actions, str):
            self._subset = np.where(np.array(self._gt['label']) == actions)[0]

        # Multiple actions given
        else:
            self._subset = np.concatenate(
                [np.where(np.array(self._gt['label']) == action)[0]
                 for action in actions]
            )

            # Sort indices for consistency
            self._subset.sort()

        self._unique_actions = None
        return len(self._subset)

    def unsubset(self):
        self._subset = None


class MomentsInTimeActOpsDataset(MomentsInTimeDataset):
    def __init__(self, dpath=None, split='train', transforms=None,
                 n_frames=12, max_returned=None,
                 cache_dir='~/.cache/mit_actops', action_mapping=None,
                 keep_only_superclasses=False, glove_init=True):
        """
            Parameters:
                dpath (str): path to dataset root directory
                split (str): 'train'/'training' or 'val'/'validation'
                transforms: all transforms that should be applied when
                    __getitem__ is called - If not given, defaults to
                    ToTensor()
                n_frames (int): number of frames to extract from each video
                    when __getitem__ is called
                max_returned (int): max dset size, useful for quick demos
                cache_dir (str): location for storing intermediate files
                action_mapping (dict): mapping all allowable classes to their
                    superclass. If not given, no superclassing is done
        """

        train_ratio = 0.8

        split_options = {'train': 'training', 'val': 'validation'}
        self.split = split_options.get(split, split)

        self.cache = utils.Cache(cache_dir)
        self.data_cache = utils.Cache(dpath)
        self.dpath = self.data_cache.dpath

        self.idx_lookup = None
        self.n_frames = n_frames
        self.transforms = transforms if transforms is not None else tv.transforms.ToTensor()

        # TODO necessary?
        self.action_mapping = action_mapping
        self.max_returned = max_returned

        self._load_data(
            glove_init=glove_init,
            action_mapping=action_mapping,
            train_ratio=train_ratio
        )

        # TODO necessary?
        self.actions = self.unique_verbs
        self.objects = self.unique_subjects

        # AttrOps compliance
        self.attrs = self.unique_verbs
        if glove_init:
            self.objs = self.unique_subjects
        else:
            self.objs = [obj.split('_')[0] for obj in self.unique_subjects if obj is not None]

        self.pairs = self.unique_pairs
        self.attr2idx = {v: i for i, v in enumerate(sorted(self.unique_verbs))}
        self.obj2idx = {s: i for i, s in enumerate(sorted(self.unique_subjects))}
        self.pair2idx = {p: i for i, p in enumerate(sorted(self.unique_pairs))}
        self.features = None
        self.feat_dim = None
        self.obj_affordance = self.cache.unpickle('mit_actops_obj_affordance.pkl')
        self.train_obj_affordance = self.cache.unpickle('mit_actops_train_obj_affordance.pkl')

        if self.obj_affordance is None or self.train_obj_affordance is None:
            self.obj_affordance = {}
            self.train_obj_affordance = {}
            for sub in self.unique_subjects:
                candidates = [
                    verb
                    for (verb, subject) in zip(
                        self.data['verb'],
                        self.data['subject']
                    )
                    if subject == sub
                ]
                self.obj_affordance[sub] = list(set(candidates))

                candidates = [
                    verb
                    for (verb, subject, split) in zip(
                        self.data['verb'],
                        self.data['subject'],
                        self.data['split'],
                    )
                    if subject == sub and split == 'training'
                ]
                self.train_obj_affordance[sub] = list(set(candidates))
            self.cache.pickle(self.obj_affordance, 'mit_actops_obj_affordance.pkl')
            self.cache.pickle(self.train_obj_affordance, 'mit_actops_train_obj_affordance.pkl')
        return

    def sample_negative(self, verb, subject, i=0):
        new_verb, new_subject = list(self.train_pairs)[np.random.choice(len(self.train_pairs))]
        if new_verb == verb and new_subject == subject and i < 10:
            return self.sample_negative(verb, subject, i + 1)
        else:
            return (self.attr2idx[new_verb], self.obj2idx[new_subject])

    def sample_train_affordance(self, verb, subject):
        max_count = 10
        count = 0
        new_verb = verb
        while new_verb == verb and count < max_count:
            count += 1
            new_verb = np.random.choice(self.train_obj_affordance[subject])
        return self.attr2idx[new_verb]

    def sample_affordance(self, verb, subject, i=0):
        new_verb = np.random.choice(self.obj_affordance[subject])
        if new_verb == verb and i < 10:
            return self.sample_affordance(verb, subject, i + 1)
        return self.attr2idx[new_verb]

    def _load_data(self, glove_init=True, action_mapping=None, train_ratio=0.8):
        cache_fname = 'mit-actops-data'
        cache_fname += '_train-ratio={}'.format(train_ratio)
        if glove_init:
            cache_fname += '_glove-init'

        if action_mapping is not None:
            hashable = [sorted(action_mapping)]
            hashable.append([action_mapping[k] for k in hashable[0]])
            cache_fname += '_action-mapping-hash={}'.format(utils.crypthash(hashable))
        cache_fname += '.pkl'

        self.data = self.cache.unpickle(cache_fname)
        if self.data is None:
            data = {'fname': [], 'verb': [], 'subject': [], 'split': []}
            fname_to_subject = dict()

            partitions = ('training', 'validation')
            for partition in partitions:

                # Grab the MiT data
                mit_fname = self.data_cache.fpath("{}Set.csv".format(partition))
                with open(mit_fname, 'r') as fid:
                    lines = fid.readlines()

                fname, verb = zip(*map(lambda x: x.split(',')[:2], lines))
                if self.action_mapping is not None:
                    verb = [self.action_mapping.get(v, v) for v in verb]

                data['fname'].extend(fname)
                data['verb'].extend(verb)
                # Split given by dataset
                # TODO necessary?
                data['split'].extend([partition for _ in fname])

                subject_fname = self.data_cache.fpath('subject_annotations_{}.csv'.format(partition))
                with open(subject_fname, 'r') as fid:
                    lines = fid.readlines()

                # Nasty parsing for grabbing the subjects
                lines = {
                    fn: subs.strip().split('___')[0].strip('[').strip(']').strip("'")
                    for fn, subs in map(
                        lambda x: re.sub("\', \'", "\'___\'", x).split(','),
                        lines
                    )
                }

                # If using pretrained embeddings, convert to real words
                if glove_init:
                    lines = {
                        fn: x.split('_')[0]
                        for fn, x in lines.items()
                    }
                else:
                    lines = {
                        fn: None if x == 'None' else x
                        for fn, x in lines.items()
                    }

                fname_to_subject.update(lines)

            # Annotate subject
            data['subject'] = [
                fname_to_subject.get(fname)
                for i, fname in enumerate(data['fname'])
            ]

            # If action mapping is specified, keep only those
            if action_mapping is not None:
                keep = [action_mapping.get(v) is not None for v in data['verb']]
            else:
                keep = [True for v in self.data['verb']]

            # If keeping only superclasses, this filters them. OTW it's a very inefficient noop
            data = {
                key: [v for i, v in enumerate(values) if keep[i]]
                for key, values in data.items()
            }

            self.data = data
            self._split_data_actops(train_ratio=train_ratio)
            self.cache.pickle(self.data, cache_fname)

        self.unique_pairs = {
            (v, s) for v, s in zip(
                self.data['verb'],
                self.data['subject']
            )
        }
        self.unique_verbs, self.unique_subjects = [set(x) for x in zip(*self.unique_pairs)]

        self.unique_pairs = sorted(self.unique_pairs)
        self.unique_verbs = sorted(self.unique_verbs)
        self.unique_subjects = sorted(self.unique_subjects)

        self.idx_lookup = [
            i
            for i, split in enumerate(self.data['split'])
            if split == self.split
        ]

        self.test_pairs = {
            (v, s)
            for v, s, split in zip(
                self.data['verb'],
                self.data['subject'],
                self.data['split'],
            )
            if split == 'validation'
        }
        self.train_pairs = {
            (v, s)
            for v, s, split in zip(
                self.data['verb'],
                self.data['subject'],
                self.data['split'],
            )
            if split == 'training'
        }
        assert len(self.train_pairs.intersection(self.test_pairs)) == 0

        return

    def _split_data_actops(self, train_ratio=0.8):
        unique_pairs = {
            (v, s) for v, s in zip(
                self.data['verb'],
                self.data['subject']
            )
        }
        unique_pair_counts = {pair: 0 for pair in unique_pairs}
        for v, s in zip(self.data['verb'], self.data['subject']):
            unique_pair_counts[(v, s)] += 1

        unique_pairs = sorted(unique_pairs, key=lambda x: unique_pair_counts[x])

        train_pairs = []
        test_pairs = []
        train_count = 1
        test_count = 1
        ratio = train_ratio / (1 - train_ratio)

        for pair in unique_pairs:
            if train_count / test_count <= ratio:
                train_pairs.append(pair)
                train_count += unique_pair_counts[pair]
            else:
                test_pairs.append(pair)
                test_count += unique_pair_counts[pair]

        self.data['split'] = [
            'training' if (v, s) in train_pairs else 'validation'
            for v, s in zip(
                self.data['verb'],
                self.data['subject']
            )
        ]
        self.test_pairs = set(test_pairs)
        self.train_pairs = set(train_pairs)

        with ipdb.launch_ipdb_on_exception():
            assert len(self.train_pairs.intersection(self.test_pairs)) == 0

    def __len__(self, split=True):
        if split:
            length = len(self.idx_lookup)
        else:
            length = len(self.data['fname'])

        if self.max_returned is not None:
            length = min(length, self.max_returned)

        return length

    def __getitem__(self, index, split=True):
        index = self.idx_lookup[index] if split else index
        fpath = utils.pathjoin(self.dpath, self.data['split'][index], self.data['fname'][index])

        # F x C x H x W
        with ipdb.launch_ipdb_on_exception():
            frames = torch.stack([
                self.transforms(f)
                for f in utils.extract_frames(
                    fpath,
                    num_frames=self.n_frames,
                    cache_dir=self.cache.dpath
                )
            ], 0)

        verb = self.data['verb'][index]
        subject = self.data['subject'][index]
        retval = [
            frames,
            self.attr2idx[verb],
            self.obj2idx[subject],
            self.pair2idx[(verb, subject)],
        ]
        if self.split == 'training':
            neg_verb, neg_sub = self.sample_negative(verb, subject)
            inv_verb = self.sample_train_affordance(verb, subject)
            comm_verb = self.sample_affordance(inv_verb, subject)
            retval += [neg_verb, neg_sub, inv_verb, comm_verb]

        return retval

    def ensure_attrops_compliance(self):
        def load_or_compute_and_pickle_attr_obj_pairs(self, split=None):

            fname = 'mit_actops_pairs.pkl'
            if split is not None:
                fname = "_{split}_".join(fname, split('_'))
            #else:
            #    fnames = [f"_{s}_".join(fname.split('_')) for s in ('train', 'val')]

            pairs = self.cache.unpickle(fname)
            if pairs is None:
                pairs = {
                    (v, s) for v, s in zip(
                        self.data['verb'],
                        self.data['subject']
                    )
                }
                self.cache.pickle(pairs, fname)

            return pairs

        self.attr2idx = {k: i for i, k in enumerate(self.unique_verbs)}
        self.obj2idx = {k: i for i, k in enumerate(self.unique_subjects)}
        self.pair2idx = {k: i for i, k in enumerate(self.unique_pairs)}
        # TODO the problem i sprobably somwherer herer!

        self.pairs = load_or_compute_and_pickle_attr_obj_pairs()
        self.train_pairs = load_or_compute_and_pickle_attr_obj_pairs('train')
        self.test_pairs = load_or_compute_and_pickle_attr_obj_pairs('val')

        self.attrs = self.unique_verbs
        self.objs = self.unique_subjects
        self.pairs = self.unique_pairs
        self.feat_dim = None

        with ipdb.launch_ipdb_on_exception():
            assert len(self.train_pairs.intersection(self.test_pairs)) == 0

        self.obj_affordance = self.cache.unpickle('actops_obj_affordance.pkl')
        self.train_obj_affordance = self.cache.unpickle('actops_train_obj_affordance.pkl')

        if self.obj_affordance is None or self.train_obj_affordance is None:
            self.obj_affordance = {}
            self.train_obj_affordance = {}
            for subject_i in self.unique_subjects:
                candidates = [
                    verb
                    for (verb, subject_j, split) in zip(
                        self.data['verb'],
                        self.data['subject'],
                        self.data['split'],
                    )
                    if subject_j == subject_i and (split == 'training' or split == 'validation')
                ]
                self.obj_affordance[subject_i] = list(set(candidates))

                candidates = [
                    verb
                    for (verb, subject_j, split) in zip(
                        self.data['verb'],
                        self.data['subject'],
                        self.data['split'],
                    )
                    if subject_j == subject_i and split == 'training'
                ]
                self.train_obj_affordance[subject_i] = list(set(candidates))

            self.cache.pickle(
                self.obj_affordance,
                'actops_obj_affordance.pkl'
            )
            self.cache.pickle(
                self.train_obj_affordance,
                'actops_train_obj_affordance.pkl'
            )

    def batch_data(self, batch_size=512, split=False):
        """ Generator for batching out data
        """
        def indices(b_idx):
            if split:
                return (self.idx_lookup[j] for j in range(b_idx, b_idx + batch_size))
            else:
                return range(b_idx, min(b_idx + batch_size, len(self, split)))

        for i in range(0, len(self, split), batch_size):
            yield {k: [v[j] for j in indices(i)] for k, v in self.data.items()}

    def batch_getitem(self, batch_size=512, split=False):
        """ Generator for batching out data
        """
        def indices(b_idx):
            if split:
                return (self.idx_lookup[j] for j in range(b_idx, b_idx + batch_size))
            else:
                return range(b_idx, min(b_idx + batch_size, self.__len__(split)))

        for i in range(0, self.__len__(split), batch_size):
            #yield {k: [v[j] for j in indices(i)] for k, v in self.data.items()}
            yield [self.__getitem__(j, split=split) for j in indices(i)]


class MomentsInTimeActOpsDatasetActivations(MomentsInTimeActOpsDataset):
    def __init__(self, dpath=None, split='train', transforms=None,
                 n_frames=12, max_returned=None, glove_init=True,
                 cache_dir='~/.cache/mit_actops', action_mapping=None,
                 keep_only_superclasses=False, feats_fname='mit-features.t7'):
        super().__init__(dpath=dpath, split=split, transforms=transforms,
                         n_frames=n_frames, max_returned=max_returned,
                         cache_dir=cache_dir, action_mapping=action_mapping,
                         keep_only_superclasses=keep_only_superclasses,
                         glove_init=glove_init)

        if self.data_cache.exists(feats_fname):
            self.features = torch.load(self.data_cache.fpath(feats_fname))
        else:
            print("Generating features now...")
            with torch.no_grad():
                self.features = self.generate_features()
            torch.save(self.features, self.data_cache.fpath(feats_fname))
            print("{} Features generated".format(self.features.shape[0]),
                  "(n_dim = {}".format(self.features.shape[1]))
            print("Saved at {}".format(self.data_cache.fpath(feats_fname)))

        self.feat_dim = self.features.shape[-1]

        with ipdb.launch_ipdb_on_exception():
            assert len(self.train_pairs.intersection(self.test_pairs)) == 0

    def get_video_fpath(self, index, split=True):
        index = self.idx_lookup[index] if split else index
        return utils.pathjoin(self.dpath,
                              self.data['split'][index],
                              self.data['fname'][index])

    def generate_features(self, pool_method='mean'):
        old_transform = self.transforms
        self.transforms = utils.imagenet_transform('test')
        feature_extractor = tmodels.resnet18(pretrained=True)
        feature_extractor.fc = nn.Sequential()
        feature_extractor.eval().cuda()

        if pool_method == 'mean':
            pool_feats = lambda x: x.mean(0)  # NOQA
        elif pool_method == 'max':
            pool_feats = lambda x: x.max(0)[0]  # NOQA

        feats = []
        batch_size = 512
        for batch in tqdm.tqdm(self.batch_getitem(batch_size, False), total=self.__len__(False) // batch_size):
            videos = torch.cat([b[0].unsqueeze(0) for b in batch])
            videos = videos.reshape(*([-1] + list(videos.shape[-3:])))
            vid_feats = feature_extractor(videos.cuda()).cpu()
            vid_feats = vid_feats.reshape(batch_size, -1, vid_feats.shape[-1])
            vid_feats = vid_feats.mean(1) if pool_method == 'mean' else vid_feats.max(1)[0]
            feats.append(vid_feats)

        feats = torch.cat(feats)
        self.transforms = old_transform
        return feats

    def __getitem__(self, index, split=True):
        index = self.idx_lookup[index] if split else index
        verb = self.data['verb'][index]
        subject = self.data['subject'][index]
        data = [
            self.features[index],
            self.attr2idx[verb],
            self.obj2idx[subject],
            self.pair2idx[(verb, subject)],
        ]
        if self.split == 'training':
            neg_verb, neg_sub = self.sample_negative(verb, subject)
            inv_verb = self.sample_train_affordance(verb, subject)
            comm_verb = self.sample_affordance(inv_verb, subject)
            data += [neg_verb, neg_sub, inv_verb, comm_verb]

        return data


class MomentsInTimeActOpsDatasetActivationsEmbeddings(MomentsInTimeActOpsDatasetActivations):
    def __init__(self, dpath=None, split='train', transforms=None,
                 max_returned=None, cache_dir='~/.cache/mit_actops',
                 action_mapping=None, keep_only_superclasses=True,
                 embeddings_fname=None,
                 n_frames=None, glove_init=True):
        # TODO implement embeddings loading
        super().__init__(dpath=dpath, split=split, transforms=transforms,
                         n_frames=None, max_returned=max_returned,
                         cache_dir=cache_dir, action_mapping=action_mapping,
                         keep_only_superclasses=keep_only_superclasses,
                         glove_init=True)
        self._embeddings_all = None
        self._embeddings_split = None
        self.embeddings_fname = embeddings_fname
        self.model = None

    def pca(self, n_components=0.95):
        self._pca = PCA(n_components=n_components)
        self.embeddings_pca = torch.from_numpy(self._pca.fit_transform(self.embeddings))

    @property
    def embeddings(self):
        if self._embeddings_all is None:
            if self.data_cache.exists(self.embeddings_fname):
                self._embeddings_all = torch.load(self.data_cache.fpath('mit_actops_embeddings.t7'))
            else:
                print("Generating embeddings now...")
                self._embeddings_all = self.generate_embeddings()
                torch.save(self._embeddings_all, self.data_cache.fpath('mit_actops_embeddings.t7'))
                print("Done")
        if self._embeddings_split is None:
            self._embeddings_split = torch.cat([
                self._embeddings_all[i]
                for i, split in enumerate(self.data['split'])
                if split == self.split
            ])

        return self._embeddings_split

    def __getitem__(self, index, split=True):
        index = self.idx_lookup[index] if split else index
        verb = self.data['verb'][index]
        subject = self.data['subject'][index]
        data = [
            self.embeddings[index],
            self.attr2idx[verb],
            self.obj2idx[subject],
            self.pair2idx[(verb, subject)],
        ]
        if self.split == 'training':
            neg_verb, neg_sub = self.sample_negative(verb, subject)
            inv_verb = self.sample_train_affordance(verb, subject)
            comm_verb = self.sample_affordance(inv_verb, subject)
            data += [neg_verb, neg_sub, inv_verb, comm_verb]
        return data

    def get_pair(self, index):
        return self.unique_pairs[index]

    def iter_pairs(self):
        for p in self.unique_pairs.values():
            yield p

    def generate_embeddings(self, model=None):
        self.model = model if model is not None else self.model
        assert self.model is not None

        with torch.no_grad():
            batch_size = 512
            self.model.eval().cuda()
            embeddings = []
            n_batches = np.ceil(self.__len__(False) / batch_size).astype(np.uint)
            for i in tqdm.tqdm(range(0, n_batches), total=n_batches):
                indices = [i * batch_size, min((i + 1) * batch_size, self.__len__(False))]
                batch = self.features[indices[0]:indices[1]]
                embeddings.append(self.model.image_embedder(batch.cuda()).cpu())
            embeddings = torch.cat(embeddings)
        return embeddings

    def get_index_from_fname(self, fname, split=False):
        """ Gets the index of a filename. If split is True, then it returns the
            apparent index for this dataset's split. If not, returns the true
            index in the dataset
        """
        index = [i for i, fn in self.data['fname'] if fn == fname][0]
        if split:
            index = [idx for idx, lookup in enumerate(self.idx_lookup) if lookup == index]
            index = index[0] if len(index) > 0 else None
        return index

    def closest_index(self, point, index_to_exclude, use_cosine=False):
        """ Uses a combination of cosine similarity and euclidean distance to
            return the closest point to a "walked" point. The closest point is
            determined to be the one for which the euclidean distance
            multiplied by the inverse-cosine-similarity, is minimized.
        """
        def exclude_index(tensor, index):
            """ Exclude current index for mins and maxes
            """
            return torch.cat((tensor[:index], tensor[index + 1:]))

        if use_cosine:
            point_ = torch.ones_like(self.embeddings) * point

            # Compute inverse cosine similarity and euclidean distance to walked point
            cosine_sim = 1 - cosine_similarity(point_, self.embeddings)
            distance = torch.norm(self.embeddings - point, dim=1)
            cosine_sim = exclude_index(cosine_sim, index_to_exclude)
            distance = exclude_index(distance, index_to_exclude)

            # best point is one with high cosine similarity and low distance
            metric, closest = (cosine_sim * distance).min(0)
        else:
            # Compute distance to all other points before and after walk
            d0 = torch.norm(self.embeddings - self.embeddings[index_to_exclude], dim=1)
            d1 = torch.norm(self.embeddings - point, dim=1)

            # Best point is one which moved closest, indicating it is closest
            # to being on walking path
            dist_diff = exclude_index(d1 - d0, index_to_exclude)
            metric, closest = dist_diff.max(0)

        # Correct for exclusion of original point
        closest += 1 if closest <= index_to_exclude.long() else 0
        return closest, metric

    def get_random_video(self, verb=None, subject=None, split=False):
        """ Given an optional verb and optional subject, find a random video
            with those. If split is True, only return those
        """
        if split:
            choices = [i for i in range(self.__len__(False))]
        else:
            choices = [i for i in self.idx_lookup]

        if verb is not None:
            choices = [
                i for i in choices
                if self.data['verb'][i] == verb
            ]
        if subject is not None:
            choices = [
                i for i in choices
                if self.data['subject'][i] == subject
            ]
        return np.random.choice(choices) if len(choices) > 0 else None
