from os import path

import torch
from torchvision.models.detection import fasterrcnn_resnet50_fpn
from torchvision import transforms as tforms
from torchvision.transforms import functional
from PIL import Image
import pandas as pd
from tqdm import tqdm
import ipdb
from numpy import ceil

from actops import utils

DEFAULT_DPATH = utils.path("~/data")
DEFAULT_TRANSFORMS = tforms.Compose([
    tforms.ToTensor(),
])


DEBUG = False


def collate_fn(sample_list):
    collated = {
        key: sample[key]
        for sample in sample_list
        for key in set(sample_list[0].keys()) - {'image'}
    }
    collated.update({'image': torch.stack([sample['image'] for sample in sample_list])})
    return collated


class COCOActionsDataset(torch.utils.data.Dataset):
    def __init__(self, dpath=DEFAULT_DPATH, split='train',
                 transforms=DEFAULT_TRANSFORMS, df_fname='coco-actions-df.pkl',
                 reset=False, image_size=256):

        self.dpath = dpath
        self.df_fpath = path.join(dpath, df_fname)
        self.df_fpath_mod = "{0[0]}_predicted{0[1]}".format(path.splitext(self.df_fpath))

        if not reset and path.exists(self.df_fpath_mod):
            print(f"Found modified DataFrame for use at:\n  {self.df_fpath_mod}")
            self.df = pd.read_pickle(path.join(dpath, self.df_fpath_mod))
        else:
            self.df = pd.read_pickle(self.df_fpath)

        # Construct lookup table for sequential index to DataFrame Index
        # Slower on the frontend, but fast indexing and relatively low memory
        # footprint
        self.idx_lookup = {
            i: k
            for i, k in enumerate(self.df[self.df['split'] == split + '2014'].index)
        }

        #self.pre_transforms = lambda x: x
        # Pre-transforms resize the image a letterbox it so that it's a YxY square padded with black
        self.pre_transforms = ResizedLetterBox(image_size)
        self.transforms = transforms

    def __len__(self):
        """ Needed for DataLoader
            Only counts those of the specified split for proper use in the
            DataLoader
        """
        return len(self.idx_lookup)

    def __getitem__(self, idx):
        """ For DataLoader iterator
            Uses previously-built mapping from sequential index to DataFrame
            Index
        """
        entry = self.df.loc[self.idx_lookup[idx]]
        fpath = path.join(self.dpath,
                          'coco_attr',
                          entry['split'],
                          entry['image']['file_name'])

        img = Image.open(fpath)
        img = img.convert('RGB') if img.mode != 'RGB' else img
        img = self.transforms(self.pre_transforms(img))

        retval = {
            k: entry[k] for k in entry.keys()
        }
        retval.update({'image': img})
        return retval

    def update_df(self, idx, column, value, batch_size=None):
        """ Update internal dataframe (at proper index and column) with value
        """
        # If batch_size is given, it's implied that we're using this at scale
        if batch_size is not None:
            indices = [
                self.idx_lookup[i]
                for i in range(
                    idx * batch_size,
                    min(
                        (idx + 1) * batch_size,
                        len(self.idx_lookup)
                    )
                )
            ]
        else:
            indices = self.idx_lookup[idx]

        self.df.loc[indices, column] = value
        return

    def save_df(self, fpath=None):
        """ Save internal DataFrame as a copy of the original (unless original
            was already modified, which will cause overwrite)
        """
        fpath = self.df_fpath_mod if fpath is None else fpath

        self.df.to_pickle(fpath)


class ResizedLetterBox(object):
    """ Performs a resize on the image and a letterbox paddinmg on an image,
        assuming the images are returned as CHW
    """
    def __init__(self, size):
        self.size = size

    def __call__(self, image):
        
        h, w = image.size

        min_side_len = min(h, w)
        max_side_len = max(h, w)
        new_min_side_len = int(ceil(min_side_len / max_side_len * self.size))
        resized_image = functional.resize(image, new_min_side_len)

        # Create a black canvas a paste resized image into it at center
        letterboxed = Image.new(mode=image.mode, size=(self.size, self.size))
        letterboxed.paste(resized_image,
                          box=[(self.size - dim) // 2 for dim in resized_image.size[::-1]])

        return letterboxed


def find_objects(split, n_workers, batch_size, image_size=256):
    save_fpath = 'test_predicted_objects_df.pkl' if DEBUG else None

    dset = COCOActionsDataset(split=split)
    loader = torch.utils.data.DataLoader(dset,
                                         num_workers=n_workers,
                                         batch_size=batch_size,
                                         drop_last=False,
                                         collate_fn=collate_fn,
                                         shuffle=False)

    device = torch.device(0)
    model = fasterrcnn_resnet50_fpn(pretrained=True, min_size=min(image_size, 800))
    model.eval()
    model = model.cuda(device)
    t = tqdm(total=int(ceil(len(dset) / batch_size)))
    for i, data in enumerate(loader):
        t.update(1)
        with ipdb.launch_ipdb_on_exception():
            with torch.no_grad():
                images = data['image'].to(device)
                outputs = model(images)
                outputs = [{k: v.cpu() for k, v in output.items()} for output in outputs]
                dset.update_df(i, 'predictions', outputs, batch_size)

    dset.save_df(fpath=save_fpath)
    t.close()


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("nprocs", type=int, default=1,
                        help='number of workers for loading and processing data'
                        )
    parser.add_argument("batch_size", type=int, default=1024,
                        help='number of workers for loading and processing data'
                        )
    args = parser.parse_args()

    for split in ('train', 'test'):
        find_objects(split, args.nprocs, args.batch_size)
