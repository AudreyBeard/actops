import os

import pandas as pd
from matplotlib import pyplot as plt

import utils

partition = 'validation'
df_fname = os.path.join(os.path.expandvars(utils.DATA_DIR),
                        "detection_df_{}.pkl".format(partition))

df = pd.read_pickle(df_fname)

unique_actions = df['action'].unique()
unique_objects = df['object'].unique()
object_mapping = {k: i for i, k in enumerate(unique_objects)}
action_mapping = {k: i for i, k in enumerate(unique_actions)}
df['object_numeric'] = [object_mapping[obj] for obj in df['object']]
df['action_numeric'] = [action_mapping[act] for act in df['action']]

fig = df.plot.hist().get_figure()
fig.savefig("test.png")

# Let's see what objects are present for each action
for i, act_series in df.groupby('action_numeric'):
    action = unique_actions[i]
    fig = act_series.hist(
        column='object_numeric',
        grid=False,
        bins=range(len(unique_objects) + 1),
        bottom=0.5,
        xrot=45,
        figsize=(12,8),
    )[0, 0].get_figure()
    plt.title("Object Distribution in '{}'\n({} samples)".format(action.title(), len(act_series)))
    plt.xticks(ticks=[0.5 + i for i in range(len(unique_objects))],
               labels=["\n".join(o.split("_")) if o is not None else "None" for o in unique_objects])

    fig.savefig("hist_objects_of_{}.png".format(action))
    plt.close(fig)
