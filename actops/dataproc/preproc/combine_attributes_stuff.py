import os
import json

import pickle
import pandas as pd
from tqdm import tqdm
import ipdb


data_dir = os.path.expandvars('$HOME/data')
df_fpath = os.path.join(data_dir, 'coco-actions-df.pkl')


def is_action(attribute_name):
    return attribute_name.endswith('ing') and attribute_name != 'appetizing'


def build_dataframe(data_dir):
    coco_attr_fpath = os.path.join(
        data_dir,
        'coco_attr/cocottributes_eccv_version.pkl'
    )
    with open(coco_attr_fpath, 'rb') as fid:
        attr_dset = pickle.load(fid, encoding='latin1')

    attr_index_action_map = {
        i: entry['name']
        for i, entry in enumerate(sorted(attr_dset['attributes'], key=lambda x: x['id']))
        if is_action(entry['name'])
    }

    # Turn this to a set for quick intersection-checking later
    action_indices = set(action_index_map.keys())
    # IDs of COCO-Attribute samples that have objects
    coco_attr_ids__action = []

    # Actions that may be present in each sample, and their confidence
    sample_actions = []
    action_confidence = []
    sample_coco_indices = []
    coco_annot_ids__action = []
    coco_attr_dset_ids = []
    coco_attr_split = []

    # For each annotation in COCO Attributes, find 
    for i, (coco_attr_id, attr_vec) in enumerate(attr_dset['ann_vecs'].items()):
        # Get attribute indices that are likely to be in scene, based on MTurk
        # annotators for COCO-Attributes
        pos_attr_indices = np.where(attr_vec > 0.5)
    
        # Get attribute indices in this sample that are actions
        possible_actions = action_indices.intersection(pos_attr_indices[0])
        #attributes_present = [action_index_map.get(i) for i in pos_attr_indices[0]] 
        #actions_present = list(filter(lambda x: x is not None, attributes_present))
        
        if len(possible_actions) > 0:
    
            # For all actions, get their names and hang on to them
            sample_actions.append([
                action_index_map[a]
                for a in possible_actions
            ])
            coco_attr_ids__action.append(coco_attr_id)
            action_confidence.append(
                attr_vec[(np.array(list(possible_actions)).astype(np.uint), )]
            )
            #coco_annot_ids__action.append(attr_dset['patch_id_to_ann_id'][i])
            coco_annot_ids__action.append(attr_dset['patch_id_to_ann_id'][coco_attr_id])
            coco_attr_split.append(attr_dset['split'][coco_attr_id])
    
    # Construct dataset with actions present
    df = pd.DataFrame(index=range(len(sample_actions)))
    
    # COCO-Attributes ID
    df['coco_attr_id'] = coco_attr_ids__action
    
    # COCO annotation ID
    df['coco_annot_id'] = coco_annot_ids__action
    
    # Actions and confidence (from COCO Attributes)
    df['actions'] = sample_actions
    df['confidence'] = action_confidence
    df['split'] = coco_attr_split
    
    
    coco_id_annot_map__action = dict()
    coco_id_image_map__action = dict()
    coco_id_split_map = dict()
    for split in ['train', 'val']:
    
        # Grab COCO annotations
        coco_dset_fpath = os.path.join(
            data_dir,
            'annotations/instances_{}2014.json'.format(split)
        )
        with open(coco_dset_fpath, 'r') as fid:
            coco_dset = json.load(fid)
    
        # Lookup from COCO image ID to image
        coco_id_image_map = {
            img_entry['id']: img_entry
            for img_entry in coco_dset['images']
        }
    
        # Lookup from COCO annotation ID to annotation
        coco_id_annot_map = {
            annot_entry['id']: annot_entry
            for annot_entry in coco_dset['annotations']
        }
    
        # Lookup from COCO annotation ID to image
        coco_annot_image_map = {
            annot_id: coco_id_image_map[annot['image_id']]
            for annot_id, annot in coco_id_annot_map.items()
        }
    
        # For all COCO annotation IDs with actions, grab the annotation
        coco_id_annot_map__action_split = {
            key: coco_id_annot_map[key]
            for key in df['coco_annot_id']
            if coco_id_annot_map.get(key)
        }
    
        # For all COCO annotation IDs with actions, grab the image
        coco_annot_image_map__action_split = {
            key: coco_annot_image_map[key]
            for key in df['coco_annot_id']
            if coco_annot_image_map.get(key)
        }
    
        # Record the split each key comes from
        #ipdb.set_trace()
        coco_id_split_map.update({
            coco_annot_id: split
            for coco_annot_id in coco_annot_image_map__action_split.keys()
        })
        coco_id_annot_map__action.update(coco_id_annot_map__action_split)
        coco_id_image_map__action.update(coco_annot_image_map__action_split)
    
    df['coco_annot'] = [
        coco_id_annot_map__action.get(coco_annot_id)
        for coco_annot_id in df['coco_annot_id']
    ]
    df['image'] = [
        coco_id_image_map__action.get(coco_annot_id)
        for coco_annot_id in df['coco_annot_id']
    ]
    
    # Get COCO category
    coco_cats = {cat['id']: cat['name'] for cat in coco_dset['categories']}
    with ipdb.launch_ipdb_on_exception():
        df['subject'] = [coco_cats[annot['category_id']] for annot in df['coco_annot']]
    return df

    image_id_stuff_names_bboxes_map = extract_image_stuff_info(df)

    df['stuff_names'] = [image_id_stuff_names_bboxes_map[img['id']][0] for img in df['image']]
    df['stuff_bboxes'] = [image_id_stuff_names_bboxes_map[img['id']][1] for img in df['image']]

    return df


def load_stuff_dset(split, data_dir=data_dir):
    fpath = os.path.join(data_dir, 'coco_stuff/stuff_{}2017.json'.format(split))
    with open(fpath, 'r') as fid:
        stuff_dset = json.load(fid)

    return stuff_dset

def extract_image_stuff_info(df):
    splits = ['train', 'val']
    image_stuff_name_box_map = merge_dicts(*({
        split: get_stuff_names_bboxes(load_stuff_dset(split), df)
        for split in splits
    }.values()))
    return image_stuff_name_box_map


def get_stuff_names_bboxes(stuff_dset, df):
    # Unique image IDs
    print("getting unique image ids", flush=True)
    image_ids = {annot['image_id'] for annot in tqdm(stuff_dset['annotations'])}

    print("making image-annotation map", flush=True)
    # Dictionary mapping image ID to annotation IDs
    stuff_image_annots_map = {image_id: [] for image_id in image_ids}
    [
        stuff_image_annots_map[annot['image_id']].append(annot['id'])
        for annot in tqdm(stuff_dset['annotations'])
    ]

    print("making category id-name map", flush=True)
    stuff_cat_map = {cat['id']: cat['name'] for cat in tqdm(stuff_dset['categories'])}
    print("making id-annotation map", flush=True)
    stuff_id_annot_map = {annot['id']: annot for annot in tqdm(stuff_dset['annotations'])}
    print("getting annotations of each image in df", flush=True)
    stuff_annots = [stuff_image_annots_map.get(img['id']) for img in tqdm(df['image'])]

    print("creating category name list for each annotation", flush=True)

    bboxes_list = list(map(
        lambda x: 
            [
                stuff_id_annot_map.get(annot_id)['bbox']
                for annot_id in x
            ] if x is not None else None,
        tqdm(stuff_annots)
    ))

    names_list =  list(map(
        (lambda x:
            stuff_categories_to_names(
                x,
                stuff_cat_map,
                stuff_id_annot_map)),
        tqdm(stuff_annots)
    ))


    print("creating image id-stuff names map")
    image_stuff_name_bbox_map = {
        img['id']: (names, bboxes)
        if names is not None else None
        for img, names, bboxes in tqdm(zip(df['image'], names_list, bboxes_list))}

    return image_stuff_name_bbox_map


def merge_dicts(dict_a, dict_b):
    """ Merge dictionaries with identical keys - if conflicting entries and one
        is None, overwrite it
    """
    new_dict = {
        key_a: val_a
        if val_a is not None else dict_b.get(key_a)
        for key_a, val_a in dict_a.items()
    }
    return new_dict


def stuff_categories_to_names(cat_list, stuff_cat_map, stuff_id_annot_map):
    """ Converts list COCO-Stuff categories to their names
        Parameters:
            cat_list (list of ints): category labels
            stuff_cat_map (dict): mapping from category ID to name
            stuff_id_annot_map (dict): mapping from annotation ID to annotation
        Returns (list of strs) or (None)
    """
    if cat_list is not None:
        rc = [stuff_cat_map[stuff_id_annot_map[annot]['category_id']] for annot in cat_list]
    else:
        rc = None
    return rc


if __name__ == "__main__":
    df = build_dataframe(data_dir)
    df.to_pickle(df_fpath)

    stuff_categories = [
        cat['name']
        for cat in sorted(stuff_dset['categories'], key=lambda x: x['id'])
    ]

    # Unique image IDs
    image_ids = {annot['image_id'] for annot in stuff_dset['annotations']}

    stuff_image_annots_map = {
        img_id: [
            annot['id']
            for annot in stuff_dset['annotations']
            if annot['image_id'] == img_id
        ]
        for img_id in image_ids
    }
    #stuff_annot_image_map = {
    #    annot['id']: annot['image_id']
    #    for annot in stuff_dset['annotations']
    #}
