import os
import json

import torch
from tqdm import tqdm
import numpy as np
import ipdb

from actops.utils import (
    BBox,
    Cache
)

DATA_DIR = os.path.expandvars('$HOME/data/coco-actions')
COCO_INSTANCE_CATEGORY_NAMES = [
    '__background__', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
    'bus', 'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'N/A',
    'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse',
    'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'N/A', 'backpack',
    'umbrella', 'N/A', 'N/A', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis',
    'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
    'skateboard', 'surfboard', 'tennis racket', 'bottle', 'N/A', 'wine glass',
    'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich',
    'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake',
    'chair', 'couch', 'potted plant', 'bed', 'N/A', 'dining table', 'N/A',
    'N/A', 'toilet', 'N/A', 'tv', 'laptop', 'mouse', 'remote', 'keyboard',
    'cell phone', 'microwave', 'oven', 'toaster', 'sink', 'refrigerator',
    'N/A', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier',
    'toothbrush'
]
COCO_SUPER_CATEGORY_NAMES = [
    '_', 'person_accessory', 'vehicle', 'vehicle', 'vehicle', 'vehicle',
    'vehicle', 'vehicle', 'vehicle', 'vehicle', 'outdoor_obj', 'outdoor_obj',
    '_', 'outdoor_obj', 'outdoor_obj', 'outdoor_obj', 'animal', 'animal',
    'animal', 'animal', 'animal', 'animal', 'animal', 'animal', 'animal',
    'animal', '_', 'person_accessory', 'person_accessory', '_', '_',
    'person_accessory', 'person_accessory', 'person_accessory', 'sports',
    'sports', 'sports', 'sports', 'sports', 'sports', 'sports', 'sports',
    'sports', 'sports', 'kitchenware', '_', 'kitchenware', 'kitchenware',
    'kitchenware', 'kitchenware', 'kitchenware', 'kitchenware', 'food', 'food',
    'food', 'food', 'food', 'food', 'food', 'food', 'food', 'food',
    'furniture', 'furniture', 'furniture', 'furniture', '_', 'furniture',
    '_', '_', 'furniture', '_', 'electronics', 'electronics',
    'electronics', 'electronics', 'electronics', 'electronics', 'appliance',
    'appliance', 'appliance', 'appliance', 'appliance', '_',
    'indoor_obj', 'indoor_obj', 'indoor_obj', 'indoor_obj',
    'indoor_obj', 'indoor_obj', 'indoor_obj'
]

# Map from instance category to super category names
COCO_SUB_SUPER_MAP = {
    k: v
    for k, v in zip(COCO_INSTANCE_CATEGORY_NAMES,
                    COCO_SUPER_CATEGORY_NAMES)
}

# Action super classes
SUPER_ACTIONS = {
    'moving': ['moving', 'traveling'],
    'smiling': ['smiling', 'laughing', 'enjoying'],
    'holding': ['holding', 'carrying'],
    'smelling': ['smelling / sniffing'],
    'watching': ['watching / looking', 'spectating', 'learning'],
    'talking': ['talking', 'socializing'],
    'eating': ['eating', 'grazing'],
    'bending': ['bending', 'crouching'],
    'hunting': ['hunting', 'fishing'],
    'riding': ['riding', 'surfing'],
}

# Map from action subclasses to action superclasses
SUB_TO_SUPER_ACTION = {
    sub: super_action
    for super_action, subs in SUPER_ACTIONS.items()
    for sub in subs
}

# Actions that are ignored if other actions are present
COLLAPSIBLE_ACTIONS = [
    'watching',
    'participating',
    'enjoying',
    'waiting',
    'thinking',
    'listening',
    'learning',
    'smiling',
    'laying',
    'loving',
    'following',
    'hanging',
    'fishing',
]

IMAGE_SIZE = 256


def stuff_categories_to_names(cat_list, stuff_cat_map, stuff_id_annot_map):
    """ Converts list COCO-Stuff categories to their names
        Parameters:
            cat_list (list of ints): category labels
            stuff_cat_map (dict): mapping from category ID to name
            stuff_id_annot_map (dict): mapping from annotation ID to annotation
        Returns (list of strs) or (None)
    """
    if cat_list is not None:
        rc = [stuff_cat_map[stuff_id_annot_map[annot]['category_id']] for annot in cat_list]
    else:
        rc = None
    return rc


def is_action(attribute_name):
    """ A simple function for determining if a word is an action
    """
    return attribute_name.endswith('ing') and attribute_name != 'appetizing'


def merge_dicts(dict_a, dict_b):
    """ Merge dictionaries with identical keys - if conflicting entries and one
        is None, overwrite it
    """
    new_dict = {
        key_a: val_a
        if val_a is not None else dict_b.get(key_a)
        for key_a, val_a in dict_a.items()
    }
    return new_dict


def get_stuff_names_bboxes(stuff_dset, df):
    """ Gets COCO-Stuff names and bounding boxed from each annotation
    """
    # Unique image IDs
    print("getting unique image ids", flush=True)
    image_ids = {annot['image_id'] for annot in tqdm(stuff_dset['annotations'])}

    print("making image-annotation map", flush=True)
    # Dictionary mapping image ID to annotation IDs
    stuff_image_annots_map = {
        image_id: []
        for image_id in image_ids
    }

    for annot in tqdm(stuff_dset['annotations']):
        stuff_image_annots_map[annot['image_id']].append(annot['id'])

    print("making category id-name map", flush=True)
    stuff_cat_map = {
        cat['id']: cat['name']
        for cat in tqdm(stuff_dset['categories'])
    }
    print("making id-annotation map", flush=True)
    stuff_id_annot_map = {
        annot['id']: annot
        for annot in tqdm(stuff_dset['annotations'])
    }
    print("getting annotations of each image in df", flush=True)
    stuff_annots = [
        stuff_image_annots_map.get(img['id'])
        for img in tqdm(df['image'])
    ]

    print("creating category name list for each annotation", flush=True)
    bboxes_list = list(map(
        lambda x:
            [
                stuff_id_annot_map.get(annot_id)['bbox']
                for annot_id in x
            ] if x is not None else None,
        tqdm(stuff_annots)
    ))

    names_list = list(map(
        (lambda x:
            stuff_categories_to_names(
                x,
                stuff_cat_map,
                stuff_id_annot_map)),
        tqdm(stuff_annots)
    ))

    print("creating image id-stuff names map")
    image_stuff_name_bbox_map = {
        img['id']: (names, bboxes)
        if names is not None else None
        for img, names, bboxes in tqdm(zip(
            df['image'],
            names_list,
            bboxes_list
        ))
    }

    return image_stuff_name_bbox_map


def load_stuff_dset(split, data_dir=DATA_DIR):
    """ Loads the COCO-Stuff annotation data
    """
    fpath = os.path.join(
        data_dir,
        'coco-stuff/stuff_{}2017.json'.format(split)
    )
    with open(fpath, 'r') as fid:
        stuff_dset = json.load(fid)
    return stuff_dset


def extract_image_stuff_info(df):
    """ extract all important information from COCO-Stuff annotations
    """
    splits = ['train', 'val']
    image_stuff_name_box_map = merge_dicts(*({
        split: get_stuff_names_bboxes(load_stuff_dset(split), df)
        for split in splits
    }.values()))
    return image_stuff_name_box_map


def eliminate_subject_from_detections(df):
    """Removes the subject from detections so that we may look directly at objects
    """
    # Are the subject and object the same superclass and highly
    # overlapping?
    same_supercat, high_iou = zip(*[
        (
            torch.tensor([
                COCO_SUB_SUPER_MAP[object_name] == COCO_SUB_SUPER_MAP[subject_name]
                for object_name in predictions['names']
            ]),
            predictions['subject_iou'] > 0.5
        )
        if predictions is not None else ([], [])
        for predictions, subject_name in zip(
            df['predictions'],
            df['subject']
        )
    ])

    # Keep only objects that aren't the same as the subject
    objects_to_keep = [
        np.where((cat_same * box_same).numpy() == 0)
        if len(cat_same) > 0 and len(box_same) > 0 else (np.array([], dtype=np.int64), )
        for cat_same, box_same in zip(same_supercat, high_iou)
    ]

    kept = [
        {
            k: v[to_keep]
            if not isinstance(v, list) else np.array(v)[to_keep]
            for k, v in predictions.items()
        }
        if predictions is not None else None
        for predictions, to_keep in zip(
            df['predictions'], objects_to_keep
        )
    ]

    df['predictions'] = kept


def filter_bad_detections(predictions):
    """ Filters out low-confidence detections, gives names to good detections
    """
    thresh = 0.6
    if not isinstance(predictions, dict):
        return None
    good_indices = np.where(predictions['scores'].numpy() > thresh)
    filtered = {
        k: v[good_indices] for k, v in predictions.items()
    }
    filtered.update({
        'names': [
            COCO_INSTANCE_CATEGORY_NAMES[label]
            for label in filtered['labels']
        ]
    })

    return filtered


def rectify_predicted_bboxes(df):
    def select_rectified(i):
        out = df.loc[i]['predictions']
        out['boxes'] = rectified_bboxes[i]
        return out

    is_landscape, scale = zip(*[
        (img['width'] > img['height'], max(img['width'], img['height']) / IMAGE_SIZE)
        for img in df['image']
    ])
    offset = [
        (IMAGE_SIZE - (min(img['width'], img['height']) / scale[i])) // 2
        for i, img in enumerate(df['image'])
    ]
    pos_offset = [
        torch.tensor([[offset[i], 0, offset[i], 0]])
        if is_landscape[i]
        else torch.tensor([[0, offset[i], 0, offset[i]]])
        for i in range(len(is_landscape))
    ]
    rectified_bboxes = [
        (df.loc[i, 'predictions']['boxes'] - pos_offset[i]) * scale[i]
        if df.loc[i, 'predictions'] else None
        for i in range(len(df))
    ]
    print("Assigning rectified bboxes now")
    for i in tqdm(range(len(df))):
        if df.loc[i, 'predictions'] is not None:
            df.loc[i, 'predictions']['boxes'] = rectified_bboxes[i]


def compute_subject_objects_distance(df):
    """ Computes the distance between the centers of the subject and candidate objects
    """
    subject_centers, object_centers = zip(*[
        (
            BBox(torch.tensor([annot['bbox']]), 'xywh').center,
            BBox(pred['boxes'], 'xyxy').center if pred is not None else None
        )
        for annot, pred in zip(df['coco_annot'], df['predictions'])
    ])
    dist = [
        torch.nn.functional.pairwise_distance(
            sub_c,
            obj_c
        )
        if obj_c is not None else None
        for sub_c, obj_c in zip(subject_centers, object_centers)
    ]

    # Update prediction with subject-object distance
    with ipdb.launch_ipdb_on_exception():
        for pred, sub_annot, d, in zip(df['predictions'],
                                       df['coco_annot'],
                                       dist):
            if pred is not None:
                pred.update({
                    'subject_iou': BBox(pred['boxes'], 'xyxy').iou(
                        BBox(sub_annot['bbox'], 'xywh')),
                    'distance_to_subject': d
                })
    return


def estimate_likely_object(df):
    """ Estimate the most likely object based on BBox IoU
    """
    min_iou_thresh = 0.1
    likely_object_idx = [
        torch.argmax(pred['subject_iou'])
        if pred is not None and pred['subject_iou'].shape[0] > 0 and max(pred['subject_iou']) > min_iou_thresh
        else None
        for pred in df.predictions
    ]
    df['likely_prediction'] = [
        {
            key: value[idx]
            for key, value in pred.items()
        }
        if idx is not None else None and pred is not None
        for pred, idx in zip(
            df['predictions'],
            likely_object_idx
        )
    ]


def estimate_single_action_object(df):
    """ Estimates a single action and object based on a sort of
        cost-minimization between annotated verbs and predicted objects.
    """

    def simple_search_and_update(df, action, action_found, is_sv=False, only_agents=True):
        is_action, action_found = simple_action_search(df, action, action_found, only_agents)
        simple_update_df(df, action, is_action, is_sv)
        return action_found

    def simple_action_search(df, action, action_found, only_agents=True):
        valid_object = is_agent if only_agents else lambda x: True
        is_action = [
            valid_object(subject) and (action in actions) and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
            )
        ]
        action_found = [
            True if acting else found
            for acting, found in zip(is_action, action_found)
        ]
        return is_action, action_found

    def simple_update_df(df, action, is_action, is_sv=False):
        df['single_action'] = [
            action if acting else single_action
            for acting, single_action in zip(
                is_action,
                df['single_action']
            )
        ]
        df['single_object'] = [
            obj['names']
            if acting and obj is not None and not is_sv else single_object
            for obj, acting, single_object in zip(
                df['likely_prediction'],
                is_action,
                df['single_object'],
            )
        ]

    # First, superclass relevant actions
    df['actions'] = list(map(
        lambda x:
            list({
                SUB_TO_SUPER_ACTION.get(action)
                if SUB_TO_SUPER_ACTION.get(action) else action
                for action in x
            }),
        df['actions']
    ))

    # Next, eliminate some actions if they're not alone
    for action in COLLAPSIBLE_ACTIONS:
        alterable = [
            not_only and contains
            for not_only, contains in zip(
                map(lambda x: x != [action], df['actions']),
                map(lambda x: action in x, df['actions'])
            )
        ]
        _ = list(map(lambda x: x.remove(action), df['actions'][alterable]))

    subject_centers, object_centers = zip(*[
        (
            BBox(torch.tensor([annot['bbox']]), 'xywh').center,
            BBox(pred['boxes'].unsqueeze(0), 'xyxy').center
        )
        if pred is not None
        else (BBox(torch.tensor([annot['bbox']]), 'xywh').center, None)
        for annot, pred in zip(df['coco_annot'], df['likely_prediction'])
    ])

    # RIDING
    subject_is_riding = [
        is_riding(subject, obj['names'], c_sub[0], c_obj[0], actions)
        if obj is not None and c_obj is not None else False
        for subject, obj, c_sub, c_obj, actions in zip(
            df['subject'],
            df['likely_prediction'],
            subject_centers,
            object_centers,
            df['actions']
        )
    ]
    df['single_action'] = [
        'riding' if riding else None for riding in subject_is_riding
    ]
    df['single_object'] = [
        obj['names']
        if riding else None
        for obj, riding in zip(
            df['likely_prediction'],
            subject_is_riding
        )
    ]
    df['single_action'] = [
        'riding'
        if ('skiing' in multi_actions or        # NOQA
            'snowboarding' in multi_actions or  # NOQA
            'skating' in multi_actions or       # NOQA
            'surfing' in multi_actions)
        else single_action
        for multi_actions, single_action in zip(
            df['actions'],
            df['single_action']
        )
    ]
    df['single_object'] = [
        'skis'
        if 'skiing' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    df['single_object'] = [
        'snowboard'
        if 'snowboarding' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    df['single_object'] = [
        'skateboard'
        if 'skating' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    df['single_object'] = [
        'surfboard'
        if 'surfing' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    action_found = [True if action is not None else False for action in df['single_action']]

    # HOLDING
    subject_is_holding = [
        is_holding(subject, obj['names'], actions)
        if obj is not None and not found else False
        for subject, obj, found, actions in zip(
            df['subject'],
            df['likely_prediction'],
            action_found,
            df['actions'])
    ]
    df['single_action'] = [
        'holding' if holding else single_action
        for holding, single_action in zip(
            subject_is_holding,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if holding else single_object
        for obj, holding, single_object in zip(
            df['likely_prediction'],
            subject_is_holding,
            df['single_object']
        )
    ]
    action_found = [
        True if holding else found
        for holding, found in zip(subject_is_holding, action_found)
    ]

    # PLAYING SPORTS
    subject_is_playing_sports = [
        is_playing_sports(subject, actions, stuff)
        if not found else False
        for subject, actions, stuff, found in zip(
            df['subject'],
            df['actions'],
            df['stuff_names'],
            action_found
        )
    ]
    df['single_action'] = [
        'playing_sports' if playing else single_action
        for playing, single_action in zip(
            subject_is_playing_sports,
            df['single_action']
        )
    ]
    action_found = [
        True if playing else found
        for playing, found in zip(subject_is_playing_sports, action_found)
    ]

    is_standing, action_found = simple_action_search(
        df,
        'standing',
        action_found
    )
    is_sitting, action_found = simple_action_search(
        df,
        'sitting',
        action_found
    )
    is_laying, action_found = simple_action_search(
        df,
        'laying',
        action_found
    )
    df['single_action'] = [
        'standing' if standing else single_action
        for standing, single_action in zip(
            is_standing,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if standing and obj is not None and is_above(c_sub[0], c_obj[0]) else single_object
        for obj, standing, single_object, c_sub, c_obj in zip(
            df['likely_prediction'],
            is_standing,
            df['single_object'],
            subject_centers,
            object_centers
        )
    ]
    df['single_action'] = [
        'sitting' if sitting else single_action
        for sitting, single_action in zip(
            is_sitting,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if sitting and obj is not None and is_above(c_sub[0], c_obj[0]) else single_object
        for obj, sitting, single_object, c_sub, c_obj in zip(
            df['likely_prediction'],
            is_sitting,
            df['single_object'],
            subject_centers,
            object_centers
        )
    ]

    action_found = simple_search_and_update(df, 'cooking', action_found, is_sv=True, only_agents=False)

    df['single_action'] = [
        'laying' if laying else single_action
        for laying, single_action in zip(
            is_laying,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if laying and obj is not None and is_above(c_sub[0], c_obj[0]) else single_object
        for obj, laying, single_object, c_sub, c_obj in zip(
            df['likely_prediction'],
            is_laying,
            df['single_object'],
            subject_centers,
            object_centers
        )
    ]

    action_found = simple_search_and_update(df, 'cuddling', action_found)
    action_found = simple_search_and_update(df, 'sleeping', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'eating', action_found)
    action_found = simple_search_and_update(df, 'drinking', action_found)
    action_found = simple_search_and_update(df, 'running', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'walking', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'smelling', action_found)
    action_found = simple_search_and_update(df, 'waiting', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'smiling', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'talking', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'watching', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'moving', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'sitting', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'laying', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'driving', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'holding', action_found, is_sv=True, only_agents=False)


def is_vehicle(coco_cat):
    return COCO_SUB_SUPER_MAP[coco_cat] == 'vehicle'


def is_animal(coco_cat):
    return COCO_SUB_SUPER_MAP[coco_cat] == 'animal'


def is_agent(coco_cat):
    return coco_cat == 'person' or COCO_SUB_SUPER_MAP[coco_cat] == 'animal'


def is_rideable(coco_cat):
    retval = is_vehicle(coco_cat) \
        or coco_cat == 'elephant' \
        or coco_cat == 'horse' \
        or coco_cat == 'giraffe' \
        or coco_cat == 'zebra' \
        or coco_cat == 'skateboard' \
        or coco_cat == 'surfboard' \
        or coco_cat == 'snowboard' \
        or coco_cat == 'skis' \
        or coco_cat == 'cow'
    return retval


def is_above(loc_a, loc_b):
    """ Returns True if loc_a is above loc_b in image space
    """
    above = loc_a[1] < loc_b[1] + 10
    nearby = abs(loc_a[0] - loc_b[0]) < 30
    return above and nearby


def is_riding(subject_name, object_name, subject_center, object_center, possible_actions):
    """ Heuristic for determining if a subject_name is riding an object_name
    """
    retval = is_agent(subject_name) \
        and is_rideable(object_name) \
        and is_above(subject_center, object_center) \
        and ('riding' in possible_actions or 'sitting' in possible_actions or 'standing' in possible_actions) \
        and reasonable_to_ride(subject_name, object_name)
    return retval


def reasonable_to_ride(cat_a, cat_b):
    """ is it reasonable to say that cat_a is riding cat_b?
    """
    unreasonable = is_animal(cat_a) and \
        is_animal(cat_b) and \
        cat_a != 'bird' or \
        cat_a == cat_b or \
        cat_a == 'giraffe' or \
        cat_a == 'elephant' or \
        cat_a == 'cow' or \
        cat_a == 'sheep'
    return not unreasonable


def is_holding(subject_name, object_name, possible_actions):
    retval = is_agent(subject_name) \
        and is_holdable(object_name) \
        and ('holding' in possible_actions or 'waving' in possible_actions or 'stretching' in possible_actions or 'serving' in possible_actions or 'reading' in possible_actions or 'bending' in possible_actions)
    return retval


def is_holdable(obj):
    """ Is this object holdable?
    """
    retval = not COCO_SUB_SUPER_MAP[obj] == 'appliance' \
        and not COCO_SUB_SUPER_MAP[obj] == 'furniture' \
        and not (COCO_SUB_SUPER_MAP[obj] == 'animal' and  # NOQA
            not (obj == 'dog' or obj == 'cat' or obj == 'bird')) \
        and not COCO_SUB_SUPER_MAP[obj] == 'vehicle'
    return retval


def is_playing_sports(subject, possible_actions, stuff_present):
    """ Is the subject playing sports?
    """
    retval = is_agent(subject) \
        and ('playing' in possible_actions or 'participating' in possible_actions) \
        and ('playingfield' in stuff_present or 'dirt' in stuff_present or 'grass' in stuff_present)
    return retval


def merge_subject_object_bboxes(df):
    """ Merge bounding boxes of subject and object
    """
    cache = Cache('~/.cache/cocoactions')
    merged_bboxes = cache.unpickle('merged_subject_object_bboxes_xyxy.pkl')
    if merged_bboxes is None:
        sub_bboxes = cache.unpickle('subject_bboxes_xyxy.pkl')
        obj_bboxes = cache.unpickle('object_bboxes_xyxy.pkl')
        with ipdb.launch_ipdb_on_exception():
            if sub_bboxes is None:
                sub_bboxes = list(map(
                    lambda x: BBox(torch.tensor(x), 'xywh').xyxy,
                    [entry['coco_annot']['bbox'] for i, entry in df.iterrows()])
                )
                cache.pickle(sub_bboxes, 'subject_bboxes_xyxy.pkl')

            if obj_bboxes is None:
                obj_bboxes = list(map(
                    lambda x: None
                    if x is None else BBox(x['boxes'], 'xyxy').xyxy,
                    [entry['likely_prediction'] for i, entry in df.iterrows()])
                )
                cache.pickle(obj_bboxes, 'object_bboxes_xyxy.pkl')
            merged_bboxes = list(map(
                lambda x:
                    x[0].reshape(-1, 4)
                    if x[1] is None else BBox.merge(x[0], x[1]).xyxy.reshape(-1, 4),
                zip(sub_bboxes, obj_bboxes))
            )
            cache.pickle(merged_bboxes, 'merged_subject_object_bboxes_xyxy.pkl')
    return merged_bboxes


def get_dset_stats(df=None, svo_permutation_counts=None):
    permutation_count_fname = "svo_permutation_counts_dict.pkl"

    if svo_permutation_counts is None and df is not None:
        svo_permutation_counts = tryload_svo_permutation_counts(
            df,
            fname=permutation_count_fname
        )

    # Grab unique subjects, verbs, and objects
    subjects, verbs, objects = (
        set(tup)
        for tup in zip(*[
            k.split('-')
            for k in svo_permutation_counts.keys()
        ])
    )
    sv_permutations = set(['-'.join(k.split('-')[:2]) for k in svo_permutation_counts.keys()])
    sv_permutation_counts = {
        sv: sum([
            n
            for svo, n in svo_permutation_counts.items()
            if svo.startswith(sv)
        ])
        for sv in sv_permutations
    }
    subject_counts = {
        sub: sum([
            v
            for k, v in svo_permutation_counts.items()
            if k.startswith(sub)
        ])
        for sub in subjects
    }
    verb_counts = {
        verb: sum([
            v
            for k, v in svo_permutation_counts.items()
            if k.split('-')[1] == verb
        ])
        for verb in verbs
    }
    object_counts = {
        obj: sum([
            v
            for k, v in svo_permutation_counts.items()
            if k.endswith(obj)
        ])
        for obj in objects
    }
    retval = {
        'svo_permutation_counts': svo_permutation_counts,
        'subject_counts': subject_counts,
        'verb_counts': verb_counts,
        'object_counts': object_counts,
        'sv_permutation_counts': sv_permutation_counts,
    }
    return retval


def tryload_svo_permutation_counts(df, fname='svo_permutation_counts_dict.pkl'):
    """ Loads SVO permutation counts from cache if it exists, computes a new
        one and caches it if it doesn't
    """
    cache = Cache()
    print("Trying to find a precomputed SVO permutation counts dictionary...")
    if not cache.exists(fname):
        print("* Not found! - Computing now, this will take a while...")
        svo_permutation_counts = {
            "{}-{}-{}".format(subject, verb, obj):
                len(df[(df.subject == subject) & (df.single_action == verb) & (df.single_object == obj)])
                if obj is not None else
                len(df[(df.subject == subject) & (df.single_action == verb) & (df.single_object.isna())])
                for subject in df.subject.unique()
                for verb in df.single_action.unique()
                for obj in df.single_object.unique()
        }
        svo_permutation_counts = {
            k: v
            for k, v in svo_permutation_counts.items()
            if v > 0
        }
        print("* Done! - writing to disk and moving on")
        cache.pickle(svo_permutation_counts, fname)

    else:
        print("* Found! - reading from disk and moving on")
        svo_permutation_counts = cache.unpickle(fname)
    return svo_permutation_counts


def create_disjoint_svo_sets(svo_permutation_counts, train_frac=0.7,
                             split_options=['train', 'test']):
    """ Create train and test sets such that the (s, v, o) triplets of each are
        disjoint, though in general the subjects, verbs, and objects are not disjoint
        Parameters:
            svo_permutation_counts (dict): keys are subject-verb-object strings and
                values are number of samples
            split_frac (int): The rough proportion of samples to be labeled split_options[0]
            split_options (list of strings): names of splits
    """
    # First sort by count in descending order
    sorted_by_count = [
        (k, svo_permutation_counts[k])
        for k in sorted(svo_permutation_counts,
                        key=lambda x: svo_permutation_counts[x],
                        reverse=True)
    ]

    # Split_options are probably 'train' and 'test', but could be specified
    # another way
    splits = {s: dict() for s in split_options}
    ratio = train_frac / (1 - train_frac)

    # Largest class always goes to split 0
    this_split = split_options[0]
    for i, (svo, count) in enumerate(sorted_by_count):
        splits[this_split][svo] = count

        # Second-largest goes to split 1 - this biases the second split towards
        # a few high-representation classes as opposed to many
        # low-representation classes - Also biases to a larger second split
        if len(splits[split_options[1]]) == 0:
            this_split = split_options[1]

        # If there are too few in split 0, add there next
        elif len(splits[split_options[0]]) / len(splits[split_options[1]]) < ratio:
            this_split = split_options[0]
        else:
            this_split = split_options[1]

    n_each_set = {k: sum([count for count in splits[k].values()]) for k in splits.keys()}
    return splits, n_each_set
