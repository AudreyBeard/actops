""" This is the one-stop shop for creating COCO-Actions  # NOQA
1. combine_attributes_stuff.py
2. propose_objects.py
3. clean_combined.py
"""

import os

import torch
from torchvision.models.detection import fasterrcnn_resnet50_fpn
from torchvision import transforms as tforms
from torchvision.transforms import functional
from PIL import Image
import pandas as pd
from tqdm import tqdm
import ipdb
from numpy import ceil

from actops import utils
from actops.dataproc.preproc import preproc_utils

DEFAULT_DPATH = utils.path("~/data/coco-actions")
DEFAULT_TRANSFORMS = tforms.Compose([
    tforms.ToTensor(),
])


DEBUG = False


def collate_fn(sample_list):
    collated = {
        key: sample[key]
        for sample in sample_list
        for key in set(sample_list[0].keys()) - {'image'}
    }
    collated.update({'image': torch.stack([sample['image'] for sample in sample_list])})
    return collated


class COCOActionsDataset(torch.utils.data.Dataset):
    """ Frontend for COCO-Actions during dataset generation. Not intended for use with models
    """
    def __init__(self, dpath=DEFAULT_DPATH, split='train',
                 transforms=DEFAULT_TRANSFORMS, df_fname='coco-actions-df.pkl',
                 reset=False, image_size=preproc_utils.IMAGE_SIZE):

        self.dpath = dpath
        self.df_fpath = os.path.join(dpath, df_fname)
        self.df_fpath_mod = "{0[0]}_predicted{0[1]}".format(os.path.splitext(self.df_fpath))

        if not reset and os.path.exists(self.df_fpath_mod):
            print("Found modified DataFrame for use at:\n  {}".format(self.df_fpath_mod))
            self.df = pd.read_pickle(os.path.join(dpath, self.df_fpath_mod))
        else:
            self.df = pd.read_pickle(self.df_fpath)

        # Construct lookup table for sequential index to DataFrame Index
        # Slower on the frontend, but fast indexing and relatively low memory
        # footprint
        self.idx_lookup = {
            i: k
            for i, k in enumerate(self.df[self.df['split'] == split + '2014'].index)
        }

        #self.pre_transforms = lambda x: x
        # Pre-transforms resize the image a letterbox it so that it's a YxY square padded with black
        self.pre_transforms = ResizedLetterBox(image_size)
        self.transforms = transforms

    def __len__(self):
        """ Needed for DataLoader
            Only counts those of the specified split for proper use in the
            DataLoader
        """
        return len(self.idx_lookup)

    def __getitem__(self, idx):
        """ For DataLoader iterator
            Uses previously-built mapping from sequential index to DataFrame
            Index
        """
        entry = self.df.loc[self.idx_lookup[idx]]
        fpath = os.path.join(
            self.dpath,
            'coco',
            entry['split'],
            entry['image']['file_name']
        )

        img = Image.open(fpath)
        img = img.convert('RGB') if img.mode != 'RGB' else img
        img = self.transforms(self.pre_transforms(img))

        retval = {
            k: entry[k] for k in entry.keys()
        }
        retval.update({'image': img})
        return retval

    def update_df(self, idx, column, value, batch_size=None):
        """ Update internal dataframe (at proper index and column) with value
        """
        # If batch_size is given, it's implied that we're using this at scale
        if batch_size is not None:
            indices = [
                self.idx_lookup[i]
                for i in range(
                    idx * batch_size,
                    min(
                        (idx + 1) * batch_size,
                        len(self.idx_lookup)
                    )
                )
            ]
        else:
            indices = self.idx_lookup[idx]

        self.df.loc[indices, column] = value
        return

    def save_df(self, fpath=None):
        """ Save internal DataFrame as a copy of the original (unless original
            was already modified, which will cause overwrite)
        """
        fpath = self.df_fpath_mod if fpath is None else fpath

        self.df.to_pickle(fpath)


class ResizedLetterBox(object):

    """ Performs a resize on the image and a letterbox paddinmg on an image,
        assuming the images are returned as CHW
    """
    def __init__(self, size):
        self.size = size

    def __call__(self, image):
        h, w = image.size

        min_side_len = min(h, w)
        max_side_len = max(h, w)
        new_min_side_len = int(ceil(min_side_len / max_side_len * self.size))
        resized_image = functional.resize(image, new_min_side_len)

        # Create a black canvas a paste resized image into it at center
        letterboxed = Image.new(mode=image.mode, size=(self.size, self.size))
        letterboxed.paste(resized_image,
                          box=[(self.size - dim) // 2 for dim in resized_image.size[::-1]])

        return letterboxed


def find_objects(split, n_workers, batch_size,
                 image_size=preproc_utils.IMAGE_SIZE, save_fpath=None):

    dset = COCOActionsDataset(split=split)
    loader = torch.utils.data.DataLoader(
        dset,
        num_workers=n_workers,
        batch_size=batch_size,
        drop_last=False,
        collate_fn=collate_fn,
        shuffle=False
    )

    device = torch.device(0)
    model = fasterrcnn_resnet50_fpn(pretrained=True, min_size=min(image_size, 800))
    model.eval()
    model = model.cuda(device)
    t = tqdm(total=int(ceil(len(dset) / batch_size)))
    for i, data in enumerate(loader):
        t.update(1)
        with ipdb.launch_ipdb_on_exception():
            with torch.no_grad():
                images = data['image'].to(device)
                outputs = model(images)
                outputs = [{k: v.cpu() for k, v in output.items()} for output in outputs]
                dset.update_df(i, 'predictions', outputs, batch_size)

    print("Saving {}".format(save_fpath))
    dset.save_df(fpath=save_fpath)
    t.close()
    return save_fpath


def init_coco_actions(coco_attributes_fpath=None,
                      new_dpath=DEFAULT_DPATH,
                      new_fname='coco-actions-df.pkl'):
    import json
    import pickle
    import os
    import numpy as np

    # Load up COCO-Attibutes
    if coco_attributes_fpath is None:
        coco_attributes_fpath = os.path.join(
            DEFAULT_DPATH,
            'coco-attributes.pkl'
        )
    print("Loading COCO-Attributes: {}".format(coco_attributes_fpath), flush=True)
    with open(coco_attributes_fpath, 'rb') as fid:
        attr_dset = pickle.load(fid, encoding='latin1')

    # Construct mapping from index to action string
    action_index_map = {
        i: entry['name']
        for i, entry in enumerate(sorted(
            attr_dset['attributes'],
            key=lambda x: x['id']
        ))
        if preproc_utils.is_action(entry['name'])
    }

    # Turn this to a set for quick intersection-checking later
    action_indices = set(action_index_map.keys())
    # IDs of COCO-Attribute samples that have objects
    coco_attr_ids__action = []

    # Actions that may be present in each sample, and their
    # confidence
    sample_actions = []
    action_confidence = []
    coco_annot_ids__action = []
    coco_attr_split = []

    # For each annotation in COCO Attributes, find
    print("Processing all COCO-Attributes annotations", flush=True)
    for i, (coco_attr_id, attr_vec) in tqdm(enumerate(attr_dset['ann_vecs'].items())):
        # Get attribute indices that are likely to be in scene, based
        # on MTurk annotators for COCO-Attributes
        pos_attr_indices = np.where(attr_vec > 0.5)

        # Get attribute indices in this sample that are actions
        possible_actions = action_indices.intersection(pos_attr_indices[0])
        if len(possible_actions) > 0:

            # For all actions, get their names and hang on to them
            sample_actions.append([
                action_index_map[a]
                for a in possible_actions
            ])
            coco_attr_ids__action.append(coco_attr_id)
            action_confidence.append(attr_vec[(
                np.array(list(possible_actions)).astype(np.uint),
            )])
            coco_annot_ids__action.append(attr_dset['patch_id_to_ann_id'][coco_attr_id])
            coco_attr_split.append(attr_dset['split'][coco_attr_id])

    # Construct dataset with actions present
    df = pd.DataFrame(index=range(len(sample_actions)))

    # COCO-Attributes ID
    df['coco_attr_id'] = coco_attr_ids__action

    # COCO annotation ID
    df['coco_annot_id'] = coco_annot_ids__action

    # Actions and confidence (from COCO Attributes)
    df['actions'] = sample_actions
    df['confidence'] = action_confidence
    df['split'] = coco_attr_split

    coco_id_annot_map__action = dict()
    coco_id_image_map__action = dict()
    coco_id_fname_map__action = dict()
    coco_id_split_map = dict()
    print("Processing COCO splits: ", flush=True, end='')
    for split in ['train', 'val']:
        print("{} ".format(split), flush=True, end='')

        # Grab COCO annotations
        coco_dset_fpath = os.path.join(
            new_dpath,
            'coco/annotations/instances_{}2014.json'.format(split)
        )
        with open(coco_dset_fpath, 'r') as fid:
            coco_dset = json.load(fid)

        # Lookup from COCO image ID to image
        coco_id_image_map = {
            img_entry['id']: img_entry
            for img_entry in coco_dset['images']
        }

        # Lookup from COCO annotation ID to annotation
        coco_id_annot_map = {
            annot_entry['id']: annot_entry
            for annot_entry in coco_dset['annotations']
        }

        # Lookup from COCO annotation ID to file name
        coco_annot_fname_map = {
            annot_id: coco_id_image_map[annot['image_id']]['file_name']
            for annot_id, annot in coco_id_annot_map.items()
        }

        # Lookup from COCO annotation ID to image
        coco_annot_image_map = {
            annot_id: coco_id_image_map[annot['image_id']]
            for annot_id, annot in coco_id_annot_map.items()
        }

        # For all COCO annotation IDs with actions, grab the
        # annotation
        coco_id_annot_map__action_split = {
            key: coco_id_annot_map[key]
            for key in df['coco_annot_id']
            if coco_id_annot_map.get(key)
        }

        # For all COCO annotation IDs with actions, grab the image
        coco_annot_image_map__action_split = {
            key: coco_annot_image_map[key]
            for key in df['coco_annot_id']
            if coco_annot_image_map.get(key)
        }

        # For all COCO annotation IDs with actions, grab the image
        coco_annot_fname_map__action_split = {
            key: coco_annot_fname_map[key]
            for key in df['coco_annot_id']
            if coco_annot_image_map.get(key)
        }

        # Record the split each key comes from
        coco_id_split_map.update({
            coco_annot_id: split
            for coco_annot_id in coco_annot_image_map__action_split.keys()
        })
        coco_id_annot_map__action.update(coco_id_annot_map__action_split)
        coco_id_image_map__action.update(coco_annot_image_map__action_split)
        coco_id_fname_map__action.update(coco_annot_fname_map__action_split)

    print("")
    df['coco_annot'] = [
        coco_id_annot_map__action.get(coco_annot_id)
        for coco_annot_id in df['coco_annot_id']
    ]
    df['image'] = [
        coco_id_image_map__action.get(coco_annot_id)
        for coco_annot_id in df['coco_annot_id']
    ]
    df['fname'] = [
        coco_id_fname_map__action.get(coco_annot_id)
        for coco_annot_id in df['coco_annot_id']
    ]

    # Get COCO category
    coco_cats = {cat['id']: cat['name'] for cat in coco_dset['categories']}
    with ipdb.launch_ipdb_on_exception():
        df['subject'] = [coco_cats[annot['category_id']] for annot in df['coco_annot']]

    print("Processing COCO-Stuff", flush=True)
    image_id_stuff_names_bboxes_map = preproc_utils.extract_image_stuff_info(df)

    df['stuff_names'] = [image_id_stuff_names_bboxes_map[img['id']][0] for img in df['image']]
    df['stuff_bboxes'] = [image_id_stuff_names_bboxes_map[img['id']][1] for img in df['image']]

    save_name = os.path.join(new_dpath, new_fname)
    print('saving {}'.format(save_name))
    df.to_pickle(save_name)
    return save_name


# TODO pick up here
def post_prediction(df_fpath, new_fpath=None):
    """ Run this exactly once if you've just computed object locations
    """
    print("| Filtering bad detections", flush=True)
    df = pd.read_pickle(df_fpath)
    df['predictions'] = list(map(
        preproc_utils.filter_bad_detections,
        df['predictions'])
    )
    print("| Rectifying bounding boxes", flush=True)
    preproc_utils.rectify_predicted_bboxes(df)
    print("| Computing distance between subject and objects", flush=True)
    preproc_utils.compute_subject_objects_distance(df)
    print("| Eliminating subjects from detections", flush=True)
    preproc_utils.eliminate_subject_from_detections(df)
    #svo_actions = set(get_unique_actions(df)) - sv_actions
    print("| Estimating the most likely object", flush=True)
    preproc_utils.estimate_likely_object(df)
    print("| Estimating the most likely action", flush=True)
    preproc_utils.estimate_single_action_object(df)
    if new_fpath is None:
        new_fpath = "_single-estimated".join(os.path.split(df_fpath))
    print("| Saving {}".format(new_fpath))
    df.to_pickle(new_fpath)
    return df, new_fpath


def add_svo_split_to_df(df,
                        df_fpath='coco-actions-df_predicted_single-estimated_attrops-split.csv'):
    """ Run this to add Attributes as Operators-style disjoint-pair split
    """
    # Create larger crops which include subject and object
    unzipped = [
        [
            float(t)
            for t in coord[0]
        ]
        for coord in preproc_utils.merge_subject_object_bboxes(df)
    ]
    for col_name, vals in zip(
        ('crop_x1', 'crop_y1', 'crop_x2', 'crop_y2'),
        zip(*unzipped)
    ):
        df.loc[:, col_name] = vals

    stats = preproc_utils.get_dset_stats(df)
    disjoint_sets, n_each = preproc_utils.create_disjoint_svo_sets(
        stats['svo_permutation_counts']
    )
    for split, svo_counts in disjoint_sets.items():
        for svo, count in svo_counts.items():
            subject, verb, obj = svo.split('-')
            if obj == 'None':
                df.loc[
                    (df.subject == subject)        # NOQA
                    & (df.single_action == verb)   # NOQA
                    & (df.single_object.isna()),   # NOQA
                    'svo_split'] = split
            else:
                df.loc[
                    (df.subject == subject)        # NOQA
                    & (df.single_action == verb)   # NOQA
                    & (df.single_object == obj),   # NOQA
                    'svo_split'] = split
    df.to_pickle(df_fpath)
    return df, df_fpath


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("nprocs", type=int, default=1,
                        help='number of workers for loading and processing data'
                        )
    parser.add_argument("batch_size", type=int, default=1024,
                        help='number of workers for loading and processing data'
                        )
    args = parser.parse_args()

    # Initialize from COCO, COCO-Attributes, and COCO-Stuff
    print("Initializing COCO-Actions now")
    df_fpath = init_coco_actions()

    predictions_df_fpath = '_predicted'.join(os.path.splitext(df_fpath))
    if os.path.exists(predictions_df_fpath):
        print("Found cache of dataframe with found objects")
        df = pd.read_pickle(predictions_df_fpath)
    else:
        print("Finding potential objects now")
        for split in ('train', 'val'):
            print(split)
            written_df_fpath = find_objects(
                split,
                args.nprocs,
                args.batch_size,
                save_fpath=predictions_df_fpath
            )
            assert predictions_df_fpath == written_df_fpath

    single_estimated_prediction_df_fpath = "_single-estimated".join(
        os.path.splitext(predictions_df_fpath)
    )
    if os.path.exists(single_estimated_prediction_df_fpath):
        print("Found cache of dataframe with found objects")
        df = pd.read_pickle(single_estimated_prediction_df_fpath)
    else:
        print("Estimating single action and object for each sample now", flush=True)
        print("+------------------------------------------------------", flush=True)
        with ipdb.launch_ipdb_on_exception():
            df, new_fpath = post_prediction(
                df_fpath=predictions_df_fpath,
                #df_fpath=df_fpath,
                new_fpath=single_estimated_prediction_df_fpath
            )
        print("DONE\n+------------------------------------------------------", flush=True)

    split_df_fname = "_svo-split".join(
        os.path.splitext(single_estimated_prediction_df_fpath)
    )

    with ipdb.launch_ipdb_on_exception():
        assert 'fname' in df.columns

    if os.path.exists(split_df_fname):
        print("Found cache of dataframe with SVO split")
        df = pd.read_pickle(split_df_fname)
    else:
        print("Computing SVO split now")
        with ipdb.launch_ipdb_on_exception():
            df, new_fpath = add_svo_split_to_df(df, split_df_fname)
        print("Wrote out {}".format(new_fpath))

    ipdb.set_trace()
    csv_name = os.path.splitext(split_df_fname)[0] + '.csv'
    df = pd.DataFrame(data={
        k: df[k]
        for k in [
            'split',
            'svo_split',
            'fname',
            'subject',
            'single_action',
            'single_object',
            'crop_x1',
            'crop_x2',
            'crop_y1',
            'crop_y2',
        ]
    })
    if not os.path.exists(csv_name):
        print("Writing as csv now: {}".format(csv_name))
        df.to_csv(csv_name)

    print("====.   .==.  ==  == =====\n"
          "||  \\\\ //  \\\\ |\\\\ || ||\n"
          "||  || ||  || ||\\\\|| ||==\n"
          "||  // \\\\  // || \\\\| ||\n"
          "====*   `==*  ==  == =====")
