from __future__ import absolute_import

import torch
from torchvision.models.detection import fasterrcnn_resnet50_fpn

from actops import utils
from actops.dataproc.data_moments import MomentsInTimeDataset


def parse_args():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir',
                        help='Directory containing MiT data',
                        action='store',
                        default="$HOME/data/Moments_in_Time_256x256_30fps",
                        )
    parser.add_argument('--n_frames',
                        help='Number of frames for extraction',
                        action='store',
                        default=16)
    parser.add_argument('--n_workers',
                        help='Number of workers for loading',
                        action='store',
                        default=1)
    parser.add_argument('--batch_size',
                        help='Object detection batch size',
                        action='store',
                        default=1)
    parser.add_argument('--split',
                        help="'train' or 'val'",
                        action='store',
                        default='val')
    args = parser.parse_args()
    return args


def getlines(fpath):
    with open(fpath, 'r') as fid:
        lines = fid.readlines()
    return lines


def annotate_moments(loader, model, lambda_mean=1, lambda_freq=0.5):
    import os
    import ipdb
    import pandas
    import tqdm
    n_frames = loader.dataset.n_frames
    #batch_size = loader.batch_size

    video_object_annotations = {}
    video_subject_annotations = {}

    # COCO super-categories
    super_objects = utils.COCO_SUPER_CATEGORY_NAMES
    # Our defined action super-categories
    super_actions = utils.MIT_SUPERCLASSES
    # Lookup for action annotation
    super_action_lookup = {v: k for k, action_list in super_actions.items() for v in action_list}

    # Subset the dataset by action super classes
    n_videos = loader.dataset.subset(super_action_lookup.keys())

    # Keep track of action, object decisions, plus all detection data for posterity

    column_names = ['filename', 'action', 'subject', 'object', 'boxes', 'labels', 'scores']
    df = pandas.DataFrame(columns=column_names, index=range(n_videos))

    device = torch.cuda.current_device()
    #ipdb.set_trace()

    t = tqdm.tqdm(total=n_videos)
    for i, data in enumerate(loader):
        t.update(1)

        with ipdb.launch_ipdb_on_exception():
            with torch.no_grad():
                actions = data['label']
                inputs = data['data'].to(device)
                inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
                outputs = model(inputs)

            for j, fn in enumerate(data['filename']):
                fn = os.sep.join(fn.split(os.sep)[-2:])
                action = super_action_lookup[actions[j]]
                pruned_outputs = utils.object_heuristics(
                    inputs[j * n_frames:(j + 1) * n_frames],
                    outputs[j * n_frames:(j + 1) * n_frames]
                )

                n_objects = max([len(out['scores']) for out in pruned_outputs])
                if n_objects > 0:
                    pred_object_labels = utils.rectify_object_annotation(
                        pruned_outputs,
                        action,
                        lambda_mean=lambda_mean,
                        lambda_freq=lambda_freq) if n_objects > 0 else None

                    found_entities = [
                        super_objects[label]
                        for label in pred_object_labels
                    ]
                    video_subject_name, video_object_name = found_entities if len(found_entities) > 1 else found_entities[0], None
                else:
                    video_object_name = None
                    video_subject_name = None

                video_object_annotations[fn] = video_object_name
                video_subject_annotations[fn] = video_subject_name

                box_label_score = list(zip(*[(out['boxes'].cpu().numpy(),
                                              out['labels'].cpu().numpy(),
                                              out['scores'].detach().cpu().numpy())
                                             for out in pruned_outputs]))
                entry = [fn,
                         action,
                         video_subject_name,
                         video_object_name,
                         box_label_score[0],
                         box_label_score[1],
                         box_label_score[2],
                         ]

                df.loc[i] = entry
                #print("{}: {}".format(fn, super_objects[pred_object_label]))
    t.close()
    with ipdb.launch_ipdb_on_exception():
        partition = os.path.split(loader.dataset.dpath)[-1]
        object_annotation_fpath = os.path.join(os.path.split(loader.dataset.dpath)[0],
                                               "object_annotations_{}.csv".format(partition))
        subject_annotation_fpath = os.path.join(os.path.split(loader.dataset.dpath)[0],
                                                "subject_annotations_{}.csv".format(partition))
        detection_df_fpath = os.path.join(os.path.split(loader.dataset.dpath)[0],
                                          "detection_df_{}.pkl".format(partition))
        df.to_pickle(detection_df_fpath)

        with open(object_annotation_fpath, "w") as fid:
            lines = "\n".join(["{},{}".format(fn, obj)
                              for fn, obj in video_object_annotations.items()])
            fid.write(lines)
        with open(subject_annotation_fpath, "w") as fid:
            lines = "\n".join(["{},{}".format(fn, obj)
                              for fn, obj in video_subject_annotations.items()])
            fid.write(lines)
    print("Done")

    return


if __name__ == "__main__":
    import os
    from torchvision import transforms
    #from moments.models import load_transforms
    args = parse_args()
    data_dir = os.path.expandvars(args.data_dir)

    transforms = transforms.ToTensor()
    dset = MomentsInTimeDataset(dpath=data_dir,
                                split=args.split,
                                transforms=transforms,
                                n_frames=int(args.n_frames))

    device = torch.device(0)
    model = fasterrcnn_resnet50_fpn(pretrained=True, min_size=256)
    model.eval()
    model = model.cuda(device)

    loader = torch.utils.data.DataLoader(dset,
                                         num_workers=int(args.n_workers),
                                         batch_size=1,
                                         drop_last=True,
                                         shuffle=False)

    annotate_moments(loader, model)
