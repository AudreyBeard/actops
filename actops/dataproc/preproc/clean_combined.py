import os
import sys

import pandas as pd
from PIL import Image, ImageDraw
import torch
import numpy as np

import ipdb

if os.path.realpath('..') not in sys.path:
    sys.path.append(os.path.realpath('..'))
from utils import (
    COCO_INSTANCE_CATEGORY_NAMES,
    COCO_SUPER_CATEGORY_NAMES,
    AnnotatedImage,
    BBox
)

data_dir = os.path.expandvars('$HOME/data')
df_fpath = os.path.join(data_dir, 'coco-actions-df.pkl')


coco_sub_super_map = {
    k: v
    for k, v in zip(COCO_INSTANCE_CATEGORY_NAMES,
                    COCO_SUPER_CATEGORY_NAMES)
}

# Revisit this
sv_actions = {
    'celebrating',
    'crouching',
    'floating',
    'hanging',
    'hiding',
    'landing',
    'laughing',
    'learning',
    'listening',
    'panting',
    'participating',
    'reaching',
    'reading',
    'running',
    'waving',
    'jumping',
    'bending',
    'shopping',
    'skating',
    'skiing',
    'sleeping',
    'smiling',
    'snowboarding',
    'socializing',
    'spectating',
    'surfing',
    'swimming',
    'talking',
    'thinking',
    'traveling',
    'waiting',
    'walking',
    'working',
}


super_actions = {
    'moving': ['moving', 'traveling'],
    'smiling': ['smiling', 'laughing', 'enjoying'],
    'holding': ['holding', 'carrying'],
    'smelling': ['smelling / sniffing'],
    'watching': ['watching / looking', 'spectating', 'learning'],
    'talking': ['talking', 'socializing'],
    'eating': ['eating', 'grazing'],
    'bending': ['bending', 'crouching'],
    'hunting': ['hunting', 'fishing'],
    'riding': ['riding', 'surfing'],
}

# Which actions are not important in context of other actions? Order is important!
collapsible_actions = [
    'watching',
    'participating',
    'enjoying',
    'waiting',
    'thinking',
    'listening',
    'learning',
    'smiling',
    'laying',
    'loving',
    'following',
    'hanging',
    'fishing',
]


# Keys are superceded by their values if values are present in scene
# TODO do I want to do this? it ultimately skews the dataset more towards the
# superceding actions
superceded_by = {
    'moving': {'running', 'walking', 'playing', 'riding', 'skiing', 'driving'},
    'running': {'playing'},
    'riding': {'driving'},
    'bending': {'eating', 'landing', 'drinking'},
    #'holding': {'playing'},
    'talking': {'holding'},
    'working': {'walking'},
}

supercedes = {
    'stretching': {'bending', 'moving'},
    'holding': {'stretching'},
}

supercedes_all = [
    'skiing',
    'snowboarding',
    'racing',
    'hunting',
]


def post_prediction():
    """ Run this exactly once if you've just computed object locations
    """
    df = pd.read_pickle(df_fpath)
    df['predictions'] = list(map(filter_bad_detections, df['predictions']))
    rectify_predicted_bboxes(df)
    compute_subject_objects_distance(df)
    eliminate_subject_from_detections(df)
    svo_actions = set(get_unique_actions(df)) - sv_actions
    estimate_likely_object(df)
    estimate_single_action_object(df)
    new_fpath = "_single-estimated".join(os.path.split(df_fpath))
    df.to_pickle(new_fpath)
    return df



def filter_bad_detections(predictions):
    """ Filters out low-confidence detections, gives names to good detections
    """
    thresh = 0.6
    good_indices = np.where(predictions['scores'].numpy() > thresh)
    filtered = {
        k: v[good_indices] for k, v in predictions.items()
    }
    filtered.update({
        'names': [
            COCO_INSTANCE_CATEGORY_NAMES[label]
            for label in filtered['labels']
        ]
    })

    return filtered


def compute_subject_objects_distance(df):
    subject_centers, object_centers = zip(*[
        (
            BBox(torch.tensor([annot['bbox']]), 'xywh').center,
            BBox(pred['boxes'], 'xyxy').center
        )
        for annot, pred in zip(df['coco_annot'], df['predictions'])
    ])
    dist = [
        torch.nn.functional.pairwise_distance(
            sub_c,
            obj_c
        )
        for sub_c, obj_c in zip(subject_centers, object_centers)
    ]
    #[
    #    pred.update({'distance_to_subject': d})
    #    for pred, d in zip(df['predictions'], dist)
    #]
    import ipdb
    with ipdb.launch_ipdb_on_exception():
        [
            pred.update({
                'subject_iou': BBox(pred['boxes'], 'xyxy').iou(BBox(sub_annot['bbox'], 'xywh')),
                'distance_to_subject': d
            })
            for pred, sub_annot, d in zip(
                df['predictions'],
                df['coco_annot'],
                dist
            )
        ]


def eliminate_subject_from_detections(df):
    """Removes the subject from detections so that we may look directly at objects
    """
    same_supercat, high_iou = zip(*[
        (
            torch.tensor([
                coco_sub_super_map[object_name] == coco_sub_super_map[subject_name]
                for object_name in predictions['names']
            ]),
            predictions['subject_iou'] > 0.5
        )
        for predictions, subject_name in zip(df['predictions'], df['subject'])
    ])
    objects_to_keep = [
        np.where((cat_same * box_same).numpy() == 0)
        if len(cat_same) > 0 and len(box_same) > 0 else (np.array([], dtype=np.int64), )
        for cat_same, box_same in zip(same_supercat, high_iou)
    ]
    kept = [
        {
            k: v[to_keep] if not isinstance(v, list) else np.array(v)[to_keep]
            for k, v in predictions.items()
        }
        for predictions, to_keep in zip(
            df['predictions'], objects_to_keep
        )
    ]

    df['predictions'] = kept


def rectify_predicted_bboxes(df):
    def select_rectified(i):
        out = df.loc[i]['predictions']
        out['boxes'] = rectified_bboxes[i]
        return out
        
    is_landscape, scale = zip(*[
        (img['width'] > img['height'], max(img['width'], img['height']) / 256)
        for img in df['image']
    ])
    offset = [
        (256 - (min(img['width'], img['height']) / scale[i])) // 2
        for i, img in enumerate(df['image'])
    ]
    pos_offset = [
        torch.tensor([[offset[i], 0, offset[i], 0]])
        if is_landscape[i]
        else torch.tensor([[0, offset[i], 0, offset[i]]])
        for i in range(len(is_landscape))
    ]
    rectified_bboxes = [
        (df.loc[i, 'predictions']['boxes'] - pos_offset[i]) * scale[i]
        for i in range(len(df))
    ]
    df['predictions'] = [select_rectified(i) for i in range(len(df))]


def estimate_likely_object(df):
    min_iou_thresh = 0.1
    likely_object_idx = [
        torch.argmax(pred['subject_iou'])
        if pred['subject_iou'].shape[0] > 0
            and max(pred['subject_iou']) > min_iou_thresh
        else None
        for pred in df.predictions
    ]
    df['likely_prediction'] = [
        {
            key: value[idx]
            for key, value in pred.items()
        }
        if idx is not None else None
        for pred, idx in zip(
            df['predictions'],
            likely_object_idx
        )
    ]


def estimate_single_action_object(df):
    """ Estimates a single action and object based on a sort of
        cost-minimization between annotated verbs and predicted objects.
    """
    def is_rideable(coco_cat):
        retval = is_vehicle(coco_cat) or coco_cat == 'elephant' or coco_cat == 'horse' or coco_cat == 'giraffe' or coco_cat == 'zebra' or coco_cat == 'skateboard' or coco_cat == 'surfboard' or coco_cat == 'snowboard' or coco_cat == 'skis' or coco_cat == 'cow'
        return retval

    def reasonable_to_ride(cat_a, cat_b):
        """ is it reasonable to say that cat_a is riding cat_b?
        """
        unreasonable = is_animal(cat_a) and \
                       is_animal(cat_b) and \
                       cat_a != 'bird' or \
                       cat_a == cat_b or \
                       cat_a == 'giraffe' or \
                       cat_a == 'elephant' or \
                       cat_a == 'cow' or \
                       cat_a == 'sheep'
        return not unreasonable

    def is_holdable(obj):
        return not coco_sub_super_map[obj] == 'appliance' and not coco_sub_super_map[obj] == 'furniture' and not (coco_sub_super_map[obj] == 'animal' and not (obj == 'dog' or obj == 'cat' or obj == 'bird')) and not coco_sub_super_map[obj] == 'vehicle'

    def is_above(loc_a, loc_b):
        """ Returns True if loc_a is above loc_b in image space
        """
        above = loc_a[1] < loc_b[1] + 10 
        nearby = abs(loc_a[0] - loc_b[0]) < 30
        return above and nearby

    def simple_search_and_update(df, action, action_found, is_sv=False, only_agents=True):
        is_action, action_found = simple_action_search(df, action, action_found, only_agents)
        simple_update_df(df, action, is_action, is_sv)
        return action_found

    def simple_action_search(df, action, action_found, only_agents=True):
        valid_object = is_agent if only_agents else lambda x: True
        is_action = [
            valid_object(subject)
            and (action in actions)
            and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
            )
        ]
        action_found = [
            True if acting else found
            for acting, found in zip(is_action, action_found)
        ]
        return is_action, action_found

    def simple_update_df(df, action, is_action, is_sv=False):
        df['single_action'] = [
            action if acting else single_action
            for acting, single_action in zip(
                is_action,
                df['single_action']
            )
        ]
        df['single_object'] = [
            obj['names']
            if acting and obj is not None and not is_sv else single_object
            for obj, acting, single_object in zip(
                df['likely_prediction'],
                is_action,
                df['single_object'],
            )
        ]

    # First, superclass relevant actions
    df['actions'] = list(map(
        lambda x:
            list({
                sub_to_super_action.get(action)
                if sub_to_super_action.get(action) else action
                for action in x
            }),
        df['actions']
    )) 

    # Next, eliminate some actions if they're not alone
    for action in collapsible_actions:
        alterable = [
            not_only and contains
            for not_only, contains in zip(
                map(lambda x: x != [action], df['actions']),
                map(lambda x: action in x, df['actions'])
            )
        ]
        _ = list(map(lambda x: x.remove(action), df['actions'][alterable]))

    subject_centers, object_centers = zip(*[
        (
            BBox(torch.tensor([annot['bbox']]), 'xywh').center,
            BBox(pred['boxes'].unsqueeze(0), 'xyxy').center
        )
        if pred is not None
            else (BBox(torch.tensor([annot['bbox']]), 'xywh').center, None)
        for annot, pred in zip(df['coco_annot'], df['likely_prediction'])
    ])

    # RIDING
    is_riding = [
        is_agent(subject)
        and is_rideable(obj['names'])
        and is_above(c_sub[0], c_obj[0])
        and ('riding' in actions or 'sitting' in actions or 'standing' in actions)
        and reasonable_to_ride(subject, obj['names'])
        if obj is not None and c_obj is not None else False
        for subject, obj, c_sub, c_obj, actions in zip(
            df['subject'],
            df['likely_prediction'],
            subject_centers,
            object_centers,
            df['actions']
        )
    ]
    df['single_action'] = [
        'riding' if riding else None for riding in is_riding
    ]
    df['single_object'] = [
        obj['names']
        if riding else None
        for obj, riding in zip(
            df['likely_prediction'],
            is_riding
        )
    ]
    df['single_action'] = [
        'riding'
        if ('skiing' in multi_actions or
            'snowboarding' in multi_actions or
            'skating' in multi_actions or
            'surfing' in multi_actions)
        else single_action
        for multi_actions, single_action in zip(
            df['actions'],
            df['single_action']
        )
    ]
    df['single_object'] = [
        'skis'
        if 'skiing' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    df['single_object'] = [
        'snowboard'
        if 'snowboarding' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    df['single_object'] = [
        'skateboard'
        if 'skating' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    df['single_object'] = [
        'surfboard'
        if 'surfing' in multi_actions else single_object
        for multi_actions, single_object in zip(
            df['actions'],
            df['single_object']
        )
    ]
    action_found = [True if action is not None else False for action in df['single_action']]

    # HOLDING
    is_holding = [
        is_agent(subject)
        and is_holdable(obj['names'])
        #and reasonable_to_hold(subject, obj['names'])
        and ('holding' in actions or 'waving' in actions or 'stretching' in actions or 'serving' in actions or 'reading' in actions or 'bending' in actions)
        and not found
        if obj is not None else False
        for subject, obj, found, actions in zip(
            df['subject'],
            df['likely_prediction'],
            action_found,
            df['actions'])
    ]
    df['single_action'] = [
        'holding' if holding else single_action
        for holding, single_action in zip(
            is_holding,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if holding else single_object
        for obj, holding, single_object in zip(
            df['likely_prediction'],
            is_holding,
            df['single_object']
        )
    ]
    action_found = [
        True if holding else found
        for holding, found in zip(is_holding, action_found)
    ]

    # PLAYING SPORTS
    is_playing_sports = [
        is_agent(subject)
        and ('playing' in actions or 'participating' in actions)

        and ('playingfield' in stuff or 'dirt' in stuff or 'grass' in stuff)
        and not found
        for subject, actions, stuff, found in zip(
            df['subject'],
            df['actions'],
            df['stuff_names'],
            action_found
        )
    ]
    df['single_action'] = [
        'playing_sports' if playing else single_action
        for playing, single_action in zip(
            is_playing_sports,
            df['single_action']
        )
    ]
    action_found = [
        True if playing else found
        for playing, found in zip(is_playing_sports, action_found)
    ]

    if False:
        is_standing = [
            is_agent(subject)
            and ('standing' in actions)
            and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
                )
            ]
        action_found = [
            True if standing else found
            for standing, found in zip(is_standing, action_found)
        ]

        is_sitting = [
            is_agent(subject)
            and ('sitting' in actions)
            and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
                )
            ]
        action_found = [
            True if sitting else found
            for sitting, found in zip(is_sitting, action_found)
        ]
        is_laying = [
            is_agent(subject)
            and ('laying' in actions)
            and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
                )
            ]
        action_found = [
            True if laying else found
            for laying, found in zip(is_laying, action_found)
        ]
        is_cuddling = [
            is_agent(subject)
            and ('cuddling' in actions)
            and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
                )
            ]
        action_found = [
            True if cuddling else found
            for cuddling, found in zip(is_cuddling, action_found)
        ]
        df['single_action'] = [
            'cuddling' if cuddling else single_action
            for cuddling, single_action in zip(
                is_cuddling,
                df['single_action']
            )
        ]
        df['single_object'] = [
            obj['names']
            if cuddling and obj is not None else single_object
            for obj, cuddling, single_object in zip(
                df['likely_prediction'],
                is_cuddling,
                df['single_object'],
            )
        ]
        is_sleeping = [
            is_agent(subject)
            and ('sleeping' in actions)
            and not found
            for subject, actions, found in zip(
                df['subject'],
                df['actions'],
                action_found
            )
        ]
        action_found = [
            True if sleeping else found
            for sleeping, found in zip(is_sleeping, action_found)
        ]
        df['single_action'] = [
            'sleeping' if sleeping else single_action
            for sleeping, single_action in zip(
                is_sleeping,
                df['single_action']
            )
        ]
        df['single_object'] = [
            obj['names']
            if sleeping and obj is not None else single_object
            for obj, sleeping, single_object in zip(
                df['likely_prediction'],
                is_sleeping,
                df['single_object'],
            )
        ]


    is_standing, action_found = simple_action_search(
        df,
        'standing',
        action_found
    )
    is_sitting, action_found = simple_action_search(
        df,
        'sitting',
        action_found
    )
    is_laying, action_found = simple_action_search(
        df,
        'laying',
        action_found
    )
    df['single_action'] = [
        'standing' if standing else single_action
        for standing, single_action in zip(
            is_standing,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if standing and obj is not None and is_above(c_sub[0], c_obj[0]) else single_object
        for obj, standing, single_object, c_sub, c_obj in zip(
            df['likely_prediction'],
            is_standing,
            df['single_object'],
            subject_centers,
            object_centers
        )
    ]
    df['single_action'] = [
        'sitting' if sitting else single_action
        for sitting, single_action in zip(
            is_sitting,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if sitting and obj is not None and is_above(c_sub[0], c_obj[0]) else single_object
        for obj, sitting, single_object, c_sub, c_obj in zip(
            df['likely_prediction'],
            is_sitting,
            df['single_object'],
            subject_centers,
            object_centers
        )
    ]

    action_found = simple_search_and_update(df, 'cooking', action_found, is_sv=True, only_agents=False)

    df['single_action'] = [
        'laying' if laying else single_action
        for laying, single_action in zip(
            is_laying,
            df['single_action']
        )
    ]
    df['single_object'] = [
        obj['names']
        if laying and obj is not None and is_above(c_sub[0], c_obj[0]) else single_object
        for obj, laying, single_object, c_sub, c_obj in zip(
            df['likely_prediction'],
            is_laying,
            df['single_object'],
            subject_centers,
            object_centers
        )
    ]

    action_found = simple_search_and_update(df, 'cuddling', action_found)
    action_found = simple_search_and_update(df, 'sleeping', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'eating', action_found)
    action_found = simple_search_and_update(df, 'drinking', action_found)
    action_found = simple_search_and_update(df, 'running', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'walking', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'smelling', action_found)
    action_found = simple_search_and_update(df, 'waiting', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'smiling', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'talking', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'watching', action_found, is_sv=True)
    action_found = simple_search_and_update(df, 'moving', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'sitting', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'laying', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'driving', action_found, is_sv=True, only_agents=False)
    action_found = simple_search_and_update(df, 'holding', action_found, is_sv=True, only_agents=False)



def is_vehicle(coco_cat):
    return coco_sub_super_map[coco_cat] == 'vehicle'

def is_animal(coco_cat):
    return coco_sub_super_map[coco_cat] == 'animal'


def is_agent(coco_cat):
    return coco_cat == 'person' or coco_sub_super_map[coco_cat] == 'animal'


def action_object_heuristics(df):
    # People holding things
    import ipdb
    with ipdb.launch_ipdb_on_exception():
        has_action = action_present(df, 'holding')
        has_action_and_subject = has_action[has_action.subject == 'person']
        has_likely_objects = [
            max(pred['subject_iou']) > 0.2
            if pred['subject_iou'].shape[0] > 0 else False
            for pred in has_action_and_subject.predictions
        ]
        likely_objects = [
            pred['names'][torch.argmax(pred['subject_iou'])]
            if has_likely_objects[i] else None
            for i, pred in enumerate(has_action_and_subject.predictions)
        ]
        

    return [(i, obj) for i, obj in enumerate(likely_objects) if obj]
    


def print_subject_actions_objects(df, loc=0, i=None):
    entry = df.loc[loc] if i is None else df.iloc[i]
    print(entry['subject'])
    print(entry['actions'])
    print(entry['predictions']['names'])
    print(entry['predictions']['subject_iou'])


def get_action_counts(df):
    return {action: len(action_present(df, action)) for action in get_unique_actions(df)}


def object_heuristics(df_entry):
    def action_is(action):
        return action in df_entry['actions']

    def subject_is(sub):
        return df_entry['subject'] == sub

    def subject_is_agent():
        return coco_sub_super_map[df_entry['subject']] == 'animal' or df_entry['subject'] == 'person'

    if action_is('playing'):
        if subject_is('person'):
            if 'playingfield' in df_entry['stuff_names'] and 'sand' in df_entry['stuff_names']:
                obj = 'baseball'

    if action_is('holding'):
        if subject_is_agent():
            # XYWH
            subject_bbox = BBox(df_entry['coco_annot']['bbox'], 'xywh')
            # XYWH
            stuff_bboxes = BBox(df_entry['stuff_bboxes'], 'xywh')
            # TODO location heuristics
            # TODO I probably need to run RCNN

    if action_is('landing'):
        if subject_is('bird'):
            obj = None

    if action_is('riding') or action_is('driving'):
        if subject_is('person'):
            subject_bbox = BBox(df_entry['coco_annot']['bbox'], 'xywh')
            # TODO RCNN

    if action_is('playing') or action_is('moving'):
        if subject_is_agent():
            subject_bbox = BBox(df_entry['coco_annot']['bbox'], 'xywh')
            # TODO RCNN

    if action_is('holding'):
        if subject_is_agent():
            subject_bbox = BBox(df_entry['coco_annot']['bbox'], 'xywh')
            # TODO RCNN



    return obj


# Lookup table for sub-actions -> super-actions
sub_to_super_action = {
    sub: super_action
    for super_action, subs in super_actions.items()
    for sub in subs
}


def get_image_urls(df):
    urls = [img['coco_url'] for img in df['image']]
    return urls


def get_unique_actions(df):
    """ Gets unique actions in dataframe
    """
    uniq_acts = {action for actions in df['actions'] for action in actions}
    return uniq_acts


def action_present(df, action):
    """ Returns view of dataframe with specified action present
    """
    return df[list(map(lambda x: action in x, df['actions']))]


def collapse_actions_(df, collapsible=collapsible_actions, super_map=sub_to_super_action):
    """ Collapses actions inplace
        Collapsing means superclassing some actions and eliminating a set of
        actions from the dataset, unless they are the only action present
    """
    # First, superclass relevant actions
    df['actions'] = list(map(
        lambda x:
            list({
                super_map.get(action)
                if super_map.get(action) else action
                for action in x
            }),
        df['actions']
    )) 

    # Then, process all actions that supercede everything else:
    for action in supercedes_all:
        df['actions'] = list(map(lambda x: [action] if action in x else x, df['actions']))

    # Then, ignore certain actions:
    # Only keep actions if not superceded by anything, or if superceding action
    # is not present in image
    df['actions'] = list(map(
        lambda x:
            list({
                action for action in x
                if not superceded_by.get(action)
                or not superceded_by[action].intersection(x)
            }),
        df['actions']
    ))

    # Similarly:
    # Keep actions that supercede others
    df['actions'] = list(map(
        lambda x:
            list({
                action for action in x
                if not supercedes.get(action)
                or supercedes[action].intersection(x)
            }),
        df['actions']
    ))


    # Next, eliminate some actions if they're not alone
    for action in collapsible_actions:
        alterable = [
            not_only and contains
            for not_only, contains in zip(
                map(lambda x: x != [action], df['actions']),
                map(lambda x: action in x, df['actions'])
            )
        ]
        _ = list(map(lambda x: x.remove(action), df['actions'][alterable]))


def get_single_fpath(df, index=0, data_dir=data_dir, i=None):
    entry = df.loc[index] if i is None else df.iloc[i]
    fpath = os.path.join(
        data_dir,
        'coco_attr',
        entry['split'],
        entry['image']['file_name']
    )
    return fpath


def draw_image_with_stuff(df, index=0, i=None):
    entry = df.loc[index] if i is None else df.iloc[i]
    import ipdb
    with ipdb.launch_ipdb_on_exception():
        fpath = os.path.join(
            data_dir,
            'coco_attr',
            entry['split'],
            entry['image']['file_name']
        )
        img = AnnotatedImage(fpath=fpath)

        stuff_bboxes = BBox(entry['stuff_bboxes'], 'xywh').to_xyxy()
        img.draw_bboxes(stuff_bboxes)
        img.draw_texts(stuff_bboxes[:, :2].tolist(), entry['stuff_names'])

        thing_bbox = BBox([entry['coco_annot']['bbox']], 'xywh').to_xyxy()
        img.draw_bboxes(thing_bbox, color='red')

        img.draw_texts(thing_bbox[:, :2].tolist(), [entry['subject']], color='red')

        orig_img_shape = {dim: entry['image'][dim] for dim in ('height', 'width')}
        
        object_bboxes = entry['predictions']['boxes']
        img.draw_bboxes(object_bboxes, color='blue')
        img.draw_texts(object_bboxes[:, :2].tolist(),
                       [name for name in entry['predictions']['names']],
                       color='blue')

    return img


if __name__ == "__main__":
    df = pd.read_pickle(df_fpath)
