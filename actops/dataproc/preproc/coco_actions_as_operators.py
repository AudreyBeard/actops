# NOTE:
# This dataset is constructed based on the assumption that a single action has
# been determined for each object, as in `coco/clean_combined.py`, which in
# turn relies on `coco/propose_objects.py`. If you haven't already run those
# files, this WILL NOT WORK

from __future__ import absolute_import

import os

import pandas as pd
import ipdb
import torch

from utils import Cache
import utils

DEFAULT_DPATH = os.path.expandvars("$HOME/data")
# Filename for COCO actions.
DEFAULT_DF_FNAME = 'coco-actions-df_predicted_single-estimated.pkl'

# For easier use in interactive session
df_fpath = os.path.join(DEFAULT_DPATH,
                        DEFAULT_DF_FNAME)


def add_attrops_split_to_df(df, df_fpath=df_fpath):
    """ Run this to add Attributes as Operators-style disjoint-pair split
    """
    stats = get_dset_stats(df)
    disjoint_sets, n_each = create_disjoint_subject_verb_sets(stats['sv_permutation_counts'])
    for split, sv_counts in disjoint_sets.items():
        for sv, count in sv_counts.items():
            subject, verb = sv.split('-')
            df.loc[(df.subject == subject) & (df.single_action == verb), 'attrops_split'] = split
    new_df_fpath = "_attrops-split".join(os.path.splitext(df_fpath))
    df.to_pickle(new_df_fpath)


def add_svo_split_to_df(df,
                        df_fpath='coco-actions-df_predicted_single-estimated_attrops-split.csv'):
    """ Run this to add Attributes as Operators-style disjoint-pair split
    """
    # Create larger crops which include subject and object
    unzipped = [
        [
            float(t)
            for t in coord
        ]
        for coord in zip(*merge_subject_object_bboxes(df))
    ]
    for i, coord in ('crop_x1', 'crop_y1', 'crop_x2', 'crop_y2'):
        df.loc[:, coord] = unzipped[i]

    stats = get_dset_stats(df)
    disjoint_sets, n_each = create_disjoint_svo_sets(stats['svo_permutation_counts'])
    for split, svo_counts in disjoint_sets.items():
        for svo, count in svo_counts.items():
            subject, verb, obj = svo.split('-')
            if obj == 'None':
                df.loc[(df.subject == subject) & (df.single_action == verb) & (df.single_object.isna()), 'svo_split'] = split
            else:
                df.loc[(df.subject == subject) & (df.single_action == verb) & (df.single_object == obj), 'svo_split'] = split
    new_df_fpath = "_svo-split".join(os.path.splitext(df_fpath))
    df.to_pickle(new_df_fpath)


def create_disjoint_subject_verb_sets(sv_permutation_counts, train_frac=0.7,
                                      split_options=['train', 'test']):
    """ Create train and test sets such that the (s, v) pairs of each are
        disjoint, though in general the subjects and verbs are not disjoint
        Parameters:
            sv_permutation_counts (dict): keys are subject-verb strings and
                values are number of samples
            split_frac (int): The rough proportion of samples to be labeled split_options[0]
    """
    # First sort by count in descending order
    sorted_by_count = [
        (k, sv_permutation_counts[k])
        for k in sorted(sv_permutation_counts,
                        key=lambda x: sv_permutation_counts[x],
                        reverse=True)
    ]

    # Split_options are probably 'train' and 'test', but could be specified
    # another way
    splits = {s: dict() for s in split_options}
    ratio = train_frac / (1 - train_frac)

    # Largest class always goes to split 0
    this_split = split_options[0]
    for i, (sv, count) in enumerate(sorted_by_count):
        splits[this_split][sv] = count

        # Second-largest goes to split 1 - this biases the second split towards
        # a few high-representation classes as opposed to many
        # low-representation classes - Also biases to a larger second split
        if len(splits[split_options[1]]) == 0:
            this_split = split_options[1]

        # If there are too few in split 0, add there next
        elif len(splits[split_options[0]]) / len(splits[split_options[1]]) < ratio:
            this_split = split_options[0]
        else:
            this_split = split_options[1]

    n_each_set = {k: sum([count for count in splits[k].values()]) for k in splits.keys()}
    return splits, n_each_set


def create_disjoint_svo_sets(svo_permutation_counts, train_frac=0.7,
                             split_options=['train', 'test']):
    """ Create train and test sets such that the (s, v, o) triplets of each are
        disjoint, though in general the subjects, verbs, and objects are not disjoint
        Parameters:
            svo_permutation_counts (dict): keys are subject-verb-object strings and
                values are number of samples
            split_frac (int): The rough proportion of samples to be labeled split_options[0]
            split_options (list of strings): names of splits
    """
    # First sort by count in descending order
    sorted_by_count = [
        (k, svo_permutation_counts[k])
        for k in sorted(svo_permutation_counts,
                        key=lambda x: svo_permutation_counts[x],
                        reverse=True)
    ]

    # Split_options are probably 'train' and 'test', but could be specified
    # another way
    splits = {s: dict() for s in split_options}
    ratio = train_frac / (1 - train_frac)

    # Largest class always goes to split 0
    this_split = split_options[0]
    for i, (svo, count) in enumerate(sorted_by_count):
        splits[this_split][svo] = count

        # Second-largest goes to split 1 - this biases the second split towards
        # a few high-representation classes as opposed to many
        # low-representation classes - Also biases to a larger second split
        if len(splits[split_options[1]]) == 0:
            this_split = split_options[1]

        # If there are too few in split 0, add there next
        elif len(splits[split_options[0]]) / len(splits[split_options[1]]) < ratio:
            this_split = split_options[0]
        else:
            this_split = split_options[1]

    n_each_set = {k: sum([count for count in splits[k].values()]) for k in splits.keys()}
    return splits, n_each_set


def tryload_svo_permutation_counts(df, fname='svo_permutation_counts_dict.pkl'):
    """ Loads SVO permutation counts from cache if it exists, computes a new
        one and caches it if it doesn't
    """
    cache = Cache()
    print("Trying to find a precomputed SVO permutation counts dictionary...")
    if not cache.exists(fname):
        print("* Not found! - Computing now, this will take a while...")
        svo_permutation_counts = {
            "{}-{}-{}".format(subject, verb, obj):
                len(df[(df.subject == subject) & (df.single_action == verb) & (df.single_object == obj)])
                if obj is not None else
                len(df[(df.subject == subject) & (df.single_action == verb) & (df.single_object.isna())])
                for subject in df.subject.unique()
                for verb in df.single_action.unique()
                for obj in df.single_object.unique()
        }
        svo_permutation_counts = {
            k: v
            for k, v in svo_permutation_counts.items()
            if v > 0
        }
        print("* Done! - writing to disk and moving on")
        cache.pickle(svo_permutation_counts, fname)

    else:
        print("* Found! - reading from disk and moving on")
        svo_permutation_counts = cache.unpickle(fname)
    return svo_permutation_counts


def get_dset_stats(df=None, svo_permutation_counts=None):
    permutation_count_fname = "svo_permutation_counts_dict.pkl"

    if svo_permutation_counts is None and df is not None:
        svo_permutation_counts = tryload_svo_permutation_counts(
            df,
            fname=permutation_count_fname
        )

    # Grab unique subjects, verbs, and objects
    subjects, verbs, objects = (
        set(tup)
        for tup in zip(*[
            k.split('-')
            for k in svo_permutation_counts.keys()
        ])
    )
    sv_permutations = set(['-'.join(k.split('-')[:2]) for k in svo_permutation_counts.keys()])
    sv_permutation_counts = {
        sv: sum([
            n
            for svo, n in svo_permutation_counts.items()
            if svo.startswith(sv)
        ])
        for sv in sv_permutations
    }
    subject_counts = {
        sub: sum([
            v
            for k, v in svo_permutation_counts.items()
            if k.startswith(sub)
        ])
        for sub in subjects
    }
    verb_counts = {
        verb: sum([
            v
            for k, v in svo_permutation_counts.items()
            if k.split('-')[1] == verb
        ])
        for verb in verbs
    }
    object_counts = {
        obj: sum([
            v
            for k, v in svo_permutation_counts.items()
            if k.endswith(obj)
        ])
        for obj in objects
    }
    retval = {
        'svo_permutation_counts': svo_permutation_counts,
        'subject_counts': subject_counts,
        'verb_counts': verb_counts,
        'object_counts': object_counts,
        'sv_permutation_counts': sv_permutation_counts,
    }
    return retval


def merge_subject_object_bboxes(df):
    cache = utils.Cache('~/.cache/cocoactions')
    merged_bboxes = cache.unpickle('merged_subject_object_bboxes_xyxy.pkl')
    if merged_bboxes is None:
        sub_bboxes = cache.unpickle('subject_bboxes_xyxy.pkl')
        obj_bboxes = cache.unpickle('object_bboxes_xyxy.pkl')
        with ipdb.launch_ipdb_on_exception():
            if sub_bboxes is None:
                sub_bboxes = list(map(
                    lambda x: utils.BBox(torch.tensor(x), 'xywh').xyxy,
                    [entry['coco_annot']['bbox'] for i, entry in df.iterrows()])
                )
                cache.pickle(sub_bboxes, 'subject_bboxes_xyxy.pkl')

            if obj_bboxes is None:
                obj_bboxes = list(map(
                    lambda x: None if x is None else utils.BBox(x['boxes'], 'xyxy').xyxy,
                    [entry['likely_prediction'] for i, entry in df.iterrows()])
                )
                cache.pickle(obj_bboxes, 'object_bboxes_xyxy.pkl')
            merged_bboxes = list(map(
                lambda x: x[0] if x[1] is None else utils.BBox.merge(x[0], x[1]).xyxy[0],
                zip(sub_bboxes, obj_bboxes))
            )
            cache.pickle(merged_bboxes, 'merged_subject_object_bboxes_xyxy.pkl')
    return merged_bboxes


if __name__ == "__main__":
    df = pd.read_pickle(df_fpath)
    add_attrops_split_to_df(df, df_fpath=df_fpath)
    add_svo_split_to_df(df)
