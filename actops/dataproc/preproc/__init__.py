import sys
import os

root_dir = os.path.expandvars("$HOME/otto/research/actions_as_operators/code")
if root_dir not in sys.path:
    sys.path.append(root_dir)
