import os

import torch
from torchvision.models.detection import fasterrcnn_resnet50_fpn
from torchvision import transforms
import ipdb
#from PIL import Image, ImageDraw
import numpy as np

from annotate_moments import MomentsInTimeDataset
from utils import AnnotatedImage, write_mp4, object_heuristics, valid_video
from utils import COCO_INSTANCE_CATEGORY_NAMES as coco_names
from utils import MIT_SUPERCLASSES, COCO_SUPER_CATEGORY_NAMES
import utils
#from moments.models import load_transforms

DSET_FPS = 30
DSET_VID_TIME = 3  # Seconds
data_dir = os.path.expandvars("$HOME/data/Moments_in_Time_256x256_30fps")
n_workers = 1
n_samples = 10
batch_size = 1
reduction = 2
n_frames = DSET_FPS * DSET_VID_TIME // reduction
#n_frames = 32

#transforms = load_transforms()
trans = transforms.ToTensor()
dset = MomentsInTimeDataset(dpath=data_dir,
                            split='train',
                            transforms=trans,
                            n_frames=n_frames,
                            max_returned=n_samples)

device = torch.device(0)

model = fasterrcnn_resnet50_fpn(pretrained=True, min_size=256)
model.eval()
model = model.cuda(device)

loader = torch.utils.data.DataLoader(dset,
                                     num_workers=n_workers,
                                     batch_size=batch_size,
                                     drop_last=True,
                                     shuffle=False)


def load_one(raw=False):
    assert batch_size == 1
    for i, data in enumerate(loader):
        inputs = data['data'].to(device)
        action = data['label']
        inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
        outputs = model(inputs)
        break
    if raw:
        return data, outputs, action
    else:

        return inputs, outputs, action


def get_confident_outputs(outputs, min_conf=0.7, max_bbox_size=0.8):
    def pred_conf(outputs, confident_indices, key):
        return [outputs[i][key][confident_indices[i]]
                for i in range(len(outputs))]

    # First, filter out detections that are low confidence
    idx_conf = [np.where(pred['scores'].cpu() > min_conf) for pred in outputs]

    # If nothing passed confidence threshold, grab best candidate (as long as
    # something was detected and its confidence is above some lower-threshold minimum

    empty = (np.array([], dtype=np.int64), )
    idx_conf = [idx
                if len(idx[0]) > 0 or len(outputs[j]['scores']) == 0
                else (np.array([0]), ) if outputs[j]['scores'][0] > 0.3 else empty
                for j, idx in enumerate(idx_conf)]

    conf = {
        'scores': pred_conf(outputs, idx_conf, 'scores'),
        'labels': pred_conf(outputs, idx_conf, 'labels'),
        'boxes': pred_conf(outputs, idx_conf, 'boxes'),
    }

    return conf


def generate_annotated_images(inputs, outputs, action, min_conf):
    def get_object_texts(labels_tensor, scores):
        return ['{} {:.2f}'.format(coco_names[label], score)
                for label, score in zip(labels_tensor.detach().cpu().numpy(),
                                        scores.detach().cpu().numpy())]

    def get_object_xys(boxes_tensor):
        return [(boxes_tensor[i, 0].item(), boxes_tensor[i, 1].item())
                for i in range(boxes_tensor.shape[0])]

    def get_action_xys(boxes_tensor):
        return [(boxes_tensor[i, 0].item(), boxes_tensor[i, 1].item() + 10)
                for i in range(boxes_tensor.shape[0])]

    annot_imgs = []
    for i in range(inputs.shape[0]):
        object_texts = get_object_texts(outputs['labels'][i], outputs['scores'][i])
        object_xys = get_object_xys(outputs['boxes'][i])
        action_texts = [action[0] for xy in object_xys]
        action_xys = get_action_xys(outputs['boxes'][i])

        annot_img = AnnotatedImage(inputs[i, ...])

        annot_img.draw_bboxes(outputs['boxes'][i])
        annot_img.draw_texts(object_xys, object_texts)
        annot_img.draw_texts(action_xys, action_texts)
        annot_imgs.append(annot_img)

    return annot_imgs


def make_annotated_video(inputs, outputs, action, super_categories=False):
    def get_object_texts(labels_tensor, scores, super_categories=False):
        if super_categories:
            object_texts = ['{} {:.2f}'.format(COCO_SUPER_CATEGORY_NAMES[label], score)
                            for label, score
                            in zip(labels_tensor.detach().cpu().numpy(),
                                   scores.detach().cpu().numpy())]
        else:
            object_texts = ['{} {:.2f}'.format(coco_names[label], score)
                            for label, score
                            in zip(labels_tensor.detach().cpu().numpy(),
                                   scores.detach().cpu().numpy())]

        return object_texts

    def get_object_xys(boxes_tensor):
        return [(boxes_tensor[i, 0].item(), boxes_tensor[i, 1].item())
                for i in range(boxes_tensor.shape[0])]

    def get_action_xys(boxes_tensor):
        return [(boxes_tensor[i, 0].item(), boxes_tensor[i, 1].item() + 10)
                for i in range(boxes_tensor.shape[0])]

    annot_imgs = []
    for i in range(inputs.shape[0]):
        object_texts = get_object_texts(outputs[i]['labels'],
                                        outputs[i]['scores'],
                                        super_categories=super_categories)
        object_xys = get_object_xys(outputs[i]['boxes'])
        action_texts = [action for xy in object_xys]
        action_xys = get_action_xys(outputs[i]['boxes'])

        annot_img = AnnotatedImage(inputs[i, ...])

        annot_img.draw_bboxes(outputs[i]['boxes'])
        annot_img.draw_texts(object_xys, object_texts)
        annot_img.draw_texts(action_xys, action_texts)
        annot_imgs.append(annot_img)

    return annot_imgs


def run_predictions(loader, model, fps=None):
    if fps is None:
        fps = n_frames / 3

    with ipdb.launch_ipdb_on_exception():
        for i, data in enumerate(loader):
            print("Batch {}".format(i), end='')
            with torch.no_grad():
                print(" - Processing data", end='', flush=True)
                inputs = data['data'].to(device)
                action = data['label']
                inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
                outputs = model(inputs)

            outputs = object_heuristics(inputs, outputs)
            print(" - Generating annotated videos", end='', flush=True)
            for j in range(batch_size):
                annot_imgs = generate_annotated_images(
                    inputs[j * n_frames:(j + 1) * n_frames, ...],
                    outputs[j * n_frames:(j + 1) * n_frames],
                    action[j:j + 1],
                    0.7
                )
                name = "annotated_videos/{}_{}.mp4".format(action[j], i)
                print("  writing {}".format(name), flush=True)
                write_mp4(annot_imgs,
                          name=name,
                          fps=fps)

        print("DONE")


def sample_all_actions(dset, loader, model, n_samples, startfrom=None, endat=None):
    dset.max_returned = n_samples
    actions = dset.actions
    with ipdb.launch_ipdb_on_exception():
        if startfrom:
            i = 0
            found = False
            while not found:
                if actions[i].startswith(startfrom):
                    found = True
                else:
                    i += 1
            actions = actions[i:]

        if endat:
            i = 0
            found = False
            while not found:
                if actions[i].startswith(endat):
                    found = True
                else:
                    i += 1
            actions = actions[:i]

    for action in actions:
        dset.subset(action)
        run_predictions(loader, model, fps=n_frames)


def action_demo(dset, loader, model, action_superclass, n_samples=30, start_idx=0, skip=[]):
    fps = n_frames // 2
    dset.max_returned = n_samples * 2 + start_idx
    demo_video = []
    n_skipped = 0
    for i, data in enumerate(loader):
        if i < start_idx or (i - start_idx) in skip:
            n_skipped += 1
            continue

        print("Batch {}".format(i), end='')
        with torch.no_grad():
            print(" - Processing data", end='', flush=True)
            inputs = data['data'].to(device)
            action = data['label']
            inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
            outputs = model(inputs)

        # Heuristics
        outputs = object_heuristics(inputs, outputs, action[0])
        if max([len(out) for out in outputs]) == 0:
            print("Found no objects - moving on...", flush=True)
            continue

        if not valid_video(inputs):
            print("\nFound invalid video")
            n_skipped += 1
            continue

        print(" - Generating annotated subvideo", flush=True)
        for j in range(batch_size):
            video = make_annotated_video(
                inputs[j * n_frames:(j + 1) * n_frames, ...],
                outputs[j * n_frames:(j + 1) * n_frames],
                action_superclass,
            )
            demo_video.extend(video)

        # Once we've found enough, we're done
        if i >= n_samples + n_skipped:
            break

    name = "demos/{}_super.mp4".format(action_superclass)
    print("Writing {}".format(name), flush=True)
    write_mp4(demo_video,
              name=name,
              fps=fps)


def aggregate_actions(dset, loader, model, n_samples):
    fps = n_frames // 3
    dset.max_returned = n_samples
    for superclass, actions_list in MIT_SUPERCLASSES.items():
        dset.subset(actions_list)

        aggregated_video = []
        for i, data in enumerate(loader):
            print("Batch {}".format(i), end='')
            with torch.no_grad():
                print(" - Processing data", end='', flush=True)
                inputs = data['data'].to(device)
                action = data['label']
                inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
                outputs = model(inputs)

            outputs = object_heuristics(outputs)
            print(" - Generating annotated subvideo", flush=True)
            for j in range(batch_size):
                annot_imgs = generate_annotated_images(
                    inputs[j * n_frames:(j + 1) * n_frames, ...],
                    outputs[j * n_frames:(j + 1) * n_frames],
                    action[j:j + 1],
                    0.7
                )
                aggregated_video.extend(annot_imgs)

        name = "demos/{}_{}.mp4".format(superclass, "_".join(actions_list))
        print("Writing {}".format(name), flush=True)
        write_mp4(aggregated_video,
                  name=name,
                  fps=fps)


def create_superclass_annotated(dset, loader, model, super_action, n_samples=30):
    fps = n_frames // 6

    demo_video = []
    dset.subset(MIT_SUPERCLASSES[super_action])
    dset.max_returned = None
    n_skipped = 0
    for i, data in enumerate(loader):

        print("Batch {}".format(i), end='')
        with torch.no_grad():
            print(" - Processing data", end='', flush=True)
            inputs = data['data'].to(device)
            action = data['label']
            inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
            outputs = model(inputs)

        # Heuristics
        outputs = object_heuristics(inputs, outputs)
        if max([len(out) for out in outputs]) == 0:
            print("Found no objects - moving on...", flush=True)
            n_skipped += 1
            continue

        if not valid_video(inputs, verbose=True):
            print("\nFound invalid video")
            n_skipped += 1
            continue

        print(" - Generating annotated subvideo", flush=True)
        for j in range(batch_size):
            video = make_annotated_video(
                inputs[j * n_frames:(j + 1) * n_frames, ...],
                outputs[j * n_frames:(j + 1) * n_frames],
                super_action,
                super_categories=True,
            )
            demo_video.extend(video)

        # Once we've found enough, we're done
        if i >= n_samples + n_skipped:
            break

    name = "annotated_videos/super_{}.mp4".format(super_action)
    write_mp4(demo_video,
              name=name,
              fps=fps,
              verbose=True)
    print("DONE")


def demo_heuristics(dset, loader, model, n_samples=30, super_action_dict=None):
    fps = n_frames // 2
    dset.max_returned = None
    if super_action_dict is None:
        super_action_dict = MIT_SUPERCLASSES

    for super_action, sub_actions in super_action_dict.items():
        n_skipped = 0
        agg_video = []
        dset.subset(sub_actions)

        for i, data in enumerate(loader):
            print("Batch {}".format(i), end='')
            with torch.no_grad():
                print(" - Processing data", end='', flush=True)
                inputs = data['data'].to(device)
                action = data['label']
                inputs = inputs.permute(0, 2, 1, 3, 4).contiguous().reshape(-1, 3, 256, 256)
                outputs = model(inputs)

            # Heuristics
            with ipdb.launch_ipdb_on_exception():
                outputs = object_heuristics(inputs, outputs)

            if max([len(out['scores']) for out in outputs]) == 0:
                print("Found no objects - moving on...", flush=True)
                n_skipped += 1
                continue

            #if not valid_video(inputs, verbose=True):
            #    print("\nFound invalid video")
            #    n_skipped += 1
            #    continue

            with ipdb.launch_ipdb_on_exception():
                video_object = utils.rectify_object_annotation(outputs,
                                                               super_action,
                                                               lambda_mean=1,
                                                               lambda_freq=0.5)
            for j in range(batch_size):
                video = make_annotated_video(
                    inputs[j * n_frames:(j + 1) * n_frames, ...],
                    outputs[j * n_frames:(j + 1) * n_frames],
                    super_action,
                )
                agg_video.extend(video)

            name = "annotated_videos/heuristics/{}_{}_{}.mp4".format(super_action,
                                                                     COCO_SUPER_CATEGORY_NAMES[video_object],
                                                                     i)
            write_mp4(video,
                      name=name,
                      fps=fps//3,
                      verbose=True)

            if i >= n_samples + n_skipped:
                break
        name = "annotated_videos/heuristics/{}_{}-aggregated.mp4".format(super_action,
                                                                         n_samples)
        write_mp4(agg_video,
                  name=name,
                  fps=fps,
                  verbose=True)

    print("DONE")


if __name__ == "__main__":
    run_predictions(loader, model)
