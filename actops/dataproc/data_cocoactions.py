import torch
from torch import nn
import pandas as pd
import torchvision as tv
import torchvision.models as tmodels
import tqdm
from PIL import Image
import ipdb
import numpy as np
#from memory_profiler import profile

from actops.external.attributes_as_operators.utils.utils import chunks
from actops import utils


DEFAULT_DPATH = utils.path("$HOME/data")
# Filename for COCO actions.
DEFAULT_DF_FNAME = 'coco-actions-df_predicted_single-estimated.pkl'

__all__ = [
    "COCOActionsCompositionDataset",
    "COCOActionsCompositionDatasetActivations",
    "COCOActionsCompositionDatasetActivationsEmbeddings",
    "COCOActionsSVODataset",
    "COCOActionsSVODatasetActivations",
    "COCOActionsSVODatasetActivationsTriplet",
    "COCOActionsSVODatasetActivationsEmbeddings",
    "save_sample",
]


class COCOActionsCompositionDataset(torch.utils.data.Dataset):
    def __init__(self, dpath=DEFAULT_DPATH, split='train',
                 transforms=None, reset=False, pre_transform_size=256,
                 df_fname='coco-actions-df_predicted_single-estimated_attrops-split.pkl',
                 attrops_compliance=True):

        self.cache = utils.Cache('~/.cache/attrops')
        self.dpath = utils.path(dpath)
        self.df_fpath = utils.pathjoin(self.dpath, df_fname)
        self.split = split

        self.data = self.cache.unpickle(self.__class__.__name__ + '.pkl')
        if self.data is None:
            try:
                print("  Loading dataset DataFrame from\n    {}\n  ...".format(self.df_fpath), end='')
                self.df = pd.read_pickle(self.df_fpath)
                print("DONE")
            except FileNotFoundError as e:
                print("Could not find DataFrame file:\n  {}".format(self.df_fpath))
                print("Did you already detect objects and use those to determine a \
                       single action for each sample, then split into the correct \
                       disjoint-pair-set splits?")
                raise e("{}".format(self.df_fpath))

        # Ignore all samples that don't have an action assigned to them
        self.df = self.df[self.df.single_action.notna()]

        # Construct lookup table for sequential index to DataFrame Index
        # Slower on the frontend, but fast indexing and relatively low memory
        # footprint
        self._split_col = 'attrops_split'
        self._split_mapping = {'train': 'train', 'test': 'test'}
        #split_col = 'split'
        #split_mapping = {'train': 'train2014', 'val': 'val2014'}
        self.idx_lookup = {
            i: k
            for i, k in enumerate(self.df_split(split).index)
        }

        # Pre-transforms resize the image a letterbox it so that it's a YxY
        # square padded with black
        self.pre_transforms = utils.ResizedLetterBox(pre_transform_size) if pre_transform_size else lambda x: x
        self.transforms = tv.transforms.ToTensor() if transforms is None else transforms

        if attrops_compliance:
            print("  Ensuring AttrOps compliance")
            self.ensure_attrops_compliance()

        ipdb.set_trace()
        self.data = {
            'subject': list(self.df['subject']),
            'verb': list(self.df['single_action']),
            'object': list(self.df['single_object']),
            'fpath': [
                utils.pathjoin(
                    self.dpath,
                    'coco',
                    self._parent_dir(entry.image['file_name']),
                    entry.image['file_name']
                )
                for _, entry in self.df.iterrows()
            ],
            'split': [
                'test' if (entry['single_action'], entry['subject']) in self.test_pairs
                else 'train'
                for _, entry in self.df.iterrows()
            ],
        }
        self.cache.pickle(self.data, self.__class__.__name__ + '.pkl')

        print("  Done initializing {}".format(self.__class__.__name__))

    def create_and_pickle_affordances(self, prefix='actops'):
        self.obj_affordance = dict()
        self.train_obj_affordance = dict()
        for subject_i in self.objs:
            self.obj_affordance[subject_i] = [
                verb
                for verb, subject_j in self.pairs
                if subject_j == subject_i
            ]

            self.train_obj_affordance[subject_i] = [
                verb
                for verb, subject_j in self.train_pairs
                if subject_j == subject_i
            ]
        self.cache.pickle(
            self.obj_affordance,
            prefix + '_obj_affordance.pkl'
        )
        self.cache.pickle(
            self.train_obj_affordance,
            prefix + '_train_obj_affordance.pkl'
        )

    def sample_negative(self, attr, obj):
        new_attr, new_obj = (attr, obj)
        new_attr, new_obj = list(self.train_pairs)[np.random.choice(len(self.train_pairs))]
        if new_attr == attr and new_obj == obj:
            new_attr, new_obj = list(self.train_pairs)[np.random.choice(len(self.train_pairs))]
        return (self.attr2idx[new_attr], self.obj2idx[new_obj])

    def sample_train_affordance(self, attr, obj):
        max_count = 10
        count = 0
        new_attr = attr
        while new_attr == attr and count < max_count:
            count += 1
            new_attr = np.random.choice(self.train_obj_affordance[obj])
        return self.attr2idx[new_attr]

    def sample_affordance(self, attr, obj):
        new_attr = np.random.choice(self.obj_affordance[obj])
        if new_attr == attr:
            return self.sample_affordance(attr, obj)
        return self.attr2idx[new_attr]

    def df_split(self, split):
        """ Lazy way of getting dataframe entries for the split
        """
        split_col = 'attrops_split'
        split_mapping = {'train': 'train', 'test': 'test'}

        return self.df.loc[self.df[split_col] == split_mapping[split]]

    def __len__(self):
        """ Needed for DataLoader
            Only counts those of the specified split for proper use in the
            DataLoader
        """
        return len(self.idx_lookup)

    @property
    def train_data(self):
        """ Lazy way of getting the train_data attribute, as AttrOps requires it
        """
        return self._train_or_test_data('train')

    @property
    def test_data(self):
        """ Lazy way of getting the test_data attribute, as AttrOps requires it
        """
        return self._train_or_test_data('test')

    def _train_or_test_data(self, split):
        """ Lazy compiler of (img_path, attr, obj), called by test_data and train_data
        """
        data = [
            (
                utils.pathjoin(self.dpath,
                               'coco',
                               entry['split'],
                               entry['image']['file_name']),
                entry['single_action'],
                entry['subject']
            )
            for i, entry in self.df_split(split).iterrows()
        ]
        return data

    def load_img(self, fpath):
        img = Image.open(fpath)

        # convert() performs a copy even if correct mode, so avoid if possible
        img = img.convert('RGB') if img.mode != 'RGB' else img
        img = self.transforms(img)
        return img

    def __getitem__(self, idx):
        """ For DataLoader iterator
            Uses previously-built mapping from sequential index to DataFrame
            Index
        """
        entry = self.df.loc[self.idx_lookup[idx]]
        fpath = utils.pathjoin(self.dpath,
                               'coco',
                               entry['split'],
                               entry['image']['file_name'])
        img = self.load_img(fpath)

        # Attributes as Operators code returns (img, attr, obj)
        attr = entry['single_action']
        obj = entry['subject']
        data = [
            img,
            self.attr2idx[attr],
            self.obj2idx[obj],
            self.pair2idx[(entry['single_action'], entry['subject'])]
        ]

        if self.split == 'train':
            neg_attr, neg_obj = self.sample_negative(attr, obj)  # negative example for triplet loss
            inv_attr = self.sample_train_affordance(attr, obj)  # attribute for inverse regularizer
            comm_attr = self.sample_affordance(inv_attr, obj)   # attribute for commutative regularizer
            data += [neg_attr, neg_obj, inv_attr, comm_attr]

        return data

    def ensure_attrops_compliance(self):
        """ Set all attributes that are expected by the Attributes as Operators
            code. Assumption is that none of these are required for other
            implementations.
        """
        def load_or_compute_and_pickle_attr_obj_pairs(split=None):

            # File name
            fname = 'actops_pairs.pkl'
            if split is not None:
                fname = ("_{}_".format(split)).join(fname.split('_'))

            if self.cache.exists(fname):
                # If file is cached, just grab it
                pairs = self.cache.unpickle(fname)

            else:
                # If file isn't cached, compute it
                if split is not None:
                    pairs = {
                        (entry['single_action'], entry['subject'])
                        for i, entry in self.df_split(split).iterrows()
                    }
                else:
                    pairs = {
                        (entry['single_action'], entry['subject'])
                        for i, entry in self.df.iterrows()
                    }

                # Cache for later
                self.cache.pickle(pairs, fname)

            return pairs

        # AttrOps Compliance
        self.feat_dim = None
        self.attrs = sorted(self.df['single_action'].unique())
        self.objs = sorted(self.df['subject'].unique())

        # List of unique pairs in dataset
        self.pairs = sorted(load_or_compute_and_pickle_attr_obj_pairs())
        self.train_pairs = []
        self.test_pairs = []

        # Compute number of instances for each pair
        pair_counts = {k: 0 for k in self.pairs}
        for verb, subject in self.pairs:
            pair_counts[(verb, subject)] = len(self.df[(self.df['subject'] == subject) & (self.df['single_action'] == verb)])

        # Put pairs into train or test iteratively, greedily keeping a rough
        # balance between the two
        sorted_pairs = sorted(self.pairs, key=lambda x: pair_counts[x], reverse=True)
        train_count = test_count = 1
        train_ratio = 0.8
        ratio = train_ratio / (1 - train_ratio)
        for pair in sorted_pairs:
            if train_count / test_count <= ratio:
                self.train_pairs.append(pair)
                self.df.loc[(self.df['single_action'] == pair[0]) & (self.df['subject'] == pair[1]), 'split'] = 'train'
                train_count += pair_counts[pair]
            else:
                self.test_pairs.append(pair)
                test_count += pair_counts[pair]
                self.df.loc[(self.df['single_action'] == pair[0]) & (self.df['subject'] == pair[1]), 'split'] = 'test'

        self.attr2idx = {attr: idx for idx, attr in enumerate(sorted(self.attrs))}
        self.obj2idx = {obj: idx for idx, obj in enumerate(sorted(self.objs))}
        self.pair2idx = {pair: idx for idx, pair in enumerate(sorted(self.pairs))}

        with ipdb.launch_ipdb_on_exception():
            assert len(set(self.train_pairs).intersection(set(self.test_pairs))) == 0

        self.obj_affordance = self.cache.unpickle('actops_obj_affordance.pkl')
        self.train_obj_affordance = self.cache.unpickle('actops_train_obj_affordance.pkl')
        if self.obj_affordance is None or self.train_obj_affordance is None:
            self.create_and_pickle_affordances('actops')


class COCOActionsCompositionDatasetActivations(COCOActionsCompositionDataset):
    """ Following the convention in ./attributes_as_operators/data/datasets.py
    """
    def __init__(self, dpath=DEFAULT_DPATH, split='train',
                 transforms=None, reset=False, pre_transform_size=256,
                 df_fname='coco-actions-df_predicted_single-estimated_attrops-split.pkl',
                 attrops_compliance=True, compute_features=True, subset=0.9, data_fname=None):
        print("Initializing {}".format(self.__class__.__name__))
        self.cache = utils.Cache('~/.cache/attrops')
        self.dpath = self.cache.dpath
        self.transforms = tv.transforms.ToTensor() if transforms is None else transforms
        self.split = split

        data_fname = self.__class__.__name__ + '.pkl' if data_fname is None else data_fname
        self.data = self.cache.unpickle(data_fname)
        if self.data is None:
            super().__init__(dpath=dpath, split=split, transforms=transforms,
                             reset=reset, pre_transform_size=pre_transform_size,
                             df_fname=df_fname, attrops_compliance=attrops_compliance)
            self.data = {
                'subject': list(self.df['subject']),
                'verb': list(self.df['single_action']),
                'object': list(self.df['single_object']),
                'fpath': [
                    utils.pathjoin(
                        self.dpath,
                        'coco',
                        self._parent_dir(entry.image['file_name']),
                        entry.image['file_name']
                    )
                    for _, entry in self.df.iterrows()
                ],
                'split': [
                    'test' if (entry['single_action'], entry['subject']) in self.test_pairs
                    else 'train'
                    for _, entry in self.df.iterrows()
                ],
            }
            self.cache.pickle(self.data, self.__class__.__name__ + '.pkl')
        else:
            self.attrs = sorted(set(self.data['verb']))
            self.objs = sorted(set(self.data['subject']))
            self.pairs = sorted(set([
                (v, s)
                for v, s in zip(
                    self.data['verb'],
                    self.data['subject']
                )
            ]))
            self.train_pairs = sorted(set([
                (v, s)
                for v, s, split in zip(
                    self.data['verb'],
                    self.data['subject'],
                    self.data['split']
                )
                if split == 'train'
            ]))
            self.test_pairs = sorted(set([
                (v, s)
                for v, s, split in zip(
                    self.data['verb'],
                    self.data['subject'],
                    self.data['split']
                )
                if split == 'test'
            ]))
            self.attr2idx = {attr: idx for idx, attr in enumerate(sorted(self.attrs))}
            self.obj2idx = {obj: idx for idx, obj in enumerate(sorted(self.objs))}
            self.pair2idx = {pair: idx for idx, pair in enumerate(sorted(self.pairs))}
            self.obj_affordance = self.cache.unpickle('actops_obj_affordance.pkl')
            self.train_obj_affordance = self.cache.unpickle('actops_train_obj_affordance.pkl')
            self.idx_lookup = [
                i
                for i, split in enumerate(self.data['split'])
                if split == self.split
            ]

        feat_fname = 'features.t7'
        if not self.cache.exists(feat_fname) and compute_features:
            print("{} not found - computing features now...".format(self.cache.fpath(feat_fname)))
            with torch.no_grad():
                self.generate_features(feat_fname)

        if compute_features:
            print("Found {} - loading features now...".format(self.cache.fpath(feat_fname)))
            activation_data = torch.load(self.cache.fpath(feat_fname))
            self.data['features'] = activation_data['features']
            self.activations = dict(zip(activation_data['files'], activation_data['features']))
            self.feat_dim = activation_data['features'].size(1)
            print("* {} activations loaded".format(len(self.data['features'])))
        if subset is not None:
            old_len = len(self)
            self.subset(subset)
            print("* reduced dataset from {} to {}\n"
                  "* by pruning {} train pairs "
                  "and {} test pairs".format(old_len,
                                             len(self),
                                             len(self.train_pairs),
                                             len(self.test_pairs)))

    def __len__(self):
        if self.split:
            length = len(self.idx_lookup)
        else:
            length = len(self.data['subject'])
        return length

    def generate_features(self, out_file):
        """
            Generates features if not already computed
        """
        try:
            data = self.train_data + self.test_data
        except AttributeError:
            # We're loading from a cache
            ipdb.set_trace()
            data = list(zip(self.data['fpath'], self.data['verb'], self.data['subject']))
        transform = utils.imagenet_transform('test')  # NOQA
        feat_extractor = tmodels.resnet18(pretrained=True)
        feat_extractor.fc = nn.Sequential()
        feat_extractor.eval().cuda()

        image_feats = []
        image_files = []
        with ipdb.launch_ipdb_on_exception():
            for chunk in tqdm.tqdm(chunks(data, 512), total=int(np.ceil(len(data) / 512))):
                files, attrs, objs = zip(*chunk)
                imgs = list(map(self.load_img, files))
                feats = feat_extractor(torch.stack(imgs, 0).cuda())
                image_feats.append(feats.data.cpu())
                image_files += files
        image_feats = torch.cat(image_feats, 0)
        print('features for %d images generated' % (len(image_files)))

        torch.save({'features': image_feats, 'files': image_files}, self.cache.fpath(out_file))

    def _parent_dir(self, fname):
        return 'val2014' if fname[5:8] == 'val' else 'train2014'

    def get_fpath(self, img_fname):
        fpath = utils.pathjoin(
            self.dpath,
            'coco',
            self._parent_dir(img_fname),
            img_fname)

        return fpath

    def subset(self, proportion_to_keep):
        """ Subset a portion of the data which is most common - used to get a
            better indication of the net's performance
        """

        # Turn into tensors for quicker access
        data_numeric = {
            'subject': torch.tensor([
                self.obj2idx[s]
                for s in self.data['subject']
            ]).long(),
            'verb': torch.tensor([
                self.attr2idx[v]
                for v in self.data['verb']
            ]).long(),
            'split': torch.tensor([
                0 if split == 'train' else 1
                for split in self.data['split']
            ]).long()
        }
        train_pairs_numeric = torch.stack([
            torch.tensor([self.attr2idx[v], self.obj2idx[s]])
            for v, s in self.train_pairs
        ])
        test_pairs_numeric = torch.stack([
            torch.tensor([self.attr2idx[v], self.obj2idx[s]])
            for v, s in self.test_pairs
        ])
        #pairs_numeric = torch.stack([
        #    torch.tensor([self.attr2idx[v], self.obj2idx[s]])
        #    for v, s in self.pairs
        #])

        # Number of instances of train and test pairs
        train_pair_counts = torch.stack([
            (data_numeric['subject'] == s) * (data_numeric['verb'] == v)
            for v, s in train_pairs_numeric
        ]).sum(dim=1)
        test_pair_counts = torch.stack([
            (data_numeric['subject'] == s) * (data_numeric['verb'] == v)
            for v, s in test_pairs_numeric
        ]).sum(dim=1)

        # Number of samples to keep
        n_samples_train = int((proportion_to_keep * (data_numeric['split'] == 0).sum().float()).item())
        n_samples_test = int((proportion_to_keep * (data_numeric['split'] == 1).sum().float()).item())

        # Sorted for iterative selection
        descending_order_train = train_pair_counts.argsort(descending=True)
        descending_order_test = test_pair_counts.argsort(descending=True)

        # Iteratively select pairs until we have enough
        n_selected = 0
        selected_pair_indices_train = []
        i = 0
        while n_selected < n_samples_train:
            selected_pair_indices_train.append(descending_order_train[i])
            n_selected += train_pair_counts[descending_order_train[i]]
            i += 1

        n_selected = 0
        selected_pair_indices_test = []
        i = 0
        while n_selected < n_samples_test:
            selected_pair_indices_test.append(descending_order_test[i])
            n_selected += test_pair_counts[descending_order_test[i]]
            i += 1

        # Tensor of indices to select
        selected_pair_indices_train = torch.stack(selected_pair_indices_train)
        selected_pair_indices_test = torch.stack(selected_pair_indices_test)

        # Inverse lookups for rebuilding string lists
        idx2obj = {v: k for k, v in self.obj2idx.items()}
        idx2attr = {v: k for k, v in self.attr2idx.items()}

        # Subset numeric labels
        test_pairs_numeric = test_pairs_numeric[selected_pair_indices_test]
        train_pairs_numeric = train_pairs_numeric[selected_pair_indices_train]

        # Subset string labels
        self.train_pairs = {
            (idx2attr[verb.item()], idx2obj[subject.item()])
            for verb, subject in train_pairs_numeric
        }
        self.test_pairs = {
            (idx2attr[verb.item()], idx2obj[subject.item()])
            for verb, subject in test_pairs_numeric
        }
        self.pairs = self.train_pairs.union(self.test_pairs)

        # Flags for tossing unneeded data
        train_to_keep = torch.stack([
            (data_numeric['subject'] == s) * (data_numeric['verb'] == v)
            for v, s in train_pairs_numeric
        ], dim=1).sum(dim=1)
        test_to_keep = torch.stack([
            (data_numeric['subject'] == s) * (data_numeric['verb'] == v)
            for v, s in test_pairs_numeric
        ], dim=1).sum(dim=1)
        to_keep = (train_to_keep + test_to_keep) > 0

        # Subset data
        self.data = {
            k: [
                item
                for keep, item in zip(
                    to_keep, v
                )
                if keep
            ]
            for k, v in self.data.items()
        }

        # Rebuild index lookup table
        if self.split:
            self.idx_lookup = [i for i, split in enumerate(self.data['split']) if split == self.split]
        return

    def __getitem__(self, index):
        index = self.idx_lookup[index] if self.split else index
        verb = self.data['verb'][index]
        subject = self.data['subject'][index]
        retval = [
            self.data['features'][index],
            self.attr2idx[verb],
            self.obj2idx[subject],
            self.pair2idx[(verb, subject)],
        ]
        if self.split == 'train':
            # negative example for triplet loss
            neg_attr, neg_obj = self.sample_negative(verb, subject)

            # attribute for inverse regularizer
            inv_attr = self.sample_train_affordance(verb, subject)

            # attribute for commutative regularizer
            comm_attr = self.sample_affordance(inv_attr, subject)

            retval += [neg_attr, neg_obj, inv_attr, comm_attr]
        return retval


class COCOActionsCompositionDatasetActivationsEmbeddings(COCOActionsCompositionDatasetActivations):
    def __init__(self, dpath=DEFAULT_DPATH, split='train', subset=0.9,
                 embeddings_fname='coco-actions-sv_embeddings_tsne.t7',
                 model=None):
        super().__init__(dpath=dpath, split=split, transforms=None,
                         reset=False, pre_transform_size=None,
                         data_fname='COCOActionsCompositionDatasetActivations.pkl',
                         attrops_compliance=True, compute_features=True,
                         subset=subset)
        self.model = model

        if self.cache.exists(embeddings_fname):
            self.embeddings = torch.load(self.cache.fpath(embeddings_fname))
        elif model is None:
            print("Embeddings not found. Compute embeddings with "
                  "self.embed(model)")
        else:
            raise NotImplementedError("No support for integrated embedding generation yet")

    def embed(self, model, batch_size=512,
              embeddings_fname='coco-actions-sv_embeddings.t7'):
        if self.cache.exists(embeddings_fname):
            self.embeddings = torch.load(self.cache.fpath(embeddings_fname))
        else:
            model.eval()
            embeddings = []
            for b_i in range(0, int(np.ceil(len(self.data['features']) / batch_size))):
                lo = b_i * batch_size
                hi = min((b_i + 1) * batch_size, len(self.data['features']))
                feats_i = torch.stack(self.data['features'][lo:hi])
                embeddings.append(model.image_embedder(feats_i.cuda()).cpu())

            self.embeddings = torch.cat(embeddings)
            torch.save(self.embeddings, self.cache.fpath(embeddings_fname))
            print("Generated embeddings")

    def __getitem__(self, index):
        index = self.idx_lookup[index] if self.split is not None else index
        retval = super().__getitem__(index)
        if self.embeddings is not None:
            retval[0] = self.embeddings[0]
        return retval


class COCOActionsSVODataset(torch.utils.data.Dataset):
    """
    """
    def __init__(self, dpath='~/data', split='train', verbosity=1,
                 transforms=None, glove_init=True, subset_proportion=0.9,
                 cache=None, data_cache=None, train_ratio=0.7,
                 dset_fname='coco-actions-df_predicted_single-estimated_svo-split.csv',
                 ):

        self.train_ratio = train_ratio
        if cache is None:
            self.cache = utils.Cache('~/.cache/cocoactions')
        else:
            self.cache = cache
        if data_cache is None:
            self.data_cache = utils.Cache(dpath, verbosity)
        else:
            self.data_cache = data_cache

        self.split = split
        self.verbosity = verbosity
        self.transforms = transforms

        self.data = self._process_csv(utils.pathjoin(self.dpath, dset_fname))

        if glove_init:
            self.glove_compliant()

        self.set_auxiliary_attributes()

        if subset_proportion is not None:
            self.subset_common_svo(proportion=subset_proportion)
        else:
            self.split_svo()

        self.features = None

    def set_auxiliary_attributes(self, **kwargs):
        """ Compute and set all auxiliary attributes, for when data is loaded
            or significantly adjusted. If the value has been computed at call
            time, it may be specified in **kwargs to avoid unneccessary
            computation
            Parameters:
                kwargs (dict): keys are attributes of the dataset that have
                    been precomputed, and so should not be computed here
        """
        for k, v in kwargs.items():
            self.__setattr__(k, v)

        # Pre-generated SVO split - probably overridden in subclasses
        if 'idx_lookup' not in kwargs:
            self.idx_lookup = [
                i for i, split in enumerate(self.data['svo_split'])
                if split == self.split
            ]

        if not self.data.get('split', False):
            self.data['split'] = self.data['svo_split']

        if 'subjects' not in kwargs:
            self.subjects = sorted({s for s in self.data['subject']})
        if 'verbs' not in kwargs:
            self.verbs = sorted({v for v in self.data['verb']})
        if 'objects' not in kwargs:
            self.objects = sorted({o for o in self.data['object']})

        if 'subject2idx' not in kwargs:
            self.subject2idx = {k: i for i, k in enumerate(self.subjects)}
        if 'verb2idx' not in kwargs:
            self.verb2idx = {k: i for i, k in enumerate(self.verbs)}
        if 'object2idx' not in kwargs:
            self.object2idx = {k: i for i, k in enumerate(self.objects)}

        if 'unique_svo' not in kwargs:
            self.unique_svo = sorted({
                (sub, verb, obj)
                for sub, verb, obj in zip(
                    self.data['subject'],
                    self.data['verb'],
                    self.data['object']
                )
            })

        if 'svo_test' not in kwargs:
            self.svo_test = sorted({
                (sub, verb, obj)
                for sub, verb, obj, split in zip(
                    self.data['subject'],
                    self.data['verb'],
                    self.data['object'],
                    self.data['split']
                )
                if split != 'train'
            })

        if 'svo_train' not in kwargs:
            self.svo_train = sorted({
                (sub, verb, obj)
                for sub, verb, obj, split in zip(
                    self.data['subject'],
                    self.data['verb'],
                    self.data['object'],
                    self.data['split']
                )
                if split == 'train'
            })

        if 'svo2idx' not in kwargs:
            self.svo2idx = {k: i for i, k in enumerate(self.unique_svo)}

        # Inverse lookups for rebuilding string lists
        if 'idx2subject' not in kwargs:
            self.idx2subject = {v: k for k, v in self.subject2idx.items()}
        if 'idx2verb' not in kwargs:
            self.idx2verb = {v: k for k, v in self.verb2idx.items()}
        if 'idx2object' not in kwargs:
            self.idx2object = {v: k for k, v in self.object2idx.items()}

        if 'all_possible_svo' not in kwargs:
            self.all_possible_svo = [
                (s, v, o)
                for s in self.subjects
                for v in self.verbs
                for o in self.objects
            ]

    @property
    def dpath(self):
        return self.data_cache.dpath

    def split_svo(self, data_numeric=None, svo_numeric=None):
        ratio = self.train_ratio / (1 - self.train_ratio)
        n_added_test = n_added_train = 1
        svo_train = []
        svo_test = []
        # TODO pick up here

        if data_numeric is None:
            data_numeric = {
                'subject': torch.tensor([
                    self.subject2idx[s]
                    for s in self.data['subject']
                ]).long(),
                'verb': torch.tensor([
                    self.verb2idx[v]
                    for v in self.data['verb']
                ]).long(),
                'object': torch.tensor([
                    self.object2idx[v]
                    for v in self.data['object']
                ]).long(),
            }

        if svo_numeric is None:
            svo_numeric = torch.stack([
                torch.tensor([self.subject2idx[s], self.verb2idx[v], self.object2idx[o]])
                for s, v, o in self.unique_svo
            ])

        # Number of instances of SVO triplets
        svo_counts = torch.stack([
            (data_numeric['subject'] == s) *    # NOQA
            (data_numeric['verb'] == v) *       # NOQA
            (data_numeric['object'] == o)
            for s, v, o in svo_numeric
        ]).sum(dim=1)

        # Sort SVO by counts in descending order
        sorted_svo = [self.unique_svo[i.item()] for i in svo_counts.argsort(descending=True)]

        for i, svo in enumerate(sorted_svo):
            if n_added_train / n_added_test <= ratio:
                svo_train.append(svo)
                n_added_train += svo_counts[i]
            else:
                svo_test.append(svo)
                n_added_test += svo_counts[i]

        self.svo_train = sorted(svo_train)
        self.svo_test = sorted(svo_test)

        updated_split = [
            'train'
            if (s, v, o) in self.svo_train
            else 'test'
            for s, v, o in zip(
                self.data['subject'],
                self.data['verb'],
                self.data['object']
            )
        ]
        self.data['split'] = updated_split
        return self.svo_train, self.svo_test

    def subset_common_svo(self, proportion):
        """ Subset a portion of the data which is most common - used to get a
            better indication of the net's performance
        """

        # Turn into tensors for quicker access
        data_numeric = {
            'subject': torch.tensor([
                self.subject2idx[s]
                for s in self.data['subject']
            ]).long(),
            'verb': torch.tensor([
                self.verb2idx[v]
                for v in self.data['verb']
            ]).long(),
            'object': torch.tensor([
                self.object2idx[v]
                for v in self.data['object']
            ]).long(),
        }

        svo_numeric = torch.stack([
            torch.tensor([self.subject2idx[s], self.verb2idx[v], self.object2idx[o]])
            for s, v, o in self.unique_svo
        ])

        # Number of instances of SVO triplets
        svo_counts = torch.stack([
            (data_numeric['subject'] == s) *    # NOQA
            (data_numeric['verb'] == v) *       # NOQA
            (data_numeric['object'] == o)
            for s, v, o in svo_numeric
        ]).sum(dim=1)

        # Number of samples to keep
        n_samples = int(proportion * len(data_numeric['subject']))

        # Sorted for iterative selection
        descending_order = svo_counts.argsort(descending=True)

        # Iteratively select pairs until we have enough to span the desired proportion of observations
        n_selected = 0
        selected_pair_indices = []
        i = 0
        while n_selected < n_samples:
            selected_pair_indices.append(descending_order[i])
            n_selected += svo_counts[descending_order[i]]
            i += 1

        if self.verbosity > 0:
            print("[{}] keeping {} of {} samples".format(
                self.__class__.__name__,
                n_selected,
                svo_counts.sum()
            ))

        # Tensor of indices to select
        selected_pair_indices = torch.stack(selected_pair_indices)

        # Inverse lookups for rebuilding string lists
        self.idx2subject = {v: k for k, v in self.subject2idx.items()}
        self.idx2verb = {v: k for k, v in self.verb2idx.items()}
        self.idx2object = {v: k for k, v in self.object2idx.items()}

        # Subset numeric svo labels
        svo_numeric = svo_numeric[selected_pair_indices]

        # Subset string labels this is needed for splitting after this
        self.unique_svo = sorted({
            (self.idx2subject[s.item()], self.idx2verb[v.item()], self.idx2object[o.item()])
            for s, v, o in svo_numeric
        })

        # Flags for tossing unneeded data
        to_keep = torch.stack([
            (data_numeric['subject'] == s) *    # NOQA
            (data_numeric['verb'] == v) *       # NOQA 
            (data_numeric['object'] == o)
            for s, v, o in svo_numeric
        ], dim=1).sum(dim=1)

        # Subset data
        self.data = {
            k: [
                item
                for keep, item in zip(
                    to_keep, v
                )
                if keep
            ]
            for k, v in self.data.items()
        }

        # Sets split string in internal "data" dict, and sets self.svo_train
        # and self.svo_test
        self.split_svo()

        self.set_auxiliary_attributes(
            svo_train=self.svo_train,
            svo_test=self.svo_test,
            idx2subject=self.idx2subject,
            idx2verb=self.idx2verb,
            idx2object=self.idx2object,
            unique_svo=self.unique_svo,
        )

        return

    def _process_csv(self, csv_fpath):
        for i, line in enumerate(open(csv_fpath, 'r')):
            if i == 0:
                column = {k: i for i, k in enumerate(line.strip().split(','))}
                column['index'] = 0
                data = {
                    'index': [],
                    'coco_split': [],
                    'fname': [],
                    'subject': [],
                    'verb': [],
                    'object': [],
                    'svo_split': [],
                    'crop': [],
                }
            else:
                splat = line.strip().split(',')
                if splat[column['single_action']] != '':
                    data['index'].append(int(splat[column['index']]))
                    data['coco_split'].append(splat[column['split']])
                    data['fname'].append(splat[column['fname']])
                    data['subject'].append(splat[column['subject']])
                    data['verb'].append(splat[column['single_action']])
                    data['object'].append((lambda x: None if x == '' else x)(splat[column['single_object']]))
                    data['svo_split'].append(splat[column['svo_split']])
                    data['crop'].append([float(splat[column[c]]) for c in ('crop_x1', 'crop_y1', 'crop_x2', 'crop_y2')])
        return data

    def img_loader(self, fpath, crop_box=None, do_transform=False):
        img = Image.open(fpath)
        img = img.crop(crop_box) if crop_box else img

        # convert() performs a copy even if correct mode, so avoid if possible
        img = img.convert('RGB') if img.mode != 'RGB' else img
        img = self.transforms(img) if do_transform else img
        return img

    def __len__(self, split=True):
        length = len(self.idx_lookup) if split else len(self.data['index'])
        return length

    def __getitem__(self, idx, only_split=True):
        if only_split:
            item_dict = {k: v[self.idx_lookup[idx]] for k, v in self.data.items()}
        else:
            item_dict = {k: v[idx] for k, v in self.data.items()}
        fpath = utils.pathjoin(self.dpath,
                               'coco',
                               item_dict['coco_split'],
                               item_dict['fname'])
        retval = {
            'fpath': fpath,
            'subject': item_dict['subject'],
            'verb': item_dict['verb'],
            'object': item_dict['object'],
            'crop': item_dict['crop']
        }
        if self.transforms is not None:
            retval['image'] = self.img_loader(retval['fpath'], retval['crop'])

        return retval

    def indices_with(self, split=True, **kwargs):
        """ Gets indices of samples with the specified subject, verb, object
        """
        sub = obj = verb = 'unset'

        if kwargs.get('subject'):
            sub = kwargs['subject']
        if kwargs.get('verb'):
            verb = kwargs['verb']
        if kwargs.get('object'):
            obj = kwargs['object']

        correct_idx = (lambda x: self.idx_lookup[x]) if split else (lambda x: x)

        if split:
            choices_idx = [i for i in range(len(self))]
        else:
            choices_idx = [i for i in range(len(self.data['index']))]

        if obj is None:
            choices_idx = [
                i for i in choices_idx
                if self.data['object'][correct_idx(i)] is None
            ]
        elif obj.lower() == 'any':
            choices_idx = [
                i for i in choices_idx
                if self.data['object'][correct_idx(i)] is not None
            ]
        elif obj.lower() != 'unset':
            choices_idx = [
                i for i in choices_idx
                if self.data['object'][correct_idx(i)] == obj
            ]

        if verb != 'unset':
            choices_idx = [
                i for i in choices_idx
                if self.data['verb'][correct_idx(i)] == verb
            ]

        if sub != 'unset':
            choices_idx = [
                i for i in choices_idx
                if self.data['subject'][correct_idx(i)] == sub
            ]

        return choices_idx

    def batch_data(self, batch_size=512, split=False):
        """ Generator for batching out data
        """
        def indices(b_idx):
            if split:
                return (self.idx_lookup[j] for j in range(b_idx, b_idx + batch_size))
            else:
                return range(b_idx, min(b_idx + batch_size, len(self.data['index'])))

        if split:
            for i in range(0, len(self), batch_size):
                yield {k: [v[j] for j in indices(i)] for k, v in self.data.items()}
        else:
            for i in range(0, len(self.data['index']), batch_size):
                yield {k: [v[j] for j in indices(i)] for k, v in self.data.items()}

    def glove_compliant(self):
        self.data['subject'] = list(map(
            lambda x: ''.join(x.split(' ')),
            self.data['subject']
        ))
        self.data['verb'] = list(map(
            lambda x: ''.join(x.split(' ')),
            self.data['verb']
        ))
        self.data['object'] = list(map(
            lambda x: ''.join(x.split(' ')) if x is not None else "None",
            self.data['object']
        ))
        return


class COCOActionsSVODatasetActivations(COCOActionsSVODataset):
    def __init__(self, dpath='~/data', split='train', verbosity=1,
                 feats_fname='coco-actions-features.t7', train_ratio=0.7,
                 subset_proportion=0.9, cache=None, data_cache=None,
                 dset_fname='coco-actions-df_predicted_single-estimated_svo-split.csv',
                 ):
        self.train_ratio = train_ratio
        super().__init__(
            dpath=dpath,
            split=split,
            verbosity=verbosity,
            dset_fname=dset_fname,
            transforms=None,
            cache=cache,
            train_ratio=train_ratio,
            subset_proportion=subset_proportion
        )

        self.load_or_compute_features(feats_fname)

    def load_or_compute_features(self, feats_fname):
        # Load up precomputed features if possible, compute and save if not
        if 'features' in self.data.keys():
            self.features = torch.stack(self.data['features'])
        else:
            if self.data_cache.exists(feats_fname):
                self.features = torch.load(self.data_cache.fpath(feats_fname))
            else:
                with torch.no_grad():
                    self.features = self.generate_features()
                torch.save(self.features, self.data_cache.fpath(feats_fname))

            self.features = torch.cat(self.features)

    def __len__(self):
        return len(self.idx_lookup)

    def __getitem__(self, idx):
        fpath, sub, verb, obj = super().__getitem__(idx)

        retval = {
            'features': self.features[self.idx_lookup[idx]],
            'subject': self.subject2idx[sub],
            'verb': self.verb2idx[verb],
            'object': self.object2idx[obj] if obj != 'None' else -1,
        }

        return retval

    def generate_features(self, batch_size=128):
        transform = utils.imagenet_transform('test')

        # Load a pretrained model and use conv outputs as features
        feature_extractor = tmodels.resnet18(pretrained=True)
        feature_extractor.fc = nn.Sequential()
        feature_extractor.eval().cuda()

        features = []
        for batch in tqdm.tqdm(self.batch_data(batch_size, split=False), total=len(self.data['index']) // batch_size):
            # Construct file path name for all samples, load them, and transform them accordingling
            img_fpaths = [
                utils.pathjoin(self.dpath,
                               'coco',
                               coco_split,
                               fname)
                for coco_split, fname in zip(batch['coco_split'],
                                             batch['fname'])
            ]
            imgs = [
                transform(self.img_loader(fpath, crop_box=crop_box, do_transform=False))
                for fpath, crop_box in zip(img_fpaths, batch['crop'])
            ]
            features.append(feature_extractor(torch.stack(imgs, 0).cuda()).cpu())
        return features


class COCOActionsSVODatasetActivationsTriplet(COCOActionsSVODatasetActivations):
    """ Performs random negative sampling
    """
    #@profile
    def __init__(self, dpath='~/data', split='train', verbosity=1, n_candidates=3,
                 feats_fname='coco-actions-features.t7', sample_only_valid_triplets=False,
                 subset_proportion=0.9, cache=None, data_cache=None, train_ratio=0.7, glove_init=True,
                 dset_fname='coco-actions-df_predicted_single-estimated_svo-split.csv',
                 null_object_index=-1,
                 ):
        """
            Parameters:
                dpath (str): where do I find the actual data?
                split (str): which split should I grab?
                verbosity (int): how much information should I print?
                n_candidates (int): how many negative candidates should I return for mining?
                feats_fname (str): the name of the precomputed features file,
                    located in the cache
                sample_only_valid_triplets (bool): should I sample all possible
                    SVO, or only ones that are present in the dataset?
                subset_proportion (float): How much of the data should I use? I
                    set this to 0.9 because I found that many instances were
                    present only once
                cache (actops.utils.Cache): cache object where we can save and
                    load intermediate files from
                data_cache (actops.utils.Cache): TODO
                train_ratio (float): how much of the data is training data?
                glove_init (bool): should I initialize with GloVe vectors? TODO
                    I think this is deprecated
                dset_fname (str): the name of the dataset in the cache
                null_object_index (int or None): if int, this is what null
                    objects will be mapped to. If None, there will be no
                    mapping - this is preferred as of 2019-11-03
        """

        self.train_ratio = train_ratio
        if cache is None:
            self.cache = utils.Cache('~/.cache/cocoactions')
        else:
            self.cache = cache
        if data_cache is None:
            self.data_cache = utils.Cache(dpath, verbosity)
        else:
            self.data_cache = data_cache

        internal_data_cache_name = self.create_data_cache_name(
            ext='.t7',
            subset_proportion=subset_proportion,
            sample_only_valid_triplets=sample_only_valid_triplets,
            train_ratio=self.train_ratio,
        )

        data = self.cache.load(internal_data_cache_name)

        if data is None:
            super().__init__(dpath=dpath, split=split, verbosity=verbosity,
                             subset_proportion=None, train_ratio=self.train_ratio,
                             cache=self.cache, data_cache=self.data_cache,
                             dset_fname=dset_fname, feats_fname=feats_fname)
            self.data['features'] = self.features
            if subset_proportion is not None:
                self.subset_common_svo(proportion=subset_proportion)
            self.cache.save(self.data, internal_data_cache_name)
        else:
            self.data = data

            # Needed for setting auxiliary attributes
            self.split = split

            if glove_init:
                self.glove_compliant()
            self.set_auxiliary_attributes()
            self.load_or_compute_features(feats_fname)

        self.n_candidates = n_candidates

        self._prep_for_negative_sampling(sample_only_valid_triplets)

        if null_object_index is None:
            self.null_object_index = self.object2idx['None']
        else:
            self.null_object_index = null_object_index

    def create_data_cache_name(self, ext='.pkl', **kwargs):
        name = "{}_data-cache".format(self.__class__.__name__)
        for k in sorted(kwargs):
            v = kwargs[k]
            param_name = '-'.join(k.split('_'))
            name += '_{}'.format(param_name)
            name += '={}'.format(v) if v is not True and v is not False else ''
        name += ext if ext.startswith('.') else ".{}".format(ext)
        return name

    def _prep_for_negative_sampling(self, sample_only_valid_triplets=True):
        #self.subject_numeric = torch.tensor([self.subject2idx[sub] for sub in self.data['subject']])
        #self.verb_numeric = torch.tensor([self.verb2idx[verb] for verb in self.data['verb']])
        #self.object_numeric = torch.tensor([self.object2idx[obj] for obj in self.data['object']])
        #self.neg_idx = dict()
        #self._indices = torch.arange(len(self.data['subject']))

        #self._mask = self.bool_and if sample_only_valid_triplets else self.bool_or
        self._negative_svo_candidates = dict()
        if sample_only_valid_triplets:
            self.all_possible_svo = self.unique_svo

    def bool_and(self, *args):
        #return x * y * z
        return torch.prod(torch.stack(args), dim=0) > 0

    def bool_or(self, *args):
        #return (x + y + z) > 0
        return torch.sum(torch.stack(args), dim=0) > 0

    def stable_negative_svo(self, svo):
        if self._negative_svo_candidates.get(svo) is None:
            negative_svo_candidates = np.array([
                svo_i
                for svo_i in self.all_possible_svo
                if svo_i != svo
            ])
            self._negative_svo_candidates[svo] = negative_svo_candidates[0]
        return self._negative_svo_candidates[svo]

    def sample_negative_svo(self, svo):
        """ Samples negative SVOs of the whole space of possible SVO actions
        """
        # TODO IDK if this is the right way of going about doing this
        negative_svo_candidates = self._negative_svo_candidates.get(svo)
        if negative_svo_candidates is None:
            negative_svo_candidates = np.array([
                svo_i
                for svo_i in self.all_possible_svo
                if svo_i != svo
            ])
            self._negative_svo_candidates[svo] = negative_svo_candidates

        negative_svo_indices = torch.from_numpy(np.random.choice(
            np.arange(len(negative_svo_candidates)),
            self.n_candidates,
            replace=False
        ))
        negative_samples = negative_svo_candidates[negative_svo_indices]
        return negative_samples

    @classmethod
    def split_test_val(cls, split_ratio=0.5, **kwargs):
        def n_assigned(count, assignment, split):
            return sum([count[k] for k, v in assignment.items() if v == split])

        kwargs.update(split='test')
        dset_test = cls(**kwargs)
        dset_val = cls(**kwargs)
        test_val_split_cache_fname = 'test_val_indices_svo_split={}.pkl'.format(split_ratio)
        test_val_split = dset_test.cache.load(test_val_split_cache_fname)
        if test_val_split is not None:
            dset_test.idx_lookup = test_val_split['dset_test.idx_lookup']
            dset_val.idx_lookup = test_val_split['dset_val.idx_lookup']
            dset_test.svo_test = test_val_split['dset_test.svo_test']
            dset_val.svo_val = test_val_split['dset_val.svo_val']
        else:
            # Count up the numbers of SVO instances
            svo_count = dict()
            for i in range(len(dset_test)):
                sub = dset_test.data['subject'][dset_test.idx_lookup[i]]
                verb = dset_test.data['verb'][dset_test.idx_lookup[i]]
                obj = dset_test.data['object'][dset_test.idx_lookup[i]]
                if svo_count.get((sub, verb, obj)):
                    svo_count[(sub, verb, obj)] += 1
                else:
                    svo_count[(sub, verb, obj)] = 1

            # Sort by number of instances in descending order and allocate the first two
            svo_sorted = sorted(svo_count.keys(), key=lambda x: svo_count[x], reverse=True)
            svo_assignment = {svo_sorted[0]: 'val',
                              svo_sorted[1]: 'test'}

            # Go through the rest of the SVO and allocate to keep the splits approximately equal
            for i in range(2, len(svo_sorted)):
                n_val = n_assigned(svo_count, svo_assignment, 'val')
                n_test = n_assigned(svo_count, svo_assignment, 'test')
                curr_ratio = n_val / (n_val + n_test)

                if curr_ratio > split_ratio:
                    svo_assignment[svo_sorted[i]] = 'test'
                else:
                    svo_assignment[svo_sorted[i]] = 'val'

            dset_test.idx_lookup = []
            dset_val.idx_lookup = []
            for i, (s, v, o) in enumerate(zip(dset_test.data['subject'],
                                              dset_test.data['verb'],
                                              dset_test.data['object'])):
                if svo_assignment.get((s, v, o), '') == 'test':
                    dset_test.idx_lookup.append(i)
                elif svo_assignment.get((s, v, o), '') == 'val':
                    dset_val.idx_lookup.append(i)

            dset_test.svo_test = set([k for k, v in svo_assignment.items() if v == 'test'])
            dset_val.svo_val = set([k for k, v in svo_assignment.items() if v == 'val'])

            # Save this for later use
            dset_test.cache.save({
                'dset_test.idx_lookup': dset_test.idx_lookup,
                'dset_val.idx_lookup': dset_val.idx_lookup,
                'dset_test.svo_test': dset_test.svo_test,
                'dset_val.svo_val': dset_val.svo_val,
            }, test_val_split_cache_fname)

        return dset_test, dset_val

    #@profile
    def __getitem__(self, index):
        index = self.idx_lookup[index]
        sub = self.data['subject'][index]
        verb = self.data['verb'][index]
        obj = self.data['object'][index]

        retval = [
            self.features[index],
            self.subject2idx[sub],
            self.verb2idx[verb],
        ]
        if self.null_object_index is None:
            retval.append(self.object2idx[obj])
        else:
            retval.append(self.null_object_index)

        if self.split == 'train':
            # Last three are tuples of length self.n_candidates for subjects,
            # verbs, and objects of negative svo triplets
            negative_svo = self.sample_negative_svo((sub, verb, obj))

            if self.n_candidates > 1:
                sub_neg, verb_neg, obj_neg = [svo_ni for svo_ni in zip(*negative_svo)]
                retval += [
                    [self.subject2idx[s] for s in sub_neg],
                    [self.verb2idx[v] for v in verb_neg],
                ]

                # If we specified what index to use for null objects, use that for them
                if self.null_object_index is not None:
                    retval.append([
                        self.object2idx[o]
                        if o != 'None' else self.null_object_index
                        for o in obj_neg
                    ])
                else:
                    retval.append([
                        self.object2idx[o]
                        for o in obj_neg
                    ])
            else:
                # Only one negative candidate
                sub_neg, verb_neg, obj_neg = negative_svo

                retval += [
                    self.subject2idx[sub_neg],
                    self.verb2idx[verb_neg],
                ]

                if self.null_object_index is None:
                    retval.append(self.object2idx[obj_neg])
                else:
                    retval.append(self.null_object_index)

        else:
            negative_svo = self.stable_negative_svo((sub, verb, obj))
            retval += [
                self.subject2idx[negative_svo[0]],
                self.verb2idx[negative_svo[1]],
            ]
            if self.null_object_index is None:
                retval.append(self.object2idx[negative_svo[2]])
            else:
                retval.append(self.null_object_index)

        return retval


class COCOActionsSVODatasetActivationsEmbeddings(COCOActionsSVODatasetActivationsTriplet):
    def __init__(self, dpath='~/data', split='train', verbosity=1, n_candidates=3,
                 feats_fname='coco-actions-features.t7', sample_only_valid_triplets=False,
                 subset_proportion=0.9, cache=None, data_cache=None, model=None,
                 train_ratio=0.7,
                 embeddings_fname='coco-actions-svo-embeddings.t7',
                 dset_fname='coco-actions-df_predicted_single-estimated_svo-split.csv',
                 ):

        self.train_ratio = train_ratio
        if cache is None:
            self.cache = utils.Cache('~/.cache/cocoactions')
        else:
            self.cache = cache
        if data_cache is None:
            self.data_cache = utils.Cache(dpath, verbosity)
        else:
            self.data_cache = data_cache

        internal_data_cache_name = self.create_data_cache_name(
            ext='.t7',
            subset_proportion=subset_proportion,
            sample_only_valid_triplets=sample_only_valid_triplets,
            train_ratio=self.train_ratio,

        )

        data = self.cache.load(internal_data_cache_name)

        if data is None:
            super().__init__(dpath=dpath, split=split, verbosity=verbosity,
                             subset_proportion=subset_proportion, train_ratio=self.train_ratio,
                             cache=self.cache, data_cache=self.data_cache,
                             dset_fname=dset_fname, feats_fname=feats_fname)
            self.cache.save(self.data, internal_data_cache_name)
        else:
            self.data = data

            # Needed for setting auxiliary attributes
            self.split = split

            self.set_auxiliary_attributes()
            self.load_or_compute_features(feats_fname)

        self.model = model
        self.load_or_generate_and_save_embeddings(embeddings_fname=embeddings_fname)
        if self.embeddings is None:
            print("No embeddings found, and no model given. "
                  "Run generate_embeddings(model) to do it")

        self.data['embeddings'] = self.embeddings

    def create_data_cache_name(self, ext='.pkl', **kwargs):
        name = "{}_data-cache".format(self.__class__.__name__)
        for k in sorted(kwargs):
            v = kwargs[k]
            param_name = '-'.join(k.split('_'))
            name += '_{}'.format(param_name)
            name += '={}'.format(v) if v is not True and v is not False else ''
        name += ext if ext.startswith('.') else ".{}".format(ext)
        return name

    def load_or_generate_and_save_embeddings(self, model=None, batch_size=512,
                                             embeddings_fname='coco-actions-svo-embeddings.t7'):

        if self.cache.exists(embeddings_fname):
            self.embeddings = self.cache.load(embeddings_fname)
            return

        if model is None:
            model = self.model

        if model is None:
            self.embeddings = None
            return

        model.eval()
        embeddings = []
        for b_i in range(0, int(np.ceil(len(self.data['features']) / batch_size))):
            lo = b_i * batch_size
            hi = min((b_i + 1) * batch_size, len(self.data['features']))
            feats_i = torch.stack(self.data['features'][lo:hi])
            embeddings.append(model.visual_embedder(feats_i.cuda()).cpu())

        self.embeddings = torch.cat(embeddings)
        self.data['embeddings'] = self.embeddings
        torch.save(self.embeddings, self.cache.fpath(embeddings_fname))
        print("Generated embeddings")

    def __len__(self):
        return len(self.idx_lookup)

    def __getitem__(self, idx):
        fpath, sub, verb, obj = super().__getitem__(idx)

        retval = {
            'features': self.features[self.idx_lookup[idx]],
            'subject': self.subject2idx[sub],
            'verb': self.verb2idx[verb],
            'object': self.object2idx[obj],
        }

        return retval

    def generate_features(self, batch_size=128):
        transform = utils.imagenet_transform('test')

        # Load a pretrained model and use conv outputs as features
        feature_extractor = tmodels.resnet18(pretrained=True)
        feature_extractor.fc = nn.Sequential()
        feature_extractor.eval().cuda()

        features = []
        for batch in tqdm.tqdm(self.batch_data(batch_size, split=False), total=len(self.data['index']) // batch_size):
            # Construct file path name for all samples, load them, and transform them accordingling
            img_fpaths = [
                utils.pathjoin(self.dpath,
                               'coco',
                               coco_split,
                               fname)
                for coco_split, fname in zip(batch['coco_split'],
                                             batch['fname'])
            ]
            imgs = [
                transform(self.img_loader(fpath, crop_box=crop_box, do_transform=False))
                for fpath, crop_box in zip(img_fpaths, batch['crop'])
            ]
            features.append(feature_extractor(torch.stack(imgs, 0).cuda()).cpu())
        return features


class COCOActionsSVODatasetTriplet(COCOActionsSVODatasetActivationsTriplet):
    def __init__(self, **kwargs):

        if kwargs.get('transforms'):
            self.transforms = kwargs.pop('transforms')
        else:
            self.transforms = tv.transforms.ToTensor()

        if kwargs.get('resize_dims'):
            self.resize_dims = kwargs.pop('resize_dims')
            if isinstance(self.resize_dims, int):
                self.resize_dims = [self.resize_dims, self.resize_dims]
        else:
            self.resize_dims = [400, 400]

        super().__init__(**kwargs)

    def __getitem__(self, index):
        retval = super().__getitem__(index)

        index = self.idx_lookup[index]
        fpath = utils.pathjoin(
            self.dpath,
            'coco',
            self.data['coco_split'][index],  # TODO verify this
            self.data['fname'][index]
        )

        img = Image.open(fpath)

        retval[0] = self.transforms(img)
        return retval


def save_sample(sample, dpath='./images'):
    """ Saves an image named according to its content
    """
    save_path = "{}-{}".format(sample['subject'], sample['verb'])
    if sample['object'] is not None:
        save_path += "-{}".format(sample['object'])
    save_path = '_'.join(save_path.split(' ')) + '.png'
    save_path = utils.pathjoin(dpath, save_path)

    sample['image'].save(save_path)
    print("Saved {}".format(save_path))
