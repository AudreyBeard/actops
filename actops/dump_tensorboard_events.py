from argparse import ArgumentParser

from tensorface import TensorFace


METRICS = [
    's_acc_open_val',
    'v_acc_open_val',
    'o_acc_open_val',
    's_acc_open_train',
    'v_acc_open_train',
    'o_acc_open_train',
]


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('logdir',
                        help='location of log files')
    parser.add_argument('outfile',
                        help='path to write (csv)')
    return parser.parse_args()


def write_run_values(outfile, all_runs):
    """ Write last values for all runs
    """
    def parse_lambda(split_line, param_name, n_subparams=1):
        split_on_hyphen = [
            s.split('=')[-1].split('-')
            for s in split_line
            if s.startswith("{}=".format(param_name))
        ][0]
        i = 0
        lambda_parsed = []
        if len(split_on_hyphen) == 1:
            lambda_parsed = ','.join([split_on_hyphen[0] for _ in range(n_subparams)])
        else:
            while i < len(split_on_hyphen):
                if split_on_hyphen[i].endswith('e'):
                    lambda_parsed.append("{}-{}".format(
                        split_on_hyphen[i],
                        split_on_hyphen[i + 1],
                    ))
                    i += 1
                else:
                    lambda_parsed.append(split_on_hyphen[i])
                i += 1
            lambda_parsed = ','.join(lambda_parsed)
        return lambda_parsed

    with open(outfile, 'w') as fid:
        line = 'model name,{},{}\n'.format(
            ','.join([
                'lr',
                'lambda_sv_s',
                'lambda_sv_v',
                'lambda_ov_o',
                'lambda_ov_v',
                'lambda_vis_s',
                'lambda_vis_v',
                'lambda_vis_o',
            ]),
            ','.join(METRICS),
        )
        fid.write(line)
        for run_name, run in all_runs.items():
            line = run_name + ','

            # Get model hyperparameters
            splat = run_name.split('_')
            lr = [
                s.split('=')[-1]
                for s in splat
                if s.startswith('lr=')
            ][0]
            lambda_ling = parse_lambda(splat, 'lambda-ling', 4)
            lambda_vis = parse_lambda(splat, 'lambda-vis', 3)

            line += '{},{},{},'.format(lr, lambda_ling, lambda_vis)

            # Get the metrics
            for metric in METRICS:
                if run.get(metric):
                    line += '{:.4f},'.format(run[metric]['value'][-1])
                else:
                    line += ','

            # Toss last comma and add newline
            line = line[:-1]
            line += '\n'
            fid.write(line)


if __name__ == "__main__":
    args = parse_args()
    face = TensorFace(args.logdir)

    print("Loading logs from {}".format(face.dpath))
    face.load()

    print("Grabbing all scalar data")
    all_scalars = face.all_scalars()

    final_values = {
        scalar_name: {
            run_name: run_data['value'][-1]
            for run_name, run_data in all_scalars[scalar_name].items()
        }
        for scalar_name in METRICS
    }

    best_values = {
        scalar_name: max(run.items(), key=lambda x: x[1])
        for scalar_name, run in final_values.items()
    }
    all_runs = face.all_runs()
    if True:
        write_run_values(args.outfile, all_runs)

    else:
        with open(args.outfile, 'w') as fid:
            fid.write('model name,' + ','.join(METRICS) + '\n')
            for run_name, run in all_runs.items():
                line = run_name + ','
                for metric in METRICS:
                    if run.get(metric):
                        line += '{:.5f},'.format(run[metric]['value'][-1])
                    else:
                        line += ','

                # Toss last comma and add newline
                line = line[:-1]
                line += '\n'
                fid.write(line)
