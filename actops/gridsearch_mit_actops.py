# NOTE this is just copypasted from ./attributes_as_operators/train.py

import argparse
import os
#import itertools

#import glob
import torch
#import torch.nn as nn
import torch.optim as optim
#import torch.nn.functional as F
import numpy as np
import tqdm
#import torchvision.models as tmodels
from tensorboard_logger import configure, log_value
import torch.backends.cudnn as cudnn
import ipdb

#from attributes_as_operators.utils import utils as attrops_utils
from attributes_as_operators.models import models as attrops_models
from attributes_as_operators.data import dataset as attrops_dset

#from datasets import COCOActionsCompositionDatasetActivations, MomentsInTimeActOpsDataset
from data_moments import MomentsInTimeActOpsDatasetActivations
from data_cocoactions import COCOActionsCompositionDatasetActivations, COCOActionsSVODatasetActivations
from utils import Cache
import utils

cudnn.benchmark = True

parser = argparse.ArgumentParser()
parser.add_argument('--subset', type=float, default=0.9, help='proportion of data to subset')
parser.add_argument('--randomize_lr', action='store_true', default=False, help='Flag to randomize lr in same order of magnitude')
parser.add_argument('--uniform_random', action='store_true', default=False, help='Flag to NOT use He normal')

parser.add_argument('--dataset', default='moments', help='moments')
parser.add_argument('--data_dir', default='$HOME/data/Moments_in_Time_256x256_30fps', help='data root dir')
parser.add_argument('--cv_dir', default='$HOME/.cache/mit_actops_gridsearch', help='dir to save checkpoints to')
parser.add_argument('--load', default=None, help='path to checkpoint to load from')
parser.add_argument('--additional_info', default=None, help='More info to add to the log file name for easier reference')
parser.add_argument('--cuda_device', default=0, type=int, help='CUDA device to use')


# model parameters
parser.add_argument('--model', default='attributeop', help='visprodNN|redwine|labelembed+|attributeop')
parser.add_argument('--emb_dim', type=int, default=300, help='dimension of common embedding space')
parser.add_argument('--nlayers', type=int, default=2, help='number of layers for labelembed+')
parser.add_argument('--glove_init', action='store_true', default=False, help='initialize inputs with word vectors')
parser.add_argument('--clf_init', action='store_true', default=False, help='initialize inputs with SVM weights')
parser.add_argument('--static_inp', action='store_true', default=False, help='do not optimize input representations')

# regularizers
parser.add_argument('--lambda_aux', type=float, default=0.0)
parser.add_argument('--lambda_inv', type=float, default=0.0)
parser.add_argument('--lambda_comm', type=float, default=0.0)
parser.add_argument('--lambda_ant', type=float, default=0.0)

# optimization
parser.add_argument('--workers', type=int, default=8)
parser.add_argument('--batch_size', type=int, default=512)
parser.add_argument('--lr', type=float, default=1e-4)
parser.add_argument('--wd', type=float, default=5e-5)
parser.add_argument('--save_every', type=int, default=100)
parser.add_argument('--eval_val_every', type=int, default=20)
parser.add_argument('--max_epochs', type=int, default=1000)
args = parser.parse_args()


def create_nice_name(args, additional_params=None):
    def add_regularizer(nice_name, lambda_name, added_lambda):
        if args.__getattribute__('lambda_' + lambda_name) > 0.0:
            if not added_lambda:
                nice_name += '__lambda'
                added_lambda = True
            nice_name += '_{}={}'.format(lambda_name, args.__getattribute__('lambda_' + lambda_name))
        return nice_name, added_lambda

    nice_name = args.additional_info if args.additional_info else ''
    nice_name += args.dataset
    nice_name += '__he_init' if not args.uniform_random else '__uniform_init'
    nice_name += '_aux_too' if args.lambda_aux > 0 else ''
    nice_name += '__subset={:.2f}'.format(args.subset) if args.subset < 1 else ''
    added_lambda = False

    for reg_name in ['aux', 'inv', 'comm']:
        nice_name, added_lambda = add_regularizer(nice_name, reg_name, added_lambda)
    nice_name += "__lr={:0.2e}".format(args.lr)

    if additional_params:
        for param_name in additional_params:
            nice_name += '__{}={}'.format(param_name, args.__getattribute__(param_name))

    return nice_name


# Make sure cache exists
cache = Cache(dpath=args.cv_dir)
cache.cp(os.path.basename(__file__))
cache.cp('attributes_as_operators/models/models.py')
cache.write_str(args, 'args.txt')

#----------------------------------------------------------------

with torch.cuda.device(args.cuda_device):
    def train(epoch):
        model.train()

        train_loss = 0.0
        with ipdb.launch_ipdb_on_exception():
            for idx, data in tqdm.tqdm(enumerate(trainloader), total=len(trainloader)):

                data = [d.cuda() for d in data]
                loss, _ = model(data)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                train_loss += loss.item()

        train_loss = train_loss / len(trainloader)
        log_value('train_loss', train_loss, epoch)
        # Print weights
        log_value('model.obj_embedder.weight[0, 0]', model.obj_embedder.weight[0, 0].item(), epoch)
        log_value('model.obj_embedder.weight[9, 9]', model.obj_embedder.weight[9, 9].item(), epoch)
        log_value('model.obj_embedder.weight__relat', model.obj_embedder.weight[0, 0].item() / model.obj_embedder.weight[9, 9].item(), epoch)

        log_value('model.obj_clf.weight[0, 0]', model.obj_clf.weight[0, 0].item(), epoch)
        log_value('model.obj_clf.weight[9, 9]', model.obj_clf.weight[9, 9].item(), epoch)
        log_value('model.obj_clf.weight__relat', model.obj_clf.weight[0, 0].item() / model.obj_clf.weight[9, 9].item(), epoch)

        log_value('model.attr_ops[0][0, 0]', model.attr_ops[0][0, 0].item(), epoch)
        log_value('model.attr_ops[0][9, 9]', model.attr_ops[0][9, 9].item(), epoch)
        log_value('model.attr_ops__relat', model.attr_ops[0][0, 0].item() / model.attr_ops[0][9, 9].item(), epoch)

        log_value('model.attr_clf.weight[0, 0]', model.attr_clf.weight[0, 0].item(), epoch)
        log_value('model.attr_clf.weight[9, 9]', model.attr_clf.weight[9, 9].item(), epoch)
        log_value('model.attr_clf.weight__relat', model.attr_clf.weight[0, 0].item() / model.attr_clf.weight[9, 9].item(), epoch)

        log_value('model.image_embedder.mod[0].weight[0, 0]', model.image_embedder.mod[0].weight[0, 0].item(), epoch)
        log_value('model.image_embedder.mod[0].weight[9, 9]', model.image_embedder.mod[0].weight[9, 9].item(), epoch)
        log_value('image_embedder.mod[0].weight__relat', model.image_embedder.mod[0].weight[0, 0].item() / model.image_embedder.mod[0].weight[9, 9].item(), epoch)

        print('E: %d | L: %.2E' % (epoch, train_loss))

    def test(epoch, nice_name):

        model.eval()

        accuracies = []
        for idx, data in tqdm.tqdm(enumerate(testloader), total=len(testloader)):

            data = [d.cuda() for d in data]
            _, predictions = model(data)

            attr_truth, obj_truth = data[1], data[2]
            results = evaluator.score_model(predictions, obj_truth)
            match_stats = evaluator.evaluate_predictions(results, attr_truth, obj_truth)
            accuracies.append(match_stats)

        accuracies = zip(*accuracies)
        accuracies = map(torch.mean, map(torch.cat, accuracies))
        attr_acc, obj_acc, closed_acc, open_acc, objoracle_acc = accuracies

        log_value('test_attr_acc', attr_acc, epoch)
        log_value('test_obj_acc', obj_acc, epoch)
        log_value('test_closed_acc', closed_acc, epoch)
        log_value('test_open_acc', open_acc, epoch)
        log_value('test_objoracle_acc', objoracle_acc, epoch)
        print('(test) E: %d | A: %.3f | O: %.3f | Cl: %.3f | Op: %.4f | OrO: %.4f' % (epoch, attr_acc, obj_acc, closed_acc, open_acc, objoracle_acc))

        if epoch > 0 and epoch % args.save_every == 0:
            state = {
                'net': model.state_dict(),
                'epoch': epoch,
            }
            fname = 'ckpt_E_%d_A_%.3f_O_%.3f_Cl_%.3f_Op_%.3f.t7' % (epoch, attr_acc, obj_acc, closed_acc, open_acc)
            nice_name = '__' + nice_name
            fname = nice_name.join(os.path.splitext(fname))
            torch.save(state, cache.fpath(fname))

#----------------------------------------------------------------#

    action_mapping = {v: k for k, action_list in utils.MIT_SUPERCLASSES.items() for v in action_list}
    if args.dataset == 'moments':
        dpath = utils.path('~/data/Moments_in_Time_256x256_30fps')
        trainset = MomentsInTimeActOpsDatasetActivations(
            dpath=dpath,
            split='training',
            transforms=attrops_dset.imagenet_transform('train'),
            n_frames=4,
            action_mapping=action_mapping,
            keep_only_superclasses=True
        )
        testset = MomentsInTimeActOpsDatasetActivations(
            dpath=dpath,
            split='validation',
            transforms=attrops_dset.imagenet_transform('test'),
            n_frames=4,
            action_mapping=action_mapping,
            keep_only_superclasses=True
        )
    elif args.dataset == 'cocoactions':
        dpath = utils.path('~/data')
        trainset = COCOActionsCompositionDatasetActivations(
            dpath=dpath,
            split='train',
            transforms=attrops_dset.imagenet_transform('train'),
            pre_transform_size=None,
            attrops_compliance=True
        )
        testset = COCOActionsCompositionDatasetActivations(
            dpath=dpath,
            split='test',
            transforms=attrops_dset.imagenet_transform('test'),
            pre_transform_size=None,
            attrops_compliance=True
        )
    else:
        dpath = utils.path('~/data')
        trainset = COCOActionsSVODatasetActivations(
            dpath=dpath,
            split='train',
            #transforms=attrops_dset.imagenet_transform('train'),
        )
        testset = COCOActionsSVODatasetActivations(
            dpath=dpath,
            split='test',
            #transforms=attrops_dset.imagenet_transform('test'),
        )

    trainloader = torch.utils.data.DataLoader(
        trainset,
        batch_size=args.batch_size,
        shuffle=True,
        num_workers=args.workers
    )
    testloader = torch.utils.data.DataLoader(
        testset,
        batch_size=args.batch_size,
        shuffle=False,
        num_workers=args.workers
    )

    if args.model == 'visprodNN':
        model = attrops_models.VisualProductNN(trainset, args)
    elif args.model == 'redwine':
        model = attrops_models.RedWine(trainset, args)
    elif args.model == 'labelembed+':
        model = attrops_models.LabelEmbedPlus(trainset, args)
    elif args.model == 'attributeop':
        model = attrops_models.AttributeOperator(trainset, args)

    evaluator = attrops_models.Evaluator(trainset, model)

    if args.model == 'redwine':
        params = filter(lambda p: p.requires_grad, model.parameters())
        optimizer = optim.SGD(params, lr=0.01, weight_decay=args.wd, momentum=0.9)
    elif args.model == 'attributeop':
        attr_params = [param for name, param in model.named_parameters() if 'attr_op' in name and param.requires_grad]
        other_params = [param for name, param in model.named_parameters() if 'attr_op' not in name and param.requires_grad]
        optim_params = [{'params': attr_params, 'lr': 0.1 * args.lr}, {'params': other_params}]
        optimizer = optim.Adam(optim_params, lr=args.lr, weight_decay=args.wd)
    else:
        params = filter(lambda p: p.requires_grad, model.parameters())
        optimizer = optim.Adam(params, lr=args.lr, weight_decay=args.wd)

    if not args.uniform_random:
        model.image_embedder.mod[0] = utils.set_he_normal(
            model.image_embedder.mod[0],
            args.emb_dim
        )
        if args.lambda_aux > 0:
            model.obj_clf = utils.set_he_normal(
                model.obj_clf,
                args.emb_dim
            )
            model.attr_clf = utils.set_he_normal(
                model.attr_clf,
                args.emb_dim
            )

    model.cuda()
    print(model)

    start_epoch = 0
    if args.load is not None:
        with ipdb.launch_ipdb_on_exception():
            checkpoint = torch.load(args.load)
            model.load_state_dict(checkpoint['net'])
            start_epoch = checkpoint['epoch']
            print('loaded model from', os.path.basename(args.load))

    # Randomly scale lr by a 3-sigfig number in [0, 10)
    if args.randomize_lr:
        args.lr = args.lr * (int(np.random.rand() * 1000) / 100)

    nice_name = create_nice_name(args)
    logdir = cache.fpath(utils.pathjoin('log', nice_name))
    configure(logdir, flush_secs=5)
    print('Tensorboard logs at\n  {}'.format(logdir))
    for epoch in range(start_epoch, start_epoch + args.max_epochs + 1):
        train(epoch)
        if epoch % args.eval_val_every == 0:
            with torch.no_grad():
                test(epoch, nice_name)
