import argparse
import os

import numpy as np
import torch
from torch import nn
import ipdb
from tqdm import tqdm

# Added because of https://github.com/pytorch/pytorch/issues/973
import resource
rlimit = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (2048, rlimit[1]))
# Alternatively:
#torch.multiprocessing.set_sharing_strategy('file_system')

# Should be unnecessary if you pip installed
#import sys
#if 'actops' not in sys.path:
#    sys.path.append(os.path.realpath('.'))

# Import components for Actops
from actops.linguistic_actions import (
    LinguisticActionTripletEmbeddingSystem,
    LinguisticActionEmbeddingModel,
    EmbeddingPredictor,
    SVONegativeMiner,
)

# Import components for Attrops
from actops.external.attributes_as_operators.models.models import AttributeOperator
from actops.attrops_system import (
    AttropsSVNegativeMiner,
    AttropsSVTripletEmbeddingSystem,
    AttropsNamespace,
    AttropsEmbeddingPredictor
)

# Import datasets
from actops.dataproc.data_cocoactions import (
    COCOActionsSVODatasetActivationsTriplet,
    COCOActionsSVODatasetActivationsEmbeddings,
)

# Import components for FC network
from actops.svo_classification_system import (
    SVOClassificationPredictor,
    SVOClassificationModel,
    SVOClassificationSystem,
)

from actops import utils

# Constants
VISUAL_EMBEDDER_DEPTH = 3
MODEL_CODE_OPTIONS = ['actops', 'attrops', 'fc']
FIXED_FC_CLF_DEPTH = 5


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_dir',
        default='~/data/coco-actions',
        help='location of data'
    )
    parser.add_argument(
        '--df_fname',
        default='coco-actions-df_predicted_single-estimated_svo-split.pkl',
        help='name of pickled DataFrame file containing annotations'
    )
    parser.add_argument(
        '--csv_fname',
        default=None,
        help="name of csv of dataframe. If not given, it's inferred from df_fname"
    )
    parser.add_argument(
        '--clf_depth',
        default=FIXED_FC_CLF_DEPTH,
        type=int,
        help='Number of layers to use for a FC network (see: svo_classification_system.SVOClassificationModel)'
    )
    parser.add_argument(
        '--n_candidates',
        default=3,
        type=int,
        help='Positive and negative samples to use when mining hard positives and negatives'
    )
    parser.add_argument(
        '--eval_val_every',
        default=10,
        type=int,
        help='How often should we evaluate validation data and try to save model?'
    )
    parser.add_argument(
        '--n_workers',
        default=8,
        type=int,
        help='Number of threads'
    )
    parser.add_argument(
        '--embed_dim',
        default=300,
        type=int,
        help='dimensionality of embedding space'
    )
    parser.add_argument(
        '--batch_size',
        default=512,
        type=int,
        help='loader batch size'
    )
    parser.add_argument(
        '--test',
        action='store_true',
        help='run tests instead of train'
    )
    parser.add_argument(
        '--test_batch',
        action='store_true',
        help='run test batches instead of train'
    )
    parser.add_argument(
        '--prep',
        action='store_true',
        help='prepare for training'
    )
    parser.add_argument(
        '--reset',
        action='store_true',
        help='Reset model, ignoring caches'
    )
    parser.add_argument(
        '--randomize_lr',
        action='store_true',
        help='randomize LR in same order of magnitude'
    )
    parser.add_argument(
        '--nice_name',
        default='actops',
        help='Human-readable name (or prefix) for model saves'
    )
    parser.add_argument(
        '--load',
        default='unspecified',
        help='model checkpoint to load, only used when restarting or testing'
    )
    parser.add_argument(
        '--model',
        default='actops',
        help='What kind of model?',
        choices=MODEL_CODE_OPTIONS,
    )
    parser.add_argument(
        '--cache_dir',
        default='~/.cache/cocoactions',
        help='location for all cached data'
    )
    parser.add_argument(
        '--verbosity',
        default=1,
    )
    parser.add_argument(
        '--triplet_margin',
        default=0.5,
        type=float,
        help='margin for use in triplet loss'
    )
    parser.add_argument(
        '--lambda_vis',
        default=1.0,
        type=float,
        help='weight for visual loss in actops'
    )
    parser.add_argument(
        '--lambda_ling',
        default=1.0,
        type=float,
        help='weight for linguistic loss in actops'
    )
    parser.add_argument(
        '--lambda_aux',
        default=1.0,
        type=float,
        help='weight for auxiliary loss in attrops'
    )
    parser.add_argument(
        '--lambda_comm',
        default=1.0,
        type=float,
        help='weight for commutative loss in attrops'
    )
    parser.add_argument(
        '--epochs',
        default=200,
        type=int,
        help='epochs to train'
    )
    parser.add_argument(
        '--device',
        default=0,
        type=int,
        help='CUDA device to use'
    )
    parser.add_argument(
        '--lr',
        default=8e-4,
        type=float,
        help='Learning rate'
    )
    parser.add_argument(
        '--momentum',
        default=0.9,
        type=float,
        help='SGD momentum'
    )
    parser.add_argument(
        '--train_ratio',
        default=0.7,
        type=float,
        help='ratio of train images to total'
    )
    parser.add_argument(
        '--sample_all_triplet_combos',
        action='store_true',
        help='Do we work in a space of all possible triplet combinations? Less bias, harder to learn.'
    )
    parser.add_argument(
        '--test_tracker_file',
        default='test_tracker.csv',
        help='where do we keep track of test performance metrics?'
    )
    parser.add_argument(
        '--embed',
        action='store_true',
        help='Should we embed the validation data?'
    )
    parser.add_argument(
        '--overwrite_tests',
        action='store_true',
        help='Should we overwrite our current tests?'
    )
    return parser.parse_args()


def init_system(args, dsets, loaders):
    """ Initializes and the NetworkSystem object for training and testing
    """
    with ipdb.launch_ipdb_on_exception():
        with torch.cuda.device(args.device):
            if args.model == 'actops':

                # Create nice name according to structure of arguments
                nice_name = LinguisticActionTripletEmbeddingSystem.create_nice_name(args)

                # Which metrics should we track (in tensorboard)
                trackable_metrics = [
                    'loss_train',
                    'loss_triplet_train',
                    'loss_vis_train',
                    'loss_ling_train',
                    'loss_val',
                    'acc_open_val',
                    'acc_closed_val',
                ]

                # Build the model
                model = LinguisticActionEmbeddingModel(
                    dset=dsets['train'],
                    vis_in_dim=dsets['train'].features.shape[1],
                    word_in_dim=args.embed_dim,
                    out_dim=args.embed_dim,
                    lambda_vis=args.lambda_vis,
                    lambda_ling=args.lambda_ling,
                    n_subjects=len(loaders['train'].dataset.subjects),
                    n_verbs=len(loaders['train'].dataset.verbs),
                    n_objects=len(loaders['train'].dataset.objects),
                    visual_embedder_depth=VISUAL_EMBEDDER_DEPTH,
                ).cuda()

                # Optimize with SGD
                optimizer = torch.optim.SGD(
                    model.parameters(),
                    lr=args.lr,
                    momentum=args.momentum
                )

                # The model and miner use different objecives purely because
                # the miner needs the loss for every sample in the batch
                # (reduction='none') and the optimizer doesn't
                objective = nn.TripletMarginLoss(margin=args.triplet_margin)
                mining_objective = nn.TripletMarginLoss(
                    margin=args.triplet_margin,
                    reduction='none'
                )

                # Predictors, since embedding prediction requires nontrivial
                # computation
                predictor_open = EmbeddingPredictor(
                    dset=dsets['train'],
                    model=model
                )
                predictor_closed = EmbeddingPredictor(
                    dset=dsets['train'],
                    svo=dsets['train'].svo_test,
                    model=model
                )

                miner = SVONegativeMiner(
                    model=model,
                    objective=mining_objective,
                    device=torch.cuda.current_device(),
                    n_candidates=args.n_candidates
                )

                hash_on = {
                    'model': model,
                    'objective': objective,
                    'optimizer': optimizer,
                    'mining_objective': mining_objective,
                }

                system = LinguisticActionTripletEmbeddingSystem(
                    eval_val_every=args.eval_val_every,
                    save_every=-1,
                    selection_metric='acc_open_val',
                    selection_metric_goal='max',
                    reset=args.reset,
                    device=torch.cuda.current_device(),
                    verbosity=args.verbosity,
                    hash_on=hash_on,
                    nice_name=nice_name,
                    metrics=trackable_metrics,
                    model=model,
                    objective=objective,
                    miner=miner,
                    loaders=loaders,
                    optimizer=optimizer,
                    margin=args.triplet_margin,
                    lambda_vis=args.lambda_vis,
                    lambda_ling=args.lambda_ling,
                    predictor_open=predictor_open,
                    predictor_closed=predictor_closed,
                )

            elif args.model == 'attrops':
                nice_name = AttropsSVTripletEmbeddingSystem.create_nice_name(args)

                trackable_metrics = [
                    'loss_train',
                    'loss_val',
                    'acc_open_val',
                    'acc_closed_val',
                    #'error_top_1_train',
                    #'error_top_5_train',
                    #'error_top_1_val',
                    #'error_top_5_val',
                    #'pos_loss_train',
                    #'neg_loss_train',
                ]

                # AttributeOperator expects certain attributes of dsets
                # objs and attrs are subject and verb words, respectively
                # pairs are expected to be (attr, obj) words
                for split, dset in dsets.items():
                    dset.feat_dim = dset.data['features'][0].shape[0]
                    dset.objs = dset.subjects
                    dset.attrs = dset.verbs
                    dset.attr2idx = dset.verb2idx
                    dset.obj2idx = dset.subject2idx
                    dset.pairs = [(v, s) for s, v, _ in dset.unique_svo]
                    dset.pair2idx = {pair: i for i, pair in enumerate(dset.pairs)}

                    # Grab the SVO triplets according to the split (train
                    # should have more), and turn them into the (attr, obj)
                    # pairs that AttributeOperator expects
                    dset.test_pairs, dset.train_pairs = [
                        [(v, s) for s, v, _ in split_pairs]
                        for split_pairs in sorted(
                            dset.split_svo(),
                            key=len
                        )
                    ]

                # Create dummy args for AttributeOperator
                attrops_args = AttropsNamespace(
                    emb_dim=args.embed_dim,
                    glove_init=True,
                    dataset='coco-actions-svo',
                    lambda_ant=0,
                    lambda_inv=0,
                    lambda_comm=args.lambda_comm,
                    lambda_aux=args.lambda_aux,
                    static_inp=False
                )

                model = AttributeOperator(
                    dset=dsets['train'],
                    args=attrops_args
                ).cuda()

                optimizer = torch.optim.SGD(
                    model.parameters(),
                    lr=args.lr,
                    momentum=args.momentum
                )

                predictor_open = AttropsEmbeddingPredictor(
                    dset=dsets['train'],
                    model=model
                )
                predictor_closed = AttropsEmbeddingPredictor(
                    dset=dsets['train'],
                    pairs=dsets['train'].test_pairs,
                    model=model
                )

                # The miner uses the triplet loss for computing the hardest
                mining_objective = nn.TripletMarginLoss(
                    margin=args.triplet_margin,
                    reduction='none'
                )

                miner = AttropsSVNegativeMiner(
                    model=model,
                    device=torch.cuda.current_device(),
                    objective=mining_objective,
                    n_candidates=args.n_candidates
                )

                hash_on = {
                    'model': model,
                    'optimizer': optimizer,
                }

                # TODO fix this
                system = AttropsSVTripletEmbeddingSystem(
                    eval_val_every=args.eval_val_every,
                    save_every=-1,
                    selection_metric='acc_open_val',
                    selection_metric_goal='max',
                    reset=args.reset,
                    device=torch.cuda.current_device(),
                    verbosity=args.verbosity,
                    hash_on=hash_on,
                    nice_name=nice_name,
                    metrics=trackable_metrics,
                    model=model,
                    miner=miner,
                    loaders=loaders,
                    optimizer=optimizer,
                    margin=args.triplet_margin,
                    lambda_aux=args.lambda_aux,
                    lambda_comm=args.lambda_comm,
                    predictor_open=predictor_open,
                    predictor_closed=predictor_closed,
                )

            elif args.model == 'fc':
                nice_name = SVOClassificationSystem.create_nice_name(args)

                trackable_metrics = [
                    'loss_train',
                    'loss_subject_train',
                    'loss_verb_train',
                    'loss_object_train',
                    'loss_val',
                    'acc_open_svo_val',
                    'acc_open_sv_val',
                    'acc_open_v_val',
                ]

                model = SVOClassificationModel(
                    dset=dsets['train'],
                    vis_in_dim=dsets['train'].features.shape[1],
                    clf_depth=args.clf_depth,
                ).cuda()

                objective = nn.CrossEntropyLoss()
                optimizer = torch.optim.SGD(
                    model.parameters(),
                    lr=args.lr,
                    momentum=args.momentum
                )

                hash_on = {
                    'model': model,
                    'optimizer': optimizer,
                }

                system = SVOClassificationSystem(
                    eval_frequency=args.eval_val_every,
                    save_every=-1,
                    selection_metric='acc_open_svo_val',
                    selection_metric_goal='max',
                    reset=args.reset,
                    device=torch.cuda.current_device(),
                    verbosity=args.verbosity,
                    hash_on=hash_on,
                    nice_name=nice_name,
                    metrics=trackable_metrics,
                    model=model,
                    loaders=loaders,
                    optimizer=optimizer,
                    objective=objective,
                    margin=args.triplet_margin,
                )
            else:
                raise ValueError('Model type not understood')
            return system


def train(args):
    # If specced, generate random LR in approximately same order of magnitude
    if args.randomize_lr:
        args.lr = args.lr * (int(np.random.rand() * 1000) / 100)

    import ipdb
    with ipdb.launch_ipdb_on_exception():
        dsets = {
            split: COCOActionsSVODatasetActivationsTriplet(
                dpath=args.data_dir,
                split=split,
                verbosity=int(args.verbosity),
                train_ratio=args.train_ratio,
                sample_only_valid_triplets=not args.sample_all_triplet_combos,
                n_candidates=args.n_candidates,
            )
            for split in ('train', 'test')
        }
    # To appeas the network system
    dsets['val'] = dsets['test']
    loaders = {
        split: torch.utils.data.DataLoader(
            dsets[split],
            batch_size=args.batch_size,
            shuffle=split == 'train',
            num_workers=args.n_workers
        )
        for split in dsets.keys()
    }
    system = init_system(args, dsets, loaders)

    with torch.cuda.device(args.device):
        with ipdb.launch_ipdb_on_exception():
            system.train(args.epochs)


def save_test_results(performance_dict, model_save_name, file_save_name, overwrite=False):
    keys, values = list(zip(*[(k, performance_dict[k]) for k in sorted(performance_dict)]))

    header = 'save_name,' + ','.join(keys)
    this_line = model_save_name + ',' + ','.join(['{:.6f}'.format(v.item()) for v in values])
    if os.path.exists(file_save_name) and not overwrite:
        with open(file_save_name, 'r') as fid:
            lines = fid.readlines()
            lines = [l for l in lines if not l.startswith(model_save_name)]
            use_header = False if len(lines) and lines[0].startswith(header) else True
    else:
        lines = []
        use_header = True
    with open(file_save_name, 'w') as fid:
        if use_header:
            fid.write(header + '\n')
        for line in lines:
            fid.write(line)
        fid.write(this_line + '\n')


def load_cached_system_data(fpath):
    import pickle
    with open(fpath, 'rb') as fid:
        cached = pickle.load(fid)
    return cached


def plot_tsne(args):
    from matplotlib import pyplot as plt
    data = torch.load(args.load)

    fig, axs = plt.subplots(1, 1, subplot_kw=dict(xticks=[], yticks=[]))

    fig.suptitle(args.title, fontsize=20)

    axes = fig.add_subplot(1, 1, 1, xticks=[], yticks=[])
    axes.scatter(
        data[:, 0],
        data[:, 1],
        c=np.array([[0.75, 0.75, 0.75]]),
        marker='.',
    )
    save_name = '_'.join(args.title.split()) + '.png'
    fig.set_size_inches(w=6, h=6)
    fig.savefig(save_name)
    plt.close('all')


def show_verb_embeddings(dset, title=None, verbs=None, color_indices=None):
    with torch.no_grad():
        from matplotlib import pyplot as plt
        if verbs is None:
            verbs = sorted({svo[1] for svo in dset.svo_test})
        else:
            verbs = sorted(verbs)
        verb_svo_lookup = {k: [svo for svo in dset.svo_test if svo[1] == k] for k in verbs}
        if color_indices is None:
            colors = utils.distinct_colors(len(verbs))
        else:
            colors = utils.distinct_colors(max(color_indices) + 1)
            colors = [colors[i] for i in color_indices]

        verb_indices = {
            verb: torch.LongTensor([
                i
                for i in dset.idx_lookup
                if dset.data['verb'][i] == verb
            ])
            for verb in verbs
        }
        verb_indices = {k: v for k, v in verb_indices.items() if len(v) > 0}
        n_verbs = len(verb_indices)
        n_rows = 1
        n_cols = int(np.ceil(n_verbs / n_rows))
        while n_cols > 4:
            n_rows += 1
            n_cols = int(np.ceil(n_verbs / n_rows))

        fig, axs = plt.subplots(n_rows, n_cols, subplot_kw=dict(frame_on=False, xticks=[], yticks=[]))
        only_val = torch.LongTensor(dset.idx_lookup)

        if title is None:
            title = 'Validation Data in t-SNE Space\n(Colored by Actions)'
        fig.suptitle(title, fontsize=20)
        #axes = [None for i in range(n_verbs)]
        ipdb.set_trace()
        for verb_index, (verb, data_indices) in enumerate(verb_indices.items()):
            ax_row = verb_index // n_cols
            ax_col = verb_index % n_cols
            this_verb_svo = '\n'.join([' '.join(svo) for svo in verb_svo_lookup[verb]])
            this_title = '{}\n{}'.format(verb, this_verb_svo)
            if isinstance(axs, np.ndarray):
                axs[ax_row][ax_col] = fig.add_subplot(n_rows, n_cols, verb_index + 1, title=this_title, frame_on=True, xticks=[], yticks=[])
                axs[ax_row][ax_col].scatter(
                    dset.embeddings[only_val, 0],
                    dset.embeddings[only_val, 1],
                    c=np.array([[.75, .75, .75, .75]]),
                    marker='.',
                    label=verb,
                )
                axs[ax_row][ax_col].scatter(
                    dset.embeddings[data_indices, 0],
                    dset.embeddings[data_indices, 1],
                    c=np.array([colors[verb_index]]) / 255,
                    marker='.',
                    label=verb,
                )
            else:
                axs = fig.add_subplot(n_rows, n_cols, verb_index + 1, title=this_title, frame_on=True, xticks=[], yticks=[])
                axs.scatter(
                    dset.embeddings[only_val, 0],
                    dset.embeddings[only_val, 1],
                    c=np.array([[.75, .75, .75, .75]]),
                    marker='.',
                    label=verb,
                )
                axs.scatter(
                    dset.embeddings[data_indices, 0],
                    dset.embeddings[data_indices, 1],
                    c=np.array([colors[verb_index]]) / 255,
                    marker='.',
                    label=verb,
                )
        save_name = '_'.join(title.split())
        if not save_name.endswith('.png'):
            save_name += '.png'
        fig.set_size_inches(w=12 * n_cols, h=12 * n_rows)
        print('saving {}'.format(save_name))
        fig.savefig(save_name)
        plt.close('all')


def test(args, load_paths=None):
    with torch.cuda.device(args.device):
        # Initialization for loop later
        model = None
        predictor_open = predictor_closed = None
        dsets = {
            split: COCOActionsSVODatasetActivationsTriplet(
                dpath=args.data_dir,
                split=split,
                verbosity=int(args.verbosity),
                train_ratio=args.train_ratio,
                sample_only_valid_triplets=not args.sample_all_triplet_combos,
                n_candidates=args.n_candidates,
            )
            for split in ['train', 'test']
        }
        # To appease the network system
        dsets['val'] = dsets['test']
        loaders = {
            split: torch.utils.data.DataLoader(
                dsets[split],
                batch_size=args.batch_size,
                shuffle=split == 'train',
                num_workers=args.n_workers
            )
            for split in dsets.keys()
        }

        if load_paths is None:
            model, predictor_open, predictor_closed = update_load_model(
                args.load,
                dsets,
                loaders,
                model_code=args.model,
                model=model,
                predictor_open=predictor_open,
                predictor_closed=predictor_closed
            )
            run_record_tests(
                args.load,
                loaders,
                dsets,
                model,
                model_code=args.model,
                predictor_open=predictor_open,
                predictor_closed=predictor_closed,
                overwrite=args.overwrite_tests
            )

        else:
            # load_paths should be list or similar iterable container of strings
            assert not isinstance(load_paths, str)
            load_paths = [p for p in load_paths if 'aux' not in p]

            for fpath in tqdm(load_paths):
                print("Model at:\n  {}".format(fpath))
                print("  Loading...")
                model, predictor_open, predictor_closed = update_load_model(
                    fpath,
                    dsets,
                    loaders,
                    model,
                    predictor_open,
                    predictor_closed
                )

                # Run and record test with current settings
                print("  Evaluating...", flush=True)
                run_record_tests(
                    fpath,
                    loaders,
                    dsets,
                    model,
                    model_code=args.model,
                    predictor_open=predictor_open,
                    predictor_closed=predictor_closed,
                    overwrite=args.overwrite_tests
                )

        print("Done evaluating model(s). Find the results at {}".format(args.test_tracker_file))
    return


def update_load_model(fpath, dsets, loaders, model_code='actops', **kwargs):
    """ Updates model or loads it as needed.
        Currently checks:
            model.out_dim
            model.lambda_vis
            model.lambda_ling
    """
    if model_code not in MODEL_CODE_OPTIONS:
        raise ValueError('model_code must be one of {}'.format(MODEL_CODE_OPTIONS))

    model = kwargs.get('model')
    predictor_open = kwargs.get('predictor_open')
    predictor_closed = kwargs.get('predictor_closed')

    # Parse out relevant information from filepath
    if model_code == 'actops':
        parsed_args_dict = LinguisticActionTripletEmbeddingSystem.parse_nice_name(fpath)
    elif model_code == 'attrops':
        parsed_args_dict = AttropsSVTripletEmbeddingSystem.parse_nice_name(fpath)
    elif model_code == 'fc':
        parsed_args_dict = SVOClassificationSystem.parse_nice_name(fpath)

    saved_info = torch.load(fpath)

    # If model doesn't exist yet, load it
    if model is None:
        reload_model = True
    else:
        # If the existing model's embedding dimension is different from the new
        # one, or the old model had no visual loss term and the new one does,
        # reload the model
        diff = []

        # Commented out because all models have same dimensionality, but not
        # all have this attribute
        if False:
            # Different embedding dimensions
            diff.append(model.out_dim != parsed_args_dict['embed-dim'])

        # Different models have different hyperparameters
        if model_code == 'actops':
            # Old linguistic loss is None and new isn't, or old is not None and new is
            diff.append(model.lambda_ling <= 0 and parsed_args_dict['lambda-ling'] > 0)
            diff.append(model.lambda_ling > 0 and parsed_args_dict['lambda-ling'] <= 0)

            # Old visual loss is None and new isn't, or old is not None and new is
            diff.append(model.lambda_vis <= 0 and parsed_args_dict['lambda-vis'] > 0)
            diff.append(model.lambda_vis > 0 and parsed_args_dict['lambda-vis'] <= 0)

        elif model_code == 'attrops':
            # Old linguistic loss is None and new isn't, or old is not None and new is
            diff.append(model.args.lambda_comm <= 0 and parsed_args_dict['lambda-comm'] > 0)
            diff.append(model.args.lambda_comm > 0 and parsed_args_dict['lambda-comm'] <= 0)

            # Old visual loss is None and new isn't, or old is not None and new is
            diff.append(model.args.lambda_aux <= 0 and parsed_args_dict['lambda-aux'] > 0)
            diff.append(model.args.lambda_aux > 0 and parsed_args_dict['lambda-aux'] <= 0)

        elif model_code == 'fc':
            pass

        reload_model = any(diff)

    if reload_model:
        if model_code == 'actops':
            model = LinguisticActionEmbeddingModel(
                dset=dsets['train'],
                vis_in_dim=dsets['train'].features.shape[1],
                word_in_dim=int(parsed_args_dict['embed-dim']),
                out_dim=int(parsed_args_dict['embed-dim']),
                lambda_vis=parsed_args_dict['lambda-vis'],
                lambda_ling=parsed_args_dict['lambda-ling'],
                n_subjects=len(loaders['train'].dataset.subjects),
                n_verbs=len(loaders['train'].dataset.verbs),
                n_objects=len(loaders['train'].dataset.objects),
                visual_embedder_depth=VISUAL_EMBEDDER_DEPTH,
            ).cuda()
            predictor_open = EmbeddingPredictor(
                dset=dsets['train'],
            )
            predictor_closed = EmbeddingPredictor(
                dset=dsets['train'],
                svo=dsets['train'].svo_test,
            )
        elif model_code == 'attrops':
            # Only set the required dset attributes if we haven't already done that
            try:
                assert dsets['train'].feat_dim > 0
            except AttributeError:
                for split, dset in dsets.items():
                    dset.feat_dim = dset.data['features'][0].shape[0]
                    dset.objs = dset.subjects
                    dset.attrs = dset.verbs
                    dset.attr2idx = dset.verb2idx
                    dset.obj2idx = dset.subject2idx
                    dset.pairs = [(v, s) for s, v, _ in dset.unique_svo]
                    dset.pair2idx = {pair: i for i, pair in enumerate(dset.pairs)}

                    # Grab the SVO triplets according to the split (train
                    # should have more), and turn them into the (attr, obj)
                    # pairs that AttributeOperator expects
                    dset.test_pairs, dset.train_pairs = [
                        [(v, s) for s, v, _ in split_pairs]
                        for split_pairs in sorted(
                            dset.split_svo(),
                            key=len
                        )
                    ]
            # Create dummy args for AttributeOperator
            attrops_args = AttropsNamespace(
                emb_dim=args.embed_dim,
                glove_init=True,
                dataset='coco-actions-svo',
                lambda_ant=0,
                lambda_inv=0,
                lambda_comm=parsed_args_dict['lambda-comm'],
                lambda_aux=parsed_args_dict['lambda-aux'],
                static_inp=False
            )
            model = AttributeOperator(
                dset=dsets['train'],
                args=attrops_args
            ).cuda()
            predictor_open = AttropsEmbeddingPredictor(
                dset=dsets['train'],
                model=model
            )
            predictor_closed = AttropsEmbeddingPredictor(
                dset=dsets['train'],
                pairs=dsets['train'].test_pairs,
                model=model
            )
        elif model_code == 'fc':
            model = SVOClassificationModel(
                dset=dsets['train'],
                vis_in_dim=dsets['train'].features.shape[1],
                clf_depth=FIXED_FC_CLF_DEPTH,
            ).cuda()
            predictor_open = SVOClassificationPredictor(
                dset=dsets['train'],
            )
            predictor_closed = None
    else:
        if model_code == 'actops':
            # If the model doesn't need to be redefined, just update the
            # lambda_vis and lambda_ling term (which may change nothing, if
            # it's the same)
            model.lambda_ling = parsed_args_dict['lambda-ling']
            model.lambda_vis = parsed_args_dict['lambda-vis']
        elif model_code == 'attrops':
            model.args.lambda_comm = parsed_args_dict['lambda-comm']
            model.args.lambda_aux = parsed_args_dict['lambda-aux']
        elif model_code == 'fc':
            pass

    # Load the state_dict and update predictors' embeddings
    model.load_state_dict(saved_info['model'])
    model = model.cuda()
    if model_code in ['attrops', 'actops']:
        predictor_open.generate_embeddings(model)
        predictor_closed.generate_embeddings(model)

    return model, predictor_open, predictor_closed


def run_record_tests(model_fpath, loaders, dsets, model, model_code,
                     predictor_open, predictor_closed, overwrite=False):

    # Different models have different capabilities
    predicted_svo_open = predicted_pair_open = None
    actual_svo = actual_pair = None
    predicted_svo_closed = predicted_pair_closed = None
    if model_code in ['actops', 'fc']:
        predicted_svo_open = []
        actual_svo = []
        if model_code == 'actops':
            predicted_svo_closed = []
    elif model_code == 'attrops':
        predicted_pair_open = []
        predicted_pair_closed = []
        actual_pair = []

    # To reflect the different output formats of the networks
    visual_embeddings = outs = None
    for i, data in enumerate(loaders['test']):
        visual = data[0].cuda()
        if model_code == 'actops':
            visual_embeddings, _ = model(visual, None, None, None)
        elif model_code == 'attrops':
            visual_embeddings = model.image_embedder(visual)
        elif model_code == 'fc':
            outs = model(visual)

        subs_i, verbs_i, objs_i = [
            torch.LongTensor(t).cuda()
            for t in data[1:4]
        ]

        # Indices of actual pair (V, S) or triple (S, V, O)
        if model_code == 'attrops':
            actual_pair.append(data[1:3])
        else:
            actual_svo.append(data[1:4])

        # Get predictions
        if model_code == 'actops':
            # Indices of predicted S, V, O in open-world setting
            predicted_svo_open.append([
                [
                    dsets['test'].subject2idx[s],
                    dsets['test'].verb2idx[v],
                    dsets['test'].object2idx[o],
                ]
                for s, v, o in map(
                    lambda x: predictor_open(x),
                    visual_embeddings
                )
            ])

            # Indices of predicted S, V, O in closed-world setting
            predicted_svo_closed.append([
                [
                    dsets['test'].subject2idx[s],
                    dsets['test'].verb2idx[v],
                    dsets['test'].object2idx[o],
                ]
                for s, v, o in map(
                    lambda x: predictor_closed(x),
                    visual_embeddings
                )
            ])
        elif model_code == 'attrops':
            # Indices of predicted V, S in open-world setting
            predicted_pair_open.append([
                [
                    dsets['test'].attr2idx[v],
                    dsets['test'].obj2idx[s],
                ]
                for v, s in map(
                    lambda x: predictor_open(x),
                    visual_embeddings
                )
            ])

            # Indices of predicted S, V, O in closed-world setting
            predicted_pair_closed.append([
                [
                    dsets['test'].attr2idx[v],
                    dsets['test'].obj2idx[s],
                ]
                for v, s in map(
                    lambda x: predictor_closed(x),
                    visual_embeddings
                )
            ])
        elif model_code == 'fc':
            # Indices of predicted V, S in open-world setting
            predicted_svo_open.append(predictor_open(outs))

    if model_code in ['actops', 'fc']:
        # Stack everything into a Nx3 tensor (SVO for each sample)
        actual_svo = torch.cat(list(map(lambda x: torch.stack(x, 1), actual_svo))).float()
        actual_svo[actual_svo < 0] = 0

        predicted_svo_open = torch.cat(list(map(torch.tensor, predicted_svo_open))).float()

        if model_code == 'actops':
            predicted_svo_closed = torch.cat(list(map(torch.tensor, predicted_svo_closed))).float()
            correct_prediction_closed = (actual_svo == predicted_svo_closed).float()
        else:
            correct_prediction_closed = None

        # Was the prediction for S, V, O correct?
        correct_prediction_open = (actual_svo.cpu() == predicted_svo_open.cpu()).float()

    elif model_code == 'attrops':
        # Stack everything into a Nx3 tensor (SVO for each sample)
        actual_pair = torch.cat(list(map(lambda x: torch.stack(x, 1), actual_pair))).float()
        actual_pair[actual_pair < 0] = 0

        predicted_pair_open = torch.cat(list(map(torch.tensor, predicted_pair_open))).float()
        predicted_pair_closed = torch.cat(list(map(torch.tensor, predicted_pair_closed))).float()

        # Was the prediction for S, V, O correct?
        correct_prediction_open = (actual_pair == predicted_pair_open).float()
        correct_prediction_closed = (actual_pair == predicted_pair_closed).float()

    # Compute open accuracy
    correct_svo_open = ((correct_prediction_open).sum(1) == 3).float()
    correct_sv_open = ((correct_prediction_open[:, :2]).sum(1) == 2).float()
    correct_v_open = correct_prediction_open[:, 1].float()

    svo_acc_open = correct_svo_open.sum() / len(correct_svo_open)
    sv_acc_open = correct_sv_open.sum() / len(correct_sv_open)
    v_acc_open = correct_v_open.sum() / len(correct_v_open)

    if model_code in ['actops', 'attrops']:
        correct_svo_closed = ((correct_prediction_closed).sum(1) == 3).float()
        correct_sv_closed = ((correct_prediction_closed[:, :2]).sum(1) == 2).float()
        correct_v_closed = correct_prediction_closed[:, 1].float()

        # Composition accuracy
        svo_acc_closed = correct_svo_closed.sum() / len(correct_svo_closed)
        sv_acc_closed = correct_sv_closed.sum() / len(correct_sv_open)
        v_acc_closed = correct_v_closed.sum() / len(correct_v_open)
    else:
        svo_acc_closed = sv_acc_closed = v_acc_closed = torch.tensor(-1)

    if model_code == 'attrops':
        svo_acc_closed = svo_acc_open = torch.tensor(-1)

    performance_dict = {
        'svo_accuracy_open': svo_acc_open,
        'svo_accuracy_closed': svo_acc_closed,
        'sv_accuracy_open': sv_acc_open,
        'sv_accuracy_closed': sv_acc_closed,
        'v_accuracy_open': v_acc_open,
        'v_accuracy_closed': v_acc_closed,
    }

    save_name = ('_' + model.__class__.__name__).join(os.path.splitext(args.test_tracker_file))

    save_test_results(performance_dict, model_fpath, save_name, overwrite=overwrite)


def get_best_model_checkpoints(root_dir):
    def is_checkpoint(filepath):
        fn = os.path.split(filepath)[-1]
        return fn.startswith('E=') and fn.endswith('.t7')

    def checkpoint_epoch(filepath):
        e = os.path.splitext(os.path.split(filepath)[-1])[0]
        return int(e.split('=')[-1])

    models_dir = utils.Cache(root_dir)
    model_paths = [models_dir.fpath(p) for p in models_dir.ls() if p != 'logs']
    best_checkpoints = []
    for model_path in model_paths:
        checkpoints = list(filter(is_checkpoint, utils.ls_r(model_path)))
        if len(checkpoints):
            checkpoints = sorted(checkpoints, key=checkpoint_epoch)
            best_checkpoints.append(checkpoints[-1])

    return best_checkpoints


def embed(args):
    with torch.cuda.device(args.device):
        # Initialization for loop later
        model = None
        model_class_name = os.path.split(os.path.split(args.load)[0])[-1]
        parsed_args_dict = LinguisticActionTripletEmbeddingSystem.parse_nice_name(args.load)
        embeddings_fname = '{}_{}_{}_E={}_embeddings.t7'.format(
            parsed_args_dict['user_defined_nice_name'],
            parsed_args_dict['dataset'],
            model_class_name,
            parsed_args_dict['epoch']
        )
        dsets = {
            split: COCOActionsSVODatasetActivationsEmbeddings(
                dpath=args.data_dir,
                split=split,
                verbosity=int(args.verbosity),
                train_ratio=args.train_ratio,
                sample_only_valid_triplets=False,
                n_candidates=0,
                embeddings_fname=embeddings_fname,
            )
            for split in ['train', 'test']
        }

        # To appease the network system
        dsets['val'] = dsets['test']

        model = LinguisticActionEmbeddingModel(
            dset=dsets['train'],
            vis_in_dim=dsets['train'].features.shape[1],
            word_in_dim=args.embed_dim,
            out_dim=args.embed_dim,
            lambda_vis=args.lambda_vis,
            lambda_ling=args.lambda_ling,
            n_subjects=len(dsets['train'].subjects),
            n_verbs=len(dsets['train'].verbs),
            n_objects=len(dsets['train'].objects),
            visual_embedder_depth=VISUAL_EMBEDDER_DEPTH,
        ).cuda()

        for dset in dsets.values():
            dset.model = model
            dset.load_or_generate_and_save_embeddings(embeddings_fname=embeddings_fname, batch_size=args.batch_size)
        print("Generated embeddings for data - can be found at {}".format(
            dsets['train'].cache.fpath(embeddings_fname))
        )


def test_batch(args):
    with ipdb.launch_ipdb_on_exception():
        dsets = {
            split: COCOActionsSVODatasetActivationsTriplet(
                dpath=args.data_dir,
                split=split,
                verbosity=int(args.verbosity),
                train_ratio=args.train_ratio,
                sample_only_valid_triplets=not args.sample_all_triplet_combos,
                n_candidates=args.n_candidates,
            )
            for split in ('train', 'test')
        }
        # To appeas the network system
        dsets['val'] = dsets['test']
        loaders = {
            split: torch.utils.data.DataLoader(
                dsets[split],
                batch_size=args.batch_size,
                shuffle=split == 'train',
                num_workers=args.n_workers
            )
            for split in dsets.keys()
        }
        model = predictor_open = predictor_closed = None

        checkpoints = get_best_model_checkpoints(args.load)
        for fpath in tqdm(checkpoints):
            print("Model at:\n  {}".format(fpath))
            print("  Loading...")
            # TODO start here
            # Outdated models can be ignored
            if args.model == 'actops':
                if 'aux' in fpath:
                    continue

            model, predictor_open, predictor_closed = update_load_model(
                fpath,
                dsets,
                loaders,
                model_code=args.model,
                model=model,
                predictor_open=predictor_open,
                predictor_closed=predictor_closed
            )

            # Run and record test with current settings
            print("  Evaluating...", flush=True)
            run_record_tests(
                fpath,
                loaders,
                dsets,
                model,
                model_code=args.model,
                predictor_open=predictor_open,
                predictor_closed=predictor_closed,
                overwrite=args.overwrite_tests
            )

        save_name = ('_' + model.__class__.__name__).join(os.path.splitext(args.test_tracker_file))
        print("Done evaluating model(s). Find the results at {}".format(save_name))


if __name__ == "__main__":
    args = parse_args()
    if args.test_batch:
        print('*******')
        print('Testing a batch of models')
        if args.overwrite_tests:
            print("CAUTION: OVERWRITING EXISTING DATA. KILL PROGRAM NOW IF YOU DON'T WANT THIS.")
        print('*******')
        test_batch(args)
    elif args.test:
        test(args)
    elif args.embed:
        embed(args)
    else:
        train(args)
