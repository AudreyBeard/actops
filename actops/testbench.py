""" testbench.py
    2019-11-01:
        Implementing and testing SV Actops system
"""
from argparse import ArgumentParser
import os

import torch
from torchvision import transforms as tforms
from torch import nn
import numpy as np
import ipdb
#from memory_profiler import profile

from actops import utils
from actops.dataproc.data_cocoactions import COCOActionsSVODatasetActivationsTriplet
from actops.actops_svo import (
    ActopsSVOTripletEmbeddingModel,
    ActopsSVOTripletEmbeddingSystem,
    ActopsSVOPredictor,
    SVONegativeMiner
)

VISUAL_EMBEDDER_DEPTH = 3
MODEL_CODE_OPTIONS = ['actops', 'attrops', 'fc']
FIXED_FC_CLF_DEPTH = 5
IMAGE_DIM = 128


def parse_args():
    parser = ArgumentParser()
    parser.add_argument(
        '--backbone',
        default='resnet',
        help='If we use learnable features, what should be our feature extractor?'
    )
    parser.add_argument(
        '--learnable_features',
        action='store_true',
        help='should we use learnable features?'
    )
    parser.add_argument(
        '--monitor_weights',
        action='store_true',
        help='monitor a whole buncha weights?'
    )
    parser.add_argument(
        '--debug',
        action='store_true',
        help='do debugging?'
    )
    parser.add_argument(
        '--dpath',
        default='~/data/coco-actions',
        help='location of data'
    )
    parser.add_argument(
        '--df_fname',
        default='coco-actions-df_predicted_single-estimated_svo-split.pkl',
        help='name of pickled DataFrame file containing annotations'
    )
    parser.add_argument(
        '--csv_fname',
        default=None,
        help="name of csv of dataframe. If not given, it's inferred from df_fname"
    )
    parser.add_argument(
        '--vis_embedder_depth',
        default=VISUAL_EMBEDDER_DEPTH,
        type=int,
        help='Number of layers to use for a FC network (see: svo_classification_system.SVOClassificationModel)'
    )
    parser.add_argument(
        '--clf_depth',
        default=FIXED_FC_CLF_DEPTH,
        type=int,
        help='Number of layers to use for a FC network (see: svo_classification_system.SVOClassificationModel)'
    )
    parser.add_argument(
        '--use-miner',
        action='store_true',
        help='Should we use a miner?'
    )
    parser.add_argument(
        '--generate_kaiming_presets',
        action='store_true',
        help='should we generate a bunch of kaiming presets?'
    )
    parser.add_argument(
        '--n_presets',
        default=5,
        type=int,
        help='number of kaiming presets to generate'
    )
    parser.add_argument(
        '--n_candidates',
        default=3,
        type=int,
        help='Positive and negative samples to use when mining hard positives and negatives'
    )
    parser.add_argument(
        '--eval_val_every',
        default=10,
        type=int,
        help='How often should we evaluate validation data and try to save model?'
    )
    parser.add_argument(
        '--n_workers',
        default=8,
        type=int,
        help='Number of threads'
    )
    parser.add_argument(
        '--embed_dim',
        default=300,
        type=int,
        help='dimensionality of embedding space'
    )
    parser.add_argument(
        '--batch_size',
        default=512,
        type=int,
        help='loader batch size'
    )
    parser.add_argument(
        '--test',
        action='store_true',
        help='run tests instead of train'
    )
    parser.add_argument(
        '--test_batch',
        action='store_true',
        help='run test batches instead of train'
    )
    parser.add_argument(
        '--prep',
        action='store_true',
        help='prepare for training'
    )
    parser.add_argument(
        '--reset',
        action='store_true',
        help='Reset model, ignoring caches'
    )

    # Randomizers
    parser.add_argument(
        '--randomize_lr',
        action='store_true',
        help='randomize LR in same order of magnitude'
    )
    parser.add_argument(
        '--randomize_lambda_ling',
        action='store_true',
        help='randomize lambda_ling in same order of magnitude'
    )
    parser.add_argument(
        '--randomize_lambda_vis',
        action='store_true',
        help='randomize lambda_vis in same order of magnitude'
    )

    parser.add_argument(
        '--nice_name',
        default='actops',
        help='Human-readable name (or prefix) for model saves'
    )
    parser.add_argument(
        '--state_dict_fpath',
        default=None,
        help='model state_dict to load, for specific initialization schemes'
    )
    parser.add_argument(
        '--load',
        default='unspecified',
        help='model checkpoint to load, only used when restarting or testing'
    )
    parser.add_argument(
        '--model',
        default='actops',
        help='What kind of model?',
        choices=MODEL_CODE_OPTIONS,
    )
    parser.add_argument(
        '--cache_dir',
        default='~/.cache/cocoactions',
        help='location for all cached data'
    )
    parser.add_argument(
        '--verbosity',
        default=1,
    )
    parser.add_argument(
        '--triplet_margin',
        default=0.5,
        type=float,
        help='margin for use in triplet loss'
    )
    parser.add_argument(
        '--lambda_vis',
        default='1.0',
        help='weight for visual loss in actops'
    )
    parser.add_argument(
        '--lambda_ling',
        default='1.0',
        help='weight for linguistic loss in actops'
    )
    parser.add_argument(
        '--lambda_aux',
        default=1.0,
        type=float,
        help='weight for auxiliary loss in attrops'
    )
    parser.add_argument(
        '--lambda_comm',
        default=1.0,
        type=float,
        help='weight for commutative loss in attrops'
    )
    parser.add_argument(
        '--epochs',
        default=200,
        type=int,
        help='epochs to train'
    )
    parser.add_argument(
        '--device',
        default=0,
        type=int,
        help='CUDA device to use'
    )
    parser.add_argument(
        '--lr',
        default=8e-4,
        type=float,
        help='Learning rate'
    )
    parser.add_argument(
        '--momentum',
        default=0.9,
        type=float,
        help='SGD momentum'
    )
    parser.add_argument(
        '--train_ratio',
        default=0.7,
        type=float,
        help='ratio of train images to total'
    )
    parser.add_argument(
        '--sample_all_triplet_combos',
        action='store_true',
        help='Do we work in a space of all possible triplet combinations? Less bias, harder to learn.'
    )
    parser.add_argument(
        '--test_tracker_file',
        default='test_tracker.csv',
        help='where do we keep track of test performance metrics?'
    )
    parser.add_argument(
        '--embed',
        action='store_true',
        help='Should we embed the validation data?'
    )
    parser.add_argument(
        '--overwrite_tests',
        action='store_true',
        help='Should we overwrite our current tests?'
    )
    args = parser.parse_args()

    args.lambda_vis = [float(lambda_) for lambda_ in args.lambda_vis.split(',')]
    args.lambda_ling = [float(lambda_) for lambda_ in args.lambda_ling.split(',')]

    return args


def init_dsets(args):
    args.n_candidates = args.n_candidates if args.use_miner else 1
    dset_cache = utils.Cache('~/.cache.bak/cocoactions')
    if args.learnable_features:
        transforms = {
            'train': tforms.Compose([
                tforms.RandomHorizontalFlip(),
                tforms.RandomRotation(10),
                tforms.Resize(int(IMAGE_DIM * 1.2)),
                tforms.RandomResizedCrop(IMAGE_DIM),
                tforms.ColorJitter(
                    brightness=0.1,
                    contrast=0.1,
                    saturation=0.1,
                    hue=0.1
                ),
                tforms.ToTensor()
            ]),
            'val': tforms.Compose([
                tforms.Resize((IMAGE_DIM, IMAGE_DIM)),
                tforms.ToTensor()
            ]),
            'test': tforms.Compose([
                tforms.Resize((IMAGE_DIM, IMAGE_DIM)),
                tforms.ToTensor()
            ])
        }

        from actops.dataproc.data_cocoactions import COCOActionsSVODatasetTriplet
        dsets = {'train': COCOActionsSVODatasetTriplet(
            dpath=args.dpath,
            split='train',
            verbosity=int(args.verbosity),
            train_ratio=args.train_ratio,
            sample_only_valid_triplets=not args.sample_all_triplet_combos,
            n_candidates=args.n_candidates,
            cache=dset_cache,
            null_object_index=None,
            transforms=transforms['train'],
        )}
        t_v = COCOActionsSVODatasetTriplet.split_test_val(
            0.5,
            dpath=args.dpath,
            verbosity=int(args.verbosity),
            train_ratio=args.train_ratio,
            sample_only_valid_triplets=not args.sample_all_triplet_combos,
            n_candidates=args.n_candidates,
            null_object_index=None,
            cache=dset_cache,
            transforms=transforms['val'],
        )
        dsets['test'], dsets['val'] = t_v
    else:
        dsets = {
            'train': COCOActionsSVODatasetActivationsTriplet(
                dpath=args.dpath,
                split='train',
                verbosity=int(args.verbosity),
                train_ratio=args.train_ratio,
                sample_only_valid_triplets=not args.sample_all_triplet_combos,
                n_candidates=args.n_candidates,
                cache=dset_cache,
                null_object_index=None
            )
        }
        t_v = COCOActionsSVODatasetActivationsTriplet.split_test_val(
            0.5,
            dpath=args.dpath,
            verbosity=int(args.verbosity),
            train_ratio=args.train_ratio,
            sample_only_valid_triplets=not args.sample_all_triplet_combos,
            n_candidates=args.n_candidates,
            null_object_index=None,
            cache=dset_cache,
        )
        dsets['test'], dsets['val'] = t_v
    return dsets


#@profile
def train(args):
    if args.randomize_lr:
        args.lr = args.lr * (int(np.random.rand() * 1000) / 100)
    if args.randomize_lambda_vis:
        args.lambda_vis = [
            lambda_ * (int(np.random.rand() * 1000) / 100)
            for lambda_ in args.lambda_vis
        ]
    if args.randomize_lambda_ling:
        args.lambda_ling = [
            lambda_ * (int(np.random.rand() * 1000) / 100)
            for lambda_ in args.lambda_ling
        ]

    # Put everything on the specified device
    with torch.cuda.device(args.device):
        with ipdb.launch_ipdb_on_exception():

            dsets = init_dsets(args)

            if False:
                def saveimgs(index=0):
                    import torchvision.transforms as tforms
                    img = dsets['train'][index][0]
                    tforms.ToPILImage()(dsets['train'].transforms(img)).save('transformed.jpg')
                    img.save('untransformed.jpg')
                saveimgs()
            if args.debug:
                ipdb.set_trace()
                dsets['train'][0]

            loaders = {
                split: torch.utils.data.DataLoader(
                    dsets[split],
                    batch_size=args.batch_size,
                    shuffle=split == 'train',
                    num_workers=args.n_workers
                )
                for split in dsets.keys()
            }

            nice_name = ActopsSVOTripletEmbeddingSystem.create_nice_name(args)

            # Metrics returned from the system's forward pass
            system_metrics = [
                'loss',
                'loss_triplet',
                'loss_vis_sub',
                'loss_vis_verb',
                'loss_ling_sv_sub',
                'loss_ling_sv_verb',
                'loss_ling_ov_obj',
                'loss_ling_ov_verb',
                'svo_acc_open',
                'svo_acc_closed',
                'sv_acc_open',
                'sv_acc_closed',
                's_acc_open',
                's_acc_closed',
                'v_acc_open',
                'v_acc_closed',
                'o_acc_open',
                'o_acc_closed',
            ]

            # Trackable metrics (what is reported in tensorboard)
            trackable_metrics = [m + '_train' for m in system_metrics]
            trackable_metrics += [m + '_val' for m in system_metrics]

            if args.learnable_features:
                visual_input_dim = IMAGE_DIM
            else:
                visual_input_dim = dsets['train'].features.shape[1]

            model = ActopsSVOTripletEmbeddingModel(
                visual_input_dim=visual_input_dim,
                out_dim=args.embed_dim,
                lambda_ling=args.lambda_ling,
                lambda_vis=args.lambda_vis,
                subjects=dsets['train'].subjects,
                verbs=dsets['train'].verbs,
                objects=dsets['train'].objects,
                visual_embedder_depth=args.vis_embedder_depth,
                glove_init=True,
                null_object_index=dsets['train'].object2idx['None'],
                use_precomputed_features=not args.learnable_features,
                backbone=args.backbone,
            ).cuda()

            if args.state_dict_fpath is not None:
                fpath = os.path.realpath(os.path.expandvars(os.path.expanduser(
                    args.state_dict_fpath
                )))
                state_dict = torch.load(fpath)
                model.load_state_dict(state_dict)
                model.cuda()

            objective = nn.TripletMarginLoss(margin=args.triplet_margin)
            optimizer = torch.optim.SGD(
                model.parameters(),
                lr=args.lr,
                momentum=args.momentum
            )
            predictor_open = ActopsSVOPredictor(
                dset=dsets['train'],
                model=model,
                svo=sorted(dsets['train'].svo_train + list(dsets['val'].svo_val))
            )
            if args.debug:
                ipdb.set_trace()

            predictor_closed = ActopsSVOPredictor(
                dset=dsets['val'],
                model=model,
                svo=dsets['val'].svo_val
            )

            if args.use_miner:
                mining_objective = nn.TripletMarginLoss(
                    margin=args.triplet_margin,
                    reduction='none'
                )
                miner = SVONegativeMiner(
                    model=model,
                    objective=mining_objective,
                    device=torch.cuda.current_device(),
                    n_candidates=args.n_candidates
                )
            else:
                mining_objective = miner = None

            hash_on = {
                'None': None,
            }

            if args.monitor_weights:
                for i in (0, 2, 4):
                    for j in range(2, min(model.visual_embedder.layers[i].weight.shape), 50):
                        trackable_metrics.append('visual_embedder[{0}][{1}, {1}]_val'.format(i, j))
                for i in range(len(model.subjective_verb_ops)):
                    for j in range(0, min(model.subjective_verb_ops[i].shape), 100):
                        trackable_metrics.append('subjective_verb[{0}][{1}, {1}]_val'.format(i, j))
                        trackable_metrics.append('objective_verb[{0}][{1}, {1}]_val'.format(i, j))

            system = ActopsSVOTripletEmbeddingSystem(
                eval_val_every=args.eval_val_every,
                save_every=-1,
                selection_metric='svo_acc_open_val',
                selection_metric_goal='max',
                device=torch.cuda.current_device(),
                verbosity=args.verbosity,
                hash_on=hash_on,
                nice_name=nice_name,
                metrics=trackable_metrics,
                model=model,
                objective=objective,
                miner=miner,
                loaders=loaders,
                optimizer=optimizer,
                margin=args.triplet_margin,
                lambda_vis=args.lambda_vis,
                lambda_ling=args.lambda_ling,
                predictor_open=predictor_open,
                predictor_closed=predictor_closed,
                monitor_weights=args.monitor_weights,
            )

            system.train(args.epochs)


if __name__ == "__main__":
    args = parse_args()
    if args.generate_kaiming_presets:
        from actops_svo import generate_kaiming_presets
        dsets = init_dsets(args)
        generate_kaiming_presets(dsets, args, args.n_presets)

    else:
        train(args)
