#!/bin/bash
#SBATCH --time=240
#SBATCH --ntasks=32
#SBATCH --partition=dcs
#SBATCH --mail-type=ALL
#SBATCH --mail-user=DLANbrdj
#SBATCH --gres=gpu:1
#SBATCH --mail-user=beardj2@rpi.edu
srun \
    --job-name=run_1 \
    python testbench.py \
        --learnable_features \
        --lambda_ling=0.01,0.01,0.01,0.01 \
        --lambda_vis=0.01,0.01,0.01 \
        --randomize_lambda_ling \
        --randomize_lambda_vis \
        --lr=1e-3 \
        --randomize_lr \
        --n_workers=32 &

srun \
    --job-name=run_2 \
    python testbench.py \
        --learnable_features \
        --lambda_ling=0.01,0.01,0.01,0.01 \
        --lambda_vis=0.01,0.01,0.01 \
        --randomize_lambda_ling \
        --randomize_lambda_vis \
        --lr=1e-3 \
        --randomize_lr \
        --n_workers=32 &

srun \
    --job-name=run_3 \
    python testbench.py \
        --learnable_features \
        --lambda_ling=0.01,0.01,0.01,0.01 \
        --lambda_vis=0.01,0.01,0.01 \
        --randomize_lambda_ling \
        --randomize_lambda_vis \
        --lr=1e-3 \
        --randomize_lr \
        --n_workers=32 &

srun \
    --job-name=run_4 \
    python testbench.py \
        --learnable_features \
        --lambda_ling=0.01,0.01,0.01,0.01 \
        --lambda_vis=0.01,0.01,0.01 \
        --randomize_lambda_ling \
        --randomize_lambda_vis \
        --lr=1e-3 \
        --randomize_lr \
        --n_workers=32 &
wait

