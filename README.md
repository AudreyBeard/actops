# Visio-Semantic Action Representation
Primarily authored by Audrey Beard in the summer of 2019
This project was undertaken while interning at [TRASH](https://trash.app), and
was inspired by work done by Nagarajan and Grauman in [Attributes as
Operators](https://arxiv.org/pdf/1803.09851.pdf). The code to their work can be
found at [the GitHub
repo](https://github.com/Tushar-N/attributes-as-operators). To evaluate
performance on our dataset, I forked that repo, which can be found
[here](https://github.com/AudreyBeard/attributes-as-operators).

## Quick Start
### Prerequisites:
- Python 3.6 or higher
- Pip3
- git-lfs (for models and datasets)
- Preferably a virtual environment

### Usage
1. `setup.sh` to set up everything you need
2. `pip install actops` to install all other dependencies
3. `python actops/dataproc/preproc/make_coco_actions.py NPROCS BATCH_SIZE` to create the dataset
4. `python main.py` as the entry point

## Basic Concepts
Actions aren't objects, so don't treat them like they are. In embedding
problems, objects may be treated as vectors and compared to other object
vectors or a prototypical sample. Intuitively, one can imagine a slice of pizza
without necessarily qualifying it. In contrast, actions can not be viewed this
way - consider "running," which may be a jogger, a galloping horse, an idling
car, or a rushing river. For elaboration on this idea, see Tushar Nagarajan's
and Kristen Grauman's paper "Attributes as Operators".

Actions and attributes are not directly analogous, however. Actions may be
_intransitive_ or _transitive_. Intransitive actions modify one entity (or one
group of entities), like "person running" or "dogs sleeping", etc. Transitive
actions, on the other hand, involve both a subject (the actor) and the object
(the acted-upon). This distinction is important, both semantically and visually
("person eating pizza" is very different from "pizza eating person"). 

With all that in mind, we choose to model actions as #TODO finish this 

## Design Decisions
The structure of this project sometimes makes assumptions about the location of
data. With few exceptions, I designed components such that this may be
specified at runtime, but be wary. In general, a core assumption I make is that
data is located at `~/data` and code is located in subdirectories of `~/code`.
My convention for data caching is to put it in `~/.cache/______`, dependent on
the module. In some cases, like the `netdev.NetworkSystem`, the default is to
place caches, checkpoints, and `tensorboard_logger` logs in `./models` - though
this may be overridden.


## Workflow & Reproduction
This problem hasn't really enjoyed attention from the vision community, so
there aren't any decent datasets available. This fact spurred me to create two
datasets from pre-existing ones:

### Moments in Time
Moments in Time (MiT) is a dataset of approximately 1M 3-second video clips,
each labeled with one action. It was introduced in Monfort et al.'s 2019 paper
titled _Moments in time dataset: one million videos for event understanding_.
Among other things, the dataset boasts high intra-class variance, some of which
is likely not intuitive to users of the dataset. For instance, the action class
"baptizing" contains a large number of Christians dunking in water, and a small
number of "headshots" in video game screen recordings - "baptizing" apparently
being slang for such a move.

Since each video is labeled with only an action, my task was to synthesize the
subject and object from the frames. To do this, I passed a handful of frames
through an object recognition network and leveraged its predictions to estimate
the subject and object. The subject of the action was estimated to be the most
confident prediction, provided it was not too small or too large. This code can
be found in `dataproc/preproc/annotate_moments.py`. 
